# Introduction

This file gives information to help contribute to this project.

# How to contribute
You can contribute in a number of ways:

- Report an issue. Reporting it first will ensure that it is in fact an issue, and there is not already a fix in the works.
- After reporting a bug, go one step further and fix it.
- Ensure that your effort is aligned with the project’s roadmap by talking to the maintainers, especially if you are going to spend a lot of time on it.
- Adhere to code style outlined in the style guide.
- Submit a merge request through Gitlab.
- Run tests; see Running tests.

# Where to start
If you are looking to contribute, but aren’t sure where to start, read the "Adding a new processor" chapter. It explains how to add a new processor.
Processors are the brains and workers of the app.

Also, please read the source code before contributing. If something is confusing, poorly documented, poorly commented, or could otherwise be improved: open a GitLab issue and link to the file and line of code with a useful description of what you would like to see or think is missing.

# Adding a new processor
A processor is meant to process all tables from database. For every table it will do things.
There are simple processors like `TypescriptModelProcessor` or more complex like `PhpDoctrineEntityProcessor`

To add a new processor :

1. First, in processors go to the template linked with your processor. If your processor is gonna produce typescript files, you should go to the typescript folder.
2. If you don't have a template for your processor, you will need to create the folder and add in it:
   a. an Abstract class that extends `AbstractProcessor`
   b. a converter class that extends `AbstractConverter`
3. If you already have a template, you will need to add your processor class in it.
4. Your processor class must extend the abstract class for the template. E.g. if you are using the typescript template, your processor will extend `AbstractTypeScriptProcessor`.
5. Your processor name class must end with the term `Processor`, e.g. `PhpDoctrineEntityProcessor`.
6. Do what you got to do in the processor (see others to respect our way to do...), just remember:
   a. `process` method will be called, this is the entry point. Usually, you call `defaultProcess` in it that will browse table names and call your `processTable` method for each table.
   b. **getProcessorFolder** needs to return the folder name where your files will go (only the name, no path).
   c. **getProcessorName** needs to return your processor name, it must be unique for a template.
7. Add your template file in the `templates` folder. You may store your template in a subfolder related to your template language. For example, all templates related to typescript are in the `templates/typescript` folder.
8. If your template is related to a framework, you should create a subfolder (e.g. 'angular') and put your templates in it. Then in your processor you will set sub-folder name with templateFolder variable. Example : `protected templateFolder?: string = "angular";`
9. Go in the `ProcessorFactory` class and:
   a. If you are creating a new template you will need to add a `ProcessorFactory.get<template>Processor` method then change the `getProcessor` method.
   b. Add your processor to the `ProcessorFactory.get<template>Processor` method, case value may match `getProcessorName` value returned.
10. Add unit tests if necessary:
   a. You can run them with `npm run test`. AI is good for writing unit tests as long as you give her all she needs.
11. Update the `README.md` file to add your processor in it:
    a. Don't forget to add your name to the contributors list at the end of the file.
12. Update the `CHANGELOG.md` file to add your processor in it in the next release. Add the next release if there is none.

You can test your processor with debug panel by running `Run extension`.


# Note about processor versions
If you want to implement a processor that is specific to some kind of framework version or whatever, these are the rules :

User will give you the version by using the version property in the ConfigProcessor object.

Example below :
 
```JSON
    "processors": [
        { "name": "doctrine_repository", "version": 7, "parameter_prefix": "_" },
        { "name": "doctrine_entity", "version": 7, "parameter_prefix": "_" },
        { "name": "symfony_service", "parameter_prefix": "_" },
        { "name": "symfony_api_controller" }        
    ]
```

So, ProcessorFactory class must check this version to load the right processor related to this version.

Oldest processor class must be named as their version, and current processor class should not use a version in its name. When you name your processor with version, follow this pattern : <processor>_v<version>_Processor

For example, imagine a doctrine_entity processor for version 7 (related to php version) and actual default version targeting php 8, you would have : 

- `PhpDoctrineEntity_v7_Processor` : name of versioned processor
- `PhpDoctrineEntityProcessor` : name of default processor


# Thanks for your contribution !

