# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.28] - 2025-02-19

- Clarified README to better explain when and how to use Codigo Generator.

## [1.0.27] - 2025-02-15

- Improve Angular Form component
- Added youtube demos link on README
- New property on AI config : `duration_warning`

## [1.0.26] - 2025-02-14

- Fix Angular page list component
- Fix created_at and updated_at support on Angular/Ionic/Loopback processors
- Improve Loopback controller processor with AI

## [1.0.25] - 2025-02-12

- Fix bug on Angular and Ionic Angular model form template
- Improve AI completion for Angular and Ionic Angular services

## [1.0.24] - 2025-02-11

- Added Mistral AI support (@see README file)

## [1.0.23] - 2025-01-29

- Added DeepSeek support (@see README file)

## [1.0.22] - 2025-01-24

- New Service processor for loopback
- Now loopback controllers use services instead of repository (best practice) 

## [1.0.21] - 2025-01-23

- Renamed `dataSourceName` extra_config property to `datasource_name` 
- Fix bug on loopback filenames
- Fix progress window when focus out
- Fix bug on loopback model timestamp type
- Fix bug with id on loopback controller
- Handle belongsTo for loopback model

## [1.0.20] - 2025-01-22

- Local translation for extension command title (if we have it)
- New processors for Loopback Framework (repository and controller)

## [1.0.19] - 2025-01-18

- Throw error if password not found in env variable
- Better support for AI with ionic and angular components 

## [1.0.18] - 2025-01-06

- Fix static name in Angular Form component template when sould be dynamic
- Renamed http to httpClient in abstract Angular service and abstract Ionic Angular service
- Fix minor bugs
- Fix with use of `await` on many processors (waiting AI work for progress bar)


## [1.0.17] - 2025-01-03

- Removed activation message at startup
- Changed prompt order for AI for better performance
- Better support for AI completion on Angular typescript files
- Fix import of scss file in Angular components
- Fix asynchronous call support on Angular components

## [1.0.16] - 2025-01-03

- Removed the template name as the base folder for relative paths.  
  - For example: If you use a `typescript` template and specify a relative path like `"out_folder": ".\\out"`, the destination folder will no longer include a `typescript` folder as the base folder for generated files (this was the previous behavior).
- Improvement of the configuration check

## [1.0.15] - 2025-01-01

- Added new **ionic angular** processors
- Now we use translation for progress window

## [1.0.14] - 2024-12-02

- Now codigo-generator can use AI to complete some processor files. This is soooo cooool !
- Added new properties `useCamelCaseForClass` and `useCamelCaseForField`
- Now class names and properties use camelCase by default  
- Renaming the following processors to singular form for consistency : **angular_service**, **row_mapper**
- Added new **typescript** processors for creating Angular components for models
- Modification of processor progress tracking

## [1.0.13] - 2024-10-11

- Bug fix on loopback processor
- Fix model name for loopback
- Renamed serviceName property for typescript service processor

## [1.0.12] - 2024-09-04

- New logo

## [1.0.11] - 2024-08-05

- New processor for spring security with JWT

## [1.0.10] - 2024-07-25

- Added `mariadb` database type (same as mysql)
- Changed jpa entity to use insertable/updatable true for ManyToOne relationship

## [1.0.9] - 2024-07-17

- Fix copy static files on typescript processor
- Using EAGER on jpa spring entity to avoid `No serializer found` error
- Using jsonignore on jpa spring entity on onetomany instead of manytoone

## [1.0.8] - 2024-07-16

- Fix one bug with mysql schema missing
- Fix one bug with sql reserved words on tablename

## [1.0.7] - 2024-07-10

- Fix bug on java Spring JPA handling uppercase letters in column names (not a good practice by the way)
- Naming of postman collection file with extra_files

## [1.0.6] - 2024-07-01

- Clarify use of `custom_base_template` on README
- Show processor name when done
- New template, welcome to csharp !
- Added csharp processors

## [1.0.5] - 2024-06-17

- Better error message when there is no database connexion
- New processor for generating Postman collection for all templates
- Added findBy methods for spring processors
- Fix bug on findBy methods for python controller processors

## [1.0.4] - 2024-06-14

- Fix bug on postgresql type translation
- Added sql TAG type support 
- Added sql ENUM type support
- New processor for python django controller

## [1.0.3] - 2024-06-07

- New template, welcome to python !
- New processor for python django model
- Fix some bugs with data type translation

## [1.0.2] - 2024-05-30

- New processors for symfony api controllers and services

## [1.0.1] - 2024-05-28

- Added categories and keywords to package.json
- New processors for spring controllers and services

## [1.0.0] - 2024-05-22

- Initial release