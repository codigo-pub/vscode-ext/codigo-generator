import {BindingScope, injectable} from '@loopback/core';
import {
  Count,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import { {{classModelName} }} from '../{{modelFolder}}';
import { {{classModelName}}Repository } from '../{{repositoryFolder}}';

@injectable({scope: BindingScope.TRANSIENT})
export class {{className}} {

  @repository({{classModelName}}Repository)
  public {{classModelNameRepositoryLower}}: {{classModelName}}Repository;

  async create({{classModelNameLower}}: Omit<{{classModelName}}, '{{pkeyFieldName}}'>): Promise<{{classModelName}}> {
    return this.{{classModelNameRepositoryLower}}.create({{classModelNameLower}});
  }

  async count(where?: Where<{{classModelName}}>): Promise<Count> {
    return this.{{classModelNameRepositoryLower}}.count(where);
  }

  async find(filter?: Filter<{{classModelName}}>): Promise<{{classModelName}}[]> {
    return this.{{classModelNameRepositoryLower}}.find(filter);
  }

  async updateAll({{classModelNameLower}}: {{classModelName}}, where?: Where<{{classModelName}}>): Promise<Count> {
    return this.{{classModelNameRepositoryLower}}.updateAll({{classModelNameLower}}, where);
  }

  async findById({{pkeyFieldName}}: {{pkeyType}}, filter?: FilterExcludingWhere<{{classModelName}}>): Promise<{{classModelName}}> {
    return this.{{classModelNameRepositoryLower}}.findById({{pkeyFieldName}}, filter);
  }

  async updateById({{pkeyFieldName}}: {{pkeyType}}, {{classModelNameLower}}: {{classModelName}}): Promise<void> {
    await this.{{classModelNameRepositoryLower}}.updateById({{pkeyFieldName}}, {{classModelNameLower}});
  }

  async replaceById({{pkeyFieldName}}: {{pkeyType}}, {{classModelNameLower}}: {{classModelName}}): Promise<void> {
    await this.{{classModelNameRepositoryLower}}.replaceById({{pkeyFieldName}}, {{classModelNameLower}});
  }

  async deleteById({{pkeyFieldName}}: {{pkeyType}}): Promise<void> {
    await this.{{classModelNameRepositoryLower}}.deleteById({{pkeyFieldName}});
  }

  {{iaCompletion}}
}
