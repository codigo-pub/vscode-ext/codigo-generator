<?php

namespace App\Controller{{folderApi}};

use App\Service\{{classServiceName}};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use InvalidArgumentException;
{{extraFields_import}}

#[Route('{{baseApi}}/{{ modelBaseApiUrl }}')]
class {{className}} extends AbstractController
{
    private {{classServiceName}} ${{ classModelNameLower }}Service;

    public function __construct({{classServiceName}} ${{ classModelNameLower }}Service)
    {
        $this->{{ classModelNameLower }}Service = ${{ classModelNameLower }}Service;
    }

    #[Route('', name: '{{ classModelNameLower }}_index', methods: ['GET'])]
    public function index(): JsonResponse
    {
        ${{ classModelNameLower }}s = $this->{{ classModelNameLower }}Service->getAll();
        return $this->json(${{ classModelNameLower }}s);
    }

    #[Route('{{ pkey_url_parameter }}', name: '{{ classModelNameLower }}_show', methods: ['GET'])]
    public function show({{ pkey_parameter }}): JsonResponse
    {
        ${{ classModelNameLower }} = $this->{{ classModelNameLower }}Service->getById({{ pkey_field }});
        if (!${{ classModelNameLower }}) {
            return $this->json(['message' => '{{ classModelName }} not found'], 404);
        }
        return $this->json(${{ classModelNameLower }});
    }

    #[Route('', name: '{{ classModelNameLower }}_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        ${{ classModelNameLower }} = $this->{{ classModelNameLower }}Service->create($data);
        return $this->json(${{ classModelNameLower }}, 201);
    }

    #[Route('{{ pkey_url_parameter }}', name: '{{ classModelNameLower }}_update', methods: ['PUT'])]
    public function update(Request $request, {{ pkey_parameter }}): JsonResponse
    {
        ${{ classModelNameLower }} = json_decode($request->getContent(), true);
        try {
            $updated{{ classModelName }} = $this->{{ classModelNameLower }}Service->updateById({{ pkey_field }}, ${{ classModelNameLower }});
        } catch (InvalidArgumentException $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }
        
        return $this->json($updated{{ classModelName }});
    }

    #[Route('{{ pkey_url_parameter }}', name: '{{ classModelNameLower }}_delete', methods: ['DELETE'])]
    public function delete({{ pkey_parameter }}): JsonResponse
    {
        $deleted = $this->{{ classModelNameLower }}Service->deleteById({{ pkey_field }});
        return $deleted ? $this->json(null, 204) : $this->json(['message' => '{{ classModelName }} not found'], 404);
    }
}
