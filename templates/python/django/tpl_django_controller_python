import json
from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist

from {{packageBaseName}}.{{modelFolder}}.{{classModelName}} import {{classModelName}}Form
from {{packageBaseName}}.{{modelFolder}}.{{classModelName}} import {{classModelName}}
{{extraFields_import}}

@method_decorator(csrf_exempt, name='dispatch')
class {{className}}(View):
	
	def dispatch(self, request, *args, **kwargs):		
		action = kwargs.get('action')
		if action == 'list' and request.method == 'GET':
			return self.list(request)
		elif action == 'retrieve' and request.method == 'GET':
			return self.retrieve(request, kwargs.get('{{pkeyName}}'))
		elif action == 'create' and request.method == 'POST':
			return self.create(request)
		elif action == 'update' and request.method == 'PUT':
			return self.update(request, kwargs.get('{{pkeyName}}'))
		elif action == 'delete' and request.method == 'DELETE':
			return self.delete(request, kwargs.get('{{pkeyName}}'))
{{extraFields_dispatch}}
		return JsonResponse({'error': 'Invalid action or method'}, status=400)	
	
	
	def list(self, request):
		# Retrieve all {{classModelNameLower}} from the database
		{{classModelNameLower}} = {{classModelName}}.objects.all()
		
		# Serialize the {{classModelNameLower}} data to JSON
		data = serializers.serialize('json', {{classModelNameLower}})
		
		# Return JSON response with HttpResponse to avoid json string representation 
		return HttpResponse(data, content_type='application/json')
	
	def retrieve(self, request, id):
		try:     
			# Retrieve a single {{classModelName}} by its ID
			{{classModelNameLower}} = {{classModelName}}.objects.get({{pkeyName}}=id)
			
			# Serialize the {{classModelName}} data to JSON
			data = serializers.serialize('json', [{{classModelNameLower}}])
			
			# Return JSON response
			return HttpResponse(data, content_type='application/json')
		except ObjectDoesNotExist:
			return JsonResponse({'success': False, 'error': 'Data not found'}, status=404)

	def create(self, request):
		# Get values as a form model
		dataJson = json.loads(request.body)
		form = {{classModelName}}Form(dataJson)
		# If values are ok, proceed
		if form.is_valid():
			# Save data
			{{classModelNameLower}} = form.save()
			data = serializers.serialize('json', [{{classModelNameLower}}])
			return JsonResponse(data, safe=False)
		return JsonResponse({'success': False, 'errors': form.errors}, status=400)

	def update(self, request, id):
		try:         
			# Retrieve a single {{classModelName}} by its ID
			{{classModelNameLower}} = {{classModelName}}.objects.get({{pkeyName}}=id)
			# Get values as a form model
			dataJson = json.loads(request.body)
			form = {{classModelName}}Form(dataJson, instance={{classModelNameLower}})
			# If values are ok, proceed
			if form.is_valid():
				# Save data
				{{classModelNameLower}} = form.save()
				data = serializers.serialize('json', [{{classModelNameLower}}])
				return JsonResponse(data, safe=False)
			return JsonResponse({'success': False, 'errors': form.errors}, status=400)
		except ObjectDoesNotExist:
			return JsonResponse({'success': False, 'error': 'Data not found'}, status=404)

	def delete(self, request, id):
		try:         
			# Retrieve a single {{classModelName}} by its ID
			{{classModelNameLower}} = {{classModelName}}.objects.get({{pkeyName}}=id)
			# Delete data
			{{classModelNameLower}}.delete()
			return JsonResponse({'success': True})
		except ObjectDoesNotExist:
			return JsonResponse({'success': False, 'error': 'Data not found'}, status=404)

{{extraFields_fields}}
