package {{packageBaseName}}.{{securityFolder}};

import {{packageBaseName}}.{{repositoryFolder}}.IRoleRepository;
import {{packageBaseName}}.{{repositoryFolder}}.IUserRepository;
import {{packageBaseName}}.{{modelFolder}}.Role;
import {{packageBaseName}}.{{modelFolder}}.User;
import {{packageBaseName}}.{{modelFolder}}.dto.RoleName;
import {{packageBaseName}}.{{modelFolder}}.dto.BearerToken;
import {{packageBaseName}}.{{modelFolder}}.dto.LoginDto;
import {{packageBaseName}}.{{modelFolder}}.dto.SignUpDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserSecurityService implements IUserSecurityService {

    private final AuthenticationManager authenticationManager;
    private final IUserRepository userRepository;
    private final IRoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtilities jwtUtilities;

    private static final String TOKEN_TYPE = "Bearer";

    @Override
    public ResponseEntity<?> register(SignUpDto rSignUpDto) {

        // Check if the {{userField}} already exists in the database
        if (userRepository.existsBy{{userFieldUpper}}(rSignUpDto.get{{userFieldUpper}}())) {
            return new ResponseEntity<>("{{userField}} already taken!", HttpStatus.BAD_REQUEST);
        }

        // Create the user in the database
        User user = new User();
        user.set{{userFieldUpper}}(rSignUpDto.get{{userFieldUpper}}());
        user.set{{passFieldUpper}}(passwordEncoder.encode(rSignUpDto.get{{passFieldUpper}}()));

        // Fetch the USER role from the database and apply it as a role in a list of roles
        Role role = roleRepository.findBy{{roleFieldUpper}}(RoleName.USER.name()).get();
        user.setRole(role);

        // Save the user
        userRepository.save(user);

        // Create the token
        String token = jwtUtilities.generateToken(rSignUpDto.get{{userFieldUpper}}(), Collections.singletonList(user.getRole().get{{roleFieldUpper}}()));
        // Return the token in the response
        return new ResponseEntity<>(new BearerToken(token, TOKEN_TYPE), HttpStatus.OK);
    }

    @Override
    public BearerToken authenticate(LoginDto rLoginDto) {
        // Authenticate the user
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(rLoginDto.get{{userFieldUpper}}(), rLoginDto.get{{passFieldUpper}}());
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // Retrieve user to generate their token
        User user = userRepository.findBy{{userFieldUpper}}(authentication.getName()).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        // Send back bearer token with claims data (user and role)
        return new BearerToken(jwtUtilities.generateToken(user.get{{userFieldUpper}}(), Collections.singletonList(user.getRole().get{{roleFieldUpper}}())), TOKEN_TYPE);
    }
}
