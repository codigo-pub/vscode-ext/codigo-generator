package {{packageBaseName}}.{{securityFolder}};

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(
        jsr250Enabled = true // To use the @RoleAllowed annotation
)
public class SecurityConfig {

    private final JwtAuthenticationFilter jwtAuthenticationFilter;

    /**
     * Creation and automatic injection of dependencies via Spring.
     * Alternatively, we could have used the @RequiredArgsConstructor annotation.
     * @param jwtAuthenticationFilter
     */
    public SecurityConfig(JwtAuthenticationFilter jwtAuthenticationFilter) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.cors(cors -> cors.configurationSource(request -> getCorsConfiguration())); // Enable CORS
        http.csrf(csrf -> csrf.disable()); // Disable CSRF
        http.sessionManagement(sessionManagement -> sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS)); // Stateless session management
        // Configure basic authorization for HTTP requests
        http.authorizeHttpRequests(
                (authorize) ->
                        authorize.requestMatchers(new MvcRequestMatcher(null, "{{baseApi}}/auth/**")).permitAll() // Allow access to all users
                                .requestMatchers(new MvcRequestMatcher(null, "{{baseApi}}/user/**")).hasAuthority("USER") // Allow access to users with USER authority
                                .anyRequest().authenticated() // Require authentication for any other request
        );
        // Add a filter for token management
        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        // Return the configuration
        return http.build();
    }

    /**
     * Defines a CORS configuration
     * @return CorsConfiguration
     */
    private CorsConfiguration getCorsConfiguration() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // Allowed headers
        corsConfiguration.setAllowedHeaders(List.of("Authorization", "Cache-Control", "Content-Type"));
        // Allowed origins (which hosts are allowed to make requests)
        corsConfiguration.setAllowedOrigins(List.of("http://localhost:4200"));
        // Allowed HTTP methods
        corsConfiguration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"));
        // If the application is secured with Authorization header
        corsConfiguration.setAllowCredentials(true);
        // Validity duration
        corsConfiguration.setMaxAge(4800L);
        // Headers exposed in the response
        corsConfiguration.setExposedHeaders(List.of("Authorization"));
        // Return the configuration
        return corsConfiguration;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

}
