using {{ packageBaseName }}.{{modelFolder}};
using {{ packageBaseName }}.{{repositoryFolder}};
{{extraFields_import}}

namespace {{ packageBaseName }}.Services;

public class {{ className }}
{
    private readonly {{classRepositoryName}} _{{classModelNameLower}}Repository;

    public {{ className }}({{classRepositoryName}} {{classModelNameLower}}Repository)
    {
        _{{classModelNameLower}}Repository = {{classModelNameLower}}Repository;
    }

    public async Task<{{classModelName}}?> GetById(params object[] keyValues)
    {
        return await _{{classModelNameLower}}Repository.GetByIdAsync(keyValues);
    }

    public async Task<IEnumerable<{{classModelName}}>> GetAll()
    {
        return await _{{classModelNameLower}}Repository.GetAllAsync();
    }

    public async Task Add({{classModelName}} entity)
    {
        await _{{classModelNameLower}}Repository.AddAsync(entity);
    }

    public async Task Update({{classModelName}} entity)
    {
        await _{{classModelNameLower}}Repository.UpdateAsync(entity);
    }

    public async Task Delete({{classModelName}} entity)
    {
        await _{{classModelNameLower}}Repository.DeleteAsync(entity);
    }

{{extraMethods}}
}
