using {{packageBaseName}}.{{modelFolder}};
using {{packageBaseName}}.{{serviceFolder}};
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
{{pkeyImport}}{{extraFields_import}}

namespace {{packageBaseName}}.{{controllerFolder}};

[ApiController]
[Route("{{baseApi}}/{{modelBaseApiUrl}}")]
public class {{className}} : ControllerBase
{
    private readonly {{classServiceName}} _{{classModelNameLower}}Service;

    public {{className}}({{classServiceName}} {{classModelNameLower}}Service)
    {
        _{{classModelNameLower}}Service = {{classModelNameLower}}Service;
    }

    /// <summary>
    /// Retrieves all instances of {{ classModelName }}.
    /// </summary>
    /// <returns>A collection of {{ classModelName }} instances.</returns>
    [HttpGet]
    public IEnumerable<{{ classModelName }}> GetAll()
    {
        return _{{classModelNameLower}}Service.GetAll();
    }

    /// <summary>
    /// Retrieves a specific {{ classModelName }} by its ID
    /// </summary>
{{pkey_csharpdoc}}
    /// <returns>The {{ classModelName }} instance if found, NotFound if not found</returns>
    [HttpGet("{{pkey_url_parameter}}")]
    public ActionResult<{{ classModelName }}> GetById({{pkey_parameter}})
    {
        var {{classModelNameLower}} = _{{classModelNameLower}}Service.GetById({{pkey_field}});
        if ({{classModelNameLower}} == null)
        {
            return NotFound();
        }
        return {{classModelNameLower}};
    }

    /// <summary>
    /// Adds a new {{ classModelName }}
    /// </summary>
    /// <param name="{{classModelNameLower}}">The {{ classModelName }} object to add</param>
    /// <returns>CreatedAtAction result with the added {{ classModelName }} and its route</returns>
    [HttpPost]
    public IActionResult Add({{ classModelName }} {{classModelNameLower}})
    {
        _{{classModelNameLower}}Service.Add({{classModelNameLower}});
        return CreatedAtAction(nameof(GetById), new { {{pkey_id_set_expected}} }, {{classModelNameLower}});
    }

    /// <summary>
    /// Updates an existing {{ classModelName }}
    /// </summary>
{{pkey_csharpdoc}}
    /// <param name="{{classModelNameLower}}">The updated {{ classModelName }} object</param>
    /// <returns>NoContent result if successful, BadRequest if IDs do not match</returns>
    [HttpPut("{{pkey_url_parameter}}")]
    public IActionResult Update({{pkey_parameter}}, {{ classModelName }} {{classModelNameLower}})
    {
       var expectedKey = new { {{pkey_id_set_expected}} };
       var actualKey = new { {{pkey_id_set_actual}} };

       if (!expectedKey.Equals(actualKey))
       {
            return BadRequest();
        }

        try
        {
            _{{classModelNameLower}}Service.Update({{classModelNameLower}});
            return Ok(new { success = true });
        }
        catch (DbUpdateConcurrencyException)
        {
            // Concurrency conflict occurred
            return Conflict(new { success = true, message = "The {{classModelNameLower}} was modified or deleted by another user. Please reload the entity and try again." });
        }
    }

    /// <summary>
    /// Deletes a {{ classModelName }} by its ID
    /// </summary>
{{pkey_csharpdoc}}
    /// <returns>NoContent result if successful, NotFound if {{ classModelName }} not found</returns>
    [HttpDelete("{{pkey_url_parameter}}")]
    public IActionResult Delete({{pkey_parameter}})
    {
        var {{classModelNameLower}} = _{{classModelNameLower}}Service.GetById({{pkey_field}});
        if ({{classModelNameLower}} == null)
        {
            return NotFound();
        }

        _{{classModelNameLower}}Service.Delete({{classModelNameLower}});
        return Ok(new { success = true });
    }
{{extraMethods}}
}
