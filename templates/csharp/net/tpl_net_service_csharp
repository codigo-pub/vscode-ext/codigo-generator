using {{ packageBaseName }}.{{modelFolder}};
using {{ packageBaseName }}.{{repositoryFolder}};

namespace {{ packageBaseName }}.Services;

public class {{ className }}
{
    private readonly {{classRepositoryName}} _{{classModelNameLower}}Repository;

    public {{ className }}({{classRepositoryName}} {{classModelNameLower}}Repository)
    {
        _{{classModelNameLower}}Repository = {{classModelNameLower}}Repository;
    }

    public {{classModelName}}? GetById(params object[] keyValues)
    {
        return _{{classModelNameLower}}Repository.GetById(keyValues);
    }

    public IEnumerable<{{classModelName}}> GetAll()
    {
        return _{{classModelNameLower}}Repository.GetAll();
    }

    public void Add({{classModelName}} entity)
    {
        _{{classModelNameLower}}Repository.Add(entity);
    }

    public void Update({{classModelName}} entity)
    {
        _{{classModelNameLower}}Repository.Update(entity);
    }

    public void Delete({{classModelName}} entity)
    {
        _{{classModelNameLower}}Repository.Delete(entity);
    }
{{extraMethods}}    
}
