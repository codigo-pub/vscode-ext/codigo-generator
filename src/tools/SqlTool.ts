/* eslint-disable @typescript-eslint/naming-convention */
import pluralize from "pluralize";

export class SqlTool {
  // Default separator for many to many tables (e.g. classroom_has_students)
  static readonly MANY_TOO_MANY_SEP = "_has_";

  /**
   * Extracts the number enclosed within parentheses from a string.
   * @param {string} str The input string containing the number within parentheses.
   * @returns {number | undefined} The extracted number, or null if no number found.
   */
  static extractNumber(str: string): number | undefined {
    // Define the regular expression to extract the number within parentheses
    const regex = /\((\d+)\)/;
    // Execute the regular expression on the string
    const match = regex.exec(str);
    // Check if a match is found
    return match && match.length >= 2 ? parseInt(match[1], 10) : undefined;
  }

  /**
   * Extracts the enum values enclosed within parentheses from a string.
   * @param {string} str The input string containing the enum within parentheses.
   * @returns {string[] | undefined} The extracted enum values, or null if no enum values found.
   */
  static extractEnumValues(str: string): string[] | undefined {
    // Define the regular expression to extract the enum values within parentheses
    const regex = /\(([^)]+)\)/;
    // Execute the regular expression on the string
    const match = regex.exec(str);
    // Check if a match is found
    if (match && match.length >= 2) {
      // split values into array of string
      return match[1].split(",").map((value) => value.trim().replace(/^'(.*)'$/, "$1"));
    }
    return undefined;
  }

  /**
   * Extracts the type without precision as explained below with some examples
   * int => int
   * int(11) => int
   * decimal(6,2) => decimal
   * @param {string} str The input string containing the number within parentheses.
   * @returns {string | undefined} The extracted type, or null if no type found.
   */
  static extractType(str: string): string | undefined {
    let findOpenB = str ? str.indexOf("(") : -1;
    return findOpenB >= 0 ? str.substring(0, findOpenB) : str;
  }

  /**
   * Extracts the other table name from a combined table name string containing "_has_".
   *
   * Given a full table name that contains "_has_" between two table names, this function
   * will return the table name that is not the known table name.
   *
   * @param {string} fullTableName - The full table name containing "_has_" between two table names.
   * @param {string} knownTableName - The known table name to be excluded.
   * @param {string} separator - tables separator, default to '_has_'
   * @returns {string} - The other table name (pluralized) that is not the known table name or fullTableName if not found
   */
  static extractOtherTableName(fullTableName: string, knownTableName: string, separator: string = SqlTool.MANY_TOO_MANY_SEP): string {
    // Split the full table name into two parts around "_has_"
    const parts = fullTableName.split(separator ?? SqlTool.MANY_TOO_MANY_SEP);
    // Check which part does not match the known table name
    if (parts.length === 2) {
      // Singularize everyone
      const knownTable = pluralize.singular(knownTableName);
      const leftPart = pluralize.singular(parts[0]);
      const rightPart = pluralize.singular(parts[1]);
      if (leftPart === knownTable) {
        return pluralize.plural(rightPart);
      } else if (rightPart === knownTable) {
        return pluralize.plural(leftPart);
      }
    }
    // If no match, return fullTableName
    return fullTableName;
  }

  /**
   * Given a full table name that contains "_has_" between two table names, this function
   * will check 'knownTableName' name on the right or left part asked
   *
   * @param {string} fullTableName - The full table name containing "_has_" between two table names.
   * @param {string} knownTableName - The known table name to be excluded.
   * @param {string} separator - [optional] tables separator, default to '_has_'
   * @param {'left' | 'right'} part - part to check, default to 'right' part
   * @returns {true} - If table name is in the part mentioned
   */
  static hasTableLeftOrRight({
    fullTableName,
    knownTableName,
    separator = SqlTool.MANY_TOO_MANY_SEP,
    part = 'right'
  }: {
    fullTableName: string;
    knownTableName: string;
    separator?: string;
    part?: string;
  }): boolean {
    // Split the full table name into two parts around "_has_"
    const parts = fullTableName.split(separator ?? SqlTool.MANY_TOO_MANY_SEP);
    // Check which part does not match the known table name
    if (parts.length === 2) {
      // Singularize everyone
      const knownTable = pluralize.singular(knownTableName);
      const leftPart = pluralize.singular(parts[0]);
      const rightPart = pluralize.singular(parts[1]);
      if (part === 'left' && leftPart === knownTable) {
        return true;
      } 
      if (part === 'right' && rightPart === knownTable) {
        return true;
      }
    }
    // If no match, return false
    return false;
  }

  /**
   * Check if this table is a manytomany table
   * @param rTablename : table name
   * @param separator : separator between both table names (default to _has_)
   * @returns
   */
  static isManyToManyTable(rTablename: string, separator: string = SqlTool.MANY_TOO_MANY_SEP): boolean {
    // Note : that we check on purpose '> 0' instead of '>= 0' cause we don't want a table that start with '_has_' for example/
    return rTablename && rTablename.indexOf(separator) > 0 ? true : false;
  }
}
