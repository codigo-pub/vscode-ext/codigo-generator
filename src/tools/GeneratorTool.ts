import pluralize from "pluralize";
import { GenericDbFKey } from "../db/model/GenericDbFKey";
import { GenericDbField } from "../db/model/GenericDbField";
import { GenericDbTable } from "../db/model/GenericDbTable";
import { ExtraField } from "../model/ExtraField";
import { IExtraFieldProcessor } from "../processors/IExtraFieldProcessor";
import { SqlTool } from "./SqlTool";
import { ManyToManyRelation } from "../model/ManyToManyRelation";
import { IProcessor } from "../processors/IProcessor";

export class GeneratorTool {
  /**
   * Extracts the env key
   * @param {string} str The input string containing the env key
   * @returns {string | undefined} The env key or undefined if there is no env key
   */
  static extractEnvKey(str: string): string | undefined {
    // Define the regular expression to extract the number within parentheses
    const regex = /\${([^}]+)}/;
    // Execute the regular expression on the string
    const match = regex.exec(str);
    // Check if a match is found
    return match && match.length >= 2 ? match[1] : undefined;
  }

  /**
   * Gets value, extracting env value if there is a placeholder (e.g. "${placeholder}")
   * @param {string} str The input string containing the value or the placeholder 
   * @returns {string | undefined} The value or value retrieve via placeholder
   */
  static getValue(str: string): string | undefined {
    const valueTmp = this.extractEnvKey(str);
    return valueTmp ? process.env[valueTmp] : str;
  }

  /**
   * Returns percentage based on current and total
   * @param rTotal : total
   * @param rCurrent : current
   * @returns percentage
   */
  static getPercent(rTotal: number, rCurrent: number): number {
    // Calculate percentage, rounded
    const percent = Math.ceil((rCurrent * 100) / rTotal);
    // Return percentage
    return percent > 100 ? 100 : percent;
  }

  /**
   * Add extra fields, for OneToMany for example
   * @param rTable : GenericDbTable making the call
   * @param rFields : fields to process
   * @param rExtraFieldProcessor : extra field processor interface (should be the processor that implements IExtraFieldProcessor)
   * @param rReferenced : true if those fields are referenced fields, false if they are only foreign keys on the current table
   * @returns extra fields data
   */
  static getExtraFields(
    rTable: GenericDbTable,
    rFields: GenericDbFKey[],
    rExtraFieldProcessor: IExtraFieldProcessor,
    rReferenced: boolean = false
  ): ExtraField {
    let extraFields = new ExtraField();
    extraFields.importTxt = [];
    extraFields.fieldsTxt = "";
    extraFields.gettersSettersTxt = "";
    // Check parameters
    if (rFields && rExtraFieldProcessor) {
      for (const refFKey of rFields) {
        const extraField = rExtraFieldProcessor.processExtraField(rTable, refFKey, rReferenced);
        if (extraField) {
          extraFields.addImports(extraField.importTxt ?? []);
          extraFields.fieldsTxt += extraField.fieldsTxt;
          extraFields.gettersSettersTxt += extraField.gettersSettersTxt ?? '';
        }
      }
    } else {
      console.error("getExtraFields - No parameters ! Probably a bug ?!");
    }
    return extraFields;
  }

  /**
   * Gets info for a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param {GenericDbTable} rTable : Table making the call
   * @param {GenericDbTable[]} rAllTables : All tables without filter cause we need information on target table (would be Classrooms table in previous example)
   * @param {GenericDbFKey} rFKey : foreign referenced key
   * @param {AbstractProcessor} rProcessor : class Processor making the calll
   * @param {string | undefined} rManyToManySep : many to many separator
   * @param {'left' | 'right' | undefined } rPart : can be set to keep only pivot tables with rTable.tablename to the left/righ part (so only one way for the findBy)
   * @returns {ManyToManyRelation} object with info on relationship to help generate findBy method
   */
  static getInfoFindBy({
    rTable,
    rAllTables,
    rFKey,
    rProcessor,
    rManyToManySep = SqlTool.MANY_TOO_MANY_SEP,
    rPart,
  }: {
    rTable: GenericDbTable;
    rAllTables: GenericDbTable[];
    rFKey: GenericDbFKey;
    rProcessor: IProcessor;
    rManyToManySep?: string;
    rPart?: "left" | "right";
  }): ManyToManyRelation | undefined {
    let manyToManyRelation;
    // Look for table on allTables to have information about it
    const manyTable = rAllTables.find((item) => item.tableName === rTable.tableName);
    // Only ManyToMany pivot tables are allowed 
    if (
      manyTable && 
      rTable.fields &&
      rFKey.tableName &&
      SqlTool.isManyToManyTable(rFKey.tableName, rManyToManySep) &&
      (!rPart ||
        SqlTool.hasTableLeftOrRight({ fullTableName: rFKey.tableName, knownTableName: rTable.tableName, separator: rManyToManySep, part: rPart }))
    ) {
      // Extract other table
      const tableDistantName = SqlTool.extractOtherTableName(rFKey.tableName, rTable.tableName, rManyToManySep);
      // Proceed only if extraction was done
      if (tableDistantName !== rFKey.tableName) {
        // Search for this table
        let tableDistant = rAllTables.find((t) => t.tableName === tableDistantName);
        // If not found, try in singular form
        if (!tableDistant) {
          const singular = pluralize.singular(tableDistantName);
          tableDistant = rAllTables.find((t) => t.tableName === singular);
        }
        // If found
        if (tableDistant) {
          // Note that we check for tableDistant.referencedFKeys, that's cause we gonna need it
          if (tableDistant?.fields && tableDistant?.fields.length > 0 && tableDistant.referencedFKeys && tableDistant.referencedFKeys.length > 0) {
            // Search for this table pkey to find type
            let keys = GeneratorTool.getPkeyFields(tableDistant.fields);
            // If more than one pkey, we don't know how to do (so far)
            // note that in query we expect pkey composite property of distant table to equal 'id'
            if (keys && keys.length === 1) {
              // Now search in actual pivot table for distant pkey name
              const refKeyForPivot = tableDistant.referencedFKeys.find((t) => t.tableName === rFKey.tableName);
              if (refKeyForPivot) {
                manyToManyRelation = new ManyToManyRelation();
                // Local
                manyToManyRelation.tableName = rTable.tableName;
                manyToManyRelation.tableNameUp = GeneratorTool.upperFirstLetter(rTable.tableName);
                manyToManyRelation.className = rProcessor.processClassNameFromTable(rTable.tableName);
                // Pivot table
                manyToManyRelation.tablePivotName = rFKey.tableName;
                manyToManyRelation.classNamePivot = rProcessor.processClassNameFromTable(rFKey.tableName);
                manyToManyRelation.pivotDistantPkeyName = refKeyForPivot.columnName;
                // Distant
                manyToManyRelation.tableDistant = tableDistant;
                manyToManyRelation.classNameDistant = rProcessor.processClassNameFromTable(manyToManyRelation.tableDistant.tableName);
                manyToManyRelation.distantPkeyField = keys[0]; // Store pkey field for convenience
                return manyToManyRelation;
              }
            }
          }
        }
      }
    }
    // Ensure we return undefined if we get here
    return undefined;
  }

  /**
   * Filter array of GenericDbTable to remove tables based on actual configuration
   * (based on skip_these_tables and only_these_tables)
   * @param rTables : tables to filter
   * @param rSkipTables : array of tables to skip
   * @param rOnlyTables : array of tables to exclusively process, if there are entries and a table is not there, it will be skipped
   * @returns filtered tables
   */
  static filterTables(rTables: GenericDbTable[], rSkipTables?: string[], rOnlyTables?: string[]) {
    // config
    const skipTables = rSkipTables ?? [];
    const onlyTables = rOnlyTables ?? [];

    return rTables.filter((currentTable) => {
      let skip = false;
      // if they are exclusive tables, check if this table is part of
      if (onlyTables.length > 0 && onlyTables.indexOf(currentTable.tableName) < 0) {
        skip = true;
      }
      // last chance to skip...
      if (!skip) {
        // check if we need to skip this table
        if (skipTables.length > 0 && skipTables.indexOf(currentTable.tableName) >= 0) {
          skip = true;
        }
      }
      return !skip;
    });
  }

  /**
   * Change first letter uppercase
   * e.g.: Myclass
   * @param rTxt : text
   * @param rLowerRightPart : true to lowercase the right part of text, so only first Letter is upper, default to false
   * @returns text with first letter uppercase and rest lowercase
   */
  static upperFirstLetter(rTxt: string, rLowerRightPart = false): string {
    if (rTxt) {
      const txt = (rLowerRightPart === true) ? rTxt.toLowerCase() : rTxt;
      return txt.length > 1 ? txt[0].toUpperCase() + txt.slice(1) : txt;
    } else {
      return rTxt;
    }
  }

  /**
   * Set first letter to lower case
   * e.g.: myVariableRepository
   * @param rTxt : text
   */
  static lowerFirstLetter(rTxt: string) {
    if (rTxt) {
      return rTxt.length > 1 ? rTxt[0].toLowerCase() + rTxt.slice(1) : rTxt;
    } else {
      return rTxt;
    }
  }

  /**
   * Return all pkey members
   * @param rFields : array of fields
   * @returns
   */
  static getPkeyFields(rFields: GenericDbField[]): GenericDbField[] {
    return rFields.filter((field) => field.isPKey);
  }

  /**
   * Return all fkey members
   * @param rFields : array of fields
   * @returns array of fields that corresponds to filter
   */
  static getFkeyFields(rFields: GenericDbField[]): GenericDbField[] {
    return rFields.filter((field) => field.isFKey);
  }

  /**
   * Get string representation of model for AI completion
   * @param rFields : array of fields
   * @param rUseCamelCaseForField : true to use camel case for field name, false otherwise, default to true
   * @returns model string representation
   */
  static getModelStringAIFromFields(rFields: GenericDbField[], rUseCamelCaseForField?: boolean | undefined): string {
    return rFields
    .map(field => `${rUseCamelCaseForField === false ? 
      GeneratorTool.processFieldName(field.fieldName ?? '') 
      : GeneratorTool.processFieldNameCamel(field.fieldName ?? '') } (type is ${field.type?.type ?? 'unknown'})`)
    .join(", ");
  }

  /**
   * Convenient method to singularize a wprd
   * @param rTxt : txt to singular
   * @returns singular word
   */
  static txtToSingularize(rTxt?: string): string | undefined {
    return rTxt ? pluralize.singular(rTxt) : rTxt;
  }

  /**
   * Convenient method to pluralize a wprd
   * @param rTxt : txt to pluralize
   * @returns plural word
   */
  static txtToPluralize(rTxt?: string): string | undefined {
    return rTxt ? pluralize.plural(rTxt) : rTxt;
  }

  /**
   * Get GenericDbFKey linked to this field
   * If there are more than one found we return undefined. Rather return nothing then the wrong one.
   * @param rField : Field
   * @param rFKeys : Foreign keys
   * @returns foreign key linked to this field
   */
  static getFKeyFromField(rField: GenericDbField, rFKeys?: GenericDbFKey[]): GenericDbFKey | undefined {
    if (rField && rFKeys && rFKeys.length > 0) {
      const fKeys = rFKeys.filter((foreignKey) => foreignKey.columnName === rField.fieldName);
      return fKeys && fKeys.length === 1 ? fKeys[0] : undefined;
    }
    return undefined;
  }

  /**
   * Get all GenericDbFKey linked to this field fkey name. Uesful to get foreign key with composite columns
   * 
   * @param rField : Field
   * @param rFKeys : Foreign keys
   * @returns foreign keys linked to this field fkey name
   */
  static getAllFKeyFromField(rField: GenericDbField, rFKeys?: GenericDbFKey[]): GenericDbFKey[] | undefined {
    const fKey = GeneratorTool.getFKeyFromField(rField, rFKeys);
    if (fKey && rFKeys) {
      return rFKeys.filter((foreignKey) => foreignKey.constraintName === fKey.constraintName);
    }
    return undefined;
  }

  /**
   * Change table name to lowercase and first letter uppercase and singularize to suit most class names convention
   * ex: Myclass
   * @param rTable : table name
   * @returns class name
   */
  static processClassNameFromTable(rTable: string): string {
    return rTable && rTable.length > 1 ? rTable[0].toUpperCase() + GeneratorTool.txtToSingularize(rTable)?.slice(1).toLowerCase() : rTable;
  }

  /**
   * Override way of getting classname from table to suit efcore way of naming models class
   * We do this in AbstractCsharpProcessor so it will also be done for every processor here in csharp
   * remember CsharpModelProcessor could be used for efcore too
   * @param rTable : table name
   * @returns
   */
  static processClassNameFromTableCamel(rTable: string): string {
    const classTmpName = GeneratorTool.processClassNameFromTable(rTable);
    return GeneratorTool.stringToCamel({ rText: classTmpName });
  }

  /**
   * Process field name for using as a class member for example
   * @param rField : field
   * @returns field name
   */
  static processFieldName(rField: string): string {
    return rField ? rField.toLowerCase() : rField;
  }

  /**
   * Process field name using camelcase for using as a class member for example
   * @param rField : field
   * @returns field name
   */
  static processFieldNameCamel(rField: string): string {
    const fieldName = GeneratorTool.processFieldName(rField);
    return fieldName ? GeneratorTool.stringToCamel({ rText: fieldName, rFirstLower: true }) : fieldName;
  }

  /**
   * Try to change text to camel
   * @param rText : text
   * @param rSep : separator (default to '_')
   * @param rFirstLower : true to make first letter of text returned be in lowerCase. If text length is 1 we ignore this rule
   * @returns text camelize
   */
  static stringToCamel({ rText, rSep = "_", rFirstLower = false }: { rText: string; rSep?: string; rFirstLower?: boolean }): string {
    let toCamel;
    // If name has "_" we split then do uppercase
    if (rText && rText.length > 1 && rText.indexOf(rSep) > -1) {
      const parts = rText.split(rSep);
      for (let i = 0; i < parts.length; i++) {
        parts[i] = parts[i].charAt(0).toUpperCase() + parts[i].slice(1);
      }
      toCamel = parts.join("");
    } else {
      toCamel = rText && rText.length > 1 ? rText.charAt(0).toUpperCase() + rText.slice(1) : rText;
    }
    return rFirstLower ? (toCamel && toCamel.length > 1 ? toCamel.charAt(0).toLowerCase() + toCamel.slice(1) : toCamel) : toCamel;
  }

  /**
   * Converts a camelCase or PascalCase string back to a separated format (e.g., snake_case or kebab-case).
   * @param rText : camelCase or PascalCase string
   * @param rSep : separator to use (default is '_')
   * @param rLowerCase : true to make the output string fully lowercase (default is true)
   * @returns text in separated format
   */
  static camelToString({ rText, rSep = "_", rLowerCase = true }: { rText: string; rSep?: string; rLowerCase?: boolean }): string {
    if (!rText || rText.length === 0) { return rText; }

    // Split at uppercase letters and join with the separator
    const separatedText = rText.replace(/([a-z])([A-Z])/g, '$1' + rSep + '$2');

    // Optionally make the result lowercase
    return rLowerCase ? separatedText.toLowerCase() : separatedText;
  }

}
