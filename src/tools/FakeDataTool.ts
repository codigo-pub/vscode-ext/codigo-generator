/**
 * This class is intend to produce fake data
 * We don't rely on tools like 'Faker' to get fake data cause we want to always give the same value
 * We will see in the future if using Faker is more appropriate
 */
export class FakeDataTool {
  /**
   * Try to get a more realistic value from a column name for fake data
   * @param rColName : column name
   * @param rLength : length
   */
  static getTxtValueFromColName(rColName: string, rLength?: number): string {
    let value = "";
    switch (rColName.toLowerCase()) {
      case "login":
      case "username":
      case "user":
      case "email":
        value = "john.doe@gmail.com";
        break;
      case "firstname":
      case "first_name":
      case "fname":
        value = "John";
        break;
      case "lastname":
      case "last_name":
      case "lname":
        value = "Doe";
        break;
      case "name":
      case "fullname":
      case "full_name":
        value = "John Doe";
        break;
      case "address":
      case "addr":
        value = "123 Main St";
        break;
      case "city":
        value = "Springfield";
        break;
      case "state":
      case "province":
        value = "CA";
        break;
      case "zipcode":
      case "zip":
      case "postal_code":
        value = "12345";
        break;
      case "country":
        value = "USA";
        break;
      case "phone":
      case "telephone":
      case "mobile":
        value = "555-1234";
        break;
      case "company":
      case "organization":
        value = "Acme Corporation";
        break;
      case "title":
      case "position":
        value = "Software Engineer";
        break;
      case "description":
      case "desc":
        value = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
        break;
      case "ip":
      case "ip_address":
        value = "192.168.1.1";
        break;
      case "url":
      case "website":
        value = "http://www.example.com";
        break;
      case "date":
      case "dob":
      case "birth_date":
        value = "1990-01-01";
        break;
      case "gender":
        value = "Male";
        break;
      default:
        value = "xxxxx";
    }
    // truncate string if needed
    if (rLength) {
      value = value.substring(0, rLength);
    }
    return value;
  }
}
