import * as vscode from "vscode";
import { l10n } from "vscode";

export class DocTool {
  /**
   * Check if document is open and is a typescript doc
   * @param rDoc : Doc
   * @param rLanguagesIdExpected : [optional] list of expected languageId ('TextDocument.languageId') for actual file  
   * @returns doc of type vscode.TextEditor
   */
  static checkDoc(rDoc?: vscode.TextEditor, rLanguagesIdExpected?: string[]): boolean {
    // check if doc is open
    if (!rDoc) {
      vscode.window.showErrorMessage(l10n.t("No file opened"));
      return false;
    }
    if (rLanguagesIdExpected) {
      // check if doc extension is handled
      const codeLang = rDoc?.document?.languageId;
      if (codeLang && rLanguagesIdExpected.indexOf(codeLang) >= 0) {
          return true;
      } else {
          vscode.window.showErrorMessage(l10n.t("Ooops, this type of file is not handled ?!"));
          return false;
      }
    }
    return true;
  }
}
