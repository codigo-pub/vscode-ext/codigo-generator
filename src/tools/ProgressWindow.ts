import * as vscode from "vscode";
import { l10n } from "vscode";

/**
 * ProgressWindow Helper Class
 */
export class ProgressWindow {
  private panel: vscode.WebviewPanel | undefined;
  private processors: string[];
  private progressValues: number[];

  constructor(processors: string[]) {
    this.processors = processors;
    this.progressValues = new Array(processors.length).fill(0); // Initialize progress to 0 for each processor
  }

  show(): void {
    // If panel already exists, we show it
    if (this.panel) {
      this.panel.reveal(vscode.ViewColumn.One);
      return;
    }

    // Create webview panel
    this.panel = vscode.window.createWebviewPanel(
      "progressWindow", 
      "Processor Progress", 
      vscode.ViewColumn.One, {
        enableScripts: true,
        retainContextWhenHidden: true, // Important, maintain context to avoid loosing data when focus out
      }
    );

    this.panel.webview.html = this.getWebviewContent();

    // Handle messages from the webview
    this.panel.webview.onDidReceiveMessage((message) => {
      if (message.command === "close") {
        this.dispose();
      }
    });

    // When dispose we set panel to undefined
    this.panel.onDidDispose(() => {
      this.panel = undefined;
    });
  }

  updateProgress(index: number, value: number): void {
    if (this.panel) {
      this.progressValues[index] = value;
      this.panel.webview.postMessage({ type: "updateProgress", progress: this.progressValues });

      // If all progress is complete, notify the webview to enable the close button
      if (this.progressValues.every((p) => p >= 100)) {
        this.panel.webview.postMessage({ type: "allDone" });
      }
    }
  }

  /**
   * Get html web view of progress page
   * @returns html web view
   */
  private getWebviewContent(): string {
    const progressBars = this.processors
      .map((name, index) => `<div>${name}: <progress id="progress-${index}" value="0" max="100"></progress></div>`)
      .join("");

    return `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Progress</title>
        <style>
          body {
            font-family: Arial, sans-serif;
            text-align: center;
            margin: 0;
            padding: 0;
          }
          .container {
            margin: auto;
          }
          #progress-container {
            display: inline-block;
            text-align: left;
            margin-top: 20px;
          }
            button {
                margin-top: 20px;
                padding: 10px 25px;
                font-size: 16px;
                font-weight: bold;
                color: #ffffff;
                background-color: #007acc; /* VS Code theme blue */
                border: none;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s ease, transform 0.2s ease;
            }

            button:hover {
                background-color: #005f99; /* Darker blue for hover */
                transform: scale(1.05); /* Slight zoom effect */
            }

            button:disabled {
                background-color: #cccccc;
                cursor: not-allowed;
            }
          img {
            margin-top: 20px;
          }
        </style>
      </head>
      <body>
        <div class="container">
            <h1>codigo-generator</h1>
            <br/>
            <img src="https://twini.fra1.cdn.digitaloceanspaces.com/vscode-ext/img/generator/img_codigo_generator_001.png" alt="codigo-generator" width="200px">
            <br/>
            <div id="progress-container">
                ${progressBars}
            </div>
            <br/>
            <button id="close-btn" disabled>${l10n.t("Process running")}...</button>
        </div>
        <script>
          const vscode = acquireVsCodeApi();
          const progressBars = ${JSON.stringify(this.processors.map((_, i) => `progress-${i}`))};
  
          window.addEventListener("message", (event) => {
            const message = event.data;
  
            if (message.type === "updateProgress") {
              message.progress.forEach((value, index) => {
                const bar = document.getElementById(progressBars[index]);
                if (bar) bar.value = value;
              });
            } else if (message.type === "allDone") {
              const btnClose = document.getElementById("close-btn"); 
              btnClose.textContent = "${l10n.t("Done")} !";
              btnClose.disabled = false;
            }
          });
  
          document.getElementById("close-btn").addEventListener("click", () => {
            vscode.postMessage({ command: "close" });
          });
        </script>
      </body>
      </html>
    `;
  }

  dispose(): void {
    this.panel?.dispose();
  }
}
