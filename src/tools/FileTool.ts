import fs from "fs";
import path from "path";
import { StaticFile } from "../model/StaticFile";
import * as vscode from "vscode";

export class FileTool {
  /**
   * Check if the last character is the folder separator
   * if it's not, we add it
   * @param rPath : path
   * @returns the new path with folder separator at the end
   */
  static addFolderSeparator(rPath?: string): string | undefined {
    if (!rPath) {
      return rPath;
    }
    return !rPath.endsWith(path.sep) ? rPath + path.sep : rPath;
  }

  /**
   * Copy relative files from source to dest folder
   * @param rBaseFolderSrc : Base source folder to look for relative static files
   * @param rBaseFolderDest : Base destination folder to copy relative static files
   * @param rFiles : List of relative files
   */
  static copyStaticFiles(rBaseFolderSrc: string, rBaseFolderDest: string, rFiles: StaticFile[]) {
    if (!fs.existsSync(rBaseFolderSrc)) {
      throw new Error(`Source folder ${rBaseFolderSrc} does not exist.`);
    }

    if (!fs.existsSync(rBaseFolderDest)) {
      throw new Error(`Destination folder ${rBaseFolderDest} does not exist.`);
    }

    for (const file of rFiles) {
      if (!file.src || !file.dest) {
        throw new Error("Both src and dest paths must be provided for each file.");
      }

      const srcPath = path.join(rBaseFolderSrc, file.src);
      const destPath = path.join(rBaseFolderDest, file.dest);

      if (!fs.existsSync(srcPath)) {
        throw new Error(`Source file ${srcPath} does not exist.`);
      }

      // Extract dest folder path of destination file (can be different from rBaseFolderDest if file.Dest already contains a folder)
      const folderPath = path.dirname(destPath);
      // Create file dest folder if needed
      if (!fs.existsSync(folderPath)) {
        fs.mkdirSync(folderPath, { recursive: true });
      }

      try {
        fs.copyFileSync(srcPath, destPath);
      } catch (error) {
        throw new Error(`Failed to copy file ${srcPath} to ${destPath}: ${error}`);
      }
    }
  }

  /**
   * Construct base path depending on configuration and provided path string
   * @param pathStr The path string to use for constructing the base path, will be append at the end
   * @param configPath The configuration path to check for relative or absolute path
   * @param addPathStr If addPathStr true, then we add pathStr in path when it is an absolute path. Useful to set this to true for
   * templates paths
   * @returns The constructed base path
   */
  static constructBasePath(pathStr: string, configPath: string | undefined, addPathStr: boolean = false): string {
    if (configPath && path.isAbsolute(configPath)) {
      // if path is absolute we don't add template name
      return addPathStr ? path.join(configPath, path.sep, pathStr, path.sep) : path.join(configPath, path.sep);
    } else if (vscode.workspace.workspaceFolders) {
      return path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, configPath ?? "", pathStr, path.sep);
    } else {
      return path.join(configPath ?? "", pathStr, path.sep);
    }
  }

  /**
   * Create new folder if needed
   * @param rFolderPath : folder path
   */
  static checkNewFolder(rFolderPath?: string): void {
    if (rFolderPath) {
      if (!fs.existsSync(rFolderPath)) {
        fs.mkdirSync(rFolderPath);
      }
    }
  }

  /**
   * Create full path from java package on base directory
   * @param rJavaPackage : java package (e.g : 'com.codigo.generator')
   * @param rBaseDir : base directory
   */
  static getFullPathFromPackage(rJavaPackage: string, rBaseDir: string): string {
    // Convert the Java package to a directory path
    const packagePath = rJavaPackage.replace(/\./g, path.sep);
    // Create the full path by joining the base directory with the package path
    return path.join(rBaseDir, packagePath);
  }

  /**
   * Create subfolders for java package on base directory
   * @param rJavaPackage : java package (e.g : 'com.codigo.generator')
   * @param rBaseDir : base directory
   */
  static createPackageDirectories(rJavaPackage: string, rBaseDir: string): void {
    // Get full path
    const fullPath = this.getFullPathFromPackage(rJavaPackage, rBaseDir);
    if (!fs.existsSync(fullPath)) {
      // Recursively create the directories
      fs.mkdirSync(fullPath, { recursive: true });
    }
  }
}
