export interface IProcessor {
  /**
   * Runs the process
   */
  process(): Promise<void>;

  /**
   * Change table name to a class name.
   * Mostly we will put table name in lowercase then first letter uppercase and finally singularize 
   * to suit most class names convention
   * ex: Myclass
   * @param rTable : table name
   * @returns class name
   */
  processClassNameFromTable(rTable: string): string;

  /**
   * get processor folder name
   * We will use it for the out folder files
   * @returns processor folder name
   */
  getProcessorFolder(): string;

  /**
   * get processor name, a name is likely to be unique for his template
   * e.g. in typescript template, we have only one 'model' processor with this name
   * but in other templates, we could find a 'model' one too
   * @returns processor name
   */
  getProcessorName(): string


  /**
   * Use this method on processor to give him the callback for updating progress bar
   * @param onProgress callback method for update progress bar 
   */
  setOnProgress(onProgress: (progress: number) => void): void;
}
