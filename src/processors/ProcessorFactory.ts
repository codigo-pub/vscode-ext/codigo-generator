/* eslint-disable @typescript-eslint/naming-convention */
import { ConfigProcessor } from "../model/ConfigProcessor";
import { IProcessor } from "./IProcessor";
import { TypescriptLoopbackModelProcessor } from "./typescript/loopback/TypescriptLoopbackModelProcessor";
import { IDatabase } from "../db/IDatabase";
import { TypescriptModelProcessor } from "./typescript/TypescriptModelProcessor";
import { TypescriptAngularServiceProcessor } from "./typescript/angular/TypescriptAngularServiceProcessor";
import { TypescriptRowmapperProcessor } from "./typescript/TypescriptRowmapperProcessor";
import { GlobalConfig } from "../model/GlobalConfig";
import { PhpModelProcessor } from "./php/PhpModelProcessor";
import { PhpPdoDaoProcessor } from "./php/PhpPdoDaoProcessor";
import { PhpDoctrineEntityProcessor } from "./php/symfony/PhpDoctrineEntityProcessor";
import { PhpDoctrineRepositoryProcessor } from "./php/symfony/PhpDoctrineRepositoryProcessor";
import * as vscode from "vscode";
import { JavaModelProcessor } from "./java/JavaModelProcessor";
import { JavaJpaEntityProcessor } from "./java/jpa/JavaJpaEntityProcessor";
import { JavaJpaRepositoryProcessor } from "./java/jpa/JavaJpaRepositoryProcessor";
import { PhpEloquentModelProcessor } from "./php/laravel/PhpEloquentModelProcessor";
import { PhpArtisanMakeModel } from "./php/laravel/PhpArtisanMakeModel";
import { JavaSpringServiceProcessor } from "./java/spring/JavaSpringServiceProcessor";
import { JavaSpringControllerProcessor } from "./java/spring/JavaSpringControllerProcessor";
import { PhpSymfonyServiceProcessor } from "./php/symfony/PhpSymfonyServiceProcessor";
import { PhpSymfonyApiControllerProcessor } from "./php/symfony/PhpSymfonyApiControllerProcessor";
import { PythonDjangoModelProcessor } from "./python/django/PythonDjangoModelProcessor";
import { PythonDjangoImportModelsProcessor } from "./python/django/PythonDjangoImportModelsProcessor";
import { PythonDjangoControllerProcessor } from "./python/django/PythonDjangoControllerProcessor";
import { PythonDjangoControllerPathProcessor } from "./python/django/PythonDjangoControllerPathProcessor";
import { PythonDjangoPostmanProcessor } from "./python/django/PythonDjangoPostmanProcessor";
import { DefaultPostmanProcessor } from "./DefaultPostmanProcessor";
import { CsharpModelProcessor } from "./csharp/CsharpModelProcessor";
import { CsharpEFCoreRepositoryProcessor } from "./csharp/efcore/CsharpEFCoreRepositoryProcessor";
import { CsharpNetControllerProcessor } from "./csharp/net/CsharpNetControllerProcessor";
import { CsharpNetServiceProcessor } from "./csharp/net/CsharpNetServiceProcessor";
import { CsharpEFCoreModelProcessor } from "./csharp/efcore/CsharpEFCoreModelProcessor";
import { CsharpNetProgramProcessor } from "./csharp/net/CsharpNetProgramProcessor";
import { JavaSpringSecurityJwtProcessor } from "./java/spring-security/JavaSpringSecurityJwtProcessor";
import { TypescriptAngularViewModelFormProcessor } from "./typescript/angular/TypescriptAngularViewModelFormProcessor";
import { TypescriptAngularViewModelCardProcessor } from "./typescript/angular/TypescriptAngularViewModelCardProcessor";
import { TypescriptAngularViewModelListProcessor } from "./typescript/angular/TypescriptAngularViewModelListProcessor";
import { TypescriptAngularViewModelListPageProcessor } from "./typescript/angular/TypescriptAngularViewModelListPageProcessor";
import { TypescriptIonicAngularServiceProcessor } from "./typescript/ionic/TypescriptIonicAngularServiceProcessor";
import { TypescriptIonicAngularViewModelCardProcessor } from "./typescript/ionic/TypescriptIonicAngularViewModelCardProcessor";
import { TypescriptIonicAngularViewModelFormProcessor } from "./typescript/ionic/TypescriptIonicAngularViewModelFormProcessor";
import { TypescriptIonicAngularViewModelListPageProcessor } from "./typescript/ionic/TypescriptIonicAngularViewModelListPageProcessor";
import { TypescriptIonicAngularViewModelListProcessor } from "./typescript/ionic/TypescriptIonicAngularViewModelListProcessor";
import { TypescriptLoopbackRepositoryProcessor } from "./typescript/loopback/TypescriptLoopbackRepositoryProcessor";
import { TypescriptLoopbackControllerProcessor } from "./typescript/loopback/TypescriptLoopbackControllerProcessor";
import { TypescriptLoopbackServiceProcessor } from "./typescript/loopback/TypescriptLoopbackServiceProcessor";

export class ProcessorFactory {
  /**
   * Get an instance of processor from given configuration
   * @param rConfig : GlobalConfig object
   * @param rConfigProcessor : Processor config
   * @param rDb : IDatabase object
   * @returns an instance of processor required
   */
  static getProcessor(rConfig: GlobalConfig, rConfigProcessor: ConfigProcessor, rDb: IDatabase): IProcessor | undefined {
    let processor: IProcessor | undefined;
    // if template and processor name was specified
    if (rConfig?.template && rConfigProcessor?.name) {
      switch (rConfig.template) {
        case "typescript":
          processor = ProcessorFactory.getTypescriptProcessor(rConfigProcessor?.name, rConfig, rConfigProcessor, rDb);
          break;
        case "php":
          processor = ProcessorFactory.getPhpProcessor(rConfigProcessor?.name, rConfig, rConfigProcessor, rDb);
          break;
        case "java":
          processor = ProcessorFactory.getJavaProcessor(rConfigProcessor?.name, rConfig, rConfigProcessor, rDb);
          break;
        case "csharp":
          processor = ProcessorFactory.getCsharpProcessor(rConfigProcessor?.name, rConfig, rConfigProcessor, rDb);
          break;
        case "python":
          processor = ProcessorFactory.getPythonProcessor(rConfigProcessor?.name, rConfig, rConfigProcessor, rDb);
          break;
        default:
          vscode.window.showErrorMessage(`Template unknown : [${rConfig.template}]`);
      }
    }
    return processor;
  }

  /**
   * Get an instance of processor from given configuration for a typescript template
   * @param rProcessorName : Processor name that we want
   * @param rConfig : GlobalConfig object
   * @param rConfigProcessor : Processor config
   * @param rDb : IDatabase object
   * @returns an instance of processor required
   */
  static getTypescriptProcessor(
    rProcessorName: string,
    rConfig: GlobalConfig,
    rConfigProcessor: ConfigProcessor,
    rDb: IDatabase
  ): IProcessor | undefined {
    let processor: IProcessor | undefined;
    switch (rProcessorName) {
      case "model":
        processor = new TypescriptModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "angular_service":
        processor = new TypescriptAngularServiceProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "angular_view_model_form":
        processor = new TypescriptAngularViewModelFormProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "angular_view_model_card":
        processor = new TypescriptAngularViewModelCardProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "angular_view_model_list":
        processor = new TypescriptAngularViewModelListProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "angular_view_model_list_page":
        processor = new TypescriptAngularViewModelListPageProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "ionic_angular_service":
        processor = new TypescriptIonicAngularServiceProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "ionic_angular_view_model_form":
        processor = new TypescriptIonicAngularViewModelFormProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "ionic_angular_view_model_card":
        processor = new TypescriptIonicAngularViewModelCardProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "ionic_angular_view_model_list":
        processor = new TypescriptIonicAngularViewModelListProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "ionic_angular_view_model_list_page":
        processor = new TypescriptIonicAngularViewModelListPageProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "loopback_model":
        processor = new TypescriptLoopbackModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "loopback_repository":
        processor = new TypescriptLoopbackRepositoryProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "loopback_controller":
        processor = new TypescriptLoopbackControllerProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "loopback_service":
        processor = new TypescriptLoopbackServiceProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "row_mapper":
        processor = new TypescriptRowmapperProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "postman":
        processor = new DefaultPostmanProcessor(rConfig, rConfigProcessor, rDb);
        break;
      default:
        vscode.window.showErrorMessage(`Processor unknown ! [${rConfigProcessor.name}]`);
        break;
    }
    return processor;
  }

  /**
   * Get an instance of processor from given configuration for a php template
   * @param rProcessorName : Processor name that we want
   * @param rConfig : GlobalConfig object
   * @param rConfigProcessor : Processor config
   * @param rDb : IDatabase object
   * @returns an instance of processor required
   */
  static getPhpProcessor(rProcessorName: string, rConfig: GlobalConfig, rConfigProcessor: ConfigProcessor, rDb: IDatabase): IProcessor | undefined {
    let processor: IProcessor | undefined;
    switch (rProcessorName) {
      case "model":
        processor = new PhpModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "pdo_dao":
        processor = new PhpPdoDaoProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "doctrine_entity":
        processor = new PhpDoctrineEntityProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "doctrine_repository":
        processor = new PhpDoctrineRepositoryProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "symfony_service":
        processor = new PhpSymfonyServiceProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "symfony_api_controller":
        processor = new PhpSymfonyApiControllerProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "eloquent_model":
        processor = new PhpEloquentModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "artisan_make_model":
        processor = new PhpArtisanMakeModel(rConfig, rConfigProcessor, rDb);
        break;
      case "postman":
        processor = new DefaultPostmanProcessor(rConfig, rConfigProcessor, rDb);
        break;
      default:
        vscode.window.showErrorMessage(`Processor unknown ! [${rConfigProcessor.name}]`);
        break;
    }
    return processor;
  }

  /**
   * Get an instance of processor from given configuration for a java template
   * @param rProcessorName : Processor name that we want
   * @param rConfig : GlobalConfig object
   * @param rConfigProcessor : Processor config
   * @param rDb : IDatabase object
   * @returns an instance of processor required
   */
  static getJavaProcessor(rProcessorName: string, rConfig: GlobalConfig, rConfigProcessor: ConfigProcessor, rDb: IDatabase): IProcessor | undefined {
    let processor: IProcessor | undefined;
    switch (rProcessorName) {
      case "model":
        processor = new JavaModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "jpa_entity":
        processor = new JavaJpaEntityProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "jpa_repository":
        processor = new JavaJpaRepositoryProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "spring_service":
        processor = new JavaSpringServiceProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "spring_controller":
        processor = new JavaSpringControllerProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "spring_security_jwt":
        processor = new JavaSpringSecurityJwtProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "postman":
        processor = new DefaultPostmanProcessor(rConfig, rConfigProcessor, rDb);
        break;
      default:
        vscode.window.showErrorMessage(`Processor unknown ! [${rConfigProcessor.name}]`);
        break;
    }
    return processor;
  }

  /**
   * Get an instance of processor from given configuration for a csharp template
   * @param rProcessorName : Processor name that we want
   * @param rConfig : GlobalConfig object
   * @param rConfigProcessor : Processor config
   * @param rDb : IDatabase object
   * @returns an instance of processor required
   */
  static getCsharpProcessor(
    rProcessorName: string,
    rConfig: GlobalConfig,
    rConfigProcessor: ConfigProcessor,
    rDb: IDatabase
  ): IProcessor | undefined {
    let processor: IProcessor | undefined;
    switch (rProcessorName) {
      case "model":
        processor = new CsharpModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "efcore_model":
        processor = new CsharpEFCoreModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "efcore_repository":
        processor = new CsharpEFCoreRepositoryProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "net_service":
        processor = new CsharpNetServiceProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "net_controller":
        processor = new CsharpNetControllerProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "net_program":
        processor = new CsharpNetProgramProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "postman":
        processor = new DefaultPostmanProcessor(rConfig, rConfigProcessor, rDb);
        break;
      default:
        vscode.window.showErrorMessage(`Processor unknown ! [${rConfigProcessor.name}]`);
        break;
    }
    return processor;
  }

  /**
   * Get an instance of processor from given configuration for a python template
   * @param rProcessorName : Processor name that we want
   * @param rConfig : GlobalConfig object
   * @param rConfigProcessor : Processor config
   * @param rDb : IDatabase object
   * @returns an instance of processor required
   */
  static getPythonProcessor(
    rProcessorName: string,
    rConfig: GlobalConfig,
    rConfigProcessor: ConfigProcessor,
    rDb: IDatabase
  ): IProcessor | undefined {
    let processor: IProcessor | undefined;
    switch (rProcessorName) {
      case "django_model":
        processor = new PythonDjangoModelProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "django_controller":
        processor = new PythonDjangoControllerProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "django_import_models":
        processor = new PythonDjangoImportModelsProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "django_controller_path":
        processor = new PythonDjangoControllerPathProcessor(rConfig, rConfigProcessor, rDb);
        break;
      case "postman":
        processor = new PythonDjangoPostmanProcessor(rConfig, rConfigProcessor, rDb);
        break;
      default:
        vscode.window.showErrorMessage(`Processor unknown ! [${rConfigProcessor.name}]`);
        break;
    }
    return processor;
  }
}
