/* eslint-disable @typescript-eslint/naming-convention */
import Supplant from "supplant";
import { IProcessor } from "./IProcessor";
import fs from "fs";
import { IConverter } from "./IConverter";
import { IDatabase } from "../db/IDatabase";
import { ConfigProcessor } from "../model/ConfigProcessor";
import { GlobalConfig } from "../model/GlobalConfig";
import { FileTool } from "../tools/FileTool";
import path from "path";
import * as vscode from "vscode";
import { GeneratorTool } from "../tools/GeneratorTool";
import { GenericDbTable } from "../db/model/GenericDbTable";
import { StaticFile } from "../model/StaticFile";
import { ExtraField } from "../model/ExtraField";
import { SqlConvertEnumDbType } from "../model/SqlConvertEnumDbType";
import { IServiceAI } from "../service/ai/IServiceAI";
import { ServiceAIFactory } from "../service/ai/ServiceAIFactory";
import { GenericDbField } from "../db/model/GenericDbField";

export abstract class AbstractProcessor implements IProcessor {
  protected db: IDatabase;
  protected config: GlobalConfig;
  protected configProcessor: ConfigProcessor;
  protected supplant: Supplant;
  protected converter: IConverter;
  protected aiService?: IServiceAI; // IT is important every processor use is own AI Service instance for future specialization

  // base folder for all templates
  protected baseTemplatesFolder: string = "";

  // if needed, you can overwrite this to define a sub-folder for your processor templates
  // it is convenient for grouping processor templates of same framework, (e.g. 'angular' folder for every angular templates)
  protected templateFolder?: string; 

  // set to true to say that this processor can use AI. doesn't mean it would cause it depends from useAI global config property too
  // every child must provide this member
  protected abstract canUseAI?: boolean;

  protected readonly STATIC_FOLDER = "static";

  protected readonly PARAM_PREFIX = "r";

  constructor(rConfig: GlobalConfig, rConfigProcessor: ConfigProcessor, rDb: IDatabase) {
    this.db = rDb;
    this.config = rConfig;
    this.configProcessor = rConfigProcessor;
    this.supplant = new Supplant();
    this.converter = this.getConverter();
    // Note that rConfig.baseTemplatesFolder is set by ServiceGenerator init method
    this.baseTemplatesFolder = rConfig.baseTemplatesFolder ?? "";
  }

  // public methods ----------------------------------------------------------------------------------------------------------------------------------
  /**
   * Change table name to lowercase and first letter uppercase and singularize to suit most class names convention
   * ex: Myclass
   * @param rTable : table name
   * @returns class name
   */
  processClassNameFromTable(rTable: string): string {
    // We use camelcase by default
    return this.configProcessor.useCamelCaseForClass === false
      ? GeneratorTool.processClassNameFromTable(rTable)
      : GeneratorTool.processClassNameFromTableCamel(rTable);
  }

  /**
   * Get base api url from table name
   * @param rTable : table name
   */
  getModelBaseApiUrlFromTable(rTable: string): string {
    const endpoint = this.configProcessor?.extra_config?.use_model_for_api_endpoint === true ? this.processClassNameFromTable(rTable) : rTable;
    return endpoint.toLowerCase();
  }
  // ---------------------------------------------------------------------------------------------------------------------------------- public methods

  // abstract methods --------------------------------------------------------------------------------------------------------------------------------
  /**
   * get processor name, a name is likely to be unique for his template
   * e.g. in typescript template, we have only one 'model' processor with this name
   * but in other templates, we could find a 'model' one too
   * @returns processor name
   */
  abstract getProcessorName(): string;

  /**
   * get processor folder name
   * We will use it for the out folder files
   * @returns processor folder name
   */
  abstract getProcessorFolder(): string;

  /**
   * Main process. Call this.defaultProcess() if there is no particularity in tables retrieving
   */
  abstract process(): Promise<void>;

  /**
   * onProgress method, careful this method must be given through setter by the one who generates the processor instance
   */
  protected onProgress?: (progress: number) => void;

  /**
   * Process for a given table name
   * @param rTable : table name
   */
  protected abstract processTable(rTable: GenericDbTable): Promise<void>;

  /**
   * Get converter for fields
   * A converter is a class that may convert a field type to a language type for example
   */
  protected abstract getConverter(): IConverter;

  /**
   * Takes an import name and generates the import call
   * @param rImportName : import name, e.g. 'java.util.Date'
   * @returns import call given that name (e.g. 'import java.util.Date;')
   */
  protected abstract getImportFrom(rImportName: string): string;
  // -------------------------------------------------------------------------------------------------------------------------------- abstract methods

  // protected methods -------------------------------------------------------------------------------------------------------------------------------
  /**
   * Default process method for most cases.
   * This method retrieves tables names from database and runs processTable on it
   * The process() method is intend to call this one
   * @param rInitFolders : true to create folders if necessary, false otherwise. Default to true
   */
  protected async defaultProcess(rInitFolders: boolean = true): Promise<void> {
    // we must do init here cause
    this.init();

    // create directory if needed
    if (rInitFolders) {
      this.initFolders();
    }

    try {
      if (this.config?.database?.database) {
        // Get all tables filtered based on configuration
        const filteredTables = await this.getAllTables();

        // If not tables, this is weird, should have some tables so we get out cause it's gonna be the same for other processors
        if (filteredTables && filteredTables.length === 0) {
          throw new Error("No tables found ?!");
        }

        let percent = 0;
        // loop through filtered tables
        for (let i = 0; i < filteredTables.length; i++) {
          const table = filteredTables[i];
          // Update percentage
          percent = GeneratorTool.getPercent(filteredTables.length, i);
          // update progress bar
          if (this.onProgress) {
            this.onProgress(percent); // Report progress
          }
          // process this table
          if (table.fields) {
            await this.processTable(table);
          } else {
            vscode.window.showWarningMessage(`Hmm, there are no fields found in table ${table.tableName}`);
          }
        }
        // Done working...
        // vscode.window.showInformationMessage(`${l10n.t("Done")} ${this.getProcessorName()} processor !`);
        if (this.onProgress) {
          this.onProgress(100); // Report end of progress by setting to 100
        }
      } else {
        return Promise.reject("No database specified in config file");
      }
    } catch (err) {
      vscode.window.showErrorMessage(`Process error : ${err}`);
    }
  }

  /**
   * Init services and other things
   * !!! Warning !!! This method can't be called from constructor cause abstract properties like canUseAI are not enabled at this time
   */
  protected init(): void {
    // If useAI is set and enabled and this processor can handle AI and AI is not deactivated for this processor, we prepare service
    if (this.config.useAI?.enabled === true && this.canUseAI && !(this.configProcessor?.useAI === false)) {
      this.aiService = ServiceAIFactory.getServiceAI(this.config.useAI);
    }
  }

  /**
   * Create folders if needed for this processor
   */
  protected initFolders() {
    // create directory if needed
    if (!fs.existsSync(this.getOutBasePath())) {
      fs.mkdirSync(this.getOutBasePath());
    }
    if (!fs.existsSync(this.getOutProcessorPath())) {
      // For processor path we use recursive true
      fs.mkdirSync(this.getOutProcessorPath(), { recursive: true });
    }
  }

  /**
   * Get all tables based on actual configuration
   * @param rNoFilter : true to get all tables without applying filter, default to false
   * @returns all tables filtered
   */
  protected async getAllTables(rNoFilter: boolean = false): Promise<GenericDbTable[]> {
    try {
      if (this.config?.database?.database) {
        // Get all tables descriptions
        const tables = await this.db.getAllTablesDescription(this.db.getSchema());
        // Filter tables based on configuration
        return rNoFilter === true ? tables : GeneratorTool.filterTables(tables, this.config?.skip_these_tables, this.config?.only_these_tables);
      }
    } catch (err) {
      vscode.window.showErrorMessage(`Process error : ${err}`);
    }
    return [];
  }

  /**
   * Copy relative files from this processor source folder to dest folder
   * @param rFiles : List of relative files
   */
  protected copyStaticFiles(rFiles: StaticFile[]) {
    try {
      if (rFiles && rFiles.length > 0) {
        const srcFolder = path.join(this.getTemplateBasePath(), this.STATIC_FOLDER);
        FileTool.copyStaticFiles(srcFolder, this.getOutProcessorPath(), rFiles);
      }
    } catch (err) {
      vscode.window.showErrorMessage(`Process error : ${err}`);
    }
  }

  /**
   * Get base template path depending on configuration
   * @returns The base template path
   */
  protected getTemplateBasePath(): string {
    if (this.config.template) {
      // if we don't have a custom base template (99% of the cases)
      const noCustomBaseTemplate = this.configProcessor?.custom_base_template === undefined;
      // Base template path for all templates
      const allTemplatesBasePath = `${this.configProcessor?.custom_base_template ?? this.baseTemplatesFolder}`;
      // if there is a template folder set for this processor and no custom_base_template we add it to template folder
      // example : if template is 'typescript' and templateFolder is set to 'angular', we will search templates on 'typescript/angular' folder
      // if no templateFolder is set, we just use template folder value. So, in previous example we will search templates on 'typescript' folder
      const templateFolder = (this.templateFolder && noCustomBaseTemplate) ? 
        path.join(this.config.template, this.templateFolder) : this.config.template;
      // If no custom template, we set addPathStr to true to add template on path cause we look for templates internally and this is the way...
      return FileTool.constructBasePath(templateFolder, allTemplatesBasePath, noCustomBaseTemplate);
    } else {
      throw new Error("No template config property");
    }
  }

  /**
   * Get base out path depending on configuration
   * @returns The base out path
   */
  protected getOutBasePath(): string {
    if (this.config.template) {
      return FileTool.constructBasePath('', this.config.out_folder);
    } else {
      throw new Error("No template config property");
    }
  }

  /**
   * get out path for processor
   * @returns full path for processor
   */
  getOutProcessorPath(): string {
    return `${this.getOutBasePath()}${this.getProcessorFolder()}${path.sep}`;
  }

  /**
   * Generate fieldname based on tablename
   * We use this for many relations OneToMany and ManyToOne
   * @param rTablename : tablename
   * @param rSingularize : true to singularize name, false otherwise. Default to false
   * @returns fieldname
   */
  protected getFieldNameMany(rTablename: string, rSingularize: boolean = false) {
    const tableName = rSingularize ? GeneratorTool.txtToSingularize(rTablename) : rTablename;
    return this.getFieldName(tableName ?? "");
  }

  /**
   * Get field name
   * @param rField : field name
   * @returns field name
   */
  protected getFieldName(rField: string) {
    // We use camelcase by default
    return this.configProcessor.useCamelCaseForField === false ? GeneratorTool.processFieldName(rField) : GeneratorTool.processFieldNameCamel(rField);
  }

  /**
   * Add an import if needed for this field
   * note that 'getImportFrom' will be implemented by child classes
   * @param rExtraField : extra field
   * @param rFieldType : field type string as converted by the converter
   */
  protected addImportForField(rExtraField: ExtraField, rFieldType?: SqlConvertEnumDbType) {
    if (rExtraField && rFieldType?.import) {
      // Add import if needed
      for (const importName of rFieldType.import) {
        rExtraField.addImport(this.getImportFrom(importName));
      }
    }
  }

  /**
   * Call AI with given text as the text to modify
   * Prompt for asking things to AI is defined in service so far
   * If there is no service AI (cause user doesn't want to use it) we return text as it is
   * @param rGeneratedText : text that AI must change
   * @param rFields : [Optionnal]List of table fields
   * @param rCustomPrompt [Optional]custom prompt to override given default prompt
   * @returns text changed (or not)
   */
  protected async getCompletionAI(
    rGeneratedText: string,
    rFields?: GenericDbField[],
    rCustomPrompt?: string | undefined
  ): Promise<string | undefined> {
    // We check if AI service is ready and if canUseAI is true cause it may have been changed at runtime (@see TypescriptAngularViewModelProcessor)
    if (this.aiService && this.canUseAI) {
      // Get model representation
      const model =
        rFields && rFields.length > 0 ? GeneratorTool.getModelStringAIFromFields(rFields, this.configProcessor.useCamelCaseForField) : undefined;
        try {
          // Get AI text completion
          return await this.aiService.generateCompletion(rGeneratedText, model, rCustomPrompt);          
        } catch (error) {
          vscode.window.showErrorMessage(`${error}`);
        }
    } else {
      return undefined;
    }
  }

  /**
   * Writes a processed template to a file, optionally including AI-generated content.
   *
   * @param rFilePath - The file path where the template will be written.
   * @param rTemplate - The template string to be processed.
   * @param rFieldsTemplate - The data object used to replace placeholders in the template.
   * @param rFields - [Optional] Array of fields for additional AI processing.
   * @param rCustomPrompt [Optional]custom prompt to override given default prompt
   * @param rOverwriteAll [Optional]Define whether or not the AI response will override all template file. Default to false.
   * The rOverwriteAll can be useful if you want to give a file to the AI to produce another file without having a destination template
   * @returns A Promise that resolves when the file has been written.
   */
  protected async writeTemplateToFile({
    rFilePath,
    rTemplate,
    rFieldsTemplate,
    rFields,
    rCustomPrompt,
    rOverwriteAll = false,
  }: {
    rFilePath: string;
    rTemplate: string;
    rFieldsTemplate: any;
    rFields?: GenericDbField[];
    rCustomPrompt?: string;
    rOverwriteAll?: boolean;
  }): Promise<void> {
    // We inject to have first shot file body
    var dataTpl = this.supplant.text(rTemplate, rFieldsTemplate);
    // Call AI if needed
    const iaCompletion = await this.getCompletionAI(dataTpl, rFields, rCustomPrompt);
    // Add iaCompletion to fieldsTemplate (iaCompletion can be undefined at this time if no AI set by user)
    if (iaCompletion) {
      rFieldsTemplate["iaCompletion"] = iaCompletion;
      // And reinject with AI completion
      dataTpl = rOverwriteAll === true ? iaCompletion : this.supplant.text(rTemplate, rFieldsTemplate);
    }
    // Write to files, careful for interface repositories, we start with a I as it is done in template
    fs.writeFileSync(rFilePath, dataTpl, "utf8");
  }

  // ------------------------------------------------------------------------------------------------------------------------------- protected methods

  public getConfig(): GlobalConfig {
    return this.config;
  }

  public setConfig(value: GlobalConfig) {
    this.config = value;
  }

  public getConfigProcessor(): ConfigProcessor {
    return this.configProcessor;
  }

  public setConfigProcessor(value: ConfigProcessor) {
    this.configProcessor = value;
  }

  public isCanUseAI(): boolean | undefined {
    return this.canUseAI;
  }
  public setCanUseAI(value: boolean) {
    this.canUseAI = value;
  }

  /**
   * Use this method on processor to give him the callback for updating progress bar
   * @param onProgress callback method for update progress bar
   */
  setOnProgress(onProgress: (progress: number) => void): void {
    this.onProgress = onProgress;
  }
}
