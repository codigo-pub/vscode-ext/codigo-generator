import { EnumDbType } from "../../db/model/EnumDbType";
import { GenericDbTypeField } from "../../db/model/GenericDbTypeField";
import { SqlConvertEnumDbType } from "../../model/SqlConvertEnumDbType";
import { AbstractConverter } from "../AbstractConverter";

export class PythonConverter extends AbstractConverter {
  readonly typeMappings: SqlConvertEnumDbType[] = [
    { sqlType: EnumDbType.char, targetType: "str" },
    { sqlType: EnumDbType.varchar, targetType: "str" },
    { sqlType: EnumDbType.uuid, targetType: "str" },
    { sqlType: EnumDbType.longtext, targetType: "str" },
    { sqlType: EnumDbType.enum, targetType: "str" },

    { sqlType: EnumDbType.long, targetType: "int" }, // in python, there is no 'long' type
    { sqlType: EnumDbType.int, targetType: "int" },
    { sqlType: EnumDbType.numeric, targetType: "int" },
    { sqlType: EnumDbType.float, targetType: "float" },
    { sqlType: EnumDbType.double, targetType: "float" },

    { sqlType: EnumDbType.date, targetType: "datetime.date", import: ["datetime"] },
    { sqlType: EnumDbType.datetime, targetType: "datetime.datetime", import: ["datetime"] },
    { sqlType: EnumDbType.timestamp, targetType: "datetime.datetime", import: ["datetime"] },
    { sqlType: EnumDbType.time, targetType: "datetime.time", import: ["datetime"] },

    { sqlType: EnumDbType.interval, targetType: "datetime.timedelta", import: ["datetime"] },

    { sqlType: EnumDbType.boolean, targetType: "bool" },

    { sqlType: EnumDbType.blob, targetType: "bytes" },

    { sqlType: EnumDbType.json, targetType: "str" },

    { sqlType: EnumDbType.array, targetType: "list[str]" },
  ];
  
  /**
   * Translate sql type to TypeScript type
   * @param {JSON} rField : Field
   * @returns typescript type
   */
  fieldConvertToType(rField?: GenericDbTypeField): SqlConvertEnumDbType | undefined {
    return this.fieldConvertFromMapping(rField, this.typeMappings);
  }
}
