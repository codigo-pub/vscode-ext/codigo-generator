/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { ExtraField } from "../../../model/ExtraField";
import { AbstractDjangoProcessor } from "./AbstractDjangoProcessor";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { PythonDjangoControllerProcessor } from "./PythonDjangoControllerProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";

export class PythonDjangoControllerPathProcessor extends AbstractDjangoProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;


  private readonly FILE_NAME = "urls_controllers.py";
  private many_to_many_sep?: string;
  private controllerProcessor?: PythonDjangoControllerProcessor; // To get findBy method name

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return ""; // at root path
  }

  getProcessorName(): string {
    return "django_controller_path";
  }

  /**
   * Not implemented as this processor gather all tables data to process them in one file !
   */
  protected processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    this.controllerProcessor = new PythonDjangoControllerProcessor(this.config, this.configProcessor, this.db);

    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    // As we are not calling defaultprocess method, we need to init services and create directory if needed
    this.init();
    this.initFolders();
    // Get all tables (filtered based on configuration)
    const tables = await this.getAllTables();
    let extraFields = new ExtraField();
    extraFields.fieldsTxt = "\t"; // tab for first element
    // Load template
    var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_django_controller_path_python", "utf8");

    if (tables && tables.length > 0) {
      for (const table of tables) {
        if (table?.tableName && this.isNotDjangoTable(table) && table?.fields && table.fields.length > 0) {
          const modelBaseApiUrl = this.getModelBaseApiUrlFromTable(table.tableName);
          const classModelName = this.processClassNameFromTable(table.tableName);
          const className = `${classModelName}Controller`;
          extraFields.addImport(`from ${this.config.packageBaseName ?? ""}.controllers.${className} import ${className}`);
          // Load urls in extraFields record
          this.loadUrlPatterns(extraFields, className, classModelName, GeneratorTool.getPkeyFields(table.fields), modelBaseApiUrl);
          // Browse ManyToMany referenced keys to add some cool methods
          if (table?.referencedFKeys && this.allTables) {
            for (const fielddata of table.referencedFKeys) {
              // Add extra method
              this.generateFindBy(table, this.allTables, fielddata, extraFields, className);
            }
          }
        }
      }
    }
    const modelTemplate = {
      extraFields_import: extraFields.getImportsTxt(),
      urlPatterns: extraFields.fieldsTxt,
    };

    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${this.FILE_NAME}`, rTemplate: tpl, rFieldsTemplate: modelTemplate });

  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @param rControllerClassName : Controller class name 
   * @returns
   */
  private generateFindBy(rTable: GenericDbTable, rAllTables: GenericDbTable[], rFKey: GenericDbFKey, rExtraMethods: ExtraField, rControllerClassName: string) {
    // Important : for django (and python) we do one way for findBy method cause django can't handle both sides like in java
    const dataRelation = GeneratorTool.getInfoFindBy({
      rTable,
      rAllTables,
      rFKey,
      rProcessor: this,
      rManyToManySep: this.many_to_many_sep,
      rPart: "right",
    });
    if (dataRelation && this.controllerProcessor) {
      // Get findBy method name to add url path pattern correctly
      const methodName = this.controllerProcessor.getFindByMethodName(dataRelation.tableName ?? '', dataRelation.classNameDistant ?? '');
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);      
      rExtraMethods.fieldsTxt += `path('${dataRelation.className?.toLowerCase()}/${dataRelation.classNameDistant?.toLowerCase()}/<${typePkey?.targetType}:${dataRelation.distantPkeyField?.fieldName}>', ${rControllerClassName}.as_view(), name='${methodName}', kwargs={'action': '${methodName}'}),\n\t`;
    }
  }

  /**
   * Generates all paths for this table
   * @param rExtraField : Extra field to store url paths as field text
   * @param rClassName : Class name (name of the controller class)
   * @param rClassModelName : class model name
   * @param rPkeyList : Pkey list
   * @param rModelBaseApiUrl Model base api endpoint (e.g. users for users table)
   */
  private loadUrlPatterns(rExtraField: ExtraField, rClassName: string, rClassModelName: string, rPkeyList: GenericDbField[], rModelBaseApiUrl: string) {
    const lowerModel = rClassModelName.toLowerCase();
    // Make url parameters fields
    let urlParameters = "";
    for (const pKey of rPkeyList) {
      if (pKey.fieldName) {
        // Get python type of the field
        const typeField = this.converter.fieldConvertToType(pKey.type);
        urlParameters += urlParameters !== "" ? "/" : "";
        urlParameters += `<${typeField?.targetType ?? ""}:${pKey.fieldName}>`;
      }
    }
    // Generates path urls
    rExtraField.fieldsTxt += `path('${rModelBaseApiUrl}', ${rClassName}.as_view(), name='list_${lowerModel}', kwargs={'action': 'list'}),\n\t`;
    rExtraField.fieldsTxt += `path('${rModelBaseApiUrl}/${urlParameters}', ${rClassName}.as_view(), name='retrieve_${lowerModel}', kwargs={'action': 'retrieve'}),\n\t`;
    rExtraField.fieldsTxt += `path('${rModelBaseApiUrl}/create', ${rClassName}.as_view(), name='create_${lowerModel}', kwargs={'action': 'create'}),\n\t`;
    rExtraField.fieldsTxt += `path('${rModelBaseApiUrl}/update/${urlParameters}', ${rClassName}.as_view(), name='update_${lowerModel}', kwargs={'action': 'update'}),\n\t`;
    rExtraField.fieldsTxt += `path('${rModelBaseApiUrl}/delete/${urlParameters}', ${rClassName}.as_view(), name='delete_${lowerModel}', kwargs={'action': 'delete'}),\n\t`;
  }

}
