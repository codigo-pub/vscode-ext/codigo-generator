/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { ExtraField } from "../../../model/ExtraField";
import { AbstractDjangoProcessor } from "./AbstractDjangoProcessor";

export class PythonDjangoImportModelsProcessor extends AbstractDjangoProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;


  private readonly FILE_NAME = "models.py";

  getProcessorFolder(): string {
    return ""; // at root path
  }
  
  getProcessorName(): string {
    return "django_import_models";
  }

  /**
   * Not implemented as this processor gather all tables to process them in one file !
   */
  protected async processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async process(): Promise<void> {

    // As we are not calling defaultprocess method, we need to init services and create directory if needed
    this.init();
    this.initFolders();
    // Get all tables (filtered based on configuration)
    const tables = await this.getAllTables();
    let extraFields = new ExtraField();
    // Load template
    var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_django_import_models_python", "utf8");

    if (tables && tables.length > 0) {
      for (const table of tables) {
        if (table?.tableName && this.isNotDjangoTable(table) && table?.fields && table.fields.length > 0) {
          var className = this.processClassNameFromTable(table.tableName);
          extraFields.addImport(`from .models.${className} import ${className}`);
        }
      }
    }
    const modelTemplate = {
      extraFields_import: extraFields.getImportsTxt(),
    };
    // Write to file
    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${this.FILE_NAME}`, rTemplate: tpl, rFieldsTemplate: modelTemplate });
  }
}
