/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { IConverter } from "../../IConverter";
import { PythonConverter } from "../PythonConverter";
import { PythonDjangoModelProcessor } from "./PythonDjangoModelProcessor";
import { AbstractDjangoProcessor } from "./AbstractDjangoProcessor";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";

export class PythonDjangoControllerProcessor extends AbstractDjangoProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;


  private many_to_many_sep?: string;

  private entityProcessor?: PythonDjangoModelProcessor;

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return "controllers";
  }

  getProcessorName(): string {
    return "django_controller";
  }

  /**
   * Custom converter for django, thanks to Django :)
   * @returns
   */
  protected getConverter(): IConverter {
    return new PythonConverter();
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    this.entityProcessor = new PythonDjangoModelProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;

    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (this.isNotDjangoTable(rTable) && rTable?.fields && this.entityProcessor) {
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // Process table data
        if (pKeys && pKeys.length > 1) {
          await this.processTableWithCompositePKey(rTable);
        }  else {
          await this.processTableWithSinglePkey(rTable);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   * @returns string for template file or null if nothing could be done
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable): Promise<void> {
    if (this.isNotDjangoTable(rTable) && rTable?.fields && this.entityProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_django_controller_python", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var className = `${classModelName}Controller`;
      const pkeyList = GeneratorTool.getPkeyFields(rTable.fields);
      var extraFields = new ExtraField();

      if (pkeyList && pkeyList.length === 1) {
        const pkey = pkeyList[0];

        // Browse ManyToMany referenced keys to add some cool methods
        if (rTable?.referencedFKeys && this.allTables) {
          for (const fielddata of rTable.referencedFKeys) {
            // Add extra method
            this.generateFindBy(rTable, this.allTables, fielddata, extraFields);
          }
        }

        const modelTemplate = {
          classModelName: classModelName,
          classModelNameLower: classModelName.toLowerCase(),
          className: className,
          packageBaseName: this.config.packageBaseName,
          modelFolder: this.entityProcessor.getProcessorFolder(),
          pkeyName: pkey.fieldName,
          extraFields_import: extraFields.getImportsTxt(),
          extraFields_fields: extraFields.fieldsTxt,
          extraFields_dispatch: extraFields?.extra?.dispatch ? extraFields?.extra?.dispatch.join("") : "",
        };

        await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.py`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
      }
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   * @returns string for template file or null if nothing could be done
   */
  protected async processTableWithCompositePKey(rTable: GenericDbTable): Promise<void> {
    if (this.isNotDjangoTable(rTable) && rTable?.fields && this.entityProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_django_controller_c_python", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var className = `${classModelName}Controller`;
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pkeyList = GeneratorTool.getPkeyFields(rTable.fields);
      const pkeyFields = this.getPkeyCompositeFields(pkeyList, rTable.fKeys, extraFields);

      const modelTemplate = {
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        extraFields_import: extraFields.getImportsTxt(),
        pkey_field: pkeyFields.field,
        pkey_parameter: pkeyFields.parameter,
        pkey_childmodels: pkeyFields.childmodels,
        pkey_childmodels_id: pkeyFields.childmodels_id,
        pkey_kwargs: pkeyFields.kwargs,
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.py`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generate different texts that are necessary for template based on pkey fields
   * @param rFields : pkey fields
   * @param rFkeys : Foregin keys
   * @param rExtraField : Extra field data
   * @returns data for template
   */
  private getPkeyCompositeFields(
    rFields: GenericDbField[],
    rFkeys: GenericDbFKey[] | undefined,
    rExtraField: ExtraField
  ): { parameter: string; childmodels_id: string; childmodels: string; field: string; kwargs: string } {
    // Prepare data
    let data = { parameter: "", childmodels_id: "", childmodels: "", field: "", kwargs: "" };
    // If fields to proceed
    if (rFields && rFields.length > 0) {
      // Browse pkfields to add them in text
      for (const field of rFields) {
        if (field?.fieldName) {
          // Add code data
          data.parameter += (data.parameter !== "" ? ", " : "") + `${field.fieldName}`;
          data.kwargs += (data.kwargs !== "" ? ", " : "") + `kwargs.get('${field.fieldName}')`;
          // get foreign key eventually
          const fKey: GenericDbFKey | undefined = GeneratorTool.getFKeyFromField(field, rFkeys);
          // if field is from a foreign key we are looking for his model
          if (fKey?.referencedTable) {
            const classModelName = this.processClassNameFromTable(fKey.referencedTable);
            const lowerClassModelName = classModelName.toLowerCase();
            // child model (we suppose model as a single pkey, cross finger)
            if (!data.childmodels_id) {
              data.childmodels = "\t\t\t# Get Models only to check if they exist";
            }
            data.childmodels_id += `\n\t\t\t${field.fieldName} = dataJson.get('${lowerClassModelName}')`;
            data.childmodels += `\n\t\t\t${lowerClassModelName} = ${classModelName}.objects.get(pk=${field.fieldName})`;
            data.field += (data.field !== "" ? ", " : "") + `${lowerClassModelName}=${lowerClassModelName}`;
            rExtraField.addImport(`from ${this.config.packageBaseName ?? ""}.models.${classModelName} import ${classModelName}`);
          } else {
            data.field += (data.field !== "" ? ", " : "") + `${field.fieldName}=${field.fieldName}`;
          }
        }
      }
    }
    return data;
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(rTable: GenericDbTable, rAllTables: GenericDbTable[], rFKey: GenericDbFKey, rExtraMethods: ExtraField) {
    // Important : for django (and python) we do one way for findBy method cause django can't handle both sides like in java
    const dataRelation = GeneratorTool.getInfoFindBy({
      rTable,
      rAllTables,
      rFKey,
      rProcessor: this,
      rManyToManySep: this.many_to_many_sep,
      rPart: "right",
    });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }
      if (!rExtraMethods.extra) {
        rExtraMethods.extra = { dispatch: [] };
      }
      // let refColumn = dataRelation.pivotDistantPkeyName;
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableName ?? "", dataRelation.classNameDistant ?? "");

      let method = `\n\tdef ${methodName}(self, request, id):
\t\t# Retrieve all ${dataRelation.tableNameUp} from the database
\t\t${dataRelation.classNameDistant?.toLowerCase()} = ${dataRelation.classNameDistant}.objects.get(pk=id)
\t\t${dataRelation.tableName} = ${dataRelation.classNameDistant?.toLowerCase()}.${dataRelation.className?.toLowerCase()}_set.all()

\t\t# Serialize data to JSON
\t\tdata = serializers.serialize('json', ${dataRelation.tableName})

\t\t# Return JSON response with HttpResponse to avoid json string representation 
\t\treturn HttpResponse(data, content_type='application/json')\n`;
      // add text to extra fields
      rExtraMethods.fieldsTxt += method;
      // import model
      rExtraMethods.addImport(`from ${this.config.packageBaseName ?? ""}.models.${dataRelation.classNameDistant} import ${dataRelation.classNameDistant}`);

      rExtraMethods.extra.dispatch.push(`\t\telif action == '${methodName}' and request.method == 'GET':
\t\t\treturn self.${methodName}(request, kwargs.get('${dataRelation.distantPkeyField?.fieldName}'))
`);
    }
  }

  /**
   * Get method name to get all xxx by yyy id
   * @param rTablename : table name that we are gonna get model list with first letter upper case (xxx)
   * @param rClassNameDistant : foreign table linked to this model (yyy)
   * @returns method name
   */
  getFindByMethodName(rTablename: string, rClassNameDistant: string) {
    return `list_${rTablename.toLowerCase()}_by_${rClassNameDistant.toLowerCase()}_id`;
  }
}
