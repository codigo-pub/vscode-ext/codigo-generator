/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { ExtraField } from "../../../model/ExtraField";
import { IExtraFieldProcessor } from "../../IExtraFieldProcessor";
import { EnumDbType } from "../../../db/model/EnumDbType";
import { SqlTool } from "../../../tools/SqlTool";
import { IConverter } from "../../IConverter";
import { PythonConverter } from "../PythonConverter";
import { EnumDbRule } from "../../../db/model/EnumDbRule";
import { AbstractDjangoProcessor } from "./AbstractDjangoProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";

export class PythonDjangoModelProcessor extends AbstractDjangoProcessor implements IExtraFieldProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;


  private many_to_many_sep?: string;

  private pkey_already_written = false;

  getProcessorFolder(): string {
    return "models";
  }

  getProcessorName(): string {
    return "django_model";
  }

  /**
   * Custom converter for django, thanks to Django :)
   * @returns
   */
  protected getConverter(): IConverter {
    return new PythonConverter();
  }

  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (this.isNotDjangoTable(rTable) && rTable?.fields && rTable.fields.length > 0) {
      this.pkey_already_written = false;
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_django_model_python", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var extraFields = new ExtraField(); // only used for imports by now
      var metaFields = new ExtraField(); // for Meta part

      // Add table name to meta ----------------------------------------------------------------------------------------------------------------------
      metaFields.fieldsTxt = "\tclass Meta:";
      metaFields.fieldsTxt += "\n\t\t# Specify the table name";
      metaFields.fieldsTxt += `\n\t\tdb_table = '${rTable.tableName}'`;
      // ---------------------------------------------------------------------------------------------------------------------- Add table name to meta

      // Is it a pivot table ?
      var isPivotTable = SqlTool.isManyToManyTable(rTable.tableName, this.many_to_many_sep);

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      var isComposite = pKeys && pKeys.length > 1; // set flag

      //If there are composite key, create property declaration
      if (isComposite) {
        metaFields.fieldsTxt += this.getCompositePKeyFieldsTxt(pKeys, rTable.tableName);
      }

      // To get ManyToMany only
      if (!isPivotTable && !isComposite) {
        extraFields = GeneratorTool.getExtraFields(rTable, rTable.referencedFKeys ?? [], this);
      }

      // Browse fields
      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var fieldType = this.converter.fieldConvertToType(fielddata.type);
          // get foreign key eventually
          const fKey: GenericDbFKey | undefined = GeneratorTool.getFKeyFromField(fielddata, rTable.fKeys);
          // inject field
          fieldsToInject += this.getDjangoField(fielddata, fKey, isComposite, extraFields);
        }
      }

      const modelTemplate = {
        tableName: rTable.tableName,
        className: className,
        // packageBaseName: this.config.packageBaseName,
        fields: fieldsToInject,
        extraFields_import: extraFields.getImportsTxt(),
        extraFields_fields: extraFields.fieldsTxt,
        metaFieldsTxt: metaFields.fieldsTxt,
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.py`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });

    }
  }

  /**
   * Generate composite pkey field entry
   * @param rPkeyFields : PKey fields array
   * @param rCompositePkeyName : composite pkey name
   * @returns
   */
  private getCompositePKeyFieldsTxt(rPkeyFields: GenericDbField[], rCompositePkeyName: string): string {
    let txt = "";
    if (rPkeyFields && rPkeyFields.length > 0) {
      txt += "\n\t\tconstraints = [ models.UniqueConstraint(fields=[ ";
      for (let i = 0; i < rPkeyFields.length; i++) {
        const field = rPkeyFields[i];
        txt += `'${field.fieldName}'` + (i < rPkeyFields.length - 1 ? ", " : "");
      }
      txt += ` ], name='${rCompositePkeyName}_pkey')]`;
    }
    return txt;
  }

  /**
   * Process extra field
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @returns extra field with texts
   */
  processExtraField(rTable: GenericDbTable, rFKey: GenericDbFKey): ExtraField {
    let extraField = new ExtraField();
    if (rFKey.tableName) {
      extraField.fieldsTxt = "";
      extraField.importTxt = [];
      extraField.gettersSettersTxt = "";
      // Note : there are no one to many in django

      // detect if this is a ManyToMany relation but we want to avoid circular dependencies (cause Python...) so we only do things
      // if the current model is in the right part
      // e.g. : We are in current model 'students' and fkey is table 'classroom_has_students', it's ok to proceed
      // but with a table 'student_has_classrooms' we would skip it
      if (
        SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep) &&
        SqlTool.hasTableLeftOrRight({ fullTableName: rFKey.tableName, knownTableName: rTable.tableName })
      ) {
        this.getOrmManyToMany(rTable, rFKey, extraField);
      }
    }
    return extraField;
  }

  /**
   * Get field declaration
   * @param rField : field
   * @param rFKey : foreign key
   * @param isComposite : true if table got composite key
   * @param rExtraFields : extra fields
   * @returns
   */
  private getDjangoField(rField: GenericDbField, rFKey: GenericDbFKey | undefined, isComposite: boolean, rExtraFields: ExtraField): string {
    let djangoField = "";
    // declare pkey property even if django auto detects conventional pkeys
    if (rField?.type) {
      const modelTyp = this.getDjangoModelsTypeByType(rField);

      // Only set length if it is not a int (by now)
      // TODO: see if we may check other types
      const length = rField.type.type !== EnumDbType.int && rField.type.length && rField.type.length > 0 ? `max_length=${rField.type.length}` : "";
      if (rField.isPKey && !isComposite) {
        djangoField += `\n\t${rField.fieldName} = models.${modelTyp}(primary_key=True`;
        if (length) {
          djangoField += `, ${length}`;
        }
        djangoField += ")";
      } else if (rFKey) {
        djangoField += this.getOrmManyToOne(rFKey, rExtraFields, rField);
      } else {
        // could be part of a composite key but not a foreign key so we check
        const primaryNoFkey = rField.isPKey ? "primary_key=True" : "";

        // If type is Array we embbed it, we only use this for simple fields (no fkey, pkey)
        let embedFieldInArrayFieldStart = "";
        let embedFieldInArrayFieldEnd = "";
        if (rField.type?.type === EnumDbType.array && !rField.isPKey && !rField.isFKey) {
          rExtraFields.addImport("from django.contrib.postgres.fields import ArrayField");
          embedFieldInArrayFieldStart += "ArrayField(";
          embedFieldInArrayFieldEnd += ", blank=True, default=list)";
        }

        // If this is an enum type and has enumeration values (extra) and enum processing not disabled
        if (
          rField?.type?.type === EnumDbType.enum &&
          rField.type.extra &&
          rField.type.extra.length > 0 &&
          !(this.configProcessor.extra_config?.use_enum === false)
        ) {
          // Create enum file
          // No need to import cause enum file will be on same package
          this.createEnumFileFromType(rField);
          const enumName = this.getEnumNameFromField(rField);
          // We take first enum value as default value, user will have to change it if he wants !
          const defaultValue = `, default=${enumName}.${rField.type.extra[0]}`;
          // Set max_length as the longest enum value
          const maxLength = Math.max(...rField.type.extra.map((str) => str.length));
          djangoField += `\n\t${rField.fieldName} = models.${modelTyp}(${enumName}, max_length=${maxLength}${defaultValue})`;
          // Add import
          rExtraFields.addImport(`from ${this.config.packageBaseName ?? ""}.models.${enumName} import ${enumName}`);
          // return field
          return djangoField;
        }

        djangoField += `\n\t${rField.fieldName} = ${embedFieldInArrayFieldStart}models.${modelTyp}(${primaryNoFkey}`;
        // Close parentheses with length if needed
        djangoField += length ? `${length})` : ")";
        djangoField += `${embedFieldInArrayFieldEnd}`;
      }
    }
    return djangoField;
  }

  /**
   * Get ORM representation of many to one for this foreign key
   * @param rFKey : Generic Foreign key object
   * @param rExtraFields : extra fields
   * @param rField : field cause we need to know if this is a pkey
   * @returns
   */
  protected getOrmManyToOne(rFKey: GenericDbFKey, rExtraFields: ExtraField, rField: GenericDbField): string {
    let txt = "";
    if (rFKey?.referencedTable) {
      // cascade rules : django only have on_delete
      const ruleBehavior = rFKey.deleteRule === EnumDbRule.CASCADE ? "models.CASCADE" : "models.DO_NOTHING";

      let className = this.processClassNameFromTable(rFKey.referencedTable);
      const nullable = rField.isNullable ? "" : "blank=False, ";
      txt += `\n\t${className.toLowerCase()} = models.OneToOneField(${className}, ${nullable}db_column='${
        rFKey.columnName
      }', on_delete=${ruleBehavior}`;

      // if composite key and pkey and not yet written, had (primary_key=True)
      if (!this.pkey_already_written && rField.isPKey) {
        txt += ", primary_key=True";
        this.pkey_already_written = true;
      }
      txt += ")";
      // add import
      rExtraFields.addImport(`from ${this.config.packageBaseName ?? ""}.models.${className} import ${className}`);
    }
    return txt;
  }

  /**
   * Get ORM representation of many to many through pivot table for this foreign key
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rExtraField : extrafield to add imports/fields/getters/setters
   * @returns
   */
  protected getOrmManyToMany(rTable: GenericDbTable, rFKey: GenericDbFKey, rExtraField: ExtraField) {
    let txt = "";
    if (rFKey?.tableName) {
      let txt = "";
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(rFKey.tableName);
      let tableDistant = SqlTool.extractOtherTableName(rFKey.tableName, rTable.tableName, this.many_to_many_sep);
      let classNameDistant = this.processClassNameFromTable(tableDistant);
      // ManyToMany annotation
      const ruleBehavior = rFKey.deleteRule === EnumDbRule.CASCADE ? "models.CASCADE" : "models.DO_NOTHING";
      const memberDistant = classNameDistant.toLowerCase();
      // classrooms = models.ManyToManyField(Classroom, through='Classrooms_has_student')
      txt += `\n\t${memberDistant} = models.ManyToManyField(${classNameDistant}, through='${className}')`;
      // Add fields
      rExtraField.fieldsTxt += txt;
      // import model
      rExtraField.addImport(`from ${this.config.packageBaseName ?? ""}.models.${classNameDistant} import ${classNameDistant}`);
    }
    return txt;
  }

  /**
   * Create an enum file from this enum field
   * @param rField : field
   */
  private createEnumFileFromType(rField: GenericDbField) {
    if (rField.type?.extra && rField.type.extra.length > 0) {
      const enumName = this.getEnumNameFromField(rField);
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_django_model_enum_python", "utf8");
      // Get enum values text
      let enumValues = "\t";
      // Briwse array to create enum fields
      rField.type.extra.forEach((element) => {
        enumValues += `\n\t${element} = '${element}'`;
      });
      // Now we inject
      var dataTpl = this.supplant.text(tpl, {
        enumName: enumName,
        packageBaseName: this.config.packageBaseName,
        enumValues: enumValues,
      });
      // Write to files
      fs.writeFileSync(`${this.getOutProcessorPath()}${enumName}.py`, dataTpl, "utf8");
    }
  }
}
