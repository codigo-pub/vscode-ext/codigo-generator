/* eslint-disable @typescript-eslint/naming-convention */
import { GenericDbField } from "../../db/model/GenericDbField";
import { AbstractProcessor } from "../AbstractProcessor";
import { IConverter } from "../IConverter";
import { PythonConverter } from "./PythonConverter";

export abstract class AbstractPythonProcessor extends AbstractProcessor {
  protected readonly OPTIONAL_CHAR = "Optional[%s] = None";

  protected getConverter(): IConverter {
    return new PythonConverter();
  }

  /**
   * Get phpdoc param part '@param <type> <param name>
   * @param rFieldType : type of the field
   * @param rFieldName : name of the field
   */
  protected addParamDoc(rFieldType: string, rFieldName: string): string {
    return `\t\t :param (${rFieldType}) ${rFieldName}`;
  }

  /**
   * Get optional field char, represents an optional field
   * @param rField : field
   * @returns optional field char
   */
  protected getOptionalField(rField: GenericDbField): string {
    const fieldName = rField.fieldName ?? '';
    return rField?.isNullable ? this.OPTIONAL_CHAR.replace("%s", fieldName) : fieldName;
  }


  /**
   * Takes an import name and generates the import call
   * @param rImportName : import name, e.g. 'math'
   * @returns import call given that name (e.g. 'import math')
   */
  protected getImportFrom(rImportName: string): string {
    return rImportName ? `import ${rImportName}` : "";
  }
}
