import { EnumDbType } from "../db/model/EnumDbType";
import { GenericDbTypeField } from "../db/model/GenericDbTypeField";
import { SqlConvertEnumDbType } from "../model/SqlConvertEnumDbType";
import { IConverter } from "./IConverter";
import * as vscode from 'vscode';

export abstract class AbstractConverter implements IConverter {
  abstract readonly typeMappings: SqlConvertEnumDbType[];
  abstract fieldConvertToType(rField: GenericDbTypeField): SqlConvertEnumDbType | undefined;

  // Mapping from database sql to more generic sql
  // for example, int(11) will become int 
  readonly typeMappingsToSql: SqlConvertEnumDbType[] = [
    { sqlType: EnumDbType.char, targetType: "char" },
    { sqlType: EnumDbType.varchar, targetType: "varchar" },
    { sqlType: EnumDbType.uuid, targetType: "uuid" },
    { sqlType: EnumDbType.longtext, targetType: "longtext" },
    { sqlType: EnumDbType.enum, targetType: "varchar" },

    { sqlType: EnumDbType.long, targetType: "long" },
    { sqlType: EnumDbType.int, targetType: "int" },
    { sqlType: EnumDbType.float, targetType: "float" },
    { sqlType: EnumDbType.double, targetType: "double" },
    { sqlType: EnumDbType.numeric, targetType: "numeric" },

    { sqlType: EnumDbType.date, targetType: "date" },
    { sqlType: EnumDbType.datetime, targetType: "datetime" },
    { sqlType: EnumDbType.timestamp, targetType: "timestamp" },
    { sqlType: EnumDbType.time, targetType: "time" },
    { sqlType: EnumDbType.interval, targetType: "varchar" },

    { sqlType: EnumDbType.boolean, targetType: "boolean" },

    { sqlType: EnumDbType.blob, targetType: "blob" },
    { sqlType: EnumDbType.json, targetType: "json" },

    { sqlType: EnumDbType.array, targetType: "array" }
  ];

  /**
   * Convert type field from db to language type based on given mapping
   * This method will be used by converters 
   * @param rField : field with type 
   * @param rTypeMappings : mapping
   * @returns SqlConvertEnumDbType object or undefined if unknown
   */
  fieldConvertFromMapping(rField?: GenericDbTypeField, rTypeMappings?: SqlConvertEnumDbType[]): SqlConvertEnumDbType | undefined {
    if (rField?.type === undefined || rTypeMappings === undefined) {
      return undefined;
    }
    // Browse mappings, looking for the good one 
    const mapping = rTypeMappings.find((typMap) => typMap.sqlType === rField.type);
    if (mapping) {
      return mapping;
    }
    vscode.window.showWarningMessage(`unknown type detected for field : [${rField.type}]`);
    return undefined;
  }

  fieldConvertToSql(rField: GenericDbTypeField): string | undefined {
    const field = this.fieldConvertFromMapping(rField, this.typeMappingsToSql);
    return field ? field.targetType : undefined;
  }
}
