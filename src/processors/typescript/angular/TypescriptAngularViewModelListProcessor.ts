/* eslint-disable @typescript-eslint/naming-convention */
import path from "path";
import { StaticFile } from "../../../model/StaticFile";
import { AbstractTypeScriptAngularComponentProcessor, AngularComponentPromptAI } from "./AbstractTypeScriptAngularComponentProcessor";

export class TypescriptAngularViewModelListProcessor extends AbstractTypeScriptAngularComponentProcessor {

  // Static files to copy to dest folder cause files generated will need those file
  protected readonly STATIC_FILES: StaticFile[] = [
    { src: "angular_confirm_dialog_ts", dest: path.join("shared", "confirm-dialog", "confirm-dialog.component.ts") },
    { src: "angular_confirm_dialog_scss", dest: path.join("shared", "confirm-dialog", "confirm-dialog.component.scss") },
    { src: "angular_confirm_dialog_html", dest: path.join("shared", "confirm-dialog", "confirm-dialog.component.html") },
  ];
  
  getComponentSuffix(): string {
    return "List";
  }

  getProcessorName(): string {
    return "angular_view_model_list";
  }

  getTemplateNames() {
    return {
      ts: { name: "tpl_angular_view_model_list_ts", canUseAI: false}, // Note that we don't want AI for ts file cause it will mess all up
      spec: { name: "tpl_angular_view_model_list_spec_ts" },
      scss: { name: "tpl_angular_view_model_list_scss" },
      html: { name: "tpl_angular_view_model_list_html", canUseAI: true},
    };
  }

  /**
   * Method to define a custom prompt for AI if needed
   * This method can be override
   */
  protected getPromptForAI(): AngularComponentPromptAI {
    return {
      ts: undefined,
      spec: undefined,
      scss: undefined,
      html: "Given this angular component file and model related, Only give me the mat-table element that will replace the iaCompletion placeholder on this view : \n",
    };
  }

  /**
   * Get imports array, you can override this method in your processor
   * import must look like : "{ CommonModule } from '@angular/common'", don't write 'import' and no ';' at the end
   * @returns imports array
   */
  protected getStandaloneImportsArray(): string[] {
    return [
      "{ CommonModule } from '@angular/common'",
      "{ MatTableModule } from '@angular/material/table'",
      "{ MatPaginatorModule } from '@angular/material/paginator'",
      "{ MatButtonModule } from '@angular/material/button'",
      "{ MatIconModule } from '@angular/material/icon'",
      "{ MatFormFieldModule } from '@angular/material/form-field'", 
    ];
  }

  /**
   * Get imports array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getStandaloneComponentImportsArray(): string[] {
    return ["CommonModule", "MatTableModule", "MatPaginatorModule", "MatButtonModule", "MatIconModule", "MatFormFieldModule"];
  }

  /**
   * Get spec extra import array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getSpecExtraImportsArray(): string[] {
    return ["MatTableModule", "MatPaginatorModule", "MatButtonModule", "MatIconModule", "MatFormFieldModule"];
  }
  
}
