/* eslint-disable @typescript-eslint/naming-convention */
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { AbstractTypeScriptAngularComponentProcessor } from "./AbstractTypeScriptAngularComponentProcessor";
import { TypescriptAngularViewModelFormProcessor } from "./TypescriptAngularViewModelFormProcessor";
import { TypescriptAngularViewModelListProcessor } from "./TypescriptAngularViewModelListProcessor";

export class TypescriptAngularViewModelListPageProcessor extends AbstractTypeScriptAngularComponentProcessor {
  
  getComponentSuffix(): string {
    return "ListPage";
  }

  getProcessorName(): string {
    return "angular_view_model_list_page";
  }

  getTemplateNames() {
    return {
      ts: { name: "tpl_angular_view_model_list_page_ts" },
      spec: { name: "tpl_angular_view_model_list_page_spec_ts" },
      scss: { name: "tpl_angular_view_model_list_page_scss" },
      html: { name: "tpl_angular_view_model_list_page_html" },
    };
  }

  /**
   * add extra fields to model template
   * @param rModelTemplate : data to inject to template
   * @param rFields : fields
   */
  protected addExtraFieldsToModelTemplate(rModelTemplate: any, rFields?: GenericDbField[]): void {
    // We must add <app-list-<xxx>> and <app-form-... component to html page, so we add this to the model template
    const processorList = new TypescriptAngularViewModelListProcessor(this.config, this.configProcessor, this.db);
    const processorForm = new TypescriptAngularViewModelFormProcessor(this.config, this.configProcessor, this.db);
    // We know that model name is already in template, we need it
    const classNameList = processorList.getClassName(rModelTemplate.modelName);
    const classNameForm = processorForm.getClassName(rModelTemplate.modelName);
    // Now generate baseFileNames
    const baseFileNameList = GeneratorTool.camelToString({ rText: classNameList, rSep: "-", rLowerCase: true });
    const baseFileNameForm = GeneratorTool.camelToString({ rText: classNameForm, rSep: "-", rLowerCase: true });
    // Finally add property to model template
    rModelTemplate["baseFileNameList"] = `${baseFileNameList}`;
    rModelTemplate["baseFileNameForm"] = `${baseFileNameForm}`;
  }
}
