/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import path from "path";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { ExtraField } from "../../../model/ExtraField";
import { FileTool } from "../../../tools/FileTool";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { AbstractTypeScriptProcessor } from "../AbstractTypeScriptProcessor";
import { TypescriptModelProcessor } from "../TypescriptModelProcessor";
import { TypescriptAngularServiceProcessor } from "./TypescriptAngularServiceProcessor";
import { StaticFile } from "../../../model/StaticFile";


// Interface to define an angular component template file (name or content and if it can/may use AI)
export interface AngularComponentTemplateFileCfg {
  name: string; 
  canUseAI?: boolean,
}

// Interface to define structure of object storing template files
// We use this to store template files content too
export interface AngularComponentTemplateFiles {
  ts: AngularComponentTemplateFileCfg;
  spec: AngularComponentTemplateFileCfg;
  scss: AngularComponentTemplateFileCfg;
  html: AngularComponentTemplateFileCfg;
}
export interface AngularComponentPromptAI {
  ts?: string;
  spec?: string;
  scss?: string;
  html?: string;
}
export interface AngularComponentTemplateCodeIfNoAI {
  ts?: string;
  spec?: string;
  scss?: string;
  html?: string;
}

/**
 * Abstract base class for TypeScript Angular component processors.
 * This class provides common functionality for generating Angular components
 * (e.g., form, list) based on database tables and templates.
 */
export abstract class AbstractTypeScriptAngularComponentProcessor extends AbstractTypeScriptProcessor {
  
  // This processor can handle AI but this is truly 'AngularComponentTemplateFiles' data though getTemplateNames() method 
  // that will define use or not of AI for files. 
  // In general, we will use AI for html generation but not so much for ts files cause it's not always efficient
  protected canUseAI?: boolean | undefined = true;

  // All templates are expected in this sub-folder
  protected templateFolder?: string = "angular";

  /**
   * Processor for handling model-related logic.
   * This is initialized during the `process` method.
   */
  protected modelProcessor?: TypescriptModelProcessor;

  /**
   * Processor for handling service-related logic.
   * This is initialized during the `process` method.
   */
  protected serviceProcessor?: AbstractTypeScriptProcessor;

  /**
   * List of static files to copy, child classes may override this property if needed
   */
  protected readonly STATIC_FILES: StaticFile[] = [];

  /**
   * Abstract method to define the template file names.
   * Each subclass must provide the names of the templates it uses.
   *
   * @returns {object} An object containing the names of the template files (and other things):
   * - `ts`: TypeScript template file.
   * - `spec`: Spec (test) template file.
   * - `scss`: SCSS (style) template file.
   * - `html`: HTML template file.
   */
  abstract getTemplateNames(): AngularComponentTemplateFiles;

  /**
   * Abstract method to define component suffix name. Don't put Component at the end.
   * e.g. : Form is a good suffix name. For model User, component name will be UserForm
   * The Component suffix will be added in template
   */
  abstract getComponentSuffix(): string;

  /**
   * Default destination base folder
   * @returns folder name
   */
  getProcessorFolder(): string {
    return "views";
  }

  /**
   * Main entry point for the processor. Initializes model and service processors
   * and executes the default process logic.
   *
   * @returns {Promise<void>} A promise that resolves when the processing is complete.
   */
  async process(): Promise<void> {
    this.modelProcessor = new TypescriptModelProcessor(this.config, this.configProcessor, this.db);
    this.serviceProcessor = new TypescriptAngularServiceProcessor(this.config, this.configProcessor, this.db);
    await this.defaultProcess();

    // Copy static files
    this.copyStaticFiles(this.STATIC_FILES);
  }

  /**
   * Converts a model name into a component class name.
   * Method needs to be public cause we will eventually need it in other processors
   *
   * @param {string} modelName The name of the model.
   * @returns {string} The name of the component class.
   */
  getClassName(modelName: string): string {
    return `${modelName}${this.getComponentSuffix()}`;
  }

  /**
   * Processes a specific database table to generate component files.
   *
   * @param {GenericDbTable} rTable The database table to process.
   * @returns {Promise<void>} A promise that resolves when the table processing is complete.
   */
  protected async processTable(rTable: GenericDbTable): Promise<void> {
    const templates = this.loadTemplates();
    const modelName = this.processClassNameFromTable(rTable.tableName);
    const className = this.getClassName(modelName);
    const baseFileName = GeneratorTool.camelToString({ rText: className, rSep: "-", rLowerCase: true });
    const standalone = this.isStandalone();
    const modelTemplate = this.getModelTemplate(rTable, modelName, className, baseFileName, standalone);
    const customPromptsForAI = this.getPromptForAI();
    const templateCodeIfNoAI = this.getTemplateCodeIfNoAI();

    // Add extra fields to model template
    this.addExtraFieldsToModelTemplate(modelTemplate, rTable.fields);

    // Create subfolder linked to this model
    const baseFolder = `${this.getOutProcessorPath()}${modelName.toLowerCase()}`;
    FileTool.checkNewFolder(baseFolder);
    // Create subfolder linked to this component (angular works this way)
    const baseFolderComponent = `${this.getOutProcessorPath()}${modelName.toLowerCase()}${path.sep}${baseFileName}`;
    FileTool.checkNewFolder(baseFolderComponent);

    this.setCanUseAI(templates.spec.canUseAI ?? false);

    // Update template model with extra code when no AI if needed 
    if (!this.canUseAI) { modelTemplate['txtToAddIfNoIA'] = templateCodeIfNoAI.spec; }
    // Write spec file
    await this.writeTemplateToFile({
      rFilePath: `${baseFolderComponent}${path.sep}${baseFileName}.component.spec.ts`,
      rTemplate: templates.spec.name,
      rFieldsTemplate: modelTemplate,
      rFields: rTable?.fields,
      rCustomPrompt: customPromptsForAI.spec,
    });

    this.setCanUseAI(templates.scss.canUseAI ?? false);
    // Update template model with extra code when no AI if needed 
    if (!this.canUseAI) { modelTemplate['txtToAddIfNoIA'] = templateCodeIfNoAI.scss; }
    // Write scss file
    await this.writeTemplateToFile({
      rFilePath: `${baseFolderComponent}${path.sep}${baseFileName}.component.scss`,
      rTemplate: templates.scss.name,
      rFieldsTemplate: modelTemplate,
      rFields: rTable?.fields,
      rCustomPrompt: customPromptsForAI.scss,
    });

    this.setCanUseAI(templates.ts.canUseAI ?? false);
    // Update template model with extra code when no AI if needed 
    if (!this.canUseAI) { modelTemplate['txtToAddIfNoIA'] = templateCodeIfNoAI.ts; }
    // Write component file, possibly using AI, careful we need to await !
    await this.writeTemplateToFile({
      rFilePath: `${baseFolderComponent}${path.sep}${baseFileName}.component.ts`,
      rTemplate: templates.ts.name,
      rFieldsTemplate: modelTemplate,
      rFields: rTable?.fields,
      rCustomPrompt: customPromptsForAI.ts,
    });

    this.setCanUseAI(templates.html.canUseAI ?? false);
    // Update template model with extra code when no AI if needed 
    if (!this.canUseAI) { modelTemplate['txtToAddIfNoIA'] = templateCodeIfNoAI.html; }
    // Generate the HTML file, possibly using AI
    const componentFile = fs.readFileSync(`${baseFolderComponent}${path.sep}${baseFileName}.component.ts`, "utf-8");
    await this.writeTemplateToFile({
      rFilePath: `${baseFolderComponent}${path.sep}${baseFileName}.component.html`,
      rTemplate: templates.html.name,
      rFieldsTemplate: modelTemplate,
      rFields: rTable?.fields,
      rCustomPrompt: `${customPromptsForAI.html}: \n${componentFile}`,
    });
  }

  /**
   * Generates the template data for the component being processed.
   *
   * @param {GenericDbTable} rTable The database table being processed.
   * @param {string} modelName The name of the model.
   * @param {string} className The name of the component class.
   * @param {string} baseFileName The base file name for the component.
   * @param {boolean} standalone Whether the component is standalone.
   * @returns {object} The template data object.
   */
  protected getModelTemplate(rTable: GenericDbTable, modelName: string, className: string, baseFileName: string, standalone: boolean): any {
    const pkeyList = GeneratorTool.getPkeyFields(rTable.fields ?? []);
    return {
      serviceName: `${modelName}Service`,
      modelFolder: this.modelProcessor?.getProcessorFolder() ?? "",
      serviceFolder: this.serviceProcessor?.getProcessorFolder() ?? "",
      modelName: modelName,
      modelNameLower: modelName.toLowerCase(),
      className: className,
      baseFileName: baseFileName,
      standalone: standalone.toString(),
      standalone_imports: this.getImports(standalone),
      standalone_component_imports: this.getComponentImports(standalone),
      standalone_providers: this.getProvidersImports(standalone),
      spec_configuration_imports: this.getSpecConfigureImports(standalone, className),
      spec_configuration_declaration: this.getSpecConfigureDeclaration(standalone, className),
      fields: this.getFieldsTxt(rTable.fields),
      pkey_field: pkeyList && pkeyList.length === 1 ? this.getFieldName(pkeyList[0].fieldName ?? "") : undefined,
    };
  }

  /**
   * Convenient method to add extra fields to model template.
   * Feel free to override it
   */
  protected addExtraFieldsToModelTemplate(rModelTemplate: any, rFields?: GenericDbField[]): void {
    // Here we do nothing but childs can do by overriding
  }

  /**
   * Loads the templates for the processor.
   *
   * @returns {object} An object containing the loaded template strings:
   * - `ts`: TypeScript template.
   * - `spec`: Spec (test) template.
   * - `scss`: SCSS (style) template.
   * - `html`: HTML template.
   */
  private loadTemplates(): AngularComponentTemplateFiles {
    const templates = this.getTemplateNames();
    return {
      ts: {name: fs.readFileSync(this.getTemplateBasePath() + templates.ts.name, "utf8"), canUseAI: templates.ts.canUseAI },
      spec: {name: fs.readFileSync(this.getTemplateBasePath() + templates.spec.name, "utf8"), canUseAI: templates.spec.canUseAI },
      scss: {name: fs.readFileSync(this.getTemplateBasePath() + templates.scss.name, "utf8"), canUseAI: templates.scss.canUseAI },
      html: {name: fs.readFileSync(this.getTemplateBasePath() + templates.html.name, "utf8"), canUseAI: templates.html.canUseAI }
    };
  }

  /**
   * Generates the imports for the component whether it is standalone or not
   *
   * @param {boolean} rStandalone Whether the component is standalone.
   * @returns {string} The import statements.
   */
  protected getImports(rStandalone: boolean): string {
    if (rStandalone) {
      const imports = new ExtraField();
      // Get array of imports and generate iport text for template
      for (const importTxt of this.getStandaloneImportsArray()) {
        if (importTxt) {
          imports.addImport(this.getImportFrom(importTxt));
        }
      }
      return imports.getImportsTxt();
    }
    return "";
  }

  /**
   * Generates the imports for component module declaration whether it is standalone or not
   * @param rStandalone Whether the component is standalone.
   * @returns The import statements for component import module
   */
  protected getComponentImports(rStandalone: boolean): string {
    const standaloneImports = this.getStandaloneComponentImportsArray() ?? [];
    return rStandalone && standaloneImports.length > 0 ? standaloneImports.join(", ") : "";
  }

  /**
   * Generates the providers extra txt content to import libraries eventually
   * @param rStandalone Whether the component is standalone.
   * @returns The providers txt content to add
   */
  protected getProvidersImports(rStandalone: boolean): string {
    const providersArray = rStandalone ? this.getStandaloneProvidersImportsArray() : [];
    return providersArray && providersArray.length > 0 ? providersArray.join(", ") : "";
  }

  /**
   * Generates the configuration imports for a spec file based on standalone value.
   *
   * @param {boolean} rStandalone Whether the component is standalone.
   * @param {string} rClassName The class name of the component.
   * @returns {string} The spec configuration imports.
   */
  protected getSpecConfigureImports(rStandalone: boolean, rClassName: string): string {
    let baseImports = this.getSpecExtraImportsArray() ?? [];
    // If standalone, add component
    if (rStandalone) {
      baseImports.push(`${rClassName}Component`);
    }
    return baseImports.join(", ");
  }

  /**
   * Generates the declaration for a spec file based on standalone value.
   *
   * @param {boolean} rStandalone Whether the component is standalone.
   * @param {string} rClassName The class name of the component.
   * @returns {string} The spec configuration declaration.
   */
  protected getSpecConfigureDeclaration(rStandalone: boolean, rClassName: string): string {
    return rStandalone ? "" : `${rClassName}Component`;
  }

  /**
   * Generates a string of field names for use in templates.
   *
   * @param {GenericDbField[]} rFields The list of database fields.
   * @returns {string} A comma-separated string of field names.
   */
  protected getFieldsTxt(rFields?: GenericDbField[]): string {
    if (rFields) {
      return rFields.map((field) => `'${field.fieldName}'`).join(", ");
    }
    return "";
  }

  /**
   * Method to define a custom prompt for AI if needed
   * This method can be override
   */
  protected getPromptForAI(): AngularComponentPromptAI {
    return {
      ts: undefined,
      spec: undefined,
      scss: undefined,
      html: "Given this angular component file and model related, give me the code for the html view : \n",
    };
  }

  /**
   * Get imports array, you can override this method in your processor
   * import must look like : "{ CommonModule } from '@angular/common'", don't write 'import' and no ';' at the end
   * @returns imports array
   */
  protected getStandaloneImportsArray(): string[] {
    return ["{ CommonModule } from '@angular/common'"];
  }

  /**
   * Get imports array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getStandaloneComponentImportsArray(): string[] {
    return ["CommonModule"];
  }

  /**
   * Get the providers extra txt content array
   * You can override this method in your processor
   * @returns import array for providers
   */
  protected getStandaloneProvidersImportsArray(): string[] {
    return [];
  }

  /**
   * Get spec extra import array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getSpecExtraImportsArray(): string[] {
    return [];
  }

  
  /**
   * Some processors may need to add some code to template if AI is not activated to ensure code is correct
   */
  protected getTemplateCodeIfNoAI(): AngularComponentTemplateCodeIfNoAI {
    return {};
  }

}
