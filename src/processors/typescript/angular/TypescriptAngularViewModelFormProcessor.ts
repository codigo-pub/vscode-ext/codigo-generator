/* eslint-disable @typescript-eslint/naming-convention */
import path from "path";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { StaticFile } from "../../../model/StaticFile";
import { AbstractTypeScriptAngularComponentProcessor, AngularComponentTemplateCodeIfNoAI, AngularComponentPromptAI } from "./AbstractTypeScriptAngularComponentProcessor";

export class TypescriptAngularViewModelFormProcessor extends AbstractTypeScriptAngularComponentProcessor {

  // Static files to copy to dest folder cause files generated will need those file
  protected readonly STATIC_FILES: StaticFile[] = [
    { src: "angular_generic_model_selector_ts", dest: path.join("shared", "generic-model-selector", "generic-model-selector.component.ts") },
    { src: "angular_generic_model_selector_scss", dest: path.join("shared", "generic-model-selector", "generic-model-selector.component.scss") },
    { src: "angular_generic_model_selector_html", dest: path.join("shared", "generic-model-selector", "generic-model-selector.component.html") },
  ];

  getComponentSuffix(): string {
    return "Form";
  }

  getProcessorName(): string {
    return "angular_view_model_form";
  }

  getTemplateNames() {
    return {
      ts: { name: "tpl_angular_view_model_form_ts", canUseAI: true},
      spec: { name: "tpl_angular_view_model_form_spec_ts" },
      scss: { name: "tpl_angular_view_model_form_scss" },
      html: { name: "tpl_angular_view_model_form_html", canUseAI: true},
    };    
  }

  /**
   * Method to define a custom prompt for AI if needed
   * This method can be override
   */
  protected getPromptForAI(): AngularComponentPromptAI {
    return {
      ts: "just implement validateFormBeforeSubmit method",
      spec: undefined,
      scss: undefined,
      html: "Given this angular component file and model related, give me the code for the html view. Don't use mat-dialog-content. Buttons with colors and icons and set close button type to avoid auto-submit : \n",
    };
  }

  /**
   * Get imports array, you can override this method in your processor
   * import must look like : "{ CommonModule } from '@angular/common'", don't write 'import' and no ';' at the end
   * @returns imports array
   */
  protected getStandaloneImportsArray(): string[] {
    return [
      "{ CommonModule } from '@angular/common'", 
      "{ ReactiveFormsModule } from '@angular/forms'", 
      "{ MatDialogModule } from '@angular/material/dialog'",
      "{ MatFormFieldModule } from '@angular/material/form-field'", 
      "{ MatInputModule } from '@angular/material/input'",
      "{ MatButtonModule } from '@angular/material/button'",
      "{ MatIconModule } from '@angular/material/icon'",
    ];
  }

  /**
   * Get imports array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getStandaloneComponentImportsArray(): string[] {
    return ["CommonModule", "ReactiveFormsModule", "MatDialogModule", "MatFormFieldModule", "MatInputModule", "MatButtonModule", "MatIconModule"];
  }

  /**
   * Get spec extra import array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getSpecExtraImportsArray(): string[] {
    return ["ReactiveFormsModule", "MatDialogModule", "MatFormFieldModule", "MatInputModule", "MatButtonModule", "MatIconModule"];
  }

  /**
   * add form group fields to model template
   * @param rModelTemplate : data to inject to template
   * @param rFields : fields
   */
  protected addExtraFieldsToModelTemplate(rModelTemplate: any, rFields?: GenericDbField[]): void {
    rModelTemplate["form_group_fields"] = this.getFormGroupFieldsTxt(rFields);
  }

  protected getTemplateCodeIfNoAI(): AngularComponentTemplateCodeIfNoAI {
    return {
      ts: `
  /**
   * Check if form is valid before submission.
   * Alerts and prevents submission if there are validation issues.
   * 
   * TODO : complete this method
   * 
   * @returns True if the form is valid, otherwise false.
   */
  validateFormBeforeSubmit(): boolean {
    return true; 
  }
    `
    };
  } 

  /**
   * Get fields text to fill FormGroup init
   * @param rFields : array of fields
   */
  private getFormGroupFieldsTxt(rFields?: GenericDbField[]): string {
    let fieldsToInject = "";
    if (rFields) {
      // Loop through fields to generate text
      for (const fielddata of rFields) {
        if (fielddata.fieldName !== undefined && fielddata.fieldName !== "" && !this.PROTECTED_FIELDS.includes(fielddata.fieldName)) {
          // Get name of field
          let fieldName = this.getFieldName(fielddata.fieldName);
          if (fieldName !== undefined && fieldName !== "") {
            // every pkey field or fkey field is set to null
            var init_value = fielddata.isPKey || fielddata.isFKey ? "null" : "''";
            fieldsToInject += `\n\t\t\t${fieldName}: [${init_value}],`;
          }
        }
      }
    }
    return fieldsToInject;
  }
  
}
