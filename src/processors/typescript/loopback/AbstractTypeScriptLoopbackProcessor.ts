import path from "path";
import { AbstractTypeScriptProcessor } from "../AbstractTypeScriptProcessor";
import fs from "fs";

/* eslint-disable @typescript-eslint/naming-convention */
export abstract class AbstractTypeScriptLoopbackProcessor extends AbstractTypeScriptProcessor {
  // All templates are expected in this sub-folder
  protected templateFolder?: string = "loopback";

  /**
   * Loopback files don't have "_", we must replace them with "-"
   * @param rFilename : filename
   * @returns filename converted to suit loopback requirements
   */
  convertFilenameForLoopback(rFilename: string): string {
    return rFilename ? rFilename.replace(/_/g, "-") : rFilename;
  }

  /**
   * Read 'index.ts' file from this base path and check if there is a line like "export * from './<rFileNameToTimport>.controller';"
   * If there is one, do nothing, otherwise add the import at the enf od the file
   * 
   * If file index.ts is not present, do nothing cause it is probably voluntary. User will have to update the file manually.
   * 
   * @param rBasePathForIndexFile : base path for index.ts file
   * @param rFileNameToTimport : filename to import
   */
  updateIndexImport(rBasePathForIndexFile: string, rFileNameToTimport: string): void {
    const indexFilePath = path.join(rBasePathForIndexFile, "index.ts");
    const importStatement = `export * from './${rFileNameToTimport}';`;

    // If file doesn't exist, it may be the first time, so we create an empty file
    if (!fs.existsSync(indexFilePath)) {
      fs.writeFileSync(indexFilePath, '');
    }

    // Check afterwards, file should exist now
    if (fs.existsSync(indexFilePath)) {
      // Read the content of the file
      const fileContent = fs.readFileSync(indexFilePath, "utf-8");

      // Check if the import statement already exists
      if (!fileContent.includes(importStatement)) {
        // Append the import statement at the end of the file
        const updatedContent = `${fileContent.trim()}\n${importStatement}\n`;
        fs.writeFileSync(indexFilePath, updatedContent, "utf-8");
      }
    }
  }
}
