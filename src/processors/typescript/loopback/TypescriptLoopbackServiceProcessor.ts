/* eslint-disable @typescript-eslint/naming-convention */
import fs from "fs";
import * as vscode from "vscode";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { AbstractTypeScriptLoopbackProcessor } from "./AbstractTypeScriptLoopbackProcessor";
import { TypescriptLoopbackModelProcessor } from "./TypescriptLoopbackModelProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { TypescriptLoopbackRepositoryProcessor } from "./TypescriptLoopbackRepositoryProcessor";

export class TypescriptLoopbackServiceProcessor extends AbstractTypeScriptLoopbackProcessor {
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = true;

  // We need these processors to get folders
  private modelProcessor?: TypescriptLoopbackModelProcessor;
  private repositoryProcessor?: TypescriptLoopbackRepositoryProcessor;

  getProcessorFolder(): string {
    return "services";
  }

  getProcessorName(): string {
    return "loopback_service";
  }

  async process(): Promise<void> {
    this.modelProcessor = new TypescriptLoopbackModelProcessor(this.config, this.configProcessor, this.db);
    this.repositoryProcessor = new TypescriptLoopbackRepositoryProcessor(this.config, this.configProcessor, this.db);
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.modelProcessor && this.repositoryProcessor) {
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // If composite primary key
        if (pKeys && pKeys.length > 1) {
          this.processTableWithCompositePKey(rTable);
        } else {
          this.processTableWithSinglePkey(rTable);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0 && this.modelProcessor && this.repositoryProcessor) {
      // Load templates
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_loopback_service_ts", "utf8");

      // Class model name
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classModelNameLower = classModelName.toLowerCase();
      // Class name
      var className = `${classModelName}Service`;
      var pkeyType = "";
      var pkeyFieldName = "";

      // Pkeys (for now we only can handle a single pkey)
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      // If not composite primary key
      if (pKeys && pKeys.length === 1) {
        const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
        pkeyType = myPkeyType?.targetType ?? "";
        pkeyFieldName = pKeys[0].fieldName ?? "id";
      }

      const modelTemplate = {
        className: className,
        classModelName: classModelName,
        classModelNameLower: GeneratorTool.lowerFirstLetter(classModelNameLower),
        classModelNameRepositoryLower: `${GeneratorTool.lowerFirstLetter(classModelName)}Repository`,
        modelFolder: this.modelProcessor.getProcessorFolder(),
        repositoryFolder: this.repositoryProcessor.getProcessorFolder(),
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        pkeyType: pkeyType,
        pkeyFieldName: pkeyFieldName,
      };

      // Careful, for loopback file names, there is no "_" we must put some "-" instead
      const fileNameForLoopback = this.convertFilenameForLoopback(rTable.tableName.toLowerCase());

      // Complete index.ts file if needed
      this.updateIndexImport(this.getOutProcessorPath(), `${fileNameForLoopback}.service`);

      await this.writeTemplateToFile({
        rFilePath: `${this.getOutProcessorPath()}${fileNameForLoopback}.service.ts`,
        rTemplate: tpl,
        rFieldsTemplate: modelTemplate,
        rFields: rTable?.fields,
      });
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   */
    protected async processTableWithCompositePKey(rTable: GenericDbTable): Promise<void> {
      if (rTable.tableName) {
        // Warning loopback 4 doesn't handle composite keys
        vscode.window.showWarningMessage(`${rTable.tableName} skipped for Service cause Loopback doesn't handle composite keys :(`);
      }
    }

}
