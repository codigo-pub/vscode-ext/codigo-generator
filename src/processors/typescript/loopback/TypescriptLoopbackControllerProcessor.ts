/* eslint-disable @typescript-eslint/naming-convention */
import fs from "fs";
import * as vscode from "vscode";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { AbstractTypeScriptLoopbackProcessor } from "./AbstractTypeScriptLoopbackProcessor";
import { TypescriptLoopbackModelProcessor } from "./TypescriptLoopbackModelProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { TypescriptLoopbackServiceProcessor } from "./TypescriptLoopbackServiceProcessor";
import { GenericDbTypeField } from "../../../db/model/GenericDbTypeField";
import { EnumDbType } from "../../../db/model/EnumDbType";
import path from "path";

export class TypescriptLoopbackControllerProcessor extends AbstractTypeScriptLoopbackProcessor {
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = true;

  // We need these processors to get folders
  private modelProcessor?: TypescriptLoopbackModelProcessor;
  private serviceProcessor?: TypescriptLoopbackServiceProcessor;

  getProcessorFolder(): string {
    return "controllers";
  }

  getProcessorName(): string {
    return "loopback_controller";
  }

  async process(): Promise<void> {
    this.modelProcessor = new TypescriptLoopbackModelProcessor(this.config, this.configProcessor, this.db);
    this.serviceProcessor = new TypescriptLoopbackServiceProcessor(this.config, this.configProcessor, this.db);
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.modelProcessor && this.serviceProcessor) {
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // If composite primary key
        if (pKeys && pKeys.length > 1) {
          this.processTableWithCompositePKey(rTable);
        } else {
          this.processTableWithSinglePkey(rTable);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0 && this.modelProcessor && this.serviceProcessor) {
      // Load templates
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_loopback_controller_ts", "utf8");

      // Class model name
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classModelNameLower = classModelName.toLowerCase();
      // Class name
      var className = `${classModelName}Controller`;
      var pkeyType = "";
      var pkeyFieldName = "";

      // Pkeys (for now we only can handle a single pkey)
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      // If not composite primary key
      if (pKeys && pKeys.length === 1) {
        const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
        pkeyType = myPkeyType?.targetType ?? "";
        pkeyFieldName = pKeys[0].fieldName ?? "id";
      }

      const modelTemplate = {
        className: className,
        classModelName: classModelName,
        classModelNameLower: GeneratorTool.lowerFirstLetter(classModelNameLower),
        classModelNameServiceLower: `${GeneratorTool.lowerFirstLetter(classModelName)}Service`,
        modelFolder: this.modelProcessor.getProcessorFolder(),
        serviceFolder: this.serviceProcessor.getProcessorFolder(),
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        pkeyType: pkeyType,
        pkeyLoopbackType: pKeys && pKeys.length === 1 ? this.getLoopbackPathTypeFromType(pKeys[0].type) : undefined,
        pkeyFieldName: pkeyFieldName,
        // <= for url path parameter, we can't add {} in template cause Loopback don't allow spaces.
        // e.g. we can't do this : @get('/{{modelBaseApiUrl}}/{ {{pkeyFieldName}} }')
        pkeyFieldNameParam: `{${pkeyFieldName}}`,
      };

      // Careful, for loopback file names, there is no "_" we must put some "-" instead
      const fileNameForLoopback = this.convertFilenameForLoopback(rTable.tableName.toLowerCase());

      // Complete index.ts file if needed
      this.updateIndexImport(this.getOutProcessorPath(), `${fileNameForLoopback}.controller`);

      // Create subfolder linked to this component (angular works this way)
      const serviceFilePath = `${this.serviceProcessor.getOutProcessorPath()}${path.sep}${fileNameForLoopback}.service.ts`;
      // Try to read service file
      const serviceFile = fs.existsSync(serviceFilePath) ? fs.readFileSync(serviceFilePath, "utf-8") : undefined;
      // Custom prompt
      const promptAI = serviceFile ? 
        `Given this loopback service file and model related, give me only the new methods for the controller cause he already have all default crud methods : \n${serviceFile}`
        : undefined;

      await this.writeTemplateToFile({
        rFilePath: `${this.getOutProcessorPath()}${fileNameForLoopback}.controller.ts`,
        rTemplate: tpl,
        rFieldsTemplate: modelTemplate,
        rFields: rTable?.fields,
        rCustomPrompt: promptAI,
      });
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   */
  protected async processTableWithCompositePKey(rTable: GenericDbTable): Promise<void> {
    if (rTable.tableName) {
      // Warning loopback 4 doesn't handle composite keys
      vscode.window.showWarningMessage(`${rTable.tableName} skipped for Controller cause Loopback doesn't handle composite keys :(`);
    }
  }

  /**
   * Convert a type of data for loopback '@param.path.<type>' annotation
   * @param rType : Type of data
   * @returns correct type for loopback annotation
   */
  private getLoopbackPathTypeFromType(rType?: GenericDbTypeField): string | undefined {
    let typTxt = undefined;
    if (rType) {
      switch (rType.type) {
        case EnumDbType.long:
        case EnumDbType.int:
        case EnumDbType.numeric:
        case EnumDbType.timestamp:
        case EnumDbType.interval:
          typTxt = "number";
          break;
        case EnumDbType.float:
          typTxt = "float";
          break;
        case EnumDbType.double:
          typTxt = "double";
          break;
        case EnumDbType.date:
          typTxt = "date";
          break;
        case EnumDbType.datetime:
          typTxt = "dateTime";
          break;
        case EnumDbType.boolean:
          typTxt = "boolean";
          break;
        case EnumDbType.blob:
          typTxt = "binary";
          break;
        default:
          typTxt = "string";
          break;
      }
    }
    return typTxt;
  }
}
