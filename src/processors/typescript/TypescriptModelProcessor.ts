/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractTypeScriptProcessor } from "./AbstractTypeScriptProcessor";
import { GenericDbTable } from "../../db/model/GenericDbTable";

export class TypescriptModelProcessor extends AbstractTypeScriptProcessor {
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "models";
  }

  getProcessorName(): string {
    return "model";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    var _this = this;

    if (rTable?.fields && rTable.fields.length > 0) {
      const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_model_ts", "utf8");
      var tpl_spec = fs.readFileSync(this.getTemplateBasePath() + "tpl_model_spec_ts", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      // FileName
      var fileName = className.toLowerCase();

      var fieldsToInject = "";
      var constructorToInject = "";
      var constructorInToInject = "";
      var specIn = ""; // fake data for spec file object constructor
      for (const fielddata of rTable.fields) {
        if (fielddata.fieldName !== undefined && fielddata.fieldName !== "") {
          // Get name of field
          let fieldName = this.getFieldName(fielddata.fieldName);
          var fieldType = _this.converter.fieldConvertToType(fielddata.type);
          // var nullable = this.getOptionalChar(fielddata);
          // to ease things, every field that is not pkey can be undefined
          var nullable = fielddata.isPKey ? "" : this.OPTIONAL_CHAR;
          fieldsToInject += `\n\t${fieldName}${nullable}: ${fieldType?.targetType};`;
          // if primary key, we set constructor fields
          if (fielddata.isPKey) {
            // if one more to constructor
            const oneMoreToInject = constructorToInject !== "" ? ", " : "\tconstructor(";
            const oneMoreInToInject = constructorInToInject !== "" ? "\n" : "";
            constructorToInject += `${oneMoreToInject}${paramPrefix}${fieldName}: ${fieldType?.targetType}`;
            constructorInToInject += `${oneMoreInToInject}\t\tthis.${fieldName} = ${paramPrefix}${fieldName};`;
            // For spec file
            if (specIn !== "") {
              specIn += ", ";
            }
            specIn += fieldType?.targetType === "string" ? "'xxx'" : "1";
          }
        }
      }

      // If there is a need for constructor
      if (constructorToInject !== "") {
        constructorToInject += ") {";
        constructorToInject += "\n" + constructorInToInject;
        constructorToInject += "\n\t}";
      }

      // Regular file
      var modelTemplate = {
        className: className,
        filename: fileName,
        fields: fieldsToInject,
        constructor: constructorToInject,
      };
      await this.writeTemplateToFile({
        rFilePath: `${this.getOutProcessorPath()}${fileName}.ts`,
        rTemplate: tpl,
        rFieldsTemplate: modelTemplate,
        rFields: rTable?.fields,
      });

      // Spec file
      var modelTemplateSpec = { className: className, filename: fileName, specIn: specIn };
      await this.writeTemplateToFile({
        rFilePath: `${this.getOutProcessorPath()}${fileName}.spec.ts`,
        rTemplate: tpl_spec,
        rFieldsTemplate: modelTemplateSpec,
      });
    }
  }
}
