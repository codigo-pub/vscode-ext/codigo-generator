import { EnumDbType } from "../../db/model/EnumDbType";
import { GenericDbTypeField } from "../../db/model/GenericDbTypeField";
import { SqlConvertEnumDbType } from "../../model/SqlConvertEnumDbType";
import { AbstractConverter } from "../AbstractConverter";


export class TypeScriptConverter extends AbstractConverter {
  readonly typeMappings: SqlConvertEnumDbType[] = [
    { sqlType: EnumDbType.char, targetType: "string" },
    { sqlType: EnumDbType.varchar, targetType: "string" },
    { sqlType: EnumDbType.uuid, targetType: "string" },
    { sqlType: EnumDbType.longtext, targetType: "string" },
    { sqlType: EnumDbType.enum, targetType: "string" },

    { sqlType: EnumDbType.long, targetType: "number" },
    { sqlType: EnumDbType.int, targetType: "number" },
    { sqlType: EnumDbType.numeric, targetType: "number" },
    { sqlType: EnumDbType.float, targetType: "number" },
    { sqlType: EnumDbType.double, targetType: "number" },

    { sqlType: EnumDbType.date, targetType: "string" },
    { sqlType: EnumDbType.datetime, targetType: "string" },
    { sqlType: EnumDbType.timestamp, targetType: "string" },
    { sqlType: EnumDbType.time, targetType: "string" },

    { sqlType: EnumDbType.interval, targetType: "string" }, // No direct equivalent, can be represented as string

    { sqlType: EnumDbType.boolean, targetType: "boolean" },

    { sqlType: EnumDbType.blob, targetType: "Buffer" },

    { sqlType: EnumDbType.json, targetType: "any" },

    { sqlType: EnumDbType.array, targetType: "[]" },
  ];

  readonly typeMappingsToSql: SqlConvertEnumDbType[] = [
    { sqlType: EnumDbType.char, targetType: "char" },
    { sqlType: EnumDbType.varchar, targetType: "varchar" },
    { sqlType: EnumDbType.uuid, targetType: "uuid" },
    { sqlType: EnumDbType.longtext, targetType: "longtext" }, //<= different from the one in AbstractConverter
    { sqlType: EnumDbType.enum, targetType: "varchar" },

    { sqlType: EnumDbType.long, targetType: "bigint" },
    { sqlType: EnumDbType.int, targetType: "int" },
    { sqlType: EnumDbType.float, targetType: "float" },
    { sqlType: EnumDbType.double, targetType: "double" },
    { sqlType: EnumDbType.numeric, targetType: "numeric" },

    { sqlType: EnumDbType.date, targetType: "date" },
    { sqlType: EnumDbType.datetime, targetType: "datetime" },
    { sqlType: EnumDbType.timestamp, targetType: "timestamp" },
    { sqlType: EnumDbType.time, targetType: "time" },
    { sqlType: EnumDbType.interval, targetType: "varchar" },

    { sqlType: EnumDbType.boolean, targetType: "boolean" },

    { sqlType: EnumDbType.blob, targetType: "blob" },
    { sqlType: EnumDbType.json, targetType: "json" },

    { sqlType: EnumDbType.array, targetType: "array" }    
  ];

  /**
   * Translate sql type to TypeScript type
   * @param {JSON} rField : Field
   * @returns typescript type
   */
  fieldConvertToType(rField?: GenericDbTypeField): SqlConvertEnumDbType | undefined {
    return this.fieldConvertFromMapping(rField, this.typeMappings);
  }
}
