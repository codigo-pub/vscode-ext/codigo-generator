/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractTypeScriptProcessor } from "./AbstractTypeScriptProcessor";
import * as vscode from "vscode";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { GeneratorTool } from "../../tools/GeneratorTool";

export class TypescriptRowmapperProcessor extends AbstractTypeScriptProcessor {
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  readonly PREFIX_TBL_PROP: string = "_tbl_";
  readonly SUFFIX_CLASS: string = "RowMapper";

  getProcessorFolder(): string {
    return "rowmapper";
  }

  getProcessorName(): string {
    return "row_mapper";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    // if there is no foreign key, so nothing to do
    if (!rTable.fKeys || rTable?.fKeys.length === 0) {
      vscode.window.showInformationMessage(`No need to create rowmapper for table ${rTable.tableName}. It has no relations.`);
    } else {
      const dataConfig = await this.processTableToRowMapper(rTable, false);
      await this.createRowMapper(rTable, dataConfig);
    }
  }

  private async processTableToRowMapper(rTable: GenericDbTable, rDeep: boolean = false): Promise<any> {
    const _this = this;
    const tblcontent: any[] = [];
    if (rTable?.fields) {
      for (const fielddata of rTable.fields) {
        if (fielddata.fieldName !== undefined && fielddata.fieldName !== "") {
          let fieldName = this.getFieldName(fielddata.fieldName);
          // if it's a foreign key and not a primary key
          if (rTable.fKeys && rTable.fKeys.length > 0 && (!fielddata.isPKey || fielddata.isFKey)) {
            //search for referenced table, must be there if it's a foreign key
            const fKey = GeneratorTool.getFKeyFromField(fielddata, rTable.fKeys);
            if (fKey?.referencedTable) {
              const jsonData: any = {};
              // Get table description
              const childTable = await this.db.getTableDescription(fKey.referencedTable, this.db?.getSchema());
              // Processing child table to get her fields
              jsonData[this.PREFIX_TBL_PROP + fKey.referencedTable] = await _this.processTableToRowMapper(childTable, true);
              tblcontent.push(jsonData);
            }
          } else {
            if (rDeep) {
              tblcontent.push(fieldName);
            }
          }
        }
      }
    }
    return tblcontent;
  }

  /**
   * Create rowmapper file based on data received
   * @param rTable : table
   */
  private async createRowMapper(rTable: GenericDbTable, rDataConfig: any): Promise<void> {
    const tpl: string = fs.readFileSync(this.getTemplateBasePath() + "/tpl_rowmapper_ts", "utf8");

    // ClassName
    var className = this.processClassNameFromTable(rTable.tableName) + this.SUFFIX_CLASS;

    // FileName
    const fileName: string = className.toLowerCase();

    let dataInject: any = {};

    for (const value in rTable.fKeys) {
      dataInject = Object.assign(dataInject, rDataConfig[value]);
    }

    const modelTemplate = {
      className: className,
      configcontent: JSON.stringify(dataInject),
    };

    await this.writeTemplateToFile({
      rFilePath: `${this.getOutProcessorPath()}${fileName}.ts`,
      rTemplate: tpl,
      rFieldsTemplate: modelTemplate,
      rFields: rTable?.fields,
    });
  }
}
