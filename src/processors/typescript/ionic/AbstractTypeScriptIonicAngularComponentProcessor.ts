/* eslint-disable @typescript-eslint/naming-convention */
import { TypescriptModelProcessor } from "../TypescriptModelProcessor";
import { AbstractTypeScriptAngularComponentProcessor, AngularComponentPromptAI } from "../angular/AbstractTypeScriptAngularComponentProcessor";
import { TypescriptIonicAngularServiceProcessor } from "./TypescriptIonicAngularServiceProcessor";

/**
 * Abstract base class for TypeScript Ionic Angular component processors.
 * This class provides common functionality for generating Angular components
 * (e.g., form, list) based on database tables and templates.
 */
export abstract class AbstractTypeScriptIonicAngularComponentProcessor extends AbstractTypeScriptAngularComponentProcessor {

  // All templates are expected in this sub-folder
  protected templateFolder?: string = "ionic";


  /**
   * Main entry point for the processor. Initializes model and service processors
   * and executes the default process logic.
   *
   * @returns {Promise<void>} A promise that resolves when the processing is complete.
   */
  async process(): Promise<void> {
    this.modelProcessor = new TypescriptModelProcessor(this.config, this.configProcessor, this.db);
    this.serviceProcessor = new TypescriptIonicAngularServiceProcessor(this.config, this.configProcessor, this.db);
    await this.defaultProcess();

    // Copy static files
    this.copyStaticFiles(this.STATIC_FILES);
  }

  /**
   * Method to define a custom prompt for AI if needed
   * This method can be override
   */
  protected getPromptForAI(): AngularComponentPromptAI {
    return {
      ts: undefined,
      spec: undefined,
      scss: undefined,
      html: "Given this ionic angular component file and model related, give me the code for the html view : \n",
    };
  }

}
