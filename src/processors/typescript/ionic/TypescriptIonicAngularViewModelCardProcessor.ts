import { AngularComponentPromptAI } from "../angular/AbstractTypeScriptAngularComponentProcessor";
import { AbstractTypeScriptIonicAngularComponentProcessor } from "./AbstractTypeScriptIonicAngularComponentProcessor";

export class TypescriptIonicAngularViewModelCardProcessor extends AbstractTypeScriptIonicAngularComponentProcessor {

  getComponentSuffix(): string {
    return "Card";
  }

  getProcessorName(): string {
    return "ionic_angular_view_model_card";
  }

  getTemplateNames() {
    return {
      ts: { name: "tpl_ionic_angular_view_model_card_ts", canUseAI: true},
      spec: { name: "tpl_ionic_angular_view_model_card_spec_ts" },
      scss: { name: "tpl_ionic_angular_view_model_card_scss" },
      html: { name: "tpl_ionic_angular_view_model_card_html", canUseAI: true},
    };      
  }

  /**
   * Get imports array, you can override this method in your processor
   * import must look like : "{ CommonModule } from '@angular/common'", don't write 'import' and no ';' at the end
   * @returns imports array
   */
  protected getStandaloneImportsArray(): string[] {
    return ["{ CommonModule } from '@angular/common'", "{ IonicModule } from '@ionic/angular'"];
  }

  /**
   * Get imports array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getStandaloneComponentImportsArray(): string[] {
    return ["CommonModule", "IonicModule"];
  }

  /**
   * Get spec extra import array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getSpecExtraImportsArray(): string[] {
    return ["IonicModule"];
  }

  /**
   * Method to define a custom prompt for AI if needed
   * This method can be override
   */
  protected getPromptForAI(): AngularComponentPromptAI {
    return {
      ts: undefined,
      spec: undefined,
      scss: undefined,
      html: "Given this ionic angular component file and model related, give me the code for the html view with a ion-card element : \n",
    };
  }

}
