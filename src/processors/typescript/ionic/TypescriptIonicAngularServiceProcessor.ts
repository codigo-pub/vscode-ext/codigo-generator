/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractTypeScriptProcessor } from "../AbstractTypeScriptProcessor";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { StaticFile } from "../../../model/StaticFile";
import { TypescriptModelProcessor } from "../TypescriptModelProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { GenericDbField } from "../../../db/model/GenericDbField";


export class TypescriptIonicAngularServiceProcessor extends AbstractTypeScriptProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = true;

  // All templates are expected in this sub-folder
  protected templateFolder?: string = "ionic";
  
  // Only used to have his folder
  private modelProcessor?: TypescriptModelProcessor;

  // Static files to copy to dest folder cause files generated will need those file
  private readonly STATIC_FILES: StaticFile[] = [
    { src: "ionic_angular_abstractapi_service_ts", dest: "abstract_api.service.ts",}
  ];

  getProcessorFolder(): string {
    return "services";
  }

  getProcessorName(): string {
    return "ionic_angular_service";
  }

  async process(): Promise<void> {
    this.modelProcessor = new TypescriptModelProcessor(this.config, this.configProcessor, this.db);
    // Runs default process
    await this.defaultProcess();
    // Copy static files, we do this after defaultProcess cause he calls initFolders() and we are sure folder exist at this time 
    this.copyStaticFiles(this.STATIC_FILES);
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    // Load templates
    var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_ionic_angular_service_ts", 'utf8');
    var tpl_spec = fs.readFileSync(this.getTemplateBasePath() + "tpl_ionic_angular_service_spec_ts", 'utf8');

    // Classname
    const className: string = this.processClassNameFromTable(rTable.tableName);
    // FileName
    var fileName = className.toLowerCase()+'.service';

    // Template config
    const modelTemplate = {
      serviceName: `${className}Service`,
      modelFolder: this.modelProcessor?.getProcessorFolder() ?? '',
      className: className,
      classNameLower: className.toLowerCase(),
      modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
      pkey_fields: rTable.fields ? this.getPkeyFieldsTxtForService(GeneratorTool.getPkeyFields(rTable.fields)) : '',
    };

    const customPrompt = "Can you read this file to add methods (with method documentation) that you consider useful and important from a business perspective ? use 'this.httpClient' for calls. \n"
      + "Just give me the new methods (very important) and please consider pessimistic scenarios but don't use catchError. Careful, all crud methos already exist in abstract class and return Observable.";

    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${fileName}.ts`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields, rCustomPrompt: customPrompt });
    // We don't want AI for spec file actually
    this.setCanUseAI(false);
    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${fileName}.spec.ts`, rTemplate: tpl_spec, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    this.setCanUseAI(true);
  }

  /**
   * Get pkey fields to use on service generic declaration
   * examples : 
   * class UserService extends AbstractAPIService<User, 'id'>
   * class ProductService extends AbstractAPIService<Product, 'productId' | 'categoryId'>
   */
  private getPkeyFieldsTxtForService(rPkeyFields?: GenericDbField[]) {
    // Map pkeys field names with union operator
    return (rPkeyFields && rPkeyFields.length > 0) ? rPkeyFields.map(item => `'${this.getFieldName(item.fieldName ?? '')}'`).join(' | ') : '';
  }
}
