/* eslint-disable @typescript-eslint/naming-convention */
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { AbstractTypeScriptIonicAngularComponentProcessor } from "./AbstractTypeScriptIonicAngularComponentProcessor";
import { TypescriptIonicAngularViewModelFormProcessor } from "./TypescriptIonicAngularViewModelFormProcessor";
import { TypescriptIonicAngularViewModelListProcessor } from "./TypescriptIonicAngularViewModelListProcessor";

export class TypescriptIonicAngularViewModelListPageProcessor extends AbstractTypeScriptIonicAngularComponentProcessor {
  // No AI for this processor
  protected canUseAI?: boolean | undefined = false;

  getComponentSuffix(): string {
    return "ListPage";
  }

  getProcessorName(): string {
    return "ionic_angular_view_model_list_page";
  }

  getTemplateNames() {
    return {
      ts: { name: "tpl_ionic_angular_view_model_list_page_ts" },
      spec: { name: "tpl_ionic_angular_view_model_list_page_spec_ts" },
      scss: { name: "tpl_ionic_angular_view_model_list_page_scss" },
      html: { name: "tpl_ionic_angular_view_model_list_page_html" },
    };    
  }

  /**
   * Get imports array, you can override this method in your processor
   * import must look like : "{ CommonModule } from '@angular/common'", don't write 'import' and no ';' at the end
   * @returns imports array
   */
  protected getStandaloneImportsArray(): string[] {
    return ["{ CommonModule } from '@angular/common'", "{ IonicModule } from '@ionic/angular'"];
  }

  /**
   * Get imports array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getStandaloneComponentImportsArray(): string[] {
    return ["CommonModule", "IonicModule"];
  }

  /**
   * Get spec extra import array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getSpecExtraImportsArray(): string[] {
    return ["IonicModule"];
  }

  /**
   * add extra fields to model template
   * @param rModelTemplate : data to inject to template
   * @param rFields : fields
   */
  protected addExtraFieldsToModelTemplate(rModelTemplate: any, rFields?: GenericDbField[]): void {
    // We must add <app-list-<xxx>> and <app-form-... component to html page, so we add this to the model template
    const processorList = new TypescriptIonicAngularViewModelListProcessor(this.config, this.configProcessor, this.db);
    const processorForm = new TypescriptIonicAngularViewModelFormProcessor(this.config, this.configProcessor, this.db);
    // We know that model name is already in template, we need it
    const classNameList = processorList.getClassName(rModelTemplate.modelName);
    const classNameForm = processorForm.getClassName(rModelTemplate.modelName);
    // Now generate baseFileNames
    const baseFileNameList = GeneratorTool.camelToString({ rText: classNameList, rSep: "-", rLowerCase: true });
    const baseFileNameForm = GeneratorTool.camelToString({ rText: classNameForm, rSep: "-", rLowerCase: true });
    // Finally add property to model template
    rModelTemplate["baseFileNameList"] = `${baseFileNameList}`;
    rModelTemplate["baseFileNameForm"] = `${baseFileNameForm}`;
  }
}
