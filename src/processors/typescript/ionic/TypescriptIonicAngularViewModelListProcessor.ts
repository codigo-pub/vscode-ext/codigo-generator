/* eslint-disable @typescript-eslint/naming-convention */
import path from "path";
import { StaticFile } from "../../../model/StaticFile";
import { AbstractTypeScriptIonicAngularComponentProcessor } from "./AbstractTypeScriptIonicAngularComponentProcessor";

export class TypescriptIonicAngularViewModelListProcessor extends AbstractTypeScriptIonicAngularComponentProcessor {

  // Static files to copy to dest folder cause files generated will need those file
  protected readonly STATIC_FILES: StaticFile[] = [
    { src: "ionic_angular_confirm_dialog_ts", dest: path.join("shared", "confirm-dialog", "confirm-dialog.component.ts") },
    { src: "ionic_angular_confirm_dialog_scss", dest: path.join("shared", "confirm-dialog", "confirm-dialog.component.scss") },
    { src: "ionic_angular_confirm_dialog_html", dest: path.join("shared", "confirm-dialog", "confirm-dialog.component.html") },
  ];
  
  getComponentSuffix(): string {
    return "List";
  }

  getProcessorName(): string {
    return "ionic_angular_view_model_list";
  }

  getTemplateNames() {
    return {
      ts: { name: "tpl_ionic_angular_view_model_list_ts", canUseAI: false}, // Note that we don't want AI for ts file cause it will mess all up
      spec: { name: "tpl_ionic_angular_view_model_list_spec_ts" },
      scss: { name: "tpl_ionic_angular_view_model_list_scss" },
      html: { name: "tpl_ionic_angular_view_model_list_html", canUseAI: true},
    };
  }

  /**
   * Get imports array, you can override this method in your processor
   * import must look like : "{ CommonModule } from '@angular/common'", don't write 'import' and no ';' at the end
   * @returns imports array
   */
  protected getStandaloneImportsArray(): string[] {
    return [
      "{ CommonModule } from '@angular/common'",
      "{ IonicModule } from '@ionic/angular'",
    ];
  }

  /**
   * Get imports array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getStandaloneComponentImportsArray(): string[] {
    return ["CommonModule", "IonicModule"];
  }

  /**
   * Get spec extra import array for the module declaration
   * You can override this method in your processor
   * @returns import array for module
   */
  protected getSpecExtraImportsArray(): string[] {
    return ["IonicModule"];
  }
}
