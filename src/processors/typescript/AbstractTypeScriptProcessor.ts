/* eslint-disable @typescript-eslint/naming-convention */
import { GenericDbField } from "../../db/model/GenericDbField";
import { AbstractProcessor } from "../AbstractProcessor";
import { IConverter } from "../IConverter";
import { TypeScriptConverter } from "./TypeScriptConverter";

export abstract class AbstractTypeScriptProcessor extends AbstractProcessor {
  protected readonly OPTIONAL_CHAR = "?";

  /**
   * Fields that we want to ignore in some cases, those are protected fields
   */
  protected readonly PROTECTED_FIELDS = ["password", "created_at", "updated_at"];

  protected getConverter(): IConverter {
    return new TypeScriptConverter();
  }

  /**
   * Get phpdoc param part '@param <type> <param name>
   * @param rFieldType : type of the field
   * @param rFieldName : name of the field
   */
  protected addParamDoc(rFieldType: string, rFieldName: string): string {
    return `\t\t * @param {${rFieldType}} ${rFieldName}`;
  }

  /**
   * Get optional field char, represents an optional field
   * @param rField : field
   * @returns optional field char
   */
  protected getOptionalChar(rField: GenericDbField): string {
    return rField?.isNullable ? this.OPTIONAL_CHAR : "";
  }

  /**
   * Takes an import name and generates the import call
   * @param rImportName : import name, e.g. '{ IConverter } from "../IConverter"'
   * @returns import call given that name (e.g. 'import { IConverter } from "../IConverter";')
   */
  protected getImportFrom(rImportName: string): string {
    return rImportName ? `import ${rImportName};` : "";
  }

  /**
   * Define if processor must build a standalone component
   * @returns true if processor is configured to build a standalone component
   */
  protected isStandalone(): boolean {
    return this.configProcessor?.extra_config?.standalone !== false;
  }
}
