/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { PhpDoctrineEntityProcessor } from "./PhpDoctrineEntityProcessor";
import { PhpSymfonyServiceProcessor } from "./PhpSymfonyServiceProcessor";
import path from "path";
import { FileTool } from "../../../tools/FileTool";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { AbstractPhpSymfonyProcessor } from "./AbstractPhpSymfonyProcessor";

export class PhpSymfonyApiControllerProcessor extends AbstractPhpSymfonyProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private entityProcessor?: PhpDoctrineEntityProcessor;
  private serviceProcessor?: PhpSymfonyServiceProcessor;
  private many_to_many_sep?: string;
  private baseApi?: string = ""; // base api
  private folderApi?: string = ""; // base api

  getProcessorFolder(): string {
    return "Controller";
  }

  getProcessorName(): string {
    return "symfony_api_controller";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new PhpDoctrineEntityProcessor(this.config, this.configProcessor, this.db);
    this.serviceProcessor = new PhpSymfonyServiceProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    this.baseApi = this.configProcessor?.extra_config?.base_api ?? "/api";
    this.folderApi = this.configProcessor?.extra_config?.folder_api ?? "Api";
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // Make folder path based on 'destFolderApi'
        const folderPath = `${this.getOutProcessorPath() + this.folderApi + path.sep}`;
        // Create dest folder if it doesn't exist
        FileTool.checkNewFolder(folderPath);
        // process table data
        if (pKeys && pKeys.length > 1) {
          await this.processTableWithCompositePKey(rTable, folderPath);
        } else {
          await this.processTableWithSinglePkey(rTable, folderPath);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   * @returns string for template file or null if nothing could be done
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable, folderPath: string): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_symfony_api_controller_php", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classServiceName = `${classModelName}Service`;
      var className = `${classModelName}Controller`;
      var pkeyType = "";
      var pkeyImport = "";
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // If not composite primary key
      if (pKeys && pKeys.length === 1) {
        const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
        pkeyType = myPkeyType?.targetType ?? "";
        // Add import if needed
        this.addImportForField(extraFields, myPkeyType);
      }

      const modelTemplate = {
        folderApi: this.folderApi ? `\\${this.folderApi}` : "",
        baseApi: this.baseApi,
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classServiceName: classServiceName,
        className: className,
        // paramPrefix: paramPrefix, // <= with symfony controller, we have same parameter name as url key name so we avoid specific prefix
        pkeyImport: pkeyImport,
        pkeyType: pkeyType,
        extraFields_import: extraFields.getImportsTxt(),
      };

      await this.writeTemplateToFile({ rFilePath: `${folderPath}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   * @returns string for template file or null if nothing could be done
   */
  protected async processTableWithCompositePKey(rTable: GenericDbTable, folderPath: string): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_symfony_api_controller_c_php", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classServiceName = `${classModelName}Service`;
      var className = `${classModelName}Controller`;
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pkeyFields = this.getPkeyCompositeFields(GeneratorTool.getPkeyFields(rTable.fields), extraFields);

      const modelTemplate = {
        folderApi: this.folderApi ? `\\${this.folderApi}` : "",
        baseApi: this.baseApi,
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classServiceName: classServiceName,
        className: className,
        pkey_phpdoc: pkeyFields.phpdoc,
        pkey_parameter: pkeyFields.parameter,
        pkey_field: pkeyFields.field,
        pkey_url_parameter: pkeyFields.url_parameter,
        extraFields_import: extraFields.getImportsTxt(),
      };

      await this.writeTemplateToFile({ rFilePath: `${folderPath}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generate different texts that are necessary for template based on pkey fields
   * @param rFields : pkey fields
   * @param rExtraFields : extraField object for extra imports, etc.
   * @returns
   */
  private getPkeyCompositeFields(
    rFields: GenericDbField[],
    rExtraFields: ExtraField
  ): { phpdoc: string; parameter: string; field: string; url_parameter: string } {
    let data = { phpdoc: "", parameter: "", field: "", url_parameter: "" };
    // Prepare data

    if (rFields && rFields.length > 0) {
      // Browse pkfields to add them in text
      for (const field of rFields) {
        // Get field type
        const fieldType = this.converter.fieldConvertToType(field.type);
        // Add import if needed
        this.addImportForField(rExtraFields, fieldType);
        // Get type
        const type = fieldType?.targetType ?? "";
        data.phpdoc += (data.parameter !== "" ? "\n" : "") + `\t * @param ${type} ${this.PREFIX_VAR}${field.fieldName} ${field.fieldName}`;
        data.parameter += (data.parameter !== "" ? ", " : "") + `${type} ${this.PREFIX_VAR}${field.fieldName}`;
        data.field += (data.field !== "" ? ", " : "") + `${this.PREFIX_VAR}${field.fieldName}`;
        data.url_parameter += `/{${field.fieldName}}`;
      }
    }
    return data;
  }
}
