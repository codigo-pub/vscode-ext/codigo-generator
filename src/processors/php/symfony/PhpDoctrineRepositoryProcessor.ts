/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { SqlTool } from "../../../tools/SqlTool";
import { AbstractPhpSymfonyProcessor } from "./AbstractPhpSymfonyProcessor";

export class PhpDoctrineRepositoryProcessor extends AbstractPhpSymfonyProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private many_to_many_sep?: string;

  getProcessorFolder(): string {
    return "Repository";
  }

  getProcessorName(): string {
    return "doctrine_repository";
  }

  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix

    // Load template
    var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_doctrine_repository_php", "utf8");
    var classModelName = this.processClassNameFromTable(rTable.tableName);
    var className = `${classModelName}Repository`;

    // Browse OneToMany referenced keys to add some cool methods
    let extraMethods = "";
    if (rTable?.referencedFKeys) {
      for (const fielddata of rTable.referencedFKeys) {
        // Add extra method
        extraMethods += this.generateFindBy(rTable, fielddata);
      }  
    }

    const modelTemplate = {
      classModelName: classModelName,
      className: className,
      packageBaseName: this.config.packageBaseName,
      paramPrefix: paramPrefix,
      extraMethods: extraMethods
    };
    // Write to file
    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'clasroom_has_students' ManyToMany table, we want 
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rFKey : foreign referenced key
   */
  private generateFindBy(rTable: GenericDbTable, rFKey: GenericDbFKey): string {
    // Only work with ManyToMany tables 
    if (rTable.fields && rFKey.tableName && SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep)) {
      const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX;
      // Search for this table pkey to find type
      let keys = GeneratorTool.getPkeyFields(rTable.fields);
      // If more than one pkey, we don't know how to do (so far)
      if (keys && keys.length === 1) {
        let pKey = keys[0];
        let className = GeneratorTool.upperFirstLetter(rTable.tableName);
        let tableDistant = SqlTool.extractOtherTableName(rFKey.tableName, rTable.tableName, this.many_to_many_sep);
        let classNameDistant = this.processClassNameFromTable(tableDistant);
        let fieldName = this.getFieldNameMany(rFKey.tableName);
        let refColumn = SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep) ? tableDistant+"_id" : rFKey.referencedColumn;
        let method = `\n\t/**
  \t * Get all ${className} by ${classNameDistant} id
  \t * @param ${pKey.type?.type} ${this.PREFIX_VAR}${refColumn} : ${classNameDistant} id
  \t * @return ${className}[] array of ${rTable.tableName}
  \t */   
  \tpublic function find${className}By${classNameDistant}(${pKey.type?.type} ${this.PREFIX_VAR}${paramPrefix}${refColumn}): array
  \t{
  \t    return $this->createQueryBuilder('x')
  \t        ->join('x.${fieldName}', 'xhy')
  \t        ->where('xhy.${refColumn} = :${refColumn}')
  \t        ->setParameter('${refColumn}', ${this.PREFIX_VAR}${paramPrefix}${refColumn})
  \t        ->select('x')
  \t        ->getQuery()
  \t        ->getResult();
  \t}
  `;
        return method;
  
      }

    }
    return "";
  }

}
