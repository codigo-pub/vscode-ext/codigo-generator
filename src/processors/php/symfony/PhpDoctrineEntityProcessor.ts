/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { EnumDbType } from "../../../db/model/EnumDbType";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { EnumDbRule } from "../../../db/model/EnumDbRule";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { IExtraFieldProcessor } from "../../IExtraFieldProcessor";
import { IConverter } from "../../IConverter";
import { PhpDoctrineConverter } from "./PhpDoctrineConverter";
import { AbstractPhpSymfonyProcessor } from "./AbstractPhpSymfonyProcessor";

export class PhpDoctrineEntityProcessor extends AbstractPhpSymfonyProcessor implements IExtraFieldProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "Entity";
  }

  getProcessorName(): string {
    return "doctrine_entity";
  }

  /**
   * Custom converter for symfony, thanks to Doctrine :)
   * @returns
   */
  protected getConverter(): IConverter {
    return new PhpDoctrineConverter();
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_doctrine_entity_php", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var gettersAndSetters = "";

      var extraFields = new ExtraField();

      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var fieldType = this.converter.fieldConvertToType(fielddata.type);
          // check if import needed
          this.addImportForField(extraFields, fieldType);
          // get foreign key eventually
          const fKey: GenericDbFKey | undefined = GeneratorTool.getFKeyFromField(fielddata, rTable.fKeys);
          // inject field
          fieldsToInject += (fieldsToInject !== "" ? "\n" : "") + this.getDoctrineField(fielddata, fieldType?.targetType, fKey);
          // getters and setters
          gettersAndSetters += this.generateGetterSettersForField(fieldName, fieldType?.targetType ?? "", this.getOptionalChar(fielddata));
        }
      }

      // Extra fields (OneToMany/ManyToOne)
      const extraFieldsFKeys = GeneratorTool.getExtraFields(rTable, rTable.fKeys ?? [], this);
      const extraFieldsRefFkeys = GeneratorTool.getExtraFields(rTable, rTable.referencedFKeys ?? [], this, true);
      extraFields.addImports(extraFieldsFKeys.importTxt);
      extraFields.addImports(extraFieldsRefFkeys.importTxt);
      extraFields.fieldsTxt = (extraFieldsFKeys.fieldsTxt ?? "") + (extraFieldsRefFkeys.fieldsTxt ?? "");
      extraFields.gettersSettersTxt = (extraFieldsFKeys.gettersSettersTxt ?? "") + (extraFieldsRefFkeys.gettersSettersTxt ?? "");

      const modelTemplate = {
        className: className,
        tableName: rTable.tableName,
        packageBaseName: this.config.packageBaseName,
        fields: fieldsToInject,
        extraFields_import: extraFields.getImportsTxt(),
        extraFields_fields: extraFields.fieldsTxt,
        extraFields_gettersSetters: extraFields.gettersSettersTxt,
        // constructor: constructorToInject,
        // in_constructor: constructorInToInject,
        gettersAndSetters: gettersAndSetters,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Process extra field
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rReferenced : true if those fields are referenced fields, false if they are only foreign keys on the current table
   * @returns extra field with texts
   */
  processExtraField(rTable: GenericDbTable, rFKey: GenericDbFKey, rReferenced: boolean = false): ExtraField {
    let extraField = new ExtraField();
    if (rFKey.tableName) {
      const tableToUse = rReferenced ? rFKey.tableName : rFKey.referencedTable ?? "";
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(tableToUse);
      let fieldName = this.getFieldNameMany(tableToUse);
      extraField.addImport(`use App\\Entity\\${className};`);
      extraField.fieldsTxt = rReferenced ? this.getOrmOneToMany(rTable, rFKey) : this.getOrmManyToOne(rFKey);
      // note that getter/setter is based on classname and not column name
      extraField.gettersSettersTxt = this.generateGetterSettersForField(fieldName, "Collection", "?");
    }
    return extraField;
  }

  private generateGetterSettersForField(rFieldName: string, rFieldType: string, rNullable: string): string {
    const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
    let upName = GeneratorTool.upperFirstLetter(rFieldName);
    let getter = `\tpublic function get${upName}(): ${rNullable}${rFieldType}
    {
        return $this->${rFieldName};
    }`;
    let setter = `\tpublic function set${upName}(${rNullable}${rFieldType} ${this.PREFIX_VAR}${paramPrefix}${rFieldName}): void
    {
        $this->${rFieldName} = ${this.PREFIX_VAR}${paramPrefix}${rFieldName};
    }`;
    return getter + "\n\n" + setter + "\n\n";
  }

  private getDoctrineField(rField: GenericDbField, rFieldType: string | undefined, rFKey: GenericDbFKey | undefined): string {
    let doctrineField = "";
    if (rField.fieldName) {
      // Generate ORM annotations based on properties
      // If pkey
      if (rField.isPKey) {
        doctrineField += "\n\t#[ORM\\Id]\n";
        // it is autogenerated ?
        doctrineField += rField.isAutogenerated ? "\t#[ORM\\GeneratedValue]\n" : "";
        doctrineField += "\t#[ORM\\Column]\n\t";
      } else if (rFKey) {
        // nothing to do if this is a foreign key, it will be done with getManyToOne (see extraFields)
        return doctrineField;
      } else {
        doctrineField += "\n\t#[ORM\\Column";
        let options: string[] = [];
        // If field is date we add mutable type
        if (rFieldType === "DateTimeInterface") {
          const mutableType = rField.type?.type === EnumDbType.date ? "DATE_MUTABLE" : "DATETIME_MUTABLE";  
          options.push(`type: Types::${mutableType}`);
        }
        if (rField.isNullable) {
          options.push("nullable: true");
        }
        if (rField.type?.length && this.showLength(rField.type?.type)) {
          options.push(`length:${rField.type.length}`);
        }
        if (options.length>0) {
          doctrineField += `(${options.join(", ")})`;
        }
        doctrineField += "]\n\t";
      }
      // Generate field declaration
      // note, with doctrine, we always set type has optional, even if it's not
      doctrineField += this.getFieldDeclaration(rFieldType, rField.fieldName);
    }
    return doctrineField;
  }

  /**
   * Get ORM representation of many to one for this foreign key
   * @param rFKey : Generic Foreign key object
   * @returns
   */
  protected getOrmManyToOne(rFKey: GenericDbFKey): string {
    let txt = "";
    if (rFKey?.referencedTable) {
      let className = this.processClassNameFromTable(rFKey.referencedTable);
      let fieldName = this.getFieldNameMany(rFKey.referencedTable);
      txt += `\n\n\t#[ORM\\ManyToOne(targetEntity: ${className}::class)]`;
      txt += `\n\t#[ORM\\JoinColumn(name: '${rFKey.columnName}', referencedColumnName: '${rFKey.referencedColumn}')]`;
      txt += "\n\t" + this.getFieldDeclaration(className, fieldName);
    }
    return txt;
  }

  /**
   * Get ORM representation of one to many for this foreign key
   * #[ORM\\OneToMany(targetEntity: Product::class, mappedBy: 'category')]
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @returns
   */
  protected getOrmOneToMany(rTable: GenericDbTable, rFKey: GenericDbFKey): string {
    let txt = "";
    if (rFKey?.tableName) {
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(rFKey.tableName);
      let fieldName = this.getFieldNameMany(rFKey.tableName);
      let refFieldName = this.getFieldNameMany(rTable.tableName); // name of the field in distant className entity
      // OneToMany annotation
      txt += `\n\n\t#[ORM\\OneToMany(mappedBy: "${refFieldName}", targetEntity: ${className}::class`;
      // for now, we only support cascade rule for both
      if (rFKey.deleteRule === EnumDbRule.CASCADE && rFKey.updateRule === EnumDbRule.CASCADE) {
        txt += `, cascade: ['persist', 'remove']`;
      }
      txt += ")]";
      txt += "\n\t" + this.getFieldDeclaration("Collection", fieldName);
    }
    return txt;
  }

  /**
   * Create field text, e.g : 'private ?Student $student;'
   * @param rFieldType : field type
   * @param rFieldname : field name
   * @param rOptional : [optional]optional character, default to optional character of the class
   * @returns
   */
  private getFieldDeclaration(rFieldType: string | undefined, rFieldname: string, rOptional: string = this.OPTIONAL_CHAR): string {
    return `private ${rOptional}${rFieldType} ${this.PREFIX_VAR}${rFieldname};`;
  }

  /**
   * return true if we need to show the length in column property (ex: #[ORM\\Column(length: 255)])
   * @param rType : EnumDbType
   * @returns
   */
  private showLength(rType?: EnumDbType): boolean {
    switch (rType) {
      case EnumDbType.varchar:
      case EnumDbType.char:
      case EnumDbType.longtext:
        return true;
      default:
        return false;
    }
  }
}
