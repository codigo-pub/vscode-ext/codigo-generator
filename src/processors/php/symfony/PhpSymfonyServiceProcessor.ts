/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { PhpDoctrineEntityProcessor } from "./PhpDoctrineEntityProcessor";
import { PhpDoctrineRepositoryProcessor } from "./PhpDoctrineRepositoryProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { ExtraField } from "../../../model/ExtraField";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { AbstractPhpSymfonyProcessor } from "./AbstractPhpSymfonyProcessor";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { SqlTool } from "../../../tools/SqlTool";

export class PhpSymfonyServiceProcessor extends AbstractPhpSymfonyProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private entityProcessor?: PhpDoctrineEntityProcessor;
  private repositoryProcessor?: PhpDoctrineRepositoryProcessor;
  private many_to_many_sep?: string;
  private paramPrefix?: string;

  getProcessorFolder(): string {
    return "Service";
  }

  getProcessorName(): string {
    return "symfony_service";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new PhpDoctrineEntityProcessor(this.config, this.configProcessor, this.db);
    this.repositoryProcessor = new PhpDoctrineRepositoryProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    this.paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.repositoryProcessor) {
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // If composite primary key
        if (pKeys && pKeys.length > 1) {
          this.processTableWithCompositePKey(rTable);
        } else {
          this.processTableWithSinglePkey(rTable);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.repositoryProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_symfony_service_php", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classRepositoryName = `${classModelName}Repository`;
      var className = `${classModelName}Service`;
      var pkeyFieldName = "";
      var pkeyType = "";
      var pkeyImport = "";
      var extraFields = new ExtraField();
      const classModelNameLower = classModelName.toLowerCase();
      
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // If not composite primary key
      if (pKeys && pKeys.length === 1) {
        const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
        pkeyType = myPkeyType?.targetType ?? "";
        pkeyFieldName = pKeys[0].fieldName ?? "id";
        // Add import if needed
        this.addImportForField(extraFields, myPkeyType);
      }

      // Browse OneToMany referenced keys to add some cool methods
      let extraMethods = "";
      if (rTable?.referencedFKeys) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          extraMethods += this.generateFindBy(rTable, fielddata, `${classModelNameLower}Repository`);
        }
      }

      const modelTemplate = {
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classRepositoryName: classRepositoryName,
        className: className,
        paramPrefix: this.paramPrefix,
        pkeyImport: pkeyImport,
        pkeyType: pkeyType,
        pkeyFieldName: GeneratorTool.upperFirstLetter(pkeyFieldName),
        extraFields_import: extraFields.getImportsTxt(),
        extraMethods: extraMethods,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   */
  protected async processTableWithCompositePKey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.repositoryProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_symfony_service_c_php", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      const classModelNameLower = classModelName.toLowerCase();
      var classRepositoryName = `${classModelName}Repository`;
      var className = `${classModelName}Service`;
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pkeyFields = this.getPkeyCompositeFields(GeneratorTool.getPkeyFields(rTable.fields), classModelNameLower, extraFields);

      // Browse OneToMany referenced keys to add some cool methods
      let extraMethods = "";
      if (rTable?.referencedFKeys) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          extraMethods += this.generateFindBy(rTable, fielddata, `${classModelNameLower}Repository`);
        }
      }

      const modelTemplate = {
        classModelName: classModelName,
        classModelNameLower: classModelNameLower,
        classRepositoryName: classRepositoryName,
        className: className,
        pkey_phpdoc: pkeyFields.phpdoc,
        pkey_parameter: pkeyFields.parameter,
        pkey_field: pkeyFields.field,
        pkey_array_in: pkeyFields.array_in,
        pkey_check_ids: pkeyFields.check_ids,
        extraFields_import: extraFields.getImportsTxt(),
        extraMethods: extraMethods,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generate different texts that are necessary for template based on pkey fields
   * @param rFields : pkey fields
   * @param rModelVariableName : class model name in lower case as
   * @param rExtraFields : extraField object for extra imports, etc.
   * @returns
   */
  private getPkeyCompositeFields(
    rFields: GenericDbField[],
    rModelVariableName: string,
    rExtraFields: ExtraField
  ): { phpdoc: string; parameter: string; field: string; array_in: string; check_ids: string } {
    let data = { phpdoc: "", parameter: "", field: "", array_in: "", check_ids: "" };
    // Prepare data

    if (rFields && rFields.length > 0) {
      // Browse pkfields to add them in text
      for (const field of rFields) {
        if (field.fieldName) {
          // Get field type
          const fieldType = this.converter.fieldConvertToType(field.type);
          // Add import if needed
          this.addImportForField(rExtraFields, fieldType);
          // Get type
          const type = fieldType?.targetType ?? "";
          data.phpdoc +=
            (data.parameter !== "" ? "\n" : "") + `\t * @param ${type} ${this.PREFIX_VAR}${this.paramPrefix}${field.fieldName} ${field.fieldName}`;
          data.parameter += (data.parameter !== "" ? ", " : "") + `${type} ${this.PREFIX_VAR}${this.paramPrefix}${field.fieldName}`;
          data.field += (data.field !== "" ? ", " : "") + `${this.PREFIX_VAR}${this.paramPrefix}${field.fieldName}`;
          data.array_in += (data.array_in !== "" ? ", " : "") + `'${field.fieldName}' => ${this.PREFIX_VAR}${this.paramPrefix}${field.fieldName}`;

          // Extract getter
          const getter = field.fieldName[0].toUpperCase() + field.fieldName.substring(1);
          data.check_ids += `\n\n\t\tif (${this.PREFIX_VAR}${this.paramPrefix}${field.fieldName} !== ${this.PREFIX_VAR}${rModelVariableName}->get${getter}()) {
  \t\t\tthrow new InvalidArgumentException('ID mismatch: ${field.fieldName} in the URL does not match ${field.fieldName} of the object. Operation not permitted.');
  \t\t}`;
        }
      }
    }
    return data;
  }

  /**
   * Generates a call to the findBy method. See repository processor for more details
   * @param rTable : Table making the call
   * @param rFKey : foreign referenced key
   * @param rClassRepositoryMember : repository member name variable 
   */
  private generateFindBy(rTable: GenericDbTable, rFKey: GenericDbFKey, rClassRepositoryMember: string): string {
    // Only work with ManyToMany tables 
    if (rTable.fields && rFKey.tableName && SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep)) {
      const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX;
      // Search for this table pkey to find type
      let keys = GeneratorTool.getPkeyFields(rTable.fields);
      // If more than one pkey, we don't know how to do (so far)
      if (keys && keys.length === 1) {
        let pKey = keys[0];
        let className = GeneratorTool.upperFirstLetter(rTable.tableName);
        let tableDistant = SqlTool.extractOtherTableName(rFKey.tableName, rTable.tableName, this.many_to_many_sep);
        let classNameDistant = this.processClassNameFromTable(tableDistant);
        let refColumn = SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep) ? tableDistant+"_id" : rFKey.referencedColumn;
        let method = `\n\t/**
  \t * Get all ${className} by ${classNameDistant} id
  \t * @param ${pKey.type?.type} ${this.PREFIX_VAR}${refColumn} : ${classNameDistant} id
  \t * @return ${className}[] array of ${rTable.tableName}
  \t */   
  \tpublic function find${className}By${classNameDistant}(${pKey.type?.type} ${this.PREFIX_VAR}${paramPrefix}${refColumn}): array
  \t{
  \t     return $this->${rClassRepositoryMember}->find${className}By${classNameDistant}(${this.PREFIX_VAR}${paramPrefix}${refColumn});
  \t}
  `;
        return method;
  
      }

    }
    return "";
  }

}
