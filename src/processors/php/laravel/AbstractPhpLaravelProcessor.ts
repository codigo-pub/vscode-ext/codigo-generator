/* eslint-disable @typescript-eslint/naming-convention */

import { AbstractPhpProcessor } from "../AbstractPhpProcessor";

export abstract class AbstractPhpLaravelProcessor extends AbstractPhpProcessor {
  // All templates are expected in this sub-folder
  protected templateFolder?: string = "laravel";
}
