/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { AbstractPhpLaravelProcessor } from "./AbstractPhpLaravelProcessor";

export class PhpArtisanMakeModel extends AbstractPhpLaravelProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private readonly FILE_NAME = "ArtisanMakeModel.sh";

  getProcessorFolder(): string {
    return "artisan_make_model";
  }
  
  getProcessorName(): string {
    return "artisan_make_model";
  }

  /**
   * Not implemented as this processor gather all tables to process them in one file !
   */
  protected processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async process(): Promise<void> {

    // As we are not calling defaultprocess method, we need to init services and create directory if needed
    this.init();
    this.initFolders();
    // Get all tables (filtered based on configuration)
    const tables = await this.getAllTables();
    let commandArtisan = [];
    // Load template
    var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_artisan_make_model_php", "utf8");

    if (tables && tables.length > 0) {
      for (const table of tables) {
        if (table?.tableName) {
          var className = this.processClassNameFromTable(table.tableName);
          // Add command with extra_config.option if set
          commandArtisan.push(`php artisan make:model ${className} ${this.configProcessor?.extra_config?.option ?? ""}`);
        }
      }
    }
    const modelTemplate = {
      commands: commandArtisan.join("\n"),
    };
    // Write to file
    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${this.FILE_NAME}`, rTemplate: tpl, rFieldsTemplate: modelTemplate });
  }
}
