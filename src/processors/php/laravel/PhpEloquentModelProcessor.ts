/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { IExtraFieldProcessor } from "../../IExtraFieldProcessor";
import { EnumDbType } from "../../../db/model/EnumDbType";
import { SqlTool } from "../../../tools/SqlTool";
import { IConverter } from "../../IConverter";
import { PhpEloquentConverter } from "./PhpEloquentConverter";
import { AbstractPhpLaravelProcessor } from "./AbstractPhpLaravelProcessor";

export class PhpEloquentModelProcessor extends AbstractPhpLaravelProcessor implements IExtraFieldProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private many_to_many_sep?: string;

  // Laravel tables that we want to skip from process
  private readonly LARAVEL_TABLES = ["cache", "cache_locks", "failed_jobs", "jobs", "job_batches", "migrations", "password_reset_tokens", "sessions"];

  // Array of protected fields we want to check
  private readonly protectedFieldsReference = ["password", "pass", "remember_token"];
  // Not fillable fields
  private readonly notFillableFieldsReference = ["created_at", "updated_at", "remember_token"];
  // A table with protected fields will be automatically be treated as authenticated
  private readonly authenticatedTablesReference = ["users", "user"];

  getProcessorFolder(): string {
    return "Models";
  }

  getProcessorName(): string {
    return "eloquent_model";
  }

  /**
   * Custom converter for laravel, thanks to Eloquent :)
   * @returns 
   */
  protected getConverter(): IConverter {
    return new PhpEloquentConverter();
  }
  
  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (this.LARAVEL_TABLES.indexOf(rTable.tableName) < 0 && rTable?.fields && rTable.fields.length > 0) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_eloquent_model_php", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var constructorToInject = "";
      var constructorInToInject = "";
      var gettersAndSetters = "";
      var fillableFields = "";
      var protectedFields = [];
      const doGettersSetters = this.configProcessor?.extra_config?.do_getters_setters ?? false;
      // Is it a pivot table ?
      var isPivotTable = SqlTool.isManyToManyTable(rTable.tableName, this.many_to_many_sep);

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      var isComposite = pKeys && pKeys.length > 1; // set flag

      //If there are composite key, create property declaration
      if (isComposite) {
        fieldsToInject += this.getCompositePKeyFieldsTxt(pKeys);
      }

      // To store main extra imports mostly
      const mainFields = new ExtraField();

      // Browse fields
      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var fieldType = this.converter.fieldConvertToType(fielddata.type);
          // get foreign key eventually
          const fKey: GenericDbFKey | undefined = GeneratorTool.getFKeyFromField(fielddata, rTable.fKeys);
          // inject field
          fieldsToInject += this.getEloquentField(fielddata, fieldType?.targetType, fKey, isComposite);

          // Is the field protected ?
          if (this.isProtected(fieldName)) {
            protectedFields.push(fieldName);
          }

          if (doGettersSetters) {
            // getters and setters
            gettersAndSetters += this.generateGetterSettersForField(fieldName, fieldType?.targetType ?? "", this.getOptionalChar(fielddata));
            // import related to getters and setters
            this.addImportForField(mainFields, fieldType);
          }

          // if primary key, we set constructor fields
          // if (fielddata.isPKey) {
          //   if (constructorToInject !== "") {
          //     constructorToInject += ", "; //if one more to constructor
          //   }
          //   const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
          //   constructorToInject += `${fieldType?.targetType} ${this.PREFIX_VAR}${paramPrefix}${fieldName}`;
          //   constructorInToInject += `${this.PREFIX_VAR}this->${fieldName} = ${this.PREFIX_VAR}${paramPrefix}${fieldName};\n\t\t\t`;
          // }
        }
      }

      // Process separatly fillable fields
      rTable.fields.forEach((field) => {
        if (field.fieldName) {
          // Fillable field ?
          const fillableField = !field.isAutogenerated && !this.isNotFillable(field.fieldName);
          // If it is actually fillable
          if (fillableField) {
            // add comma and go to next line if this is one more field to add
            const oneMore = fillableFields !== "" ? ",\n" : "";
            // If field is not a pkey or created_at/updated_at field, we add it on fillable fields
            fillableFields += `${oneMore}\t\t'${field.fieldName}'`;
          }
        }
      });

      // flag for authenticable tables
      // if table is a user table or of table has protected fields (password)
      var isAuthenticable = this.authenticatedTablesReference.indexOf(rTable.tableName) !== -1 || protectedFields.length > 0;

      // Extra fields (oneToMany)
      const extraFields = GeneratorTool.getExtraFields(rTable, rTable.referencedFKeys ?? [], this);

      // If there is a composite key, we add extra text
      if (isComposite) {
        extraFields.addImport("use Thiagoprz\\CompositeKey\\HasCompositeKey;");
      }

      let extendClass = "";
      let mainClassUse = "";
      // If authenticable class
      if (isAuthenticable) {
        extraFields.addImport("use Illuminate\\Foundation\\Auth\\User as Authenticatable;");
        extraFields.addImport("use Illuminate\\Database\\Eloquent\\Factories\\HasFactory;");
        extraFields.addImport("use Illuminate\\Notifications\\Notifiable;");
        extendClass = "Authenticatable";
        mainClassUse = "\tuse HasFactory, Notifiable;\n";
      } else if (isPivotTable) {
        // no polymorphic support by now
        extraFields.addImport("use Illuminate\\Database\\Eloquent\\Relations\\Pivot;");
        extendClass = "Pivot";
      } else {
        extendClass = "Model";
        extraFields.addImport("use Illuminate\\Database\\Eloquent\\Model;");
      }

      // Add main fields imports to extraFields
      extraFields.addImports(mainFields.importTxt);

      const modelTemplate = {
        tableName: rTable.tableName,
        className: className,
        extendClass: extendClass,
        mainClassUse: mainClassUse,
        // packageBaseName: this.config.packageBaseName,
        fields: fieldsToInject,
        fillableFields: fillableFields,
        extraFields_import: extraFields.getImportsTxt(),
        extraFields_fields: extraFields.fieldsTxt,
        protectedFields: this.getProtectedFieldsTxt(protectedFields),
        castFields: this.getCastFieldsTxt(rTable.fields),
        // extraFields_gettersSetters: extraFields.gettersSettersTxt,
        // constructor: constructorToInject,
        // in_constructor: constructorInToInject,
        gettersAndSetters: gettersAndSetters,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generate composite pkey field entry
   * @param rPkeyFields : PKey fields array
   * @returns
   */
  private getCompositePKeyFieldsTxt(rPkeyFields: GenericDbField[]): string {
    let txt = "";
    if (rPkeyFields && rPkeyFields.length > 0) {
      txt += "\n\tuse HasCompositeKey;";
      txt += "\n\tprotected $primaryKey = [ ";
      for (let i = 0; i < rPkeyFields.length; i++) {
        const field = rPkeyFields[i];
        txt += `"${field.fieldName}"` + (i < rPkeyFields.length - 1 ? ", " : "");
      }
      txt += " ];";
    }
    return txt;
  }

  /**
   * Generate protected field entry text
   * @param rProtectedFields : protected fields array
   * @returns
   */
  private getProtectedFieldsTxt(rProtectedFields: string[]): string {
    let txt = "";
    if (rProtectedFields && rProtectedFields.length > 0) {
      for (let i = 0; i < rProtectedFields.length; i++) {
        const field = rProtectedFields[i];
        txt += `\n\t\t"${field}"` + (i < rProtectedFields.length - 1 ? ", " : "");
      }
    }
    return txt ? `\tprotected $hidden = [${txt}\n\t];` : "";
  }

  /**
   * Generate cast field entry text
   * @param rCastFields : protected fields array
   * @returns
   */
  private getCastFieldsTxt(rCastFields: GenericDbField[]): string {
    let txt = "";
    if (rCastFields && rCastFields.length > 0) {
      for (let i = 0; i < rCastFields.length; i++) {
        const field = rCastFields[i];
        if (field.fieldName) {
          let entryCast = "";
          // If field is protected, we assume it is a hash
          if (this.isProtected(field.fieldName)) {
            entryCast = `'${field.fieldName}' => 'hashed'`;
          } else {
            // Add cast if needed
            switch (field.type?.type) {
              case EnumDbType.date:
                entryCast = `'${field.fieldName}' => 'date'`;
                break;
              case EnumDbType.datetime:
              case EnumDbType.time:
              case EnumDbType.timestamp:
                entryCast = `'${field.fieldName}' => 'datetime'`;
                break;
              default:
                break;
            }
          }
          // If something to insert
          if (entryCast) {
            txt += `\n\t\t${entryCast}` + (i < rCastFields.length - 1 ? ", " : "");
          }
        }
      }
    }
    return txt
      ? `\n\tprotected function casts(): array
    {
        return [${txt}\n\t\t];
    }`
      : "";
  }

  /**
   * Check if this field is one of the referenced protected fields (password, pass, etc.)
   * @param rFieldname : field name
   * @returns true if it is protected
   */
  private isProtected(rFieldname: string): boolean {
    return this.protectedFieldsReference.indexOf(rFieldname) !== -1;
  }

  /**
   * Check if this field is not fillable
   * @param rFieldname : field name
   * @returns true if it is not fillable
   */
  private isNotFillable(rFieldname: string): boolean {
    return this.notFillableFieldsReference.indexOf(rFieldname) !== -1;
  }

  /**
   * Process extra field
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rReferenced : true if those fields are referenced fields, false if they are only foreign keys on the current table
   * @returns extra field with texts
   */
  processExtraField(rTable: GenericDbTable, rFKey: GenericDbFKey, rReferenced: boolean = false): ExtraField {
    let extraField = new ExtraField();
    if (rFKey.tableName) {
      extraField.fieldsTxt = "";
      extraField.importTxt = [];
      extraField.gettersSettersTxt = "";
      this.getOrmOneToMany(rTable, rFKey, extraField);
      // detect if this is a ManyToMany relation
      if (rReferenced && SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep)) {
        extraField.addImport("use Illuminate\\Database\\Eloquent\\Relations\\BelongsToMany;");
        this.getOrmManyToMany(rTable, rFKey, extraField);
      }
    }
    return extraField;
  }

  private generateGetterSettersForField(rFieldName: string, rFieldType: string, rNullable: string): string {
    let upName = GeneratorTool.upperFirstLetter(rFieldName);
    let getter = `\tpublic function get${upName}(): ${rNullable}${rFieldType}
    {
        return $this->${rFieldName};
    }`;
    let setter = `\tpublic function set${upName}(${rNullable}${rFieldType} ${this.PREFIX_VAR}${rFieldName}): void
    {
        $this->${rFieldName} = ${this.PREFIX_VAR}${rFieldName};
    }`;
    return getter + "\n\n" + setter + "\n\n";
  }

  /**
   * Get field declaration
   * @param rField : field
   * @param rFieldType : field type
   * @param rFKey : foreign key
   * @param isComposite : true if table got composite key
   * @returns
   */
  private getEloquentField(rField: GenericDbField, rFieldType: string | undefined, rFKey: GenericDbFKey | undefined, isComposite: boolean): string {
    let doctrineField = "";
    // declare pkey property even if laravel auto detects conventional pkeys
    if (rField.isPKey && !isComposite) {
      doctrineField += `\n\tprotected $primaryKey = '${rField.fieldName}';`;
    } else if (rFKey) {
      doctrineField += this.getOrmManyToOne(rFKey);
    }
    return doctrineField;
  }

  /**
   * Get ORM representation of many to one for this foreign key
   * @param rFKey : Generic Foreign key object
   * @returns
   */
  protected getOrmManyToOne(rFKey: GenericDbFKey): string {
    let txt = "";
    if (rFKey?.referencedTable) {
      let className = this.processClassNameFromTable(rFKey.referencedTable);
      txt += `\n\n\tpublic function ${rFKey.referencedTable}(): BelongsTo {`;
      txt += `\n\t\treturn $this->belongsTo(${className}::class, "${rFKey.columnName}", "${rFKey.referencedColumn}");`;
      txt += "\n\t}";
    }
    return txt;
  }

  /**
   * Get ORM function to use one to many
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rExtraField : extrafield to add imports/fields/getters/setters
   * @returns
   */
  protected getOrmOneToMany(rTable: GenericDbTable, rFKey: GenericDbFKey, rExtraField: ExtraField) {
    if (rFKey?.tableName) {
      let txt = "";
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(rFKey.tableName);
      // OneToMany annotation
      txt += `\n\n\tpublic function ${rFKey.tableName}(): HasMany {`;
      txt += `\n\t\treturn $this->hasMany(${className}::class, "${rFKey.columnName}", "${rFKey.referencedColumn}");`;
      txt += "\n\t}";
      // Add fields
      rExtraField.fieldsTxt += txt;
      // Add imports (code commented below, it is not needed cause we are in the same namespace)
      // rExtraField.addImport(`use \\App\\${this.getProcessorFolder()}\\${className};`);
    }
  }

  /**
   * Get ORM representation of many to many through pivot table for this foreign key
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rExtraField : extrafield to add imports/fields/getters/setters
   * @returns
   */
  protected getOrmManyToMany(rTable: GenericDbTable, rFKey: GenericDbFKey, rExtraField: ExtraField) {
    let txt = "";
    if (rFKey?.tableName) {
      let txt = "";
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(rFKey.tableName);
      let tableDistant = SqlTool.extractOtherTableName(rFKey.tableName, rTable.tableName, this.many_to_many_sep);
      let classNameDistant = this.processClassNameFromTable(tableDistant);
      let refColumn = tableDistant + "_id";
      // OneToMany annotation
      txt += `\n\n\tpublic function ${tableDistant}(): BelongsToMany {`;
      txt += `\n\t\treturn $this->belongsToMany(${classNameDistant}::class, "${rFKey.tableName}", "${rFKey.columnName}", "${refColumn}")`;
      txt += `->using(${className}::class);`;
      txt += "\n\t}";
      // Add fields
      rExtraField.fieldsTxt += txt;
      // Add imports (code commented below, it is not needed cause we are in the same namespace)
      // rExtraField.addImport(`use \\App\\${this.getProcessorFolder()}\\${className};`);
    }
    return txt;
  }
}
