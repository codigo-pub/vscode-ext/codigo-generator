/* eslint-disable @typescript-eslint/naming-convention */
import { GenericDbField } from "../../db/model/GenericDbField";
import { AbstractProcessor } from "../AbstractProcessor";
import { IConverter } from "../IConverter";
import { PhpConverter } from "./PhpConverter";

export abstract class AbstractPhpProcessor extends AbstractProcessor {
  protected readonly PREFIX_VAR = "$";
  protected readonly OPTIONAL_CHAR = "?";

  protected getConverter(): IConverter {
    return new PhpConverter();
  }

  /**
   * Get phpdoc param part '@param <type> <param name>
   * @param rFieldType : type of the field
   * @param rFieldName : name of the field
   */
  protected addParamDoc(rFieldType: string, rFieldName: string): string {
    return `\t * @param ${rFieldType} ${rFieldName}`;
  }

  /**
   * Get optional field char, represents an optional field
   * @param rField : field
   * @returns optional field char
   */
  protected getOptionalChar(rField: GenericDbField): string {
    return rField?.isNullable ? this.OPTIONAL_CHAR : "";
  }

  /**
   * Takes an import name and generates the import call
   * @param rImportName : import name, e.g. 'DateTime'
   * @returns import call given that name (e.g. 'use DateTime;')
   */
  protected getImportFrom(rImportName: string): string {
    return rImportName ? `use ${rImportName};` : "";
  }
}
