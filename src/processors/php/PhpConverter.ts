import { EnumDbType } from "../../db/model/EnumDbType";
import { GenericDbTypeField } from "../../db/model/GenericDbTypeField";
import { SqlConvertEnumDbType } from "../../model/SqlConvertEnumDbType";
import { AbstractConverter } from "../AbstractConverter";


export class PhpConverter extends AbstractConverter {
  readonly typeMappings: SqlConvertEnumDbType[] = [

    { sqlType: EnumDbType.char, targetType: "string" },
    { sqlType: EnumDbType.varchar, targetType: "string" },
    { sqlType: EnumDbType.uuid, targetType: "string" },
    { sqlType: EnumDbType.longtext, targetType: "string" },
    { sqlType: EnumDbType.enum, targetType: "string" },

    { sqlType: EnumDbType.long, targetType: "int" },
    { sqlType: EnumDbType.int, targetType: "int" },
    { sqlType: EnumDbType.numeric, targetType: "float" },
    { sqlType: EnumDbType.float, targetType: "float" },
    { sqlType: EnumDbType.double, targetType: "float" },

    { sqlType: EnumDbType.date, targetType: "DateTime", import: ["DateTime"] },
    { sqlType: EnumDbType.datetime, targetType: "DateTime", import: ["DateTime"] },
    { sqlType: EnumDbType.timestamp, targetType: "DateTime", import: ["DateTime"] },
    { sqlType: EnumDbType.time, targetType: "DateTime", import: ["DateTime"] },

    { sqlType: EnumDbType.interval, targetType: "DateInterval", import: ["DateInterval"] },

    { sqlType: EnumDbType.boolean, targetType: "bool" },

    { sqlType: EnumDbType.blob, targetType: "string" },
    
    { sqlType: EnumDbType.json, targetType: "string" },

    { sqlType: EnumDbType.array, targetType: "array" },
  ];

  /**
   * Translate sql type to TypeScript type
   * @param {JSON} rField : Field
   * @returns typescript type
   */
  fieldConvertToType(rField?: GenericDbTypeField): SqlConvertEnumDbType | undefined {
    return this.fieldConvertFromMapping(rField, this.typeMappings);
  }
}
