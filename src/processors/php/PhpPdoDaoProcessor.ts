/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractPhpProcessor } from "./AbstractPhpProcessor";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { ExtraField } from "../../model/ExtraField";
import { PhpModelProcessor } from "./PhpModelProcessor";
import { GeneratorTool } from "../../tools/GeneratorTool";

export class PhpPdoDaoProcessor extends AbstractPhpProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "repository";
  }
  
  getProcessorName(): string {
    return "pdo_dao";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0) {
      var _this = this;
      // We only need pkey fields
      const pkeyFields = GeneratorTool.getPkeyFields(rTable.fields);
      const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix

      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_pdo_dao_php", "utf8");

      // FileName
      var fileName = rTable.tableName.toLowerCase();

      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var className = `${classModelName}Dao`;
      var pkeyToInject = "";
      var pkeySqlToInject = "";
      var pkeySqlBindToInject = "[";
      var paramList = "";
      var extraFields = new ExtraField();

      // Add import for model classname
      extraFields.addImport(`use ${this.config.packageBaseName ?? ''}\\Model\\${classModelName};`);

      for (const fielddata of pkeyFields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var fieldType = _this.converter.fieldConvertToType(fielddata.type);
          // check if import needed
          this.addImportForField(extraFields, fieldType);

          paramList += (paramList !== "" ? "\n" : "") + this.addParamDoc(fieldType?.targetType ?? "", `${this.PREFIX_VAR}${paramPrefix}${fieldName}`);
          // we don't check if pkey cause all are, we set pkey fields for xxxById...
          // if one more field
          if (pkeyToInject !== "") {
            pkeyToInject += ", ";
            pkeySqlToInject += "and ";
            pkeySqlBindToInject += ", ";
          }
          pkeyToInject += `${fieldType?.targetType} ${this.PREFIX_VAR}${paramPrefix}${fieldName}`;
          pkeySqlToInject += `${fieldName} = ? `;
          pkeySqlBindToInject += `${this.PREFIX_VAR}${paramPrefix}${fieldName}`;
        }
      }

      // close array of pkey bindings
      pkeySqlBindToInject += "]";

      const modelTemplate = {
        tableName: rTable.tableName,
        classModelName: classModelName,
        className: className,
        packageBaseName: this.config.packageBaseName ?? '',
        filename: fileName,
        pkeyToInject: pkeyToInject,
        pkeySqlToInject: pkeySqlToInject,
        pkeySqlBindToInject: pkeySqlBindToInject,
        paramPrefix: paramPrefix,
        paramList: paramList,
        extraFields_import: extraFields.getImportsTxt(),
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }
}
