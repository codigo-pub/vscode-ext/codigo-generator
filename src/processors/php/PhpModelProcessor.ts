/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractPhpProcessor } from "./AbstractPhpProcessor";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { ExtraField } from "../../model/ExtraField";
import { GeneratorTool } from "../../tools/GeneratorTool";

export class PhpModelProcessor extends AbstractPhpProcessor {
  
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "models";
  }
  
  getProcessorName(): string {
    return "model";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0) {
      var _this = this;

      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_model_php", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var constructorToInject = "";
      var constructorInToInject = "";
      var gettersAndSetters = "";
      var extraFields = new ExtraField();

      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var fieldType = _this.converter.fieldConvertToType(fielddata.type);
          // check if import needed
          this.addImportForField(extraFields, fieldType);

          var nullable = this.getOptionalChar(fielddata);
          // inject field
          fieldsToInject += `private ${nullable}${fieldType?.targetType ?? ""} ${this.PREFIX_VAR}${fieldName};\n\t`;

          // getters and setters
          gettersAndSetters += this.generateGetterSettersForField(fieldName, fieldType?.targetType ?? "", nullable);

          // if primary key, we set constructor fields
          if (fielddata.isPKey) {
            if (constructorToInject !== "") {
              constructorToInject += ", "; //if one more to constructor
            }
            const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
            constructorToInject += `${fieldType?.targetType} ${this.PREFIX_VAR}${paramPrefix}${fieldName}`;
            constructorInToInject += `\n\t\t${this.PREFIX_VAR}this->${fieldName} = ${this.PREFIX_VAR}${paramPrefix}${fieldName};`;
          }
        }
      }

      const modelTemplate = {
        className: className,
        packageBaseName: this.config.packageBaseName,
        fields: fieldsToInject,
        constructor: constructorToInject,
        in_constructor: constructorInToInject,
        gettersAndSetters: gettersAndSetters,
        extraFields_import: extraFields.getImportsTxt(),
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.php`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  private generateGetterSettersForField(rFieldName: string, rFieldType: string, rNullable: string): string {
    let upName = GeneratorTool.upperFirstLetter(rFieldName);
    let getter = `\tpublic function get${upName}(): ${rFieldType}
    {
        return $this->${rFieldName};
    }`;
    let setter = `\tpublic function set${upName}(${rNullable}${rFieldType} ${this.PREFIX_VAR}${rFieldName}): void
    {
        $this->${rFieldName} = ${this.PREFIX_VAR}${rFieldName};
    }`;
    return getter + "\n\n" + setter + "\n\n";
  }
}
