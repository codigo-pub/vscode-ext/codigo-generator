/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractCsharpProcessor } from "./AbstractCsharpProcessor";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { ExtraField } from "../../model/ExtraField";
import { GeneratorTool } from "../../tools/GeneratorTool";

export class CsharpModelProcessor extends AbstractCsharpProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "Models";
  }
  
  getProcessorName(): string {
    return "model";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0) {
      var _this = this;

      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_model_csharp", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var extraFields = new ExtraField();
      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var myfieldType = _this.converter.fieldConvertToType(fielddata.type);
          if (myfieldType) {
            const fieldType = myfieldType.targetType;
            // Do we need to import something ?
            if (myfieldType.import) {
              this.addImportForField(extraFields, myfieldType);
            }
            var nullable = this.getOptionalChar(fielddata);
            // inject field
            fieldsToInject += `public ${fieldType ?? ""}${nullable} ${fieldName} { get; set; }\n\t`;
          }
        }
      }

      const modelTemplate = {
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.getProcessorFolder(),
        fields: fieldsToInject,
        extraFields_import: extraFields.getImportsTxt()
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.cs`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

}
