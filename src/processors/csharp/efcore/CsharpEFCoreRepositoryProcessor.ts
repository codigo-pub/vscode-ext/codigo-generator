/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { ExtraField } from "../../../model/ExtraField";
import { AbstractCsharpEFCoreProcessor } from "./AbstractCsharpEFCoreProcessor";
import { CsharpModelProcessor } from "../CsharpModelProcessor";
import { StaticFile } from "../../../model/StaticFile";

export class CsharpEFCoreRepositoryProcessor extends AbstractCsharpEFCoreProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private entityProcessor?: CsharpModelProcessor;
  private many_to_many_sep?: string;

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return "Repositories";
  }

  getProcessorName(): string {
    return "efcore_repository";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of CsharpEntityEntityProcessor to ask for his folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new CsharpModelProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    // Set isAsync variable from processor config
    this.setAsyncFromConfig();
    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);
    // create directory if needed (cause we need abstract files)
    this.initFolders();
    // Write abstract files needed for repositories
    this.writeAbstractRepository();
    // run process
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
    // CAREFUL : we ignore manyToManyTables on purpose
    if (rTable?.fields && this.entityProcessor && !this.isSimpleManyToManyTable(rTable, this.many_to_many_sep)) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_efcore_repository_csharp", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var className = `${classModelName}Repository`;
      var pkeyType = "";
      var mainExtraFields = new ExtraField();

      // Browse ManyToMany referenced keys to add some cool methods
      const extraMethods = new ExtraField();
      if (rTable?.referencedFKeys && this.allTables) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          this.generateFindBy(rTable, this.allTables, fielddata, extraMethods);
        }
      }

      // Add imports from extraMethods if needed
      mainExtraFields.addImports(extraMethods.importTxt);

      // Add import for async Task if needed
      this.addImportAsyncIfNeeded(mainExtraFields);

      const modelTemplate = {
        classModelName: classModelName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        repositoryFolder: this.getProcessorFolder(),
        extraFields_import: mainExtraFields.getImportsTxt(),
        pkeyType: pkeyType,
        extraMethods: extraMethods.fieldsTxt,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.cs`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Write IAbstractRepository and AbstractRepository files
   */
  private writeAbstractRepository() {
    const abstractFiles: StaticFile[] = this.asyncCfg.isAsync
      ? [
          { src: "tpl_efcore_irepository_async_csharp", dest: "IAbstractRepository.cs" },
          { src: "tpl_efcore_abstract_repository_async_csharp", dest: "AbstractRepository.cs" },
        ]
      : [
          { src: "tpl_efcore_irepository_csharp", dest: "IAbstractRepository.cs" },
          { src: "tpl_efcore_abstract_repository_csharp", dest: "AbstractRepository.cs" },
        ];
    for (const abstractFile of abstractFiles) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + abstractFile.src, "utf8");
      // Now we inject
      var dataTpl = this.supplant.text(tpl, {
        packageBaseName: this.config.packageBaseName,
        repositoryFolder: this.getProcessorFolder(),
        modelFolder: this.entityProcessor?.getProcessorFolder(),
      });
      //Write to file
      fs.writeFileSync(`${this.getOutProcessorPath()}${abstractFile.dest}`, dataTpl, "utf8");
    }
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(rTable: GenericDbTable, rAllTables: GenericDbTable[], rFKey: GenericDbFKey, rExtraMethods: ExtraField) {
    const dataRelation = GeneratorTool.getInfoFindBy({ rTable, rAllTables, rFKey, rProcessor: this, rManyToManySep: this.many_to_many_sep });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }

      // Search for pivot table on all tables cause we want to know if it was skipped
      // If pivot table was not skipped it means a model exist and so query must be different !
      const pivotTable = rAllTables.find((t) => t.tableName === dataRelation.tablePivotName);
      const pivotWasSkipped = pivotTable && this.isSimpleManyToManyTable(pivotTable, this.many_to_many_sep);

      const refColumn = GeneratorTool.stringToCamel({ rText: dataRelation.pivotDistantPkeyName ?? "", rFirstLower: true });
      const refColumnInModel = GeneratorTool.stringToCamel({ rText: refColumn ?? "" });
      const distantPkeyName = GeneratorTool.stringToCamel({ rText: dataRelation.distantPkeyField?.fieldName ?? "" });
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableNameUp ?? "", dataRelation.classNameDistant ?? "");
      // Add text
      rExtraMethods.fieldsTxt += `\n\t/// <summary>
\t/// Get all ${dataRelation.tableNameUp} by ${dataRelation.classNameDistant} id
\t/// </summary>
\t/// <param name="${refColumn}">${dataRelation.classNameDistant} id</param>
\t/// <returns>Array of ${dataRelation.tableName}</returns>
\tpublic ${this.asyncCfg.asyncWrapper1}${this.asyncCfg.asyncWrapper2}List<${dataRelation.className}>${this.asyncCfg.asyncWrapper3} ${methodName}(${typePkey?.targetType} ${refColumn}) {`;
      // If pivot table was skipped, we can rely on distant model otherwise we must pass through pivot table
      if (pivotWasSkipped) {
        rExtraMethods.fieldsTxt += `\n\t\treturn ${this.asyncCfg.asyncWrapper4}_context.${GeneratorTool.stringToCamel({ rText: dataRelation.tableDistant?.tableName ?? "" })}
\t\t\t.Where(s => s.${distantPkeyName} == ${refColumn})
\t\t\t.SelectMany(s => s.${GeneratorTool.stringToCamel({ rText: dataRelation.tableName ?? "" })})`;
      } else {
        rExtraMethods.fieldsTxt += `\n\t\treturn ${this.asyncCfg.asyncWrapper4}_context.${GeneratorTool.stringToCamel({ rText: dataRelation.tablePivotName ?? "" })}
\t\t\t.Where(s => s.${GeneratorTool.stringToCamel({ rText: dataRelation.pivotDistantPkeyName ?? "" })} == ${refColumn})
\t\t\t.Select(s => s.${GeneratorTool.stringToCamel({ rText: dataRelation.tableName ?? "" })})`;
      }
      rExtraMethods.fieldsTxt += `\n\t\t\t.ToList${this.asyncCfg.asyncWrapper5}();`;
      rExtraMethods.fieldsTxt += "\n\t}";
    }
  }
}
