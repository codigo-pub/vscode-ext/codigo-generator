import { EnumDbType } from "../../../db/model/EnumDbType";
import { SqlConvertEnumDbType } from "../../../model/SqlConvertEnumDbType";
import { CsharpConverter } from "../CsharpConverter";


export class CsharpEFCoreConverter extends CsharpConverter {

  readonly typeMappings: SqlConvertEnumDbType[] = [
    { sqlType: EnumDbType.char, targetType: "string" },
    { sqlType: EnumDbType.varchar, targetType: "string" },
    { sqlType: EnumDbType.uuid, targetType: "string" },
    { sqlType: EnumDbType.longtext, targetType: "string" },
    { sqlType: EnumDbType.enum, targetType: "string" },

    { sqlType: EnumDbType.long, targetType: "ulong" },
    { sqlType: EnumDbType.int, targetType: "int" },
    { sqlType: EnumDbType.numeric, targetType: "int" },
    { sqlType: EnumDbType.float, targetType: "float" },
    { sqlType: EnumDbType.double, targetType: "double" },

    { sqlType: EnumDbType.date, targetType: "DateOnly", import: ["System"] },
    { sqlType: EnumDbType.datetime, targetType: "DateTime", import: ["System"] },
    { sqlType: EnumDbType.timestamp, targetType: "DateTime", import: ["System"] },
    { sqlType: EnumDbType.time, targetType: "TimeSpan", import: ["System"] },

    { sqlType: EnumDbType.interval, targetType: "TimeSpan", import: ["System"] },

    { sqlType: EnumDbType.boolean, targetType: "bool" },

    { sqlType: EnumDbType.blob, targetType: "byte[]" },

    { sqlType: EnumDbType.json, targetType: "string" },

    { sqlType: EnumDbType.array, targetType: "List<string>", import: ["System.Collections.Generic"] },
  ];

}
