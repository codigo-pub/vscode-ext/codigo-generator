/* eslint-disable @typescript-eslint/naming-convention */
import { EnumDbType } from "../../../db/model/EnumDbType";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { AbstractCsharpProcessor } from "../AbstractCsharpProcessor";

export abstract class AbstractCsharpEFCoreProcessor extends AbstractCsharpProcessor {

  // All templates are expected in this sub-folder
  protected templateFolder?: string = "efcore";

  /**
   * return true if we need to show the length in column property (ex: @Column(length= 255))
   * @param rType : EnumDbType
   * @returns
   */
  protected showLength(rType?: EnumDbType): boolean {
    switch (rType) {
      case EnumDbType.varchar:
      case EnumDbType.char:
      case EnumDbType.longtext:
        return true;
      default:
        return false;
    }
  }

  /**
   * Override way of getting classname from table to suit efcore way of naming models class
   * @param rTable : table name
   * @returns classname (e.g. classrooms_has_students will return ClassroomsHasStudent)
   */
  override processClassNameFromTable(rTable: string): string {
    return GeneratorTool.processClassNameFromTableCamel(rTable);
  }

  /**
   * Generate fieldname based on tablename
   * We use this for many relations OneToMany and ManyToOne
   * @param rTablename : tablename
   * @returns fieldname
   */
  override getFieldNameMany(rTablename: string) {
    return GeneratorTool.stringToCamel({ rText: rTablename});
  }
}
