/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractCsharpEFCoreProcessor } from "./AbstractCsharpEFCoreProcessor";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { IExtraFieldProcessor } from "../../IExtraFieldProcessor";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { SqlTool } from "../../../tools/SqlTool";

export class CsharpEFCoreModelProcessor extends AbstractCsharpEFCoreProcessor implements IExtraFieldProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private many_to_many_sep?: string;

  getProcessorFolder(): string {
    return "Models";
  }

  getProcessorName(): string {
    return "efcore_model";
  }

  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    // CAREFUL : we ignore manyToManyTables on purpose
    if (rTable?.fields && rTable.fields.length > 0 && !this.isSimpleManyToManyTable(rTable, this.many_to_many_sep)) {
      var _this = this;

      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_efcore_model_csharp", "utf8");

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var extraFields = new ExtraField();
      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          // First convert to Camel
          fieldName = GeneratorTool.stringToCamel({ rText: fieldName });
          var myfieldType = _this.converter.fieldConvertToType(fielddata.type);
          if (myfieldType) {
            const fieldType = myfieldType.targetType;
            // Do we need to import something ?
            if (myfieldType.import) {
              this.addImportForField(extraFields, myfieldType);
            }
            var nullable = this.getOptionalChar(fielddata);
            // if type is 'string' and field is not null we must give null value (yes i know it's weird)
            var forceNullValue = !fielddata.isNullable && myfieldType.targetType === "string" ? " = null!;" : "";
            // inject field
            fieldsToInject += `public ${fieldType ?? ""}${nullable} ${fieldName} { get; set; }${forceNullValue}\n\t`;
          }
        }
      }

      // Extra fields (OneToMany/ManyToOne)
      const extraFieldsFKeys = GeneratorTool.getExtraFields(rTable, rTable.fKeys ?? [], this);
      const extraFieldsRefFkeys = GeneratorTool.getExtraFields(rTable, rTable.referencedFKeys ?? [], this, true);
      extraFields.addImports(extraFieldsFKeys.importTxt);
      extraFields.addImports(extraFieldsRefFkeys.importTxt);
      extraFields.fieldsTxt = (extraFieldsFKeys.fieldsTxt ?? "") + (extraFieldsRefFkeys.fieldsTxt ?? "");
      extraFields.gettersSettersTxt = (extraFieldsFKeys.gettersSettersTxt ?? "") + (extraFieldsRefFkeys.gettersSettersTxt ?? "");

      const modelTemplate = {
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.getProcessorFolder(),
        fields: fieldsToInject,
        extraFields_fields: extraFields.fieldsTxt,
        extraFields_import: extraFields.getImportsTxt(),
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.cs`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Process extra field
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rReferenced : true if those fields are referenced fields, false if they are only foreign keys on the current table
   * @returns extra field with texts
   */
  processExtraField(rTable: GenericDbTable, rFKey: GenericDbFKey, rReferenced: boolean = false): ExtraField {
    let extraField = new ExtraField();
    if (rFKey.tableName) {
      extraField.fieldsTxt = rReferenced ? this.getOrmOneToMany(rTable, rFKey, extraField) : this.getOrmManyToOne(rFKey);
      // detect if this is a ManyToMany relation for a referenced field
      if (rReferenced && SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep)) {
        this.getOrmManyToMany(rTable, rFKey, extraField);
      }
    }
    return extraField;
  }

  /**
   * Get ORM representation of many to one for this foreign key
   * Note that we are in same namespace so no need to import model referenced
   * @param rFKey : Generic Foreign key object
   * @returns
   */
  protected getOrmManyToOne(rFKey: GenericDbFKey): string {
    let txt = "";
    if (rFKey?.referencedTable) {
      let className = this.processClassNameFromTable(rFKey.referencedTable);
      txt += "\n\t" + this.getFieldDeclaration(className, className);
    }
    return txt;
  }

  /**
   * Get ORM representation of one to many for this foreign key
   * Note that we are in same namespace so no need to import model referenced
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rExtraFields : Extra fields
   * @returns
   */
  protected getOrmOneToMany(rTable: GenericDbTable, rFKey: GenericDbFKey, rExtraFields: ExtraField): string {
    let txt = "";
    if (rFKey?.tableName && rFKey?.referencedTable && !SqlTool.isManyToManyTable(rFKey.tableName, this.many_to_many_sep)) {
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(rFKey.tableName);
      let fieldName = this.getFieldNameMany(rFKey.tableName);
      txt += "\n\t" + this.getFieldListDeclaration(className, fieldName);
      rExtraFields.addImport(this.getImportFrom("System.Collections.Generic"));
    }
    return txt;
  }

  /**
   * Get ORM representation of many to many through pivot table for this foreign key
   * Note that we are in same namespace so no need to import model referenced
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rExtraFields : extrafield to add imports/fields/getters/setters
   * @returns
   */
  protected getOrmManyToMany(rTable: GenericDbTable, rFKey: GenericDbFKey, rExtraFields: ExtraField) {
    let txt = "";
    if (rFKey?.tableName) {
      if (!rExtraFields.fieldsTxt) {
        rExtraFields.fieldsTxt = "";
      }
      let txt = "";
      // Get table referencing the fkey
      // let className = this.processClassNameFromTable(rFKey.tableName);
      let tableDistant = SqlTool.extractOtherTableName(rFKey.tableName, rTable.tableName, this.many_to_many_sep);
      let classNameDistant = this.processClassNameFromTable(tableDistant);
      let fieldName = this.getFieldNameMany(tableDistant);
      txt += "\n\t" + this.getFieldListDeclaration(classNameDistant, fieldName);
      rExtraFields.addImport(this.getImportFrom("System.Collections.Generic"));
      // Add fields
      rExtraFields.fieldsTxt += txt;
    }
    return txt;
  }
  /**
   * Create field text, e.g : 'public virtual Status Status { get; set; } = null!;'
   * @param rFieldType : field type
   * @param rFieldname : field name
   * @param rOptional : [optional] true to set this field as optional, false otherwise. Default to false.
   * @returns
   */
  private getFieldDeclaration(rFieldType: string | undefined, rFieldname: string, rOptional: boolean = false): string {
    return `public virtual ${rFieldType}${rOptional ? this.OPTIONAL_CHAR : ""} ${rFieldname} { get; set; } = null!;`;
  }

  /**
   * Create field list declaration text, e.g : 'public virtual ICollection<Classroom> Classrooms { get; set; } = new List<Classroom>();'
   * @param rFieldType : field type
   * @param rFieldname : field name
   * @param rOptional : [optional] true to set this field as optional, false otherwise. Default to false.
   * @returns
   */
  private getFieldListDeclaration(rFieldType: string | undefined, rFieldname: string, rOptional: boolean = false): string {
    // note : for initialization, we use '= []' instead of '= new List<${rFieldType}>()'
    return `public virtual ICollection<${rFieldType}>${rOptional ? this.OPTIONAL_CHAR : ""} ${rFieldname} { get; set; } = [];`;
  }
}
