/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { CsharpEFCoreRepositoryProcessor } from "../efcore/CsharpEFCoreRepositoryProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { ExtraField } from "../../../model/ExtraField";
import { CsharpModelProcessor } from "../CsharpModelProcessor";
import { AbstractCsharpNetProcessor } from "./AbstractCsharpNetProcessor";

export class CsharpNetServiceProcessor extends AbstractCsharpNetProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private entityProcessor?: CsharpModelProcessor;
  private repositoryProcessor?: CsharpEFCoreRepositoryProcessor;
  private many_to_many_sep?: string;

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return "Services";
  }

  getProcessorName(): string {
    return "net_service";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new CsharpModelProcessor(this.config, this.configProcessor, this.db);
    this.repositoryProcessor = new CsharpEFCoreRepositoryProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    // Set isAsync variable from processor config
    this.setAsyncFromConfig();

    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix

    // CAREFUL : we ignore manyToManyTables on purpose
    if (rTable?.fields && this.entityProcessor && this.repositoryProcessor && !this.isSimpleManyToManyTable(rTable, this.many_to_many_sep)) {
      // Load template (async or sync depend on processor config)
      const _Async = this.asyncCfg.isAsync ? "async_" : "";
      var tpl = fs.readFileSync(this.getTemplateBasePath() + `tpl_net_service_${_Async}csharp`, "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classRepositoryName = `${classModelName}Repository`;
      var className = `${classModelName}Service`;
      var pkeyFieldName = "";
      var pkeyType = "";
      var mainExtraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // FIXME : remove comments and fix or remove block if not needed
      // // If composite primary key
      // if (pKeys && pKeys.length >= 1) {
      //   if (pKeys && pKeys.length > 1) {
      //     // Get composite id classname
      //     pkeyType = CsharpEFCoreEntityCompositeKeyProcessor.getClassName(classModelName);
      //     const basePackage = this.config.packageBaseName ?? "";
      //     mainExtraFields.addImport(`\nimport ${basePackage === "" ? "" : basePackage + "."}${this.entityProcessor.getProcessorFolder()}.${pkeyType};`);
      //     pkeyFieldName = "id";
      //   } else {
      //     // If we come here, there is only one pkey (most frequent use case)
      //     // Starts by getting pkey type
      //     const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
      //     pkeyType = myPkeyType?.targetType ?? "";
      //     pkeyFieldName = pKeys[0].fieldName ?? "id";
      //     this.addImportForField(mainExtraFields, myPkeyType); // add import if needed
      //   }
      // }

      // Browse ManyToMany referenced keys to add some cool methods
      const extraMethods = new ExtraField();
      if (rTable?.referencedFKeys && this.allTables) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          this.generateFindBy(rTable, this.allTables, fielddata, extraMethods, classModelName);
        }
      }

      // Add imports from extraMethods if needed
      mainExtraFields.addImports(extraMethods.importTxt);

      // Add import for async Task if needed
      this.addImportAsyncIfNeeded(mainExtraFields);

      const modelTemplate = {
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classRepositoryName: classRepositoryName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        repositoryFolder: this.repositoryProcessor.getProcessorFolder(),
        paramPrefix: paramPrefix,
        extraFields_import: mainExtraFields.getImportsTxt(),
        pkeyType: pkeyType,
        pkeyFieldName: GeneratorTool.upperFirstLetter(pkeyFieldName),
        extraMethods: extraMethods.fieldsTxt,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.cs`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(
    rTable: GenericDbTable,
    rAllTables: GenericDbTable[],
    rFKey: GenericDbFKey,
    rExtraMethods: ExtraField,
    rClassModelName: string
  ) {
    const dataRelation = GeneratorTool.getInfoFindBy({ rTable, rAllTables, rFKey, rProcessor: this, rManyToManySep: this.many_to_many_sep });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }
      const refColumn = GeneratorTool.stringToCamel({ rText: dataRelation.pivotDistantPkeyName ?? "", rFirstLower: true });
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Repository variable (see in template)
      const repoVariable = `_${rClassModelName.toLowerCase()}Repository`;
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableNameUp ?? "", dataRelation.classNameDistant ?? "");
      // Add text
      rExtraMethods.fieldsTxt += `\n\t/// <summary>
\t/// Get all ${dataRelation.tableNameUp} by ${dataRelation.classNameDistant} id
\t/// </summary>
\t/// <param name="${refColumn}">${dataRelation.classNameDistant} id</param>
\t/// <returns>Array of ${dataRelation.tableName}</returns>
\tpublic ${this.asyncCfg.asyncWrapper1}${this.asyncCfg.asyncWrapper2}List<${dataRelation.className}>${this.asyncCfg.asyncWrapper3} ${methodName}(${typePkey?.targetType} ${refColumn}) {
\t\treturn ${this.asyncCfg.asyncWrapper4}${repoVariable}.${methodName}(${refColumn});
\t}
`;
    }
  }
}
