/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractCsharpProcessor } from "../AbstractCsharpProcessor";

export abstract class AbstractCsharpNetProcessor extends AbstractCsharpProcessor {

  // All templates are expected in this sub-folder
  protected templateFolder?: string = "net";

}
