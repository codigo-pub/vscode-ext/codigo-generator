/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { CsharpNetServiceProcessor } from "./CsharpNetServiceProcessor";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { CsharpModelProcessor } from "../CsharpModelProcessor";
import { AbstractCsharpNetProcessor } from "./AbstractCsharpNetProcessor";

export class CsharpNetControllerProcessor extends AbstractCsharpNetProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private entityProcessor?: CsharpModelProcessor;
  private serviceProcessor?: CsharpNetServiceProcessor;
  private many_to_many_sep?: string;
  private baseApi?: string = ""; // base api
  private paramPrefix?: string;

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return "Controllers";
  }

  getProcessorName(): string {
    return "net_controller";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new CsharpModelProcessor(this.config, this.configProcessor, this.db);
    this.serviceProcessor = new CsharpNetServiceProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    this.baseApi = this.configProcessor?.extra_config?.base_api ?? "api"; // <== leading slash is omitted because it's implied
    this.paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
    // Set isAsync variable from processor config
    this.setAsyncFromConfig();
    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    // CAREFUL : we ignore manyToManyTables on purpose
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor && !this.isSimpleManyToManyTable(rTable, this.many_to_many_sep)) {
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // Process data
        if (pKeys && pKeys.length > 1) {
          await this.processTableWithCompositePKey(rTable);
        } else {
          await this.processTableWithSinglePkey(rTable);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Load template
      const _Async = this.asyncCfg.isAsync ? "async_" : "";
      var tpl = fs.readFileSync(this.getTemplateBasePath() + `tpl_net_controller_${_Async}csharp`, "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classServiceName = `${classModelName}Service`;
      var className = `${classModelName}Controller`;
      var pkeyType = "";
      var pkeyImport = "";
      var pkeyName = "";
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // If we come here, there is only one pkey (most frequent use case)
      // Starts by getting pkey type
      const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
      pkeyType = myPkeyType?.targetType ?? "";
      // Convert pkey name to efcore standard 
      pkeyName = GeneratorTool.stringToCamel({rText: pKeys[0].fieldName ?? ''});
      // Add import if needed
      this.addImportForField(extraFields, myPkeyType);

      // Browse ManyToMany referenced keys to add some cool methods
      const extraMethods = new ExtraField();
      if (rTable?.referencedFKeys && this.allTables) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          this.generateFindBy(rTable, this.allTables, fielddata, extraMethods);
        }
      }

      // Add imports from extraMethods if needed
      extraFields.addImports(extraMethods.importTxt);

      // Add import for async Task if needed
      this.addImportAsyncIfNeeded(extraFields);

      const modelTemplate = {
        baseApi: this.baseApi,
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classServiceName: classServiceName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        serviceFolder: this.serviceProcessor.getProcessorFolder(),
        controllerFolder: this.getProcessorFolder(),
        pkeyImport: pkeyImport,
        pkeyType: pkeyType,
        pkeyName: pkeyName,
        extraFields_import: extraFields.getImportsTxt(),
        extraMethods: extraMethods.fieldsTxt,
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.cs`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   */
  protected async processTableWithCompositePKey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Load template
      const _Async = this.asyncCfg.isAsync ? "async_" : "";
      var tpl = fs.readFileSync(this.getTemplateBasePath() + `tpl_net_controller_${_Async}c_csharp`, "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classServiceName = `${classModelName}Service`;
      var className = `${classModelName}Controller`;
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pkeyFields = this.getPkeyCompositeFields(GeneratorTool.getPkeyFields(rTable.fields), extraFields, classModelName);

      // Add import for async Task if needed
      this.addImportAsyncIfNeeded(extraFields);
      
      // Now we inject
      const modelTemplate = {
        baseApi: this.baseApi,
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classServiceName: classServiceName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        serviceFolder: this.serviceProcessor.getProcessorFolder(),
        controllerFolder: this.getProcessorFolder(),
        pkey_csharpdoc: pkeyFields.csharpdoc,
        pkey_parameter: pkeyFields.parameter,
        pkey_field: pkeyFields.field,
        pkey_url_parameter: pkeyFields.url_parameter,
        pkey_id_set_expected: pkeyFields.id_set_expected,
        pkey_id_set_actual: pkeyFields.id_set_actual,
        extraFields_import: extraFields.getImportsTxt(),
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.cs`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generate different texts that are necessary for template based on pkey fields
   * @param rFields : pkey fields
   * @param rExtraFields : extraField object for extra imports, etc.
   * @param rClassModelName : Class model name that is related (we will put it in lowercase cause it's the expected parameter)
   * @returns data for template
   */
  private getPkeyCompositeFields(
    rFields: GenericDbField[],
    rExtraFields: ExtraField,
    rClassModelName: string
  ): { csharpdoc: string; parameter: string; field: string; url_parameter: string; id_set_expected: string; id_set_actual: string } {
    let data = { csharpdoc: "", parameter: "", field: "", url_parameter: "", id_set_expected: "", id_set_actual: "" };
    if (rFields && rFields.length > 0) {
      // Browse pkfields to add them in text
      for (const field of rFields) {
        if (field?.fieldName) {
          // Get field type
          const fieldType = this.converter.fieldConvertToType(field.type);
          // Add import if needed
          this.addImportForField(rExtraFields, fieldType);
          // Get type
          const type = fieldType?.targetType ?? "";
          // Add code data
          data.csharpdoc += (data.parameter !== "" ? "\n" : "") + `\t/// <param name="${field.fieldName}">${field.fieldName}</param>`;
          data.parameter += (data.parameter !== "" ? ", " : "") + `${type} ${field.fieldName}`;
          data.field += (data.field !== "" ? ", " : "") + `${field.fieldName}`;
          data.url_parameter += (data.url_parameter !== "" ? "/" : "") + `{${field.fieldName}}`;
          // extract setter
          const memberName = GeneratorTool.stringToCamel({rText: field.fieldName ?? ''});
          data.id_set_expected += (data.id_set_expected !== "" ? ", " : "") + `${rClassModelName.toLowerCase()}.${memberName}`;
          data.id_set_actual += (data.id_set_actual !== "" ? ", " : "") + `${memberName} = ${field.fieldName}`;
        }
      }
    }
    return data;
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(
    rTable: GenericDbTable,
    rAllTables: GenericDbTable[],
    rFKey: GenericDbFKey,
    rExtraMethods: ExtraField,
    rPart?: "left" | "right"
  ) {
    const dataRelation = GeneratorTool.getInfoFindBy({ rTable, rAllTables, rFKey, rProcessor: this, rManyToManySep: this.many_to_many_sep });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }
      let refColumn = dataRelation.pivotDistantPkeyName;
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableNameUp ?? "", dataRelation.classNameDistant ?? "");
      // Add text
      rExtraMethods.fieldsTxt += `\n\t/// <summary>
\t/// Get all ${dataRelation.tableNameUp} by ${dataRelation.classNameDistant} id
\t/// </summary>
\t/// <param name="${refColumn}">${dataRelation.classNameDistant} id</param>
\t/// <returns>A collection of ${dataRelation.tableName} instances</returns>
\t[HttpGet("${dataRelation.classNameDistant?.toLowerCase()}/{${refColumn}}")]
\tpublic ${this.asyncCfg.asyncWrapper1}${this.asyncCfg.asyncWrapper2}IEnumerable<${dataRelation.className}>${this.asyncCfg.asyncWrapper3} ${methodName}(${typePkey?.targetType} ${refColumn})
\t{
\t\treturn ${this.asyncCfg.asyncWrapper4}_${dataRelation.className?.toLowerCase()}Service.${methodName}(${refColumn});
\t}
`;
    }
  }
}
