/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { ExtraField } from "../../../model/ExtraField";
import { CsharpModelProcessor } from "../CsharpModelProcessor";
import { CsharpNetServiceProcessor } from "./CsharpNetServiceProcessor";
import { CsharpEFCoreRepositoryProcessor } from "../efcore/CsharpEFCoreRepositoryProcessor";
import { AbstractCsharpNetProcessor } from "./AbstractCsharpNetProcessor";

export class CsharpNetProgramProcessor extends AbstractCsharpNetProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private readonly FILE_NAME = "Program.cs";
  private many_to_many_sep?: string;

  getProcessorFolder(): string {
    return ""; // at root path
  }

  getProcessorName(): string {
    return "net_program";
  }

  /**
   * Not implemented as this processor gather all tables to process them in one file !
   */
  protected processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    const entityProcessor = new CsharpModelProcessor(this.config, this.configProcessor, this.db);
    const repositoryProcessor = new CsharpEFCoreRepositoryProcessor(this.config, this.configProcessor, this.db);
    const serviceProcessor = new CsharpNetServiceProcessor(this.config, this.configProcessor, this.db);

    // As we are not calling defaultprocess method, we need to init services and create directory if needed
    this.init();
    this.initFolders();
    // Get all tables (filtered based on configuration)
    const tables = await this.getAllTables();
    let extraFields = new ExtraField();
    // Load template
    var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_net_program_csharp", "utf8");
    const tableListClass = [];
    if (tables && tables.length > 0) {
      for (const table of tables) {
        if (table?.tableName && table?.fields && table.fields.length > 0 && !this.isSimpleManyToManyTable(table, this.many_to_many_sep)) {
          var className = this.processClassNameFromTable(table.tableName);
          tableListClass.push(className);
        }
      }
      // Set list of repositories first
      this.addServicesForModel(tableListClass, extraFields, "Repository");
      extraFields.fieldsTxt += '\n';
      // then list of services
      this.addServicesForModel(tableListClass, extraFields, "Service");
    }
    // Now we inject
    const modelTemplate = {
      packageBaseName: this.config.packageBaseName,
      modelFolder: entityProcessor.getProcessorFolder(),
      repositoryFolder: repositoryProcessor.getProcessorFolder(),
      serviceFolder: serviceProcessor.getProcessorFolder(),
      extraFields_import: extraFields.getImportsTxt(),
      extraFields_fields: extraFields.fieldsTxt,
    };
    // Write to files
    await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${this.FILE_NAME}`, rTemplate: tpl, rFieldsTemplate: modelTemplate });
  }

  /**
   * Add services (repository or service) for dependency injection
   * @param rClassModelNames : model class name list
   * @param rExtraFields : extra fields
   * @param rSuffixe : Suffixe to add on model class name (e.g : Repository)
   */
  private addServicesForModel(rClassModelNames: string[], rExtraFields: ExtraField, rSuffixe: string) {
    if (rClassModelNames.length > 0) {
      if (!rExtraFields.fieldsTxt) {
        rExtraFields.fieldsTxt = "";
      }
      rExtraFields.fieldsTxt += `\n// Register ${rSuffixe} list for dependency injection`;
      for (const className of rClassModelNames) {
        if (className) {
          rExtraFields.fieldsTxt += `\nbuilder.Services.AddScoped<${className}${rSuffixe}>();`;
          // Note : no need to addImport cause we are using directive with Models/Repositories/Services folder in template file
        }
      }
    }
  }
}
