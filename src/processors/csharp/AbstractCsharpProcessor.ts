/* eslint-disable @typescript-eslint/naming-convention */
import { GenericDbField } from "../../db/model/GenericDbField";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { ExtraField } from "../../model/ExtraField";
import { GeneratorTool } from "../../tools/GeneratorTool";
import { SqlTool } from "../../tools/SqlTool";
import { AbstractProcessor } from "../AbstractProcessor";
import { IConverter } from "../IConverter";
import { CsharpConverter } from "./CsharpConverter";

export abstract class AbstractCsharpProcessor extends AbstractProcessor {
  // Default configuration for async
  protected asyncCfg = {
    isAsync: true,
    asyncWrapper1: "async ",
    asyncWrapper2: "Task<",
    asyncWrapper3: ">",
    asyncWrapper4: "await ",
    asyncWrapper5: "Async",
  };
  protected readonly OPTIONAL_CHAR = "?";

  protected getConverter(): IConverter {
    return new CsharpConverter();
  }

  /**
   * Get csharpdoc param part '@param <type> <param name>
   * @param rFieldType : type of the field
   * @param rFieldName : name of the field
   */
  protected addParamDoc(rFieldType: string, rFieldName: string): string {
    return `\t\t/// <param name="${rFieldName}">${rFieldName}</param>`;
  }

  /**
   * Get optional field char, represents an optional field
   * @param rField : field
   * @returns optional field char
   */
  protected getOptionalChar(rField: GenericDbField): string {
    return rField?.isNullable ? this.OPTIONAL_CHAR : "";
  }

  /**
   * Takes an import name and generates the import call
   * @param rImportName : import name, e.g. 'csharp.util.Date'
   * @returns import call given that name (e.g. 'import csharp.util.Date;')
   */
  protected getImportFrom(rImportName: string): string {
    return rImportName ? `using ${rImportName};` : "";
  }

  /**
   * Get method name to get all xxx by yyy id
   * @param rTablenameUp : table name that we are gonna get model list with first letter upper case (xxx)
   * @param rClassNameDistant : foreign table linked to this model (yyy)
   * @returns method name
   */
  protected getFindByMethodName(rTablenameUp: string, rClassNameDistant: string) {
    return `GetAll${rTablenameUp}By${rClassNameDistant}Id`;
  }

  /**
   * Override way of getting classname from table to suit efcore way of naming models class
   * @param rTable : table name
   * @returns classname (e.g. classrooms_has_students will return ClassroomsHasStudent)
   */
  override processClassNameFromTable(rTable: string): string {
    return GeneratorTool.processClassNameFromTableCamel(rTable);
  }

  /**
   * Check if this table is a simple pivot table. Means it has separator in name (e.g. : 'classrooms_has_students')
   * and only pkeys and no other attributes
   * This method will be used mainly for efcore processors as EF Core use this logical for creating models or not
   * but we use it too in 'net' processors car they maint to work with efcore models and repositories
   * @param rTable : Table
   * @param rManyToManySep : separator for many to many table
   */
  protected isSimpleManyToManyTable(rTable: GenericDbTable, rManyToManySep: string = SqlTool.MANY_TOO_MANY_SEP): boolean {
    // Check if they are fields and this is a ManyToMany table
    if (rTable.fields && SqlTool.isManyToManyTable(rTable.tableName, rManyToManySep)) {
      // Get only pkey fields
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      // Return true if table only contains pkey fields
      return pKeys.length === rTable.fields.length;
    }
    return false;
  }

  /**
   * set isAsync to false if processor config is false
   */
  protected setAsyncFromConfig() {
    if (this.configProcessor?.extra_config?.is_async === false) {
      this.asyncCfg = {
        isAsync: false,
        asyncWrapper1: "",
        asyncWrapper2: "",
        asyncWrapper3: "",
        asyncWrapper4: "",
        asyncWrapper5: "",
      };
    }
  }

  /**
   * Add import for async Task if needed
   * @param rExtraFields : extra fields
   */
  protected addImportAsyncIfNeeded(rExtraFields: ExtraField) {
    if (this.asyncCfg.isAsync) {
      rExtraFields.addImport("using System.Threading.Tasks;");
    }
  }
}
