/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../db/model/GenericDbTable";
import { PostmanConfig } from "../model/PostmanConfig";
import { ServicePostmanBuilder } from "../service/ServicePostmanBuilder";
import { AbstractJavaProcessor } from "./java/AbstractJavaProcessor";

export class DefaultPostmanProcessor extends AbstractJavaProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private readonly FILE_NAME = "default";
  private readonly FILE_NAME_SUFFIX = "-postman";
  private many_to_many_sep?: string;

  getProcessorFolder(): string {
    return ""; // at root path
  }

  getProcessorName(): string {
    return "postman";
  }

  /**
   * Not implemented as this processor gather all tables data to process them in one file !
   */
  protected processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }

  async process(): Promise<void> {
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    // As we are not calling defaultprocess method, we need to init services and create directory if needed
    this.init();
    this.initFolders();
    // Get all tables (filtered based on configuratn)
    const tables = await this.getAllTables();
    // Filter to avoid ORM tables
    // const tablesFiltered = GeneratorTool.filterTables(tables, this.ORM_TABLES);
    // Postman configuration
    const postmanCfg = new PostmanConfig();
    postmanCfg.many_to_many_sep = this.many_to_many_sep; // If undefined, no problem, PostmanBuilder can deal with it
    postmanCfg.with_base_api = this.configProcessor.extra_config?.with_base_api;
    // Create a new Postman collection with ServicePostmanBuilder
    const postmanBuilder = new ServicePostmanBuilder(this, postmanCfg);
    // Get filename
    const fileName = postmanBuilder.getFileName(this.FILE_NAME, this.FILE_NAME_SUFFIX);
    const collection = postmanBuilder.generateCollectionFromTables(tables, fileName.replace(".json", ""));
    // Write collection to file
    fs.writeFileSync(`${this.getOutProcessorPath()}${fileName}`, JSON.stringify(collection.toJSON(), null, 2), "utf8");
  }

}
