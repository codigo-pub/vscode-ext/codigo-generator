import { GenericDbFKey } from "../db/model/GenericDbFKey";
import { GenericDbTable } from "../db/model/GenericDbTable";
import { ExtraField } from "../model/ExtraField";

export interface IExtraFieldProcessor {

  /**
   * Process extra field
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rReferenced : true if those fields are referenced fields, false if they are only foreign keys on the current table
   * @returns extra field with texts
   */
  processExtraField(rTable: GenericDbTable, rFKey: GenericDbFKey, rReferenced: boolean): ExtraField

}