import { GenericDbTypeField } from "../db/model/GenericDbTypeField";
import { SqlConvertEnumDbType } from "../model/SqlConvertEnumDbType";

export interface IConverter {
  /**
   * Convert field to known language type
   * @param {GenericDbTypeField} rField : field
   */
  fieldConvertToType(rField?: GenericDbTypeField): SqlConvertEnumDbType | undefined;

  /**
   * Convert mysql field type to generic sql type
   * for example, int(11) will become int
   * @param {GenericDbTypeField} rField : field
   * @returns type converted
   */
  fieldConvertToSql(rField?: GenericDbTypeField): string | undefined;

  /**
   * Translate sql type to something...
   * Use this only if you want something specific
   * @param {GenericDbTypeField} rField : SQL Field
   * @returns type converted
   */
  fieldConvertFromMapping(rField?: GenericDbTypeField, rTypeMappings?: SqlConvertEnumDbType[]): SqlConvertEnumDbType | undefined;

}
