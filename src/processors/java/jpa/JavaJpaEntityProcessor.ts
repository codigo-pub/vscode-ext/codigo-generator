/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { AbstractDatabase } from "../../../db/AbstractDatabase";
import { EnumDbRule } from "../../../db/model/EnumDbRule";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { IExtraFieldProcessor } from "../../IExtraFieldProcessor";
import { JavaJpaEntityCompositeKeyProcessor } from "./JavaJpaEntityCompositeKeyProcessor";
import { AbstractJavaJpaProcessor } from "./AbstractJavaJpaProcessor";
import { EnumDbType } from "../../../db/model/EnumDbType";

export class JavaJpaEntityProcessor extends AbstractJavaJpaProcessor implements IExtraFieldProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "models";
  }

  getProcessorName(): string {
    return "jpa_entity";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_jpa_entity_java", "utf8");

      // Check if lombok is deactivated
      const lombokDeActivated = this.configProcessor.extra_config?.lombok === false;

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      var fieldsToInject = "";
      // var constructorToInject = "";
      // var constructorInToInject = "";
      var gettersAndSetters = "";
      var pkeyImport = "";
      var extraFields = new ExtraField();

      // If composite primary key
      let compositeKey = false;
      if (pKeys && pKeys.length > 1) {
        compositeKey = true; // set flag to true
        const compositeProcessor = new JavaJpaEntityCompositeKeyProcessor(this.config, this.configProcessor, this.db, rTable);
        await compositeProcessor.process(); // create composite pkey file
        const compositeClassName = JavaJpaEntityCompositeKeyProcessor.getClassName(className);
        const basePackage = this.config.packageBaseName ?? "";
        pkeyImport = `\nimport ${basePackage === "" ? "" : basePackage + "."}${this.getProcessorFolder()}.${compositeClassName};`;
        fieldsToInject += "\n\t@EmbeddedId";
        fieldsToInject += `\n\tprivate ${compositeClassName} id;\n`;
      }

      for (const fielddata of rTable.fields) {
        if (fielddata.fieldName !== undefined && fielddata.fieldName !== "") {
          // Get name of field
          let fieldName = this.getFieldName(fielddata.fieldName);
          var fieldType = this.converter.fieldConvertToType(fielddata.type);
          // check if import needed
          this.addImportForField(extraFields, fieldType);
          // get foreign key eventually
          const fKey: GenericDbFKey | undefined = GeneratorTool.getFKeyFromField(fielddata, rTable.fKeys);
          // inject field
          const txtInject = this.getJpaField(fielddata, fieldType?.targetType, fKey, compositeKey);
          if (txtInject) {
            fieldsToInject += (fieldsToInject !== "" ? "\n" : "") + txtInject;
          }
          // getters and setters if lombok is deactivated
          if (lombokDeActivated) {
            gettersAndSetters += this.generateGetterSettersForField(fieldName, fieldType?.targetType ?? "", this.getOptionalChar(fielddata));
          }

          // if primary key, we set constructor fields
          // if (fielddata.isPKey) {
          //   if (constructorToInject !== "") {
          //     constructorToInject += ", "; //if one more to constructor
          //   }
          //   const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
          //   constructorToInject += `${fieldType?.targetType} ${paramPrefix}${fieldName}`;
          //   constructorInToInject += `this.${fieldName} = ${paramPrefix}${fieldName};\n\t\t\t`;
          // }
        }
      }

      // Extra config --------------------------------------------------------------------------------------------------------------------------------
      // lombok activated by default
      let lombok_import = lombokDeActivated ? "" : "\nimport lombok.Getter;\nimport lombok.Setter;";
      let lombok_class_annotate = lombokDeActivated ? "" : "@Getter\n@Setter";
      // -------------------------------------------------------------------------------------------------------------------------------- Extra config

      // If schema different from public we inject it, otherwise it is not needed
      const schema = this.db.getSchema() !== AbstractDatabase.PUBLIC_SCHEMA ? `, schema = "${this.db.getSchema()}"` : "";

      // Extra fields (OneToMany/ManyToOne)
      const extraFieldsFKeys = GeneratorTool.getExtraFields(rTable, rTable.fKeys ?? [], this);
      const extraFieldsRefFkeys = GeneratorTool.getExtraFields(rTable, rTable.referencedFKeys ?? [], this, true);
      extraFields.addImports(extraFieldsFKeys.importTxt);
      extraFields.addImports(extraFieldsRefFkeys.importTxt);
      extraFields.fieldsTxt = (extraFieldsFKeys.fieldsTxt ?? "") + (extraFieldsRefFkeys.fieldsTxt ?? "");
      extraFields.gettersSettersTxt = (extraFieldsFKeys.gettersSettersTxt ?? "") + (extraFieldsRefFkeys.gettersSettersTxt ?? "");

      // if lombok is not deactivated, erase extraFields getters setters
      if (!lombokDeActivated) {
        extraFields.gettersSettersTxt = "";
      }

      const modelTemplate = {
        className: className,
        packageBaseName: this.config.packageBaseName,
        fields: fieldsToInject,
        pkeyImport: pkeyImport,
        extraFields_import: extraFields.getImportsTxt(),
        extraFields_fields: extraFields.fieldsTxt,
        extraFields_gettersSetters: extraFields.gettersSettersTxt,
        // constructor: constructorToInject,
        // in_constructor: constructorInToInject,
        gettersAndSetters: gettersAndSetters,
        table: rTable.tableName,
        schema: schema,
        lombok_import: lombok_import,
        lombok_class_annotate: lombok_class_annotate,
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });

    }
  }

  /**
   * Process extra field
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @param rReferenced : true if those fields are referenced fields, false if they are only foreign keys on the current table
   * @returns extra field with texts
   */
  processExtraField(rTable: GenericDbTable, rFKey: GenericDbFKey, rReferenced: boolean = false): ExtraField {
    let extraField = new ExtraField();

    if (rTable.fields && rFKey.tableName) {
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      const compositeKey = (pKeys.length > 1);
      const tableToUse = rReferenced ? rFKey.tableName : rFKey.referencedTable ?? "";
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(tableToUse);
      // code commented below as model class is in the same package, so no need to import
      // const basePackage = this.config.packageBaseName ?? "";
      // extraField.addImport(`import ${basePackage === "" ? "" : basePackage + "."}${this.getProcessorFolder()}.${className};`);
      extraField.fieldsTxt = rReferenced ? this.getOrmOneToMany(rTable, rFKey) : this.getOrmManyToOne(rFKey, !compositeKey);
      // If rReferenced we must add jsonignore import
      if (rReferenced) {
        extraField.addImport("import com.fasterxml.jackson.annotation.JsonIgnore;");
      }
      // note that getter/setter is based on table name and not column name
      extraField.addImport("import java.util.List;");
      extraField.gettersSettersTxt = this.generateGetterSettersForField(tableToUse, `List<${className}>`, "");
    }
    return extraField;
  }

  /**
   * 
   * @param rField : Field
   * @param rFieldType : Type of field
   * @param rFKey : Foreign key linked to this field
   * @param rCompositeKey : true if there is a composite key 
   * @returns field text declaration
   */
  private getJpaField(rField: GenericDbField, rFieldType: string | undefined, rFKey: GenericDbFKey | undefined, rCompositeKey: boolean): string {
    let jpaField = "";
    if (rField.fieldName) {
      // Generate JPA annotations based on properties
      if (rField.isPKey && !rCompositeKey) {
        jpaField += "\t@Id\n";
        jpaField += rField.isAutogenerated ? "\t@GeneratedValue(strategy = GenerationType.IDENTITY)" : "";
      } else if (rFKey) {
        // nothing to do if this is a foreign key, it will be done with getManyToOne (see extraFields)
        return jpaField;
      } else {
        // If this is an array sql type (exist in postgresql) we add @ElementCollection
        if (rField?.type?.type === EnumDbType.array) {
          jpaField += `\t@ElementCollection\n`;
        }
        // If this is an enum type and enum processing not disabled
        if (rField?.type?.type === EnumDbType.enum && !(this.configProcessor.extra_config?.use_enum === false)) {
          jpaField += `\t@Enumerated(EnumType.STRING)\n`;
          // Overwrite type
          rFieldType = this.getEnumNameFromField(rField);
          // Create enum file 
          // No need to import cause enum file will be on same package
          this.createEnumFileFromType(rField);
        }

        jpaField += `\t@Column`;
        let options: string[] = [];
        options.push(`name = "${rField.fieldName}"`);
        if (rField.isNullable) {
          options.push("nullable=true");
        }
        if (rField.type?.length && this.showLength(rField.type?.type)) {
          options.push(`length=${rField.type.length}`);
        }
        if (options.length > 0) {
          jpaField += `(${options.join(", ")})`;
        }
      }

      // Generate field declaration
      jpaField += `\n\tprivate ${rFieldType} ${this.getFieldName(rField.fieldName)};\n`;

    }

    return jpaField;
  }

  /**
   * Get ORM representation of many to one for this foreign key
   * @param rFKey : Generic Foreign key object
   * @param rInsertableUpdatable : value for insertable/updatable
   * @returns
   */
  protected getOrmManyToOne(rFKey: GenericDbFKey, rInsertableUpdatable: boolean = true): string {
    let txt = "";
    if (rFKey?.referencedTable) {
      let className = this.processClassNameFromTable(rFKey.referencedTable);
      let fieldName = this.getFieldNameMany(rFKey.referencedTable, true);
      txt += "\n\n\t";
      txt += `\n\t@ManyToOne(fetch = FetchType.EAGER)`;
      txt += `\n\t@JoinColumn(name="${rFKey.columnName}", referencedColumnName="${rFKey.referencedColumn}", insertable=${rInsertableUpdatable}, updatable=${rInsertableUpdatable})`;
      txt += "\n\t" + this.getFieldDeclaration(className, fieldName);
    }
    return txt;
  }

  /**
   * Get ORM representation of one to many for this foreign key
   * @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
   * @param rTable : GenericDbTable making the call
   * @param rFKey : Generic Foreign key object
   * @returns
   */
  protected getOrmOneToMany(rTable: GenericDbTable, rFKey: GenericDbFKey): string {
    let txt = "";
    if (rFKey?.tableName && rFKey?.referencedTable) {
      // Get table referencing the fkey
      let className = this.processClassNameFromTable(rFKey.tableName);
      let fieldName = this.getFieldNameMany(rFKey.referencedTable, true);
      const fieldNameLocal = this.getFieldNameMany(rFKey.tableName);
      // OneToMany annotation
      txt += "\n\n\t@JsonIgnore";
      txt += `\n\t@OneToMany(mappedBy = "${fieldName}"`; // <= we expect to have property in target class named as the class itself
      // for now, we only support cascade rule for both
      if (rFKey.deleteRule === EnumDbRule.CASCADE && rFKey.updateRule === EnumDbRule.CASCADE) {
        txt += `, cascade = CascadeType.ALL, orphanRemoval = true`;
      }
      txt += ")";
      txt += "\n\t" + this.getFieldDeclaration(`List<${className}>`, fieldNameLocal);
    }
    return txt;
  }

  /**
   * Create field text, e.g : 'private ?Student $student;'
   * @param rFieldType : field type
   * @param rFieldname : field name
   * @param rOptional : [optional]optional character, default to optional character of the class
   * @returns
   */
  private getFieldDeclaration(rFieldType: string | undefined, rFieldname: string): string {
    return `private ${rFieldType} ${rFieldname};`;
  }

  /**
   * Get enum name from field when this is an enum type
   * @param rField : Field
   * @returns Field enum
   */
  private getEnumNameFromField(rField: GenericDbField): string {
    let className = this.processClassNameFromTable(rField.fieldName ?? '');
    return `${className}Enum`;
  }

  /**
   * Create an enum file from this enum field
   * @param rField : field
   */
  private createEnumFileFromType(rField: GenericDbField) {
    if (rField.type?.extra && rField.type.extra.length > 0) {
      const enumName = this.getEnumNameFromField(rField);
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_model_enum_java", "utf8");
      // Get enum values text
      let enumValues = "\t" + rField.type.extra.join(", \n\t");
      // Now we inject
      var dataTpl = this.supplant.text(tpl, {
        enumName: enumName,
        packageBaseName: this.config.packageBaseName,
        enumValues: enumValues
      });
      // Write to files
      fs.writeFileSync(`${this.getOutProcessorPath()}${enumName}.java`, dataTpl, "utf8");      
    }
  }

}
