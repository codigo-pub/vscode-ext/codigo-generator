import { EnumDbType } from "../../../db/model/EnumDbType";
import { GenericDbTypeField } from "../../../db/model/GenericDbTypeField";
import { SqlConvertEnumDbType } from "../../../model/SqlConvertEnumDbType";
import { AbstractConverter } from "../../AbstractConverter";


export class JavaJpaConverter extends AbstractConverter {

  readonly typeMappings: SqlConvertEnumDbType[] = [
    { sqlType: EnumDbType.char, targetType: "String" },
    { sqlType: EnumDbType.varchar, targetType: "String" },
    { sqlType: EnumDbType.uuid, targetType: "String" },
    { sqlType: EnumDbType.longtext, targetType: "String" },
    { sqlType: EnumDbType.enum, targetType: "String" },

    { sqlType: EnumDbType.long, targetType: "Long" },
    { sqlType: EnumDbType.int, targetType: "Long" }, //<= we put Long to ease jpa id pkey type and we must use Interface
    { sqlType: EnumDbType.numeric, targetType: "int" },
    { sqlType: EnumDbType.float, targetType: "float" },
    { sqlType: EnumDbType.double, targetType: "double" },

    { sqlType: EnumDbType.date, targetType: "Date", import: ["java.util.Date"] },
    { sqlType: EnumDbType.datetime, targetType: "LocalDateTime", import: ["java.time.LocalDateTime"] },
    { sqlType: EnumDbType.timestamp, targetType: "Timestamp", import: ["java.sql.Timestamp"] },
    { sqlType: EnumDbType.time, targetType: "Time", import:[ "java.sql.Time"] },

    { sqlType: EnumDbType.interval, targetType: "Duration", import: ["java.time.Duration"] },

    { sqlType: EnumDbType.boolean, targetType: "boolean" },

    { sqlType: EnumDbType.blob, targetType: "byte[]" },
    
    { sqlType: EnumDbType.json, targetType: "String" },

    { sqlType: EnumDbType.array, targetType: "List<String>", import: ["java.util.List"] },
  ];

  /**
   * Translate sql type to TypeScript type
   * @param {JSON} rField : Field
   * @returns typescript type
   */
  fieldConvertToType(rField?: GenericDbTypeField): SqlConvertEnumDbType | undefined {
    return this.fieldConvertFromMapping(rField, this.typeMappings);
  }
}
