/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { IDatabase } from "../../../db/IDatabase";
import { ConfigProcessor } from "../../../model/ConfigProcessor";
import { GlobalConfig } from "../../../model/GlobalConfig";
import { AbstractJavaJpaProcessor } from "./AbstractJavaJpaProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";

/**
 * This processor is called only by another processor, not supposed to be used directly
 */
export class JavaJpaEntityCompositeKeyProcessor extends AbstractJavaJpaProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private _table: GenericDbTable;

  getProcessorFolder(): string {
    return "models";
  }

  getProcessorName(): string {
    return "jpa_entity_composite";
  }

  constructor(rConfig: GlobalConfig, rConfigProcessor: ConfigProcessor, rDb: IDatabase, rTable: GenericDbTable) {
    super(rConfig, rConfigProcessor, rDb);
    this._table = rTable;
  }

  async process(): Promise<void> {
    await this.processTable(this._table);
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable?.fields.length > 0) {
      // Filter only pkeys
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length > 0) {
        // Load template
        var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_jpa_entity_composite_id_java", "utf8");

        // Check if lombok is deactivated
        const lombokDeActivated = this.configProcessor.extra_config?.lombok === false;

        // ClassName
        var parentClassName = this.processClassNameFromTable(rTable.tableName);
        var className = JavaJpaEntityCompositeKeyProcessor.getClassName(parentClassName);
        var fieldsToInject = "";
        var gettersAndSetters = "";
        // Careful, here we loop through pkeys and not through fields like we use too !
        for (const fielddata of pKeys) {
          // Get name of field
          let fieldName = fielddata.fieldName;
          if (fieldName !== undefined && fieldName !== "") {
            var fieldType = this.converter.fieldConvertToType(fielddata.type);
            // inject field
            fieldsToInject += (fieldsToInject !== "" ? "\n" : "") + this.getJpaField(fielddata, fieldType?.targetType);
            // getters and setters if lombok is deactivated
            if (lombokDeActivated) {
              gettersAndSetters += this.generateGetterSettersForField(fieldName, fieldType?.targetType ?? "", this.getOptionalChar(fielddata));
            }
          }
        }

        // Extra config --------------------------------------------------------------------------------------------------------------------------------
        // lombok activated by default
        let lombok_import = lombokDeActivated ? "" : "\nimport lombok.Getter;\nimport lombok.Setter;";
        let lombok_class_annotate = lombokDeActivated ? "" : "@Getter\n@Setter";
        // -------------------------------------------------------------------------------------------------------------------------------- Extra config

        const modelTemplate = {
          className: className,
          packageBaseName: this.config.packageBaseName,
          fields: fieldsToInject,
          gettersAndSetters: gettersAndSetters,
          lombok_import: lombok_import,
          lombok_class_annotate: lombok_class_annotate,
        };

        await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
        
      }
    }
  }

  private getJpaField(rField: GenericDbField, rFieldType: string | undefined): string {
    let jpaField = "";
    // Generate JPA annotation
    jpaField += `\t@Column`;
    let options: string[] = [];
    options.push(`name = "${rField.fieldName}"`);
    if (rField.type?.length && this.showLength(rField.type?.type)) {
      options.push(`length=${rField.type.length}`);
    }
    if (options.length > 0) {
      jpaField += `(${options.join(", ")})`;
    }
    jpaField += `\n\tprivate ${rFieldType} ${this.getFieldName(rField.fieldName ?? '')};\n`;
    return jpaField;
  }

  static getClassName(rClassname: string): string {
    return `${rClassname}Id`;
  }
}
