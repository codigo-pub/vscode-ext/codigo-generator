/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { JavaJpaEntityCompositeKeyProcessor } from "./JavaJpaEntityCompositeKeyProcessor";
import { JavaJpaEntityProcessor } from "./JavaJpaEntityProcessor";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { ExtraField } from "../../../model/ExtraField";
import { AbstractJavaJpaProcessor } from "./AbstractJavaJpaProcessor";

export class JavaJpaRepositoryProcessor extends AbstractJavaJpaProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = true;

  private entityProcessor?: JavaJpaEntityProcessor;
  private many_to_many_sep?: string;

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return "repositories";
  }

  getProcessorName(): string {
    return "jpa_repository";
  }

  async process(): Promise<void> {   
    // Starts by creating an instance of JavaJpaEntityProcessor to ask for his folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new JavaJpaEntityProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;

    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix

    if (rTable?.fields && this.entityProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_jpa_repository_java", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var className = `I${classModelName}Repository`;
      var pkeyType = "";
      var mainExtraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // Check primary key
      if (pKeys && pKeys.length >= 1) {
        if (pKeys && pKeys.length > 1) {
          // Get composite id classname
          pkeyType = JavaJpaEntityCompositeKeyProcessor.getClassName(classModelName);
          const basePackage = this.config.packageBaseName ?? "";
          mainExtraFields.addImport(`\nimport ${basePackage === "" ? "" : basePackage + "."}${this.entityProcessor.getProcessorFolder()}.${pkeyType};`);
        } else {
          // If we come here, there is only one pkey (most frequent use case)
          // Starts by getting pkey type
          const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
          pkeyType = myPkeyType?.targetType ?? "";
          this.addImportForField(mainExtraFields, myPkeyType); // add import if needed
        }
      }

      // Browse ManyToMany referenced keys to add some cool methods
      const extraMethods = new ExtraField();
      if (rTable?.referencedFKeys && this.allTables) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          this.generateFindBy(rTable, this.allTables, fielddata, extraMethods);
        }
      }

      // Add imports from extraMethods if needed
      mainExtraFields.addImports(extraMethods.importTxt);

      const modelTemplate = {
        classModelName: classModelName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        paramPrefix: paramPrefix,
        extraFields_import: mainExtraFields.getImportsTxt(),
        pkeyType: pkeyType,
        extraMethods: extraMethods.fieldsTxt,
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });

    }
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(rTable: GenericDbTable, rAllTables: GenericDbTable[], rFKey: GenericDbFKey, rExtraMethods: ExtraField) {
    const dataRelation = GeneratorTool.getInfoFindBy({ rTable, rAllTables, rFKey, rProcessor: this, rManyToManySep: this.many_to_many_sep });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }
      let refColumn = dataRelation.pivotDistantPkeyName;
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableNameUp ?? '', dataRelation.classNameDistant ?? '');
      const fieldName = this.getFieldName(refColumn ?? '');
      // Add text
      rExtraMethods.fieldsTxt += `\n\t/**
\t * Get all ${dataRelation.tableNameUp} by ${dataRelation.classNameDistant} id
\t * @param ${fieldName} : ${dataRelation.classNameDistant} id
\t * @return List<${dataRelation.className}> array of ${dataRelation.tableName}
\t */
\t@Query("SELECT s FROM ${dataRelation.className} s JOIN ${dataRelation.classNamePivot} chs ON chs.id.${fieldName} = :${fieldName}")
\tList<${dataRelation.className}> ${methodName}(@Param("${fieldName}") ${typePkey?.targetType} ${fieldName});
`;
    }
  }
}
