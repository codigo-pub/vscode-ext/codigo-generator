import { EnumDbType } from "../../../db/model/EnumDbType";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { AbstractJavaProcessor } from "../AbstractJavaProcessor";

export abstract class AbstractJavaJpaProcessor extends AbstractJavaProcessor {
  // All templates are expected in this sub-folder
  protected templateFolder?: string = "spring-jpa";
  
  /**
   * return true if we need to show the length in column property (ex: @Column(length= 255))
   * @param rType : EnumDbType
   * @returns
   */
  protected showLength(rType?: EnumDbType): boolean {
    switch (rType) {
      case EnumDbType.varchar:
      case EnumDbType.char:
      case EnumDbType.longtext:
        return true;
      default:
        return false;
    }
  }

  
  protected generateGetterSettersForField(rFieldName: string, rFieldType: string, rNullable: string): string {
    const field = this.getFieldName(rFieldName);
    let upName = GeneratorTool.upperFirstLetter(field);
    let getter = `\tpublic function get${upName}(): ${rFieldType}
    {
        return this.${field};
    }`;
    let setter = `\tpublic function set${upName}(${rNullable}${rFieldType} ${field}): void
    {
        this.${field} = ${field};
    }`;
    return getter + "\n\n" + setter + "\n\n";
  }

}
