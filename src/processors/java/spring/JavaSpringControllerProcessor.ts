/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { JavaJpaEntityCompositeKeyProcessor } from "../jpa/JavaJpaEntityCompositeKeyProcessor";
import { JavaJpaEntityProcessor } from "../jpa/JavaJpaEntityProcessor";
import { JavaSpringServiceProcessor } from "./JavaSpringServiceProcessor";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { ExtraField } from "../../../model/ExtraField";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { AbstractJavaSpringProcessor } from "./AbstractJavaSpringProcessor";

export class JavaSpringControllerProcessor extends AbstractJavaSpringProcessor {

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = true;

  private entityProcessor?: JavaJpaEntityProcessor;
  private serviceProcessor?: JavaSpringServiceProcessor;
  private many_to_many_sep?: string;
  private baseApi?: string = ""; // base api
  private loggerDeActivated?: boolean = false;
  private paramPrefix?: string;

  private allTables?: GenericDbTable[];


  getProcessorFolder(): string {
    return "controllers";
  }

  getProcessorName(): string {
    return "spring_controller";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new JavaJpaEntityProcessor(this.config, this.configProcessor, this.db);
    this.serviceProcessor = new JavaSpringServiceProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    this.baseApi = this.configProcessor?.extra_config?.base_api ?? "/api";
    // Check if logger is deactivated
    this.loggerDeActivated = this.configProcessor.extra_config?.logger === false;
    this.paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix

    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);
      if (pKeys && pKeys.length >= 1) {
        // Process table data 
        if (pKeys && pKeys.length > 1) {
          await this.processTableWithCompositePKey(rTable);
        } else {
          await this.processTableWithSinglePkey(rTable);
        }
      }
    }
  }

  /**
   * Process table that has a single pkey
   * @param rTable : table
   * @returns string for template file or null if nothing could be done
   */
  protected async processTableWithSinglePkey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_spring_controller_java", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classServiceName = `${classModelName}Service`;
      var className = `${classModelName}Controller`;
      var pkeyType = "";
      var pkeyImport = "";
      var extraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // If we come here, there is only one pkey (most frequent use case)
      // Starts by getting pkey type
      const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
      pkeyType = myPkeyType?.targetType ?? "";
      // Add import if needed
      this.addImportForField(extraFields, myPkeyType);

      // Extra config --------------------------------------------------------------------------------------------------------------------------------
      // logger activated by default
      let logger_import = this.loggerDeActivated ? "" : "\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;";
      let logger_declare = this.loggerDeActivated ? "" : `\n\tstatic final Logger LOG = LoggerFactory.getLogger(${className}.class);`;
      // -------------------------------------------------------------------------------------------------------------------------------- Extra config

      // Browse ManyToMany referenced keys to add some cool methods
      const extraMethods = new ExtraField();
      if (rTable?.referencedFKeys && this.allTables) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          this.generateFindBy(rTable, this.allTables, fielddata, extraMethods);
        }
      }

      // Add imports from extraMethods if needed
      extraFields.addImports(extraMethods.importTxt);

      // Now we inject
      const modelTemplate = {
        baseApi: this.baseApi,
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classServiceName: classServiceName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        serviceFolder: this.serviceProcessor.getProcessorFolder(),
        paramPrefix: this.paramPrefix,
        pkeyImport: pkeyImport,
        pkeyType: pkeyType,
        logger_import: logger_import,
        logger_declare: logger_declare,
        extraFields_import: extraFields.getImportsTxt(),
        extraMethods: extraMethods.fieldsTxt,
      };

      // Write to files, careful for interface repositories, we start with a I as it is done in template
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Process table that has a composite pkey
   * We use a different table to ease things
   * @param rTable : table
   * @returns string for template file or null if nothing could be done
   */
  protected async processTableWithCompositePKey(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && this.entityProcessor && this.serviceProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_spring_controller_c_java", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classServiceName = `${classModelName}Service`;
      var className = `${classModelName}Controller`;
      var extraFields = new ExtraField();

      // Get composite id classname
      var pkeyType = JavaJpaEntityCompositeKeyProcessor.getClassName(classModelName);
      const basePackage = this.config.packageBaseName ?? "";
      const pkeyImport = `\nimport ${basePackage === "" ? "" : basePackage + "."}${this.entityProcessor.getProcessorFolder()}.${pkeyType};`;

      // Get pkeys to see if have composite pkey
      const pkeyFields = this.getPkeyCompositeFields(GeneratorTool.getPkeyFields(rTable.fields), extraFields);

      // Extra config --------------------------------------------------------------------------------------------------------------------------------
      // logger activated by default
      let logger_import = this.loggerDeActivated ? "" : "\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;";
      let logger_declare = this.loggerDeActivated ? "" : `\n\tstatic final Logger LOG = LoggerFactory.getLogger(${className}.class);`;
      // -------------------------------------------------------------------------------------------------------------------------------- Extra config

      const modelTemplate = {
        baseApi: this.baseApi,
        modelBaseApiUrl: this.getModelBaseApiUrlFromTable(rTable.tableName),
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classServiceName: classServiceName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        serviceFolder: this.serviceProcessor.getProcessorFolder(),
        paramPrefix: this.paramPrefix,
        pkeyImport: pkeyImport,
        pkeyType: pkeyType,
        logger_import: logger_import,
        logger_declare: logger_declare,
        pkey_javadoc: pkeyFields.javadoc,
        pkey_parameter: pkeyFields.parameter,
        pkey_field: pkeyFields.field,
        pkey_url_parameter: pkeyFields.url_parameter,
        pkey_id_set: pkeyFields.id_set,
        extraFields_import: extraFields.getImportsTxt(),
      };

      // Write to files, careful for interface repositories, we start with a I as it is done in template
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generate different texts that are necessary for template based on pkey fields
   * @param rFields : pkey fields
   * @param rExtraFields : extraField object for extra imports, etc.
   * @returns data for template
   */
  private getPkeyCompositeFields(
    rFields: GenericDbField[],
    rExtraFields: ExtraField
  ): { javadoc: string; parameter: string; field: string; url_parameter: string; id_set: string } {
    let data = { javadoc: "", parameter: "", field: "", url_parameter: "", id_set: "" };
    if (rFields && rFields.length > 0) {
      // Browse pkfields to add them in text
      for (const field of rFields) {
        if (field?.fieldName) {
          // Get field type
          const fieldType = this.converter.fieldConvertToType(field.type);
          // Add import if needed
          this.addImportForField(rExtraFields, fieldType);
          // Get type
          const type = fieldType?.targetType ?? "";
          const fieldName = this.getFieldName(field.fieldName);
          // Add code data
          data.javadoc += (data.parameter !== "" ? "\n" : "") + `\t * @param ${type} ${fieldName} ${fieldName}`;
          data.parameter += (data.parameter !== "" ? ", " : "") + `@PathVariable ${type} ${fieldName}`;
          data.field += (data.field !== "" ? ", " : "") + `${fieldName}`;
          data.url_parameter += `/{${fieldName}}`;
          // extract setter
          const setter = fieldName[0].toUpperCase() + fieldName.substring(1);
          data.id_set += `\n\t\tid.set${setter}(${fieldName});`;
        }
      }
    }
    return data;
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(rTable: GenericDbTable, rAllTables: GenericDbTable[], rFKey: GenericDbFKey, rExtraMethods: ExtraField, rPart?: 'left' | 'right') {
    const dataRelation = GeneratorTool.getInfoFindBy({ rTable, rAllTables, rFKey, rProcessor: this, rManyToManySep: this.many_to_many_sep });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }
      let refColumn = dataRelation.pivotDistantPkeyName;
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableNameUp ?? "", dataRelation.classNameDistant ?? "");
      // Add text
      rExtraMethods.fieldsTxt += `\n\t@GetMapping("/${dataRelation.classNameDistant?.toLowerCase()}/{${refColumn}}")
  \tpublic List<${dataRelation.className}> ${methodName}(@PathVariable ${typePkey?.targetType} ${refColumn}) {
  \t\treturn this.${dataRelation.className?.toLowerCase()}Service.${methodName}(${refColumn});
  \t}
  `;
    }
  }
}
