/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { JavaJpaEntityCompositeKeyProcessor } from "../jpa/JavaJpaEntityCompositeKeyProcessor";
import { JavaJpaEntityProcessor } from "../jpa/JavaJpaEntityProcessor";
import { JavaJpaRepositoryProcessor } from "../jpa/JavaJpaRepositoryProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { ExtraField } from "../../../model/ExtraField";
import { AbstractJavaSpringProcessor } from "./AbstractJavaSpringProcessor";

export class JavaSpringServiceProcessor extends AbstractJavaSpringProcessor {
  
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = true;

  private entityProcessor?: JavaJpaEntityProcessor;
  private repositoryProcessor?: JavaJpaRepositoryProcessor;
  private many_to_many_sep?: string;
  private loggerDeActivated?: boolean = false;

  private allTables?: GenericDbTable[];

  getProcessorFolder(): string {
    return "services";
  }

  getProcessorName(): string {
    return "spring_service";
  }

  async process(): Promise<void> {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new JavaJpaEntityProcessor(this.config, this.configProcessor, this.db);
    this.repositoryProcessor = new JavaJpaRepositoryProcessor(this.config, this.configProcessor, this.db);
    this.many_to_many_sep = this.config?.database?.many_to_many_sep;
    // Check if logger is deactivated
    this.loggerDeActivated = this.configProcessor.extra_config?.logger === false;

    // Get all tables without filtering, we are gonna need it later
    this.allTables = await this.getAllTables(true);

    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix

    if (rTable?.fields && this.entityProcessor && this.repositoryProcessor) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_spring_service_java", "utf8");
      var classModelName = this.processClassNameFromTable(rTable.tableName);
      var classRepositoryName = `I${classModelName}Repository`;
      var className = `${classModelName}Service`;
      var pkeyFieldName = "";
      var pkeyType = "";
      var mainExtraFields = new ExtraField();

      // Get pkeys to see if have composite pkey
      const pKeys = GeneratorTool.getPkeyFields(rTable.fields);

      // Check primary key
      if (pKeys && pKeys.length >= 1) {
        if (pKeys && pKeys.length > 1) {
          // Get composite id classname
          pkeyType = JavaJpaEntityCompositeKeyProcessor.getClassName(classModelName);
          const basePackage = this.config.packageBaseName ?? "";
          mainExtraFields.addImport(`\nimport ${basePackage === "" ? "" : basePackage + "."}${this.entityProcessor.getProcessorFolder()}.${pkeyType};`);
          pkeyFieldName = "id";
        } else {
          // If we come here, there is only one pkey (most frequent use case)
          // Starts by getting pkey type
          const myPkeyType = this.converter.fieldConvertToType(pKeys[0].type);
          pkeyType = myPkeyType?.targetType ?? "";
          pkeyFieldName = pKeys[0].fieldName ?? "id";
          this.addImportForField(mainExtraFields, myPkeyType); // add import if needed
        }
      }

      // Browse ManyToMany referenced keys to add some cool methods
      const extraMethods = new ExtraField();
      if (rTable?.referencedFKeys && this.allTables) {
        for (const fielddata of rTable.referencedFKeys) {
          // Add extra method
          this.generateFindBy(rTable, this.allTables, fielddata, extraMethods);
        }
      }

      // Add imports from extraMethods if needed
      mainExtraFields.addImports(extraMethods.importTxt);


      // Extra config --------------------------------------------------------------------------------------------------------------------------------
      // logger activated by default
      let logger_import = this.loggerDeActivated ? "" : "\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;";
      let logger_declare = this.loggerDeActivated ? "" : `\n\tstatic final Logger LOG = LoggerFactory.getLogger(${className}.class);`;
      // -------------------------------------------------------------------------------------------------------------------------------- Extra config

      const modelTemplate = {
        classModelName: classModelName,
        classModelNameLower: classModelName.toLowerCase(),
        classRepositoryName: classRepositoryName,
        className: className,
        packageBaseName: this.config.packageBaseName,
        modelFolder: this.entityProcessor.getProcessorFolder(),
        repositoryFolder: this.repositoryProcessor.getProcessorFolder(),
        paramPrefix: paramPrefix,
        extraFields_import: mainExtraFields.getImportsTxt(),
        pkeyType: pkeyType,
        pkeyFieldName: GeneratorTool.upperFirstLetter(pkeyFieldName),
        logger_import: logger_import,
        logger_declare: logger_declare,
        extraMethods: extraMethods.fieldsTxt,
      };

      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  /**
   * Generates a findBy method to look for items in other table when OneToMany relations from a ManyToMany table
   * e.g. you are processing a student table and there is a 'classroom_has_students' ManyToMany table, we want
   * to find all students based on classroom id using this table.
   * @param rTable : Table making the call
   * @param rAllTables : All tables without filter cause we need information on target table
   * @param rFKey : foreign referenced key
   * @param rExtraMethods : Extra fields to store text
   * @returns
   */
  private generateFindBy(rTable: GenericDbTable, rAllTables: GenericDbTable[], rFKey: GenericDbFKey, rExtraMethods: ExtraField) {
    const dataRelation = GeneratorTool.getInfoFindBy({ rTable, rAllTables, rFKey, rProcessor: this, rManyToManySep: this.many_to_many_sep });
    if (dataRelation) {
      if (!rExtraMethods.fieldsTxt) {
        rExtraMethods.fieldsTxt = "";
      }
      let refColumn = dataRelation.pivotDistantPkeyName;
      // convert distant pkey type
      const typePkey = this.converter.fieldConvertToType(dataRelation.distantPkeyField?.type);
      // check if import needed
      this.addImportForField(rExtraMethods, typePkey);
      // Method name
      const methodName = this.getFindByMethodName(dataRelation.tableNameUp ?? '', dataRelation.classNameDistant ?? '');
      // Add text
      rExtraMethods.fieldsTxt += `\n\t/**
\t * Get all ${dataRelation.tableNameUp} by ${dataRelation.classNameDistant} id
\t * @param ${refColumn} : ${dataRelation.classNameDistant} id
\t * @return List<${dataRelation.className}> array of ${dataRelation.tableName}
\t */
\t@Transactional(readOnly = true)
\tpublic List<${dataRelation.className}> ${methodName}(@NonNull ${typePkey?.targetType} ${refColumn}) {
\t\treturn this.${dataRelation.className?.toLowerCase()}Repository.${methodName}(${refColumn});
\t}
`;
    }
  }
}
