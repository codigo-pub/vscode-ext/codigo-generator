/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractJavaProcessor } from "../AbstractJavaProcessor";

export abstract class AbstractJavaSpringProcessor extends AbstractJavaProcessor {
  // All templates are expected in this sub-folder
  protected templateFolder?: string = "spring";
}
