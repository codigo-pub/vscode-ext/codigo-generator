/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { JavaJpaEntityProcessor } from "../jpa/JavaJpaEntityProcessor";
import { JavaJpaRepositoryProcessor } from "../jpa/JavaJpaRepositoryProcessor";
import path from "path";
import { StaticFile } from "../../../model/StaticFile";
import { FileTool } from "../../../tools/FileTool";
import { JavaSpringControllerProcessor } from "../spring/JavaSpringControllerProcessor";
import { GeneratorTool } from "../../../tools/GeneratorTool";
import * as vscode from "vscode";
import { l10n } from "vscode";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { AbstractJavaProcessor } from "../AbstractJavaProcessor";

export class JavaSpringSecurityJwtProcessor extends AbstractJavaProcessor {

  // All templates are expected in this sub-folder
  protected templateFolder?: string = `spring-security${path.sep}jwt`;

  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  private readonly SUB_BASE_FOLDER_DTO = `dto`;
  private readonly SUB_BASE_FOLDER_CONTROLLER = `controller`;
  private readonly DEF_USERS_TABLE = "Users";
  private readonly DEF_ROLES_TABLE = "Roles";

  private entityProcessor?: JavaJpaEntityProcessor;
  private repositoryProcessor?: JavaJpaRepositoryProcessor;
  private controllerProcessor?: JavaSpringControllerProcessor;
  private loggerDeActivated?: boolean = false;
  private dtoFolder?: string;
  private controllerFolder?: string;

  // Security config data
  private securityConfigData?: {
    userTable?: GenericDbTable;
    roleTable?: GenericDbTable;
  };

  private baseApi?: string = ""; // base api

  private readonly TEMPLATE_FILES: StaticFile[] = [
    { src: "tpl_IUserSecurityService_java", dest: "IUserSecurityService" },
    { src: "tpl_JwtAuthenticationFilter_java", dest: "JwtAuthenticationFilter" },
    { src: "tpl_JwtUtilities_java", dest: "JwtUtilities" },
    { src: "tpl_SecurityConfig_java", dest: "SecurityConfig" },
    { src: "tpl_SecurityUserDetailsService_java", dest: "SecurityUserDetailsService" },
    { src: "tpl_UserSecurityService_java", dest: "UserSecurityService" },
  ];

  private readonly TEMPLATE_DTO_FILES: StaticFile[] = [
    { src: "tpl_BearerToken_java", dest: "BearerToken" },
    { src: "tpl_LoginDto_java", dest: "LoginDto" },
    { src: "tpl_SignUpDto_java", dest: "SignUpDto" },
    { src: "tpl_RoleName_java", dest: "RoleName" },
  ];

  private readonly TEMPLATE_CONTROLLER_FILES: StaticFile[] = [{ src: "tpl_LoginController_java", dest: "LoginController" }];

  getProcessorFolder(): string {
    return "security";
  }

  getProcessorName(): string {
    return "spring_security_jwt";
  }

  async process(): Promise<void> {
    // Init variables
    this.initVars();
    // Check if logger is deactivated
    // this.loggerDeActivated = this.configProcessor.extra_config?.logger === false;
    // As we are not calling defaultprocess method, we need to init services and create directory if needed
    this.init();
    this.initFolders();
    // Fill user and role repository
    this.fillUserRepository();
    this.fillRoleRepository();
    // Create dto folder
    this.dtoFolder = this.createDtoFolder();
    // Create controller folder
    this.controllerFolder = this.createControllerFolder();
    // For every template file, do what we have to do :)
    for (const templateFile of this.TEMPLATE_FILES) {
      await this.processTemplateFile(templateFile);
    }
    // For every template DTO file too
    for (const templateFile of this.TEMPLATE_DTO_FILES) {
      await this.processDtoTemplateFile(templateFile);
    }
    // For every template controller file too
    for (const templateFile of this.TEMPLATE_CONTROLLER_FILES) {
      await this.processControllerTemplateFile(templateFile);
    }
  }

  /**
   * init variables
   */
  private initVars() {
    // Starts by creating an instance of necessary processors to ask for their folder when needed
    // Remember we don't use static properties for getProcessorFolder cause we are doing a lot on abstract class and don't want to pass it
    // through methods
    this.entityProcessor = new JavaJpaEntityProcessor(this.config, this.configProcessor, this.db);
    this.repositoryProcessor = new JavaJpaRepositoryProcessor(this.config, this.configProcessor, this.db);
    this.controllerProcessor = new JavaSpringControllerProcessor(this.config, this.configProcessor, this.db);

    this.baseApi = this.configProcessor?.extra_config?.base_api ?? "/api";

    this.securityConfigData = {
      userTable: new GenericDbTable(this.DEF_USERS_TABLE),
      roleTable: new GenericDbTable(this.DEF_ROLES_TABLE),
    };

    if (this.securityConfigData.userTable && this.securityConfigData.roleTable) {
      this.addFieldAndEntity(this.configProcessor.extra_config?.user_field ?? "email", this.securityConfigData.userTable);
      this.addFieldAndEntity(this.configProcessor.extra_config?.pass_field ?? "password", this.securityConfigData.userTable);
      this.addFieldAndEntity(this.configProcessor.extra_config?.role_field ?? "name", this.securityConfigData.roleTable);
    }
  }

  /**
   * extract table name and field name.
   * Field string format is : <tablename>.<userfield>
   * @param rField : field (e.g. : 'email' or 'users.email')
   * @param rTable
   * @returns
   */
  private addFieldAndEntity(rField: string, rTable: GenericDbTable): GenericDbTable {
    const field = new GenericDbField();
    if (!rTable.fields) {
      rTable.fields = [];
    }
    const data: string[] = rField.split(".");
    if (data.length > 1) {
      rTable.tableName = data[0];
      field.fieldName = data[1];
    } else {
      field.fieldName = data[0];
    }
    rTable.fields.push(field);
    return rTable;
  }

  /**
   * Create dto folder on models folder
   * @returns dto folder path
   */
  private createDtoFolder(): string {
    const tmpFolder = FileTool.getFullPathFromPackage(this.config?.packageBaseName || "", this.getOutBasePath());
    const folder = tmpFolder + path.sep + this.entityProcessor?.getProcessorFolder() + path.sep + "dto";
    if (folder && !fs.existsSync(folder)) {
      fs.mkdirSync(folder);
    }
    return folder;
  }

  /**
   * Create controller folder
   * @returns controller folder path
   */
  private createControllerFolder(): string {
    const tmpFolder = FileTool.getFullPathFromPackage(this.config?.packageBaseName || "", this.getOutBasePath());
    const folder = tmpFolder + path.sep + this.controllerProcessor?.getProcessorFolder();
    if (folder && !fs.existsSync(folder)) {
      fs.mkdirSync(folder);
    }
    return folder;
  }

  /**
   * Not implemented as this processor gather all tables to process them in one file !
   */
  protected async processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }

  /**
   * Process template file
   * @param rTemplateFile : template file
   */
  protected async processTemplateFile(rTemplateFile: StaticFile): Promise<void> {
    if (this.securityConfigData?.userTable?.fields && this.securityConfigData?.roleTable?.fields) {
      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + rTemplateFile.src, "utf8");

      const modelTemplate = {
        packageBaseName: this.config.packageBaseName,
        securityFolder: this.getProcessorFolder(),
        modelFolder: this.entityProcessor?.getProcessorFolder(),
        repositoryFolder: this.repositoryProcessor?.getProcessorFolder(),
        baseApi: this.baseApi,
        userField: this.securityConfigData.userTable.fields[0].fieldName,
        passField: this.securityConfigData.userTable.fields[1].fieldName,
        roleField: this.securityConfigData.roleTable.fields[0].fieldName,
        userFieldUpper: GeneratorTool.upperFirstLetter(this.securityConfigData.userTable.fields[0].fieldName ?? ""),
        passFieldUpper: GeneratorTool.upperFirstLetter(this.securityConfigData.userTable.fields[1].fieldName ?? ""),
        roleFieldUpper: GeneratorTool.upperFirstLetter(this.securityConfigData.roleTable.fields[0].fieldName ?? ""),
      };
      
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${rTemplateFile.dest}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate });
    }
  }

  /**
   * Process template file
   * @param rTemplateFile : template file
   */
  protected async processDtoTemplateFile(rTemplateFile: StaticFile): Promise<void> {
    if (this.securityConfigData?.userTable?.fields && this.securityConfigData?.roleTable?.fields) {
      // Load template DTO
      var tpl = fs.readFileSync(this.getTemplateBasePath() + this.SUB_BASE_FOLDER_DTO + path.sep + rTemplateFile.src, "utf8");
      const modelTemplate = {
        packageBaseName: this.config.packageBaseName,
        securityFolder: this.getProcessorFolder(),
        modelFolder: this.entityProcessor?.getProcessorFolder(),
        userField: this.securityConfigData.userTable.fields[0].fieldName,
        passField: this.securityConfigData.userTable.fields[1].fieldName,
        userFieldUpper: GeneratorTool.upperFirstLetter(this.securityConfigData.userTable.fields[0].fieldName ?? ""),
        passFieldUpper: GeneratorTool.upperFirstLetter(this.securityConfigData.userTable.fields[1].fieldName ?? ""),
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.dtoFolder}${path.sep}${rTemplateFile.dest}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate });
    }
  }

  /**
   * Process template file
   * @param rTemplateFile : template file
   */
  protected async processControllerTemplateFile(rTemplateFile: StaticFile): Promise<void> {
    if (this.securityConfigData?.userTable?.fields && this.securityConfigData?.roleTable?.fields) {
      // Load template DTO
      var tpl = fs.readFileSync(this.getTemplateBasePath() + this.SUB_BASE_FOLDER_CONTROLLER + path.sep + rTemplateFile.src, "utf8");
      const modelTemplate = {
        packageBaseName: this.config.packageBaseName,
        securityFolder: this.getProcessorFolder(),
        controllerFolder: this.controllerProcessor?.getProcessorFolder(),
        modelFolder: this.entityProcessor?.getProcessorFolder(),
        baseApi: this.baseApi,
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.controllerFolder}${path.sep}${rTemplateFile.dest}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate });
    }
  }

  /**
   * Fill repository with useful methods for security
   * Takes a table with first field is the one we are going to use
   * @param rTable : table with fields
   */
  protected fillRepository(rTable?: GenericDbTable): void {
    if (rTable?.fields && rTable.fields.length >= 1) {
      const tmpFolder = FileTool.getFullPathFromPackage(this.config?.packageBaseName || "", this.getOutBasePath());
      const folder = tmpFolder + path.sep + this.repositoryProcessor?.getProcessorFolder();
      const classModelName = this.repositoryProcessor?.processClassNameFromTable(rTable.tableName);
      const className = `I${classModelName}Repository`;
      const file = `${folder}${path.sep}${className}.java`;
      const field = rTable.fields[0].fieldName;
      const fieldUpper = GeneratorTool.upperFirstLetter(field ?? '');
  
      if (!fs.existsSync(file)) {
        vscode.window.showErrorMessage(`${l10n.t("File does not exist")} : ${file}`);
      } else {
        var repository = fs.readFileSync(file, "utf8");
  
        // Add methods, not that we already have import for Optional from repository template for convenience
        let additionalMethods = "";
        additionalMethods += `\n\tOptional<${classModelName}> findBy${fieldUpper}(String ${field});`;
        additionalMethods += `\n\tBoolean existsBy${fieldUpper}(String ${field});`;
        additionalMethods += "\n\n}";
  
        repository = repository.replace(/}$/, additionalMethods);
  
        fs.writeFileSync(file, repository, "utf8");
      }
    }
  }

  /**
   * Fill user repository with useful methods for security
   */
  protected fillUserRepository(): void {
    this.fillRepository(this.securityConfigData?.userTable);
  }

  /**
   * Fill role repository with useful methods for security
   */
  protected fillRoleRepository(): void {
    this.fillRepository(this.securityConfigData?.roleTable);
  }
}
