/* eslint-disable @typescript-eslint/naming-convention */
import * as fs from "fs";
import { AbstractJavaProcessor } from "./AbstractJavaProcessor";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { ExtraField } from "../../model/ExtraField";
import { GeneratorTool } from "../../tools/GeneratorTool";

export class JavaModelProcessor extends AbstractJavaProcessor {
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "models";
  }
  
  getProcessorName(): string {
    return "model";
  }

  async process(): Promise<void> {
    await this.defaultProcess();
  }

  protected async processTable(rTable: GenericDbTable): Promise<void> {
    if (rTable?.fields && rTable.fields.length > 0) {
      var _this = this;

      // Load template
      var tpl = fs.readFileSync(this.getTemplateBasePath() + "tpl_model_java", "utf8");

      // Check if lombok is deactivated
      const lombokDeActivated = this.configProcessor.extra_config?.lombok === false;

      // ClassName
      var className = this.processClassNameFromTable(rTable.tableName);
      var fieldsToInject = "";
      var constructorToInject = "";
      var constructorInToInject = "";
      var gettersAndSetters = "";
      var extraFields = new ExtraField();
      for (const fielddata of rTable.fields) {
        // Get name of field
        let fieldName = fielddata.fieldName;
        if (fieldName !== undefined && fieldName !== "") {
          var myfieldType = _this.converter.fieldConvertToType(fielddata.type);
          if (myfieldType) {
            const fieldType = myfieldType.targetType;
            // Do we need to import something ?
            if (myfieldType.import) {
              this.addImportForField(extraFields, myfieldType);
            }
            var nullable = this.getOptionalChar(fielddata);
            // inject field
            fieldsToInject += `private ${nullable}${fieldType ?? ""} ${this.getFieldName(fieldName)};\n\t`;
  
            // getters and setters if lombok is deactivated
            if (lombokDeActivated) {
              gettersAndSetters += this.generateGetterSettersForField(fieldName, fieldType ?? "", nullable);
            }
  
            // if primary key, we set constructor fields
            if (fielddata.isPKey) {
              if (constructorToInject !== "") {
                constructorToInject += ", "; //if one more to constructor
              }
              const paramPrefix = this.configProcessor.parameter_prefix ?? this.PARAM_PREFIX; //parameter prefix
              constructorToInject += `${fieldType} ${paramPrefix}${fieldName}`;
              constructorInToInject += `\n\t\tthis.${fieldName} = ${paramPrefix}${fieldName};`;
            }  
          }
        }
      }

      // Extra config --------------------------------------------------------------------------------------------------------------------------------
      // lombok activated by default
      let lombok_import = lombokDeActivated ? "" : "\nimport lombok.Getter;\nimport lombok.Setter;";
      let lombok_class_annotate = lombokDeActivated ? "": "@Getter\n@Setter";
      // -------------------------------------------------------------------------------------------------------------------------------- Extra config

      const modelTemplate = {
        className: className,
        packageBaseName: this.config.packageBaseName,
        fields: fieldsToInject,
        constructor: constructorToInject,
        in_constructor: constructorInToInject,
        gettersAndSetters: gettersAndSetters,
        lombok_import: lombok_import,
        lombok_class_annotate: lombok_class_annotate,
        extraFields_import: extraFields.getImportsTxt()
      };
      // Write to file
      await this.writeTemplateToFile({ rFilePath: `${this.getOutProcessorPath()}${className}.java`, rTemplate: tpl, rFieldsTemplate: modelTemplate, rFields: rTable?.fields });
    }
  }

  private generateGetterSettersForField(rFieldName: string, rFieldType: string, rNullable: string): string {
    const field = this.getFieldName(rFieldName);
    const upName = GeneratorTool.upperFirstLetter(field);
    const getter = `\tpublic function get${upName}(): ${rFieldType}
    {
        return this.${field};
    }`;
    const setter = `\tpublic function set${upName}(${rNullable}${rFieldType} ${field}): void
    {
        this.${field} = ${field};
    }`;
    return getter + "\n\n" + setter + "\n\n";
  }
}
