/* eslint-disable @typescript-eslint/naming-convention */
import path from "path";
import { GenericDbField } from "../../db/model/GenericDbField";
import { FileTool } from "../../tools/FileTool";
import { AbstractProcessor } from "../AbstractProcessor";
import { IConverter } from "../IConverter";
import { JavaJpaConverter } from "./jpa/JavaJpaConverter";

export abstract class AbstractJavaProcessor extends AbstractProcessor {
  protected readonly OPTIONAL_CHAR = "";

  protected getConverter(): IConverter {
    return new JavaJpaConverter();
  }

  /**
   * Get javadoc param part '@param <type> <param name>
   * @param rFieldType : type of the field
   * @param rFieldName : name of the field
   */
  protected addParamDoc(rFieldType: string, rFieldName: string): string {
    return `\t\t * @param ${rFieldName}`;
  }

  /**
   * Get optional field char, represents an optional field
   * @param rField : field
   * @returns optional field char
   */
  protected getOptionalChar(rField: GenericDbField): string {
    return rField?.isNullable ? this.OPTIONAL_CHAR : "";
  }

  /**
   * Takes an import name and generates the import call
   * @param rImportName : import name, e.g. 'java.util.Date'
   * @returns import call given that name (e.g. 'import java.util.Date;')
   */
  protected getImportFrom(rImportName: string): string {
    return rImportName ? `import ${rImportName};` : "";
  }

  /**
   * Get method name to get all xxx by yyy id
   * @param rTablenameUp : table name that we are gonna get model list with first letter upper case (xxx)
   * @param rClassNameDistant : foreign table linked to this model (yyy)
   * @returns method name
   */
  protected getFindByMethodName(rTablenameUp: string, rClassNameDistant: string) {
    return `getAll${rTablenameUp}By${rClassNameDistant}Id`;
  }


  /**
   * Create necessary folders from package name
   */
  protected initPackageFolders() {
    if (this.config.packageBaseName) {
      FileTool.createPackageDirectories(this.config.packageBaseName, this.getOutBasePath());
    } else {
      throw new Error("There is no packageBaseName entry in config file");
    }
  }

  /**
   * ovveride getOutProcessorPath() to use package folder path
   * @returns full path for processor
   */
  protected getOutProcessorPath(): string {
    const fullPath = FileTool.getFullPathFromPackage(this.config.packageBaseName ?? '', this.getOutBasePath());
    return `${fullPath}${path.sep}${this.getProcessorFolder()}${path.sep}`;
  }

}
