// export as namespace "supplant";

declare module "supplant" {
  export = Supplant;
  /**
   * Supplant constructor.
   * @api public
   */
  function Supplant(): void;
  class Supplant {
    match: RegExp;
    filters: {};
    /**
     * Variable substitution on string.
     *
     * @param {String} text
     * @param {Object} model
     * @return {String}
     * @api public
     */
    text(text: string, model: any): string;
    /**
     * Get uniq identifiers from string.
     *
     * Examples:
     *
     *    .props('{{olivier + bredele}}');
     *    // => ['olivier', 'bredele']
     *
     * @param {String} text
     * @return {Array}
     * @api public
     */
    props(text: string): any[];
    /**
     * Add substitution filter.
     *
     * Examples:
     *
     *    .filter('hello', function(str) {
     *      return 'hello ' + str;
     *    });
     *
     * @param {String} name
     * @param {Function} fn
     * @return {Supplant}
     * @api public
     */
    filter(name: string, fn: Function): Supplant;
  }
}
