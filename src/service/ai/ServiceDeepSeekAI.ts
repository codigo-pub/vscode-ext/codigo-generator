/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractServiceAI, MessageTypeAI } from "./AbstractServiceAI";

export class ServiceDeepSeekAI extends AbstractServiceAI {

  // Settings
  protected getApiUrl(): string {
    return "https://api.deepseek.com/chat/completions";
  }
  protected getDefaultModel(): string {
    return "deepseek-chat";
  }

  protected async askAI(rMessages?: MessageTypeAI[]): Promise<string | undefined> {    
    return this.defaultAskAI(rMessages);
  }

  /**
   * Extracts code inside code blocks from the response string.
   * @param responseText The text from the AI response.
   * @returns The code inside the first code block found, or an empty string if none is found.
   */
  protected extractCodeFromResponse(responseText: string): string {
    return this.defaultExtractCodeFromResponse(responseText);
  }
}
