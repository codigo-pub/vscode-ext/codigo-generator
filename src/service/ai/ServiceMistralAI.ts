/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractServiceAI, MessageTypeAI } from "./AbstractServiceAI";

export class ServiceMistralAI extends AbstractServiceAI {

  // Settings
  protected getApiUrl(): string {
    return "https://api.mistral.ai/v1/chat/completions";
  }

  protected getDefaultModel(): string {
    return "ministral-8b-latest";
  }

  protected async askAI(rMessages?: MessageTypeAI[]): Promise<string | undefined> {
    return this.defaultAskAI(rMessages);
  }

  /**
   * Extracts the content inside code blocks from the response string.
   * @param responseText The text from the AI response.
   * @returns The code inside the first code block found, or an empty string if none is found.
   */
  protected extractCodeFromResponse(responseText: string): string {
    return this.defaultExtractCodeFromResponse(responseText);
  }
}
