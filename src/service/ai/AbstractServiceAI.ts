/* eslint-disable @typescript-eslint/naming-convention */
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import { AIConfig } from "../../model/AIConfig";
import { IServiceAI } from "./IServiceAI";
import { GeneratorTool } from "../../tools/GeneratorTool";
import * as vscode from "vscode";
import { l10n } from "vscode";

/**
 * Interface for sending messages
 */
export interface MessageTypeAI {
  role: string;
  content: string;
}

export abstract class AbstractServiceAI implements IServiceAI {
  protected abstract getApiUrl(): string;
  protected abstract getDefaultModel(): string;

  // If request made to AI take more then x seconds we show a warning to adjust settings
  protected readonly MAX_DURATION_WARNING: number = 10;
  protected duration_warning: number = this.MAX_DURATION_WARNING;

  protected axiosInstance: AxiosInstance;
  protected finalConfig?: AIConfig;

  private defaultConfig: Partial<AIConfig> = {};

  /**
   * constructor
   * @param config Configuration for using the AI.
   */
  constructor(config: AIConfig) {
    this.axiosInstance = axios.create();
    this.initConfig(config);
  }

  /**
   * Initialize AI config
   * @param config : AIConfig
   */
  private initConfig(config: AIConfig) {
    // Initialize base defaultConfig
    this.defaultConfig = {
      apiUrl: this.getApiUrl(),
      model: this.getDefaultModel(),
      // temperature: 0.3,
      // max_completion_tokens: 1000,
      prompt:
        "Can you read this file to add methods (with method documentation) that you consider useful and important from a business perspective ? Just give me the new methods and please consider pessimistic scenarios.",
    };

    if (!config.enabled) {
      throw new Error("AI is disabled in the configuration.");
    }

    if (!config.apiKey) {
      throw new Error("Missing API key in the configuration.");
    }

    this.finalConfig = { ...this.defaultConfig, ...config };

    // Set api key if there is a placeholder (best practices !)
    if (this.finalConfig.apiKey) {
      this.finalConfig.apiKey = GeneratorTool.getValue(this.finalConfig.apiKey);
    }

    // Set duration warning
    this.duration_warning = this.finalConfig.duration_warning ?? this.MAX_DURATION_WARNING;
  }

  /**
   * Ask AI for completion
   * @param {MessageTypeAI[]} rMessages : all messages to ask AI something
   * @returns generated text with ai completion
   */
  protected abstract askAI(rMessages?: MessageTypeAI[]): Promise<string | undefined>;

  /**
   * Extracts the content inside code blocks from the response string.
   * @param responseText The text from the AI response.
   * @returns The code inside the first code block found, or an empty string if none is found.
   */
  protected abstract extractCodeFromResponse(responseText?: string): string;

  /**
   * Use AI for completion of a generated text
   * @param rGeneratedText : generated text that we want to complete with AI
   * @param rModel : [Optional]string with model properties
   * @param rCustomPrompt [Optional]custom prompt to override given default prompt
   * @returns generated text with ai completion
   */
  async generateCompletion(rGeneratedText: string, rModel?: string, rCustomPrompt?: string): Promise<string | undefined> {
    if (this.finalConfig) {
      // Construct the messages for the chat model ---------------------------------------------------------------------------------------------------
      const systemMessages = [
        {
          role: "system",
          content: "You are a helpful assistant that help improve code. You give only the block with file content and don't say anything else",
        }, // General context for the model
      ];

      const userMessages = this.buildPrompt(rGeneratedText, rCustomPrompt ?? this.finalConfig.prompt ?? "Emprove this", rModel);
      const messages = systemMessages.concat(userMessages);
      // --------------------------------------------------------------------------------------------------- Construct the messages for the chat model

      try {
        const startTime = Date.now();

        // Ask AI ang get response string
        const aiResponse = await this.askAI(messages);

        // Get duration of request
        const endTime = Date.now();
        const duration = Math.floor((endTime - startTime) / 1000);
        // console.log(`OpenAI request duration: ${duration} secondes`);
        // If duration is longer then N secondes, we put a warning
        if (duration > this.duration_warning) {
          vscode.window.showWarningMessage(l10n.t("The request took {0} seconds, maybe you should adjust AI settings", duration));
        }

        // Extract code from response (if there is a special way to deliver content)
        const codeContent = this.extractCodeFromResponse(aiResponse);

        return codeContent || aiResponse || rGeneratedText;
      } catch (error: any) {
        const errMsg = error?.response?.data?.error?.message ? error.response.data.error.message : "Error while making the request to the AI API"; // Handle API error
        // console.error("Error while making the request to the AI API:", errMsg);
        return Promise.reject(errMsg); // Throw the error to be handled by the caller
      }
    }

    // If no suitable config is found, return undefined
    return undefined;
  }

  /**
   * Builds the message with prompt and generated text.for AI
   * @param rGeneratedText The base text.
   * @param rPrompt Prompt for asking things to AI
   * @param rModel : string with model properties
   * @returns A prompt ready to be sent to the AI.
   */
  protected buildPrompt(rGeneratedText: string, rPrompt: string, rModel?: string): MessageTypeAI[] {
    let messages: MessageTypeAI[] = [];
    // If model provided
    if (rModel) {
      messages.push({ role: "user", content: "Given this model : " + rModel });
    }
    messages.push({ role: "user", content: rPrompt });
    messages.push({ role: "user", content: rGeneratedText });
    return messages;
  }

  /**
   * Generate a default request config with apiKey from finalConfig object
   * Returns an empty object if finalConfig is undefined
   * @returns a default request config
   */
  protected getDefaultRequestConfig(): AxiosRequestConfig {
    return this.finalConfig
      ? {
          headers: {
            Authorization: `Bearer ${this.finalConfig.apiKey}`, // API key for authorization
            "Content-Type": "application/json", // Content type for JSON request
          },
        }
      : {};
  }

  /**
   * This is the default method to ask something to the AI. As many API use the same way (so far) every child service
   * can call this method through his askAI method or implement is own way
   *
   * @param rMessages messages to process
   * @returns ai completion
   */
  protected async defaultAskAI(rMessages?: MessageTypeAI[]): Promise<string | undefined> {
    const response =
      this.finalConfig && rMessages && rMessages.length > 0
        ? await this.axiosInstance.post(
            this.finalConfig.apiUrl!, // API URL
            {
              model: this.finalConfig.model, // Model (e.g., mistral-7b)
              messages: rMessages, // The structured list of messages
              max_tokens: this.finalConfig.max_completion_tokens, // Token limit for the response
              temperature: this.finalConfig.temperature, // Level of creativity (lower is more deterministic)
            },
            this.getDefaultRequestConfig()
          )
        : undefined;

    // If no answer
    if (!response?.data?.choices || response.data.choices.length === 0) {
      return undefined;
    }

    // Return the content of the response
    return response.data.choices[0]?.message?.content.trim();
  }

  /**
   * This is the default method to extracts the content inside code blocks from the response string.
   * As many API use the same way (so far) every child service can call this method through his extractCodeFromResponse method 
   * or implement is own way
   * 
   * @param responseText The text from the AI response.
   * @returns The code inside the first code block found, or an empty string if none is found.
   */
  protected defaultExtractCodeFromResponse(responseText: string): string {
    // Regular expression to match and capture content inside ```...``` blocks
    const codeBlockRegex = /```[a-zA-Z]*\n([\s\S]*?)\n```/g;
    const matches = [...responseText.matchAll(codeBlockRegex)];

    // If there are code blocks, return the first match
    if (matches.length > 0) {
      return matches[0][1].trim(); // Return the content inside the first code block
    }

    // Return an empty string if no code block is found
    return "";
  }

  public getAxiosInstance(): AxiosInstance {
    return this.axiosInstance;
  }

  public setAxiosInstance(value: AxiosInstance) {
    this.axiosInstance = value;
  }
}
