import { l10n } from "vscode";
import { AIConfig } from "../../model/AIConfig";
import { AIEnum } from "../../model/AIEnum";
import { ServiceOpenAI } from "./ServiceOpenAI";
import { ServiceDeepSeekAI } from "./ServiceDeepSeekAI";
import { IServiceAI } from "./IServiceAI";
import * as vscode from "vscode";
import { ServiceMistralAI } from "./ServiceMistralAI";

export class ServiceAIFactory {
  /**
   * Get AI service class
   * @param rConfig : AIConfig object
   * @returns
   */
  static getServiceAI(rConfig: AIConfig): IServiceAI | undefined {
    let serviceAI: IServiceAI | undefined;
    if (rConfig) {
      // If no iaNAme, set default to OPENAI
      if (!rConfig.aiName) {
        rConfig.aiName = AIEnum.OPENAI;
      }
      if (rConfig?.aiName) {
        // Set service implementation
        switch (rConfig.aiName) {
          case AIEnum.OPENAI:
            serviceAI = new ServiceOpenAI(rConfig);
            break;
          case AIEnum.DEEPSEEK:
            serviceAI = new ServiceDeepSeekAI(rConfig);
            break;
          case AIEnum.MISTRAL:
            serviceAI = new ServiceMistralAI(rConfig);
            break;
          default:
            let msg = l10n.t("Config data error");
            vscode.window.showErrorMessage(`iaName : ${msg} [${rConfig.aiName}]`);
            break;
        }
      }
    }
    return serviceAI;
  }
}
