/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractServiceAI, MessageTypeAI } from "./AbstractServiceAI";

export class ServiceOpenAI extends AbstractServiceAI {

  // Settings
  protected getApiUrl(): string {
    return "https://api.openai.com/v1/chat/completions";
  }
  protected getDefaultModel(): string {
    return "gpt-4o";
  }

  protected async askAI(rMessages?: MessageTypeAI[]): Promise<string | undefined> {
    // OpenAI has property 'max_completion_tokens' instead of 'max_tokens' so we must implement our own way instead of calling defaultAskAI method
    const response = this.finalConfig && rMessages && rMessages.length > 0
      ? await this.axiosInstance.post(
          this.finalConfig.apiUrl!, // API URL
          {
            model: this.finalConfig.model, // Model (e.g., gpt-4)
            messages: rMessages, // The structured list of messages
            max_completion_tokens: this.finalConfig.max_completion_tokens, // Token limit for the response
            temperature: this.finalConfig.temperature, // Level of creativity (lower is more deterministic)
          },
          this.getDefaultRequestConfig(),
        )
      : undefined;
      
    // If no answer
    if (!response?.data?.choices || response.data.choices.length === 0) {
      return undefined;
    }

    // Return the content of the response
    return response.data.choices[0]?.message?.content.trim();
    
  }

  /**
   * Extracts the content inside code blocks from the response string.
   * @param responseText The text from the AI response.
   * @returns The code inside the first code block found, or an empty string if none is found.
   */
  protected extractCodeFromResponse(responseText: string): string {
    return this.defaultExtractCodeFromResponse(responseText);
  }
}
