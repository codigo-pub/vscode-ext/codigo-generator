import { IServiceGenerator } from "./IServiceGenerator";
import { ServiceGenerator } from "./ServiceGenerator";
import { ConfigGenerator } from "../model/ConfigGenerator";
import { IDatabase } from "../db/IDatabase";

export class ServiceGeneratorFactory {
  static getGenerator(rConfig: ConfigGenerator, rDb: IDatabase): IServiceGenerator | undefined {
    let generator: IServiceGenerator | undefined;
    // if template was specified
    if (rDb && rConfig?.global_config?.template) {
      generator = new ServiceGenerator(rConfig, rDb);
    }
    return generator;
  }
}
