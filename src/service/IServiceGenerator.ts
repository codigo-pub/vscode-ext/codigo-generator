
export interface IServiceGenerator {

    process(): Promise<void>;

}