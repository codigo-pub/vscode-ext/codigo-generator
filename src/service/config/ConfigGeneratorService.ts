import { AbstractConfigService } from "./AbstractConfigService";
import { ConfigGenerator } from "../../model/ConfigGenerator";
import { GeneratorTool } from "../../tools/GeneratorTool";

export class ConfigGeneratorService extends AbstractConfigService<ConfigGenerator> {
  checkEnv(rCfg: ConfigGenerator): void {
    // Replace env variables
    if (rCfg?.global_config?.database?.password) {
      const keyPass = GeneratorTool.extractEnvKey(rCfg.global_config.database.password);
      if (keyPass) {
        rCfg.global_config.database.password = process.env[keyPass];
        // If password is not defined, we throw an error
        if (!rCfg.global_config.database.password) {
          throw new Error(`Password ${keyPass} not found in your env variables ?!`);
        }
      }
    }
  }

  checkConfig(rCfg: ConfigGenerator) {
    // Global config
    this.checkCfgItem("global_config", rCfg.global_config);
    this.checkCfgItem("global_config.db_type", rCfg?.global_config?.db_type);
    this.checkCfgItem("global_config.template", rCfg?.global_config?.template);
    this.checkCfgItem("global_config.out_folder", rCfg?.global_config?.out_folder);

    this.checkCfgItem("global_config.database", rCfg?.global_config?.database);
    this.checkCfgItem("global_config.database.host", rCfg?.global_config?.database?.host);
    this.checkCfgItem("global_config.database.user", rCfg?.global_config?.database?.user);
    this.checkCfgItem("global_config.database.password", rCfg?.global_config?.database?.password);
    this.checkCfgItem("global_config.database.port", rCfg?.global_config?.database?.port);
    this.checkCfgItem("global_config.database.database", rCfg?.global_config?.database?.database);

    // AI ?
    if (rCfg?.global_config?.useAI) {
      // Check mandatory fields
      this.checkCfgItem("global_config.useAI.apiKey", rCfg.global_config.useAI.apiKey);
      this.checkCfgItem("global_config.useAI.enabled", rCfg.global_config.useAI.enabled);
      // Check if user has given a model if he confused 'o' with '0' on 'gpt-4o' model name
      if (rCfg.global_config.useAI.model && rCfg.global_config.useAI.model.startsWith("gpt-40")) {
        throw new Error(`You misspelled the model name, you confused 'o' with '0' in ${rCfg.global_config.useAI.model}`);
      }
    }

    // Processors
    this.checkCfgItem("processors", rCfg.processors);
    if (rCfg.processors) {
      // Check all processors in array of processors
      for (let i = 0; i < rCfg.processors.length; i++) {
        this.checkCfgItem(`processors[${i}]`, rCfg.processors[i]?.name);
      }
    }
  }
}
