/* eslint-disable @typescript-eslint/naming-convention */
import { IDatabase } from "../db/IDatabase";
import { ConfigGenerator } from "../model/ConfigGenerator";
import { ProcessorFactory } from "../processors/ProcessorFactory";
import { ProgressWindow } from "../tools/ProgressWindow";
import { IServiceGenerator } from "./IServiceGenerator";
import * as vscode from "vscode";

export class ServiceGenerator implements IServiceGenerator {
  protected db: IDatabase;
  protected config: ConfigGenerator;

  protected readonly TEMPLATES_FOLDER = "./templates";

  constructor(rConfig: ConfigGenerator, rDb: IDatabase) {
    this.config = rConfig;
    this.db = rDb;
  }

  private async init(): Promise<void> {
    // init base template folder
    if (this.config?.context?.extensionUri && this.config.global_config) {
      this.config.global_config.baseTemplatesFolder = vscode.Uri.joinPath(this.config?.context?.extensionUri, this.TEMPLATES_FOLDER).fsPath;
    } else {
      throw new Error("No context extensionUri or no global config !");
    }
    // init db
    await this.db?.init();
  }

  async process(): Promise<void> {
    try {
      await this.init();

      if (this.db && this.config?.global_config && this.config.context) {
        // If there are processors
        if (this.config?.processors && this.config?.processors.length > 0) {
          // Filter processors that are not disable (cause default is to enable)
          const confProcessorList = this.config?.processors.filter((item) => item.generate !== false);
          const processorNames = confProcessorList.map((p) => p.name || "Unnamed Processor");
          const progressWindow = new ProgressWindow(processorNames);
          progressWindow.show();

          // browse processors
          for (let i = 0; i < confProcessorList.length; i++) {
            const confProcessor = confProcessorList[i];
            // get processor by name
            const processor = ProcessorFactory.getProcessor(this.config.global_config, confProcessor, this.db);
            //run process
            if (processor) {
              // Set progress callback method !
              processor.setOnProgress((progress: number) => {
                progressWindow.updateProgress(i, progress); // Update progress for each processor
              });
              // Then we can run process
              await processor.process(); 
              // Set to 100 when all is finish, wen can do this cause we await before
              progressWindow.updateProgress(i, 100);
            } else {
              // Should never come here...
              progressWindow.updateProgress(i, 100); // force update progress to 100 cause there is no processor (weird ?!)
            }
          }

          // progressWindow.dispose();
        }
      }

      await this.db?.close();
    } catch (error) {
      if (error instanceof Error) {
        const code = (error as any).code;
        vscode.window.showErrorMessage(`Process error: [${code}][${error.message}]`);
      } else {
        vscode.window.showErrorMessage(`Process error: [${(error as any).message ?? (error as any).code}]`);
      }      
    }
  }
}
