import { Collection, Item, ItemGroup, Request, RequestBody, Script, Event } from "postman-collection";
import { GenericDbTypeField } from "../db/model/GenericDbTypeField";
import { EnumDbType } from "../db/model/EnumDbType";
import { GenericDbTable } from "../db/model/GenericDbTable";
import { GenericDbField } from "../db/model/GenericDbField";
import { AbstractProcessor } from "../processors/AbstractProcessor";
import { PostmanConfig } from "../model/PostmanConfig";
import { GeneratorTool } from "../tools/GeneratorTool";
import { SqlTool } from "../tools/SqlTool";
import { FakeDataTool } from "../tools/FakeDataTool";

export class ServicePostmanBuilder<T extends AbstractProcessor> {
  protected processor: T;
  protected cfg: PostmanConfig;

  constructor(processor: T, rCfg: PostmanConfig) {
    this.processor = processor;
    this.cfg = rCfg;
  }

  /**
   * Get Postman filename
   * @param rDefaultFilename : default filename if no extra_config.name and no packageBaseName  
   * @param rSuffixe : suffixe to add to the name. Not used when extra_config.name is provided
   * @returns filename
   */
  getFileName(rDefaultFilename: string, rSuffixe: string): string {
    // If filename was provided we use it
    let fileName;
    if (this.processor.getConfigProcessor()?.extra_config?.name) {
      const cfgName = this.processor.getConfigProcessor().extra_config.name;
      // if cfgName ends with '.json' we don't add it
      const addExtension = !cfgName.endsWith(".json");
      fileName = `${this.processor.getConfigProcessor().extra_config.name}${addExtension ? ".json" : ""}`;
    } else {
      fileName = `${this.processor.getConfig().packageBaseName ?? rDefaultFilename}${rSuffixe}.json`;
    }
    return fileName;
  }

  /**
   * Generates Postman collection from tables
   * @param rTables : table list
   * @param rAppName : app name
   * @returns
   */
  generateCollectionFromTables(rTables: GenericDbTable[], rAppName?: string): Collection {
    // Create a new Postman collection
    const collection = new Collection({
      info: {
        name: rAppName ?? "no_name",
        version: "1.0.0",
      },
      item: [],
    });
    // Browse tables to add request items
    if (rTables && rTables.length > 0) {
      for (const table of rTables) {
        if (table?.tableName && table?.fields && table.fields.length > 0) {
          // Create a group of items for this table
          const itemGroup = new ItemGroup<Item>({
            name: table.tableName,
            description: `Requests for ${table.tableName}`,
            item: [],
          });
          // Load urls requests
          this.addRequestForTable(table, GeneratorTool.getPkeyFields(table.fields), itemGroup);
          // findBy Methods for ManyToMany tables
          this.addFindByRequestForTable(table, itemGroup);
          // Add tests to every items of itemGroup
          this.addTestsToGroup(itemGroup);
          // Add itemGroup to collection
          collection.items.add(itemGroup);
        }
      }
    }
    return collection;
  }

  /**
   * Generates all postman collection items for this table
   * @param rTable : Table
   * @param rPkeyList : Pkey list
   * @param rItemGroup : ItemGroup
   */
  addRequestForTable(rTable: GenericDbTable, rPkeyList: GenericDbField[], rItemGroup: ItemGroup<Item>) {
    if (this.cfg && this.processor) {
      const classModelName = this.processor.processClassNameFromTable(rTable.tableName);
      const baseModelApi = this.processor.getModelBaseApiUrlFromTable(rTable.tableName);
      // Make url parameters fields
      let urlParameters: string[] = [];
      for (const pKey of rPkeyList) {
        if (pKey.fieldName) {
          const value = this.getFakeValueFromField(pKey);
          let parameter = "";
          // Only take strings and numbers
          if (typeof value === "number" || typeof value === "string") {
            parameter = typeof value === "number" ? value.toString() : value;
          } else {
            // otherwise we advise by putting this parameter msg
            parameter = "<unsupported_url_parameter>";
          }
          urlParameters.push(parameter);
        }
      }
      // Generates path urls
      // Get all
      const requestItemGetAll = this.getDefaultItem({ rItemName: `Get all ${classModelName}`, rBasename: baseModelApi });
      if (this.cfg.path_adds?.get_all) {
        requestItemGetAll.request.url.path?.push(...this.cfg.path_adds.get_all);
      }
      rItemGroup.items.add(requestItemGetAll); // add item

      // Get by id
      const requestItemGetById = this.getDefaultItem({ rItemName: `Get by ${classModelName} id`, rBasename: baseModelApi });
      if (this.cfg.path_adds?.get_by_id) {
        requestItemGetById.request.url.path?.push(...this.cfg.path_adds.get_by_id);
      }
      requestItemGetById.request.url.path?.push(...urlParameters); // path
      rItemGroup.items.add(requestItemGetById); // add item

      // Create
      const requestItemCreate = this.getJsonItem({ rItemName: `Create ${classModelName}`, rBasename: baseModelApi });
      if (this.cfg.path_adds?.create) {
        requestItemCreate.request.url.path?.push(...this.cfg.path_adds.create);
      }
      requestItemCreate.request.body = this.getFakeBodyFromTableFields(rTable.fields, true);
      rItemGroup.items.add(requestItemCreate); // add item

      // Delete by id
      const requestItemDeleteById = this.getDefaultItem({ rItemName: `Delete by ${classModelName} id`, rBasename: baseModelApi, rMethod: "DELETE" });
      if (this.cfg.path_adds?.delete_by_id) {
        requestItemDeleteById.request.url.path?.push(...this.cfg.path_adds.delete_by_id);
      }
      requestItemDeleteById.request.url.path?.push(...urlParameters); // path
      rItemGroup.items.add(requestItemDeleteById); // add item

      // Update
      const requestItemUpdate = this.getJsonItem({ rItemName: `Update by ${classModelName} id`, rBasename: baseModelApi, rMethod: "PUT" });
      if (this.cfg.path_adds?.update_by_id) {
        requestItemUpdate.request.url.path?.push(...this.cfg.path_adds.update_by_id);
      }
      requestItemUpdate.request.url.path?.push(...urlParameters); // path
      requestItemUpdate.request.body = this.getFakeBodyFromTableFields(rTable.fields);
      rItemGroup.items.add(requestItemUpdate); // add item
    } else {
      console.error("Dev error - your config should have a getClassName function");
    }
  }

  /**
   * Generates url request for findXxxByYyy method (ManyToMany relation)
   * @param rTable : Table
   * @param rItemGroup : ItemGroup
   */
  addFindByRequestForTable(rTable: GenericDbTable, rItemGroup: ItemGroup<Item>) {
    if (this.cfg && this.processor) {
      const baseModelApi = this.processor.getModelBaseApiUrlFromTable(rTable.tableName);
      const manyTomany = this.cfg.many_to_many_sep ?? SqlTool.MANY_TOO_MANY_SEP;
      // Check if we can add a findXxxByYyy
      if (rTable.referencedFKeys && rTable.referencedFKeys.length > 0) {
        // If one of referenced is a pivot table
        for (const element of rTable.referencedFKeys) {
          // If table is pivot table and right part (by default) is current model we can proceed
          // Django (~ Python) can't handle both declarations ManyToMany in both sides (leads to circular reference)
          // That's why we check right part, it's to ignore this table if current model is in the other side (we only want one relation)
          if (
            element?.tableName &&
            SqlTool.isManyToManyTable(element.tableName, manyTomany) &&
            (!this.cfg.part ||
              SqlTool.hasTableLeftOrRight({ fullTableName: element.tableName, knownTableName: rTable.tableName, part: this.cfg.part }))
          ) {
            // Extract other table name
            let tableDistant = SqlTool.extractOtherTableName(element.tableName, rTable.tableName, manyTomany);
            let classNameDistant = this.processor.processClassNameFromTable(tableDistant);
            let upperModel = GeneratorTool.upperFirstLetter(rTable.tableName);
            const requestItemFindBy = this.getDefaultItem({
              rItemName: `Find ${upperModel} by ${classNameDistant} id`,
              rBasename: baseModelApi,
            });
            requestItemFindBy.request.url.path?.push(classNameDistant.toLowerCase());
            // We only handle single pkey and numeric pkey, so we force value here without checking pkeys
            requestItemFindBy.request.url.path?.push("1");
            rItemGroup.items.add(requestItemFindBy); // add item
          }
        }
      }
    }
  }

  /**
   * Get default item for Postman Collection
   * You should then change some values like name, url path, etc.
   * @param rItemName : name if the item
   * @param rBasename : base name to dd to base api
   * @param rMethod : http method (GET/POST/DELETE/...), default to GET
   * @param rJsonContent : true to set content-type as JSON (for POST and UPDATE)
   * @returns default item for postman Collection
   */
  getDefaultItem({
    rItemName,
    rBasename,
    rMethod = "GET",
    rJsonContent = false,
  }: {
    rItemName: string;
    rBasename?: string;
    rMethod?: string;
    rJsonContent?: boolean;
  }): Item {
    // For path, we expect user to use env variable for base url api in postman as best practice
    let basePath = this.cfg?.with_base_api ? ["{{base_api}}"] : [];
    if (rBasename) {
      basePath.push(rBasename);
    }
    const header = rJsonContent ? [{ key: "Content-Type", value: "application/json" }] : [];
    return new Item({
      name: rItemName,
      request: {
        method: rMethod,
        header: header,
        url: {
          protocol: "",
          host: "{{base_host}}",
          port: "{{base_port}}",
          path: basePath,
        },
      },
    });
  }

  /**
   * Shortcut to get default Item with json Content-type and method POST by default (can be overwritten)
   * @param rItemName : name if the item
   * @param rBasename : base name to dd to base api
   * @param rMethod : http method (GET/POST/DELETE/...), default to POST
   * @returns
   */
  getJsonItem({ rItemName, rBasename, rMethod = "POST" }: { rItemName: string; rBasename?: string; rMethod?: string }): Item {
    return this.getDefaultItem({ rItemName, rBasename, rMethod, rJsonContent: true });
  }

  /**
   * Get default json body configuration for post/update
   * Note that Raw body is empty
   * @returns default json body configuration
   */
  getDefaultJsonBody(): RequestBody {
    return new RequestBody({
      mode: "raw",
      raw: "{}",
    });
  }

  /**
   * Get fake value based on field and type expected
   * @param rField : sql type
   * @returns fake value
   */
  getFakeValueFromField(rField?: GenericDbField): string | number | boolean | string[] {
    let fakeValue: string | number | boolean | string[] = 1; // default if no type
    if (rField?.fieldName && rField.type) {
      const fieldType = rField.type;
      // set a fake value from type
      switch (rField.type.type) {
        case EnumDbType.char:
        case EnumDbType.varchar:
          // Repeat the text if it is less than five characters to avoid exceeding the limit
          fakeValue = FakeDataTool.getTxtValueFromColName(rField.fieldName, fieldType.length);
          break;
        case EnumDbType.uuid:
          fakeValue = "123e4567-e89b-12d3-a456-426614174000";
          break;
        case EnumDbType.enum:
          fakeValue = fieldType.extra && fieldType.extra.length > 0 ? fieldType.extra[0] : "NoEnum";
          break;
        case EnumDbType.long:
        case EnumDbType.int:
        case EnumDbType.numeric:
        case EnumDbType.float:
        case EnumDbType.double:
          fakeValue = 1;
          break;
        case EnumDbType.date:
          fakeValue = new Date().toISOString().split("T")[0];
          break;
        case EnumDbType.datetime:
        case EnumDbType.timestamp:
          fakeValue = new Date().toISOString();
          break;
        case EnumDbType.time:
          fakeValue = "00:00:00";
          break;
        case EnumDbType.interval:
          fakeValue = "1 day";
          break;
        case EnumDbType.boolean:
          fakeValue = true;
          break;
        case EnumDbType.json:
        case EnumDbType.longtext: // If it is longtext, it is probably json too
          fakeValue = JSON.stringify({ codigo: "generator" });
          break;
        case EnumDbType.blob:
          fakeValue = "Y29naWdvLWdlbmVyYXRvcg=="; // <= codigo-generator in base64
          break;
        case EnumDbType.array:
          fakeValue = ["xxx"];
          break;
        default:
          fakeValue = "xxx";
          break;
      }
    }
    return fakeValue;
  }

  /**
   * Generate a fake body for create
   * @param rFields : Fields
   * @param rExcludeAutoPKeys : true to exclude auto increment pkey fields (default to false)
   */
  getFakeBodyFromTableFields(rFields?: GenericDbField[], rExcludeAutoPKeys: boolean = false): RequestBody {
    let body = this.getDefaultJsonBody();
    let rawBody: any = {};
    if (rFields && rFields.length > 0) {
      for (const field of rFields) {
        // Need a fieldName. If rExcludeAutoPKeys and this is a pkey, we skip
        if (field.fieldName && (!rExcludeAutoPKeys || (rExcludeAutoPKeys && !field.isPKey))) {
          rawBody[field.fieldName] = this.getFakeValueFromField(field);
        }
      }
    }
    // Set raw body
    body.raw = JSON.stringify(rawBody);
    return body;
  }

  checkStatusPostResponse(): string {
    return `pm.test("Test si retour ok", () => { pm.response.to.have.status(200); });`;
  }

  /**
   * Add tests to all item group
   * @param rItemGroup : item group
   */
  addTestsToGroup(rItemGroup: ItemGroup<Item>) {
    // Iterate through each item (request) in the collection
    rItemGroup.items.each((item) => {
      if (item) {
        // It is important that every script and event is a new instance !!!
        // Prepare script
        const scriptTest = new Script();
        scriptTest.type = "text/javascript";
        scriptTest.exec = [this.checkStatusPostResponse()];
        // Create event
        const eventTest = new Event({
          listen: "test",
          script: scriptTest,
        });
        item.events.add(eventTest);
      }
    });
  }
}
