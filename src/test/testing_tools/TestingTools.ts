/* eslint-disable @typescript-eslint/naming-convention */

import * as vscode from "vscode";
import { ConfigGenerator } from "../../model/ConfigGenerator";
import sinon from "sinon";
import { GenericDbTable } from "../../db/model/GenericDbTable";
import { IDatabase } from "../../db/IDatabase";
import { AbstractDatabase } from "../../db/AbstractDatabase";

/**
 * This class provide some redundant fake objects for testing purpose 
 * As it is bound to be used by everyone, don't change some values without pragmatic thinking :)
 */
export class TestingTools {
  /**
   * Get a trivial config for test purpose
   * @returns a trivial config
   */
  static getConfig(): ConfigGenerator {
    return {
      global_config: {
        db_type: "mysql",
        database: {
          host: "localhost",
          user: "quizz_user",
          password: "${QUIZ_DB_PASS}",
          database: "quizz",
          port: 3306,
        },
        template: "php",
        out_folder: "/tmp/out",
        skip_these_tables: [],
        only_these_tables: ["tests"],
      },
      processors: [
        { name: "doctrine_repository", generate: true, parameter_prefix: "_" },
        { name: "doctrine_entity", generate: true, parameter_prefix: "_" },
        { name: "pdo_dao", generate: true, parameter_prefix: "_" },
        { name: "model", generate: true, parameter_prefix: "_" },
      ],
    } as unknown as ConfigGenerator;
  }

  static getFakeVsCodeExtensionContext(): vscode.ExtensionContext {
    return {
      subscriptions: [],
      workspaceState: {
        get: (key: string) => undefined,
        update: (key: string, value: any) => Promise.resolve(),
        keys: function (): readonly string[] {
          throw new Error("Function not implemented.");
        },
      },
      globalState: {
        get: (key: string) => undefined,
        update: (key: string, value: any) => Promise.resolve(),
        keys: () => [],
        setKeysForSync: (keys: readonly string[]) => {},
      },
      secrets: {
        get: function (key: string): Thenable<string | undefined> {
          throw new Error("Function not implemented.");
        },
        store: function (key: string, value: string): Thenable<void> {
          throw new Error("Function not implemented.");
        },
        delete: function (key: string): Thenable<void> {
          throw new Error("Function not implemented.");
        },
        onDidChange: (listener: (e: vscode.SecretStorageChangeEvent) => any) => ({ dispose: () => {} }),
      },
      extensionUri: vscode.Uri.parse("fake:extensionUri"),
      extensionPath: "fake/extensionPath",
      environmentVariableCollection: {
        replace: () => undefined,
        append: () => undefined,
        prepend: () => undefined,
        get: () => undefined,
        forEach: () => undefined,
        getScoped: function (scope: vscode.EnvironmentVariableScope): vscode.EnvironmentVariableCollection {
          throw new Error("Function not implemented.");
        },
        persistent: false,
        description: undefined,
        delete: function (variable: string): void {
          throw new Error("Function not implemented.");
        },
        clear: function (): void {
          throw new Error("Function not implemented.");
        },
        [Symbol.iterator]: function (): Iterator<[variable: string, mutator: vscode.EnvironmentVariableMutator], any, undefined> {
          throw new Error("Function not implemented.");
        },
      },
      asAbsolutePath: (relativePath: string): string => {
        throw new Error("Function not implemented.");
      },
      storageUri: vscode.Uri.parse("fake:storageUri"),
      storagePath: "fake/storagePath",
      globalStorageUri: vscode.Uri.parse("fake:globalStorageUri"),
      globalStoragePath: "fake/globalStoragePath",
      logUri: vscode.Uri.parse("fake:logUri"),
      logPath: "fake/logPath",
      extensionMode: vscode.ExtensionMode.Production,
      extension: {
        extensionPath: "fake/extensionPath",
        extensionUri: vscode.Uri.parse("fake:extensionUri"),
        id: "fakeExtensionId",
        isActive: true,
        packageJSON: {},
        extensionKind: vscode.ExtensionKind.UI,
        exports: undefined,
        activate: function (): Thenable<any> {
          throw new Error("Function not implemented.");
        },
      },
    };
  }

  /**
   * Get a db mock
   * @returns db mock
   */
  static getDbMock(): IDatabase {
    // Create a mock for db for testing
    return {
      // used
      getAllTablesDescription: sinon.stub().resolves(TestingTools.fakeAllDbTables()),
      getTableDescription: sinon.stub().resolves(TestingTools.fakeDbTable()),
      getSchema: sinon.stub().returns(AbstractDatabase.PUBLIC_SCHEMA),
      close: sinon.stub().resolves({}),

      // not used
      query: sinon.stub().resolves({}),
      init: sinon.stub().resolves({}),
      fetchRow: sinon.stub().returns({}),
    } as IDatabase;
  }

  /**
   * Get fake array of GenericDbTable
   * @returns array of GenericDbTable
   */
  static fakeAllDbTables(): GenericDbTable[] {
    return [
      {
        tableName: "classrooms",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "classroomname",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
        ],
      },
      {
        tableName: "classrooms_has_students",
        fields: [
          {
            fieldName: "classrooms_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "students_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
        ],
        fKeys: [
          {
            columnName: "classrooms_id",
            constraintName: "fk_classrooms_has_students_classrooms1",
            referencedTable: "classrooms",
            referencedColumn: "id",
          },
          {
            columnName: "students_id",
            constraintName: "fk_classrooms_has_students_students1",
            referencedTable: "students",
            referencedColumn: "id",
          },
        ],
      },
      {
        tableName: "classrooms_has_teachers",
        fields: [
          {
            fieldName: "classrooms_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "teachers_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
        ],
        fKeys: [
          {
            columnName: "classrooms_id",
            constraintName: "fk_classrooms_has_teachers_classrooms1",
            referencedTable: "classrooms",
            referencedColumn: "id",
          },
          {
            columnName: "teachers_id",
            constraintName: "fk_classrooms_has_teachers_teachers1",
            referencedTable: "teachers",
            referencedColumn: "id",
          },
        ],
      },
      {
        tableName: "classrooms_has_tests",
        fields: [
          {
            fieldName: "classrooms_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "tests_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
        ],
        fKeys: [
          {
            columnName: "classrooms_id",
            constraintName: "fk_classrooms_has_tests_classrooms1",
            referencedTable: "classrooms",
            referencedColumn: "id",
          },
          {
            columnName: "tests_id",
            constraintName: "fk_classrooms_has_tests_tests1",
            referencedTable: "tests",
            referencedColumn: "id",
          },
        ],
      },
      {
        tableName: "questions",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "title",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "optionA",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "optionB",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "optionC",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "optionD",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "correctAns",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 1,
            },
          },
          {
            fieldName: "score",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
        ],
      },
      {
        tableName: "status",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "name",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
        ],
      },
      {
        tableName: "students",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "login",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "password",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
        ],
      },
      {
        tableName: "students_has_tests",
        fields: [
          {
            fieldName: "students_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "tests_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "score",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "status",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 4,
            },
          },
          {
            fieldName: "started_at",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "date",
              length: undefined,
            },
          },
          {
            fieldName: "last_updated_at",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "date",
              length: undefined,
            },
          },
          {
            fieldName: "done_count",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "correct_answers",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "wrong_answers",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "skipped_answers",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
        ],
        fKeys: [
          {
            columnName: "students_id",
            constraintName: "fk_students_has_tests_students1",
            referencedTable: "students",
            referencedColumn: "id",
          },
          {
            columnName: "tests_id",
            constraintName: "fk_students_has_tests_tests1",
            referencedTable: "tests",
            referencedColumn: "id",
          },
        ],
      },
      {
        tableName: "teachers",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "email",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "password",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
        ],
      },
      {
        tableName: "tests",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "teacher_id",
            isNullable: false,
            isPKey: false,
            isFKey: true,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "name",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "date",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "date",
              length: undefined,
            },
          },
          {
            fieldName: "status_id",
            isNullable: false,
            isPKey: false,
            isFKey: true,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "subject",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            type: {
              type: "char",
              length: 255,
            },
          },
        ],
        fKeys: [
          {
            columnName: "teacher_id",
            constraintName: "tests_fk0",
            referencedTable: "teachers",
            referencedColumn: "id",
          },
          {
            columnName: "status_id",
            constraintName: "tests_fk1",
            referencedTable: "status",
            referencedColumn: "id",
          },
        ],
      },
      {
        tableName: "tests_has_questions",
        fields: [
          {
            fieldName: "tests_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "questions_id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            type: {
              type: "int",
              length: 11,
            },
          },
        ],
        fKeys: [
          {
            columnName: "questions_id",
            constraintName: "fk_tests_has_questions_questions1",
            referencedTable: "questions",
            referencedColumn: "id",
          },
          {
            columnName: "tests_id",
            constraintName: "fk_tests_has_questions_tests1",
            referencedTable: "tests",
            referencedColumn: "id",
          },
        ],
      },
    ] as GenericDbTable[];
  }

  /**
   * Gat fake GenericDbTable
   * @returns GenericDbTable
   */
  static fakeDbTable(): GenericDbTable {
    return {
      tableName: "classrooms",
      fields: [
        {
          fieldName: "id",
          isNullable: false,
          isPKey: true,
          isFKey: false,
          type: {
            type: "int",
            length: 11,
          },
        },
        {
          fieldName: "classroomname",
          isNullable: false,
          isPKey: false,
          isFKey: false,
          type: {
            type: "char",
            length: 255,
          },
        },
      ],
    } as GenericDbTable;
  }
}
