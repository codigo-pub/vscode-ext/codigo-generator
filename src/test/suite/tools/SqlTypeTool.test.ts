import { expect } from 'chai';
import { SqlTool } from '../../../tools/SqlTool';

// import * as myExtension from '../../extension';

suite('SqlTypeTool Test Suite', () => {
    console.log('Start SqlTypeTool tests.');

    setup(() => {});

    test('extractNumber test', () => {
        expect(SqlTool.extractNumber("int(11)")).to.equal(11);
        expect(SqlTool.extractNumber("varchar(255)")).to.equal(255);
        expect(SqlTool.extractNumber("int")).to.be.undefined;
        expect(SqlTool.extractNumber("int(abc)")).to.be.undefined;
	});

    test('extractEnumValues test', () => {
        expect(SqlTool.extractEnumValues("CREATE TYPE oui_non AS ENUM ('OUI', 'NON')")).to.deep.equal(['OUI', 'NON']);
        expect(SqlTool.extractEnumValues("ENUM('YES', 'NO', 'MAYBE')")).to.deep.equal(['YES', 'NO', 'MAYBE']);
        expect(SqlTool.extractEnumValues("ENUM(11, 12, 13)")).to.deep.equal(['11', '12', '13']);
        expect(SqlTool.extractEnumValues("ENUM()")).to.be.undefined;
        expect(SqlTool.extractEnumValues("")).to.be.undefined;
        expect(SqlTool.extractEnumValues("CREATE TYPE colors AS ENUM ()")).to.be.undefined;
    });
    
    test('extractOtherTableNameSingularize test', () => {
        // Case where table names are extracted correctly
        expect(SqlTool.extractOtherTableName("user_has_role", "user")).to.equal("roles");
        expect(SqlTool.extractOtherTableName("user_has_role", "role")).to.equal("users");
        expect(SqlTool.extractOtherTableName("user_has_role", "users")).to.equal("roles");
        expect(SqlTool.extractOtherTableName("user_has_roles", "roles")).to.equal("users");

        // Case where a custom separator is used
        expect(SqlTool.extractOtherTableName("user_roles", "user", "_")).to.equal("roles");
        expect(SqlTool.extractOtherTableName("user_roles", "role", "_")).to.equal("users");

        // Case where the known table name is not found
        expect(SqlTool.extractOtherTableName("user_has_role", "group")).to.equal("user_has_role");

        // Case where the string does not contain the expected separator
        expect(SqlTool.extractOtherTableName("userrole", "user")).to.equal("userrole");
    });

    test('hasTableLeftOrRight test', () => {
        // Case where table name is on the right part
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "user_has_role", knownTableName: "role" })).to.be.true;
        // Case where table name is on the left part
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "user_has_role", knownTableName: "user", part: 'left' })).to.be.true;
        // Case where table name is on the right part with pluralized form
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "users_has_roles", knownTableName: "roles" })).to.be.true;
        // Case where table name is on the left part with pluralized form
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "users_has_roles", knownTableName: "users", part: 'left' })).to.be.true;
        // Case where table name is not found
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "user_has_role", knownTableName: "group" })).to.be.false;
        // Case where a custom separator is used
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "user_roles", knownTableName: "roles", separator: "_" })).to.be.true;
        // Case where custom separator is used and table name is on the left part
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "user_roles", knownTableName: "user", separator: "_", part: 'left' })).to.be.true;
        // Case where string does not contain the expected separator
        expect(SqlTool.hasTableLeftOrRight({ fullTableName: "userrole", knownTableName: "user" })).to.be.false;
    });    
});
