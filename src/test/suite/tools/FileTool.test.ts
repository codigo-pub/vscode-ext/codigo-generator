import { expect } from 'chai';
import { FileTool } from '../../../tools/FileTool';
import path from 'path';
import sinon from 'sinon';

// import * as myExtension from '../../extension';

suite('FileTool Test Suite', () => {
    console.log('Start FileTool tests.');

    setup(() => {});

    test('addFolderSeparator should add path separator when needed', () => {
        expect(FileTool.addFolderSeparator("/path/to/the/stars")).to.eq("/path/to/the/stars"+path.sep);
        expect(FileTool.addFolderSeparator("/path/to/the/stars"+path.sep)).to.eq("/path/to/the/stars"+path.sep);
	});

    test('constructBasePath should construct base path correctly', () => {
        const sep = path.sep;

        const basePath1 = FileTool.constructBasePath("template", `/base/folder`);
        expect(basePath1).to.eq(`${sep}base${sep}folder${sep}`);

        const basePath2 = FileTool.constructBasePath("template", "./relative/folder");
        expect(basePath2).to.eq(`relative${sep}folder${sep}template${sep}`);

        const basePath3 = FileTool.constructBasePath("template", undefined);
        expect(basePath3).to.eq(`template${sep}`);
	});

    test('constructBasePath should construct base path correctly from vscode workspacefolder', () => {
        const sep = path.sep;

        // Mocking vscode.workspace.workspaceFolders
        const workspaceFolders = [{ uri: { fsPath: "/path/to/workspace" } }];
        const vscodeMock = { workspace: { workspaceFolders } };
        sinon.replace(require('vscode'), 'workspace', vscodeMock);

        // Test with workspaceFolders
        const basePath1 = FileTool.constructBasePath("template", "/base/folder");
        expect(basePath1).to.eq(`${sep}base${sep}folder${sep}`);

        // Restore the original method
        sinon.restore();
	});    
});
