import { expect } from 'chai';
import * as vscode from 'vscode';
import { DocTool } from '../../../tools/DocTool';

// import * as myExtension from '../../extension';

suite('DocTool Test Suite', () => {
    console.log('Start DocTool tests.');

    setup(() => {});

    test('checkDoc sould return false if no parameter', () => {
        expect(DocTool.checkDoc(undefined)).to.be.false;
	});

    test('checkDoc should return true if document is opened', () => {
        const mockTextEditor: Partial<vscode.TextEditor> = {
            document: {} as vscode.TextDocument,
            viewColumn: 1,
            options: {} as vscode.TextEditorOptions,
            edit: () => Promise.resolve(true),
            insertSnippet: () => Promise.resolve(true),
            revealRange: () => {},
            show: () => {}
        };

        expect(DocTool.checkDoc(mockTextEditor as vscode.TextEditor)).to.be.true;
    });

    test('checkDoc should return true if document languageId matches expected languageIds', () => {
        const mockTextEditor: Partial<vscode.TextEditor> = {
            document: {
                languageId: 'json'
            } as vscode.TextDocument,
            viewColumn: 1,
            options: {} as vscode.TextEditorOptions,
            edit: () => Promise.resolve(true),
            insertSnippet: () => Promise.resolve(true),
            revealRange: () => {},
            show: () => {}
        };

        expect(DocTool.checkDoc(mockTextEditor as vscode.TextEditor, ['json'])).to.be.true;
    });

    test('checkDoc should return false if document languageId does not match expected languageIds', () => {
        const mockTextEditor: Partial<vscode.TextEditor> = {
            document: {
                languageId: 'javascript'
            } as vscode.TextDocument,
            viewColumn: 1,
            options: {} as vscode.TextEditorOptions,
            edit: () => Promise.resolve(true),
            insertSnippet: () => Promise.resolve(true),
            revealRange: () => {},
            show: () => {}
        };

        expect(DocTool.checkDoc(mockTextEditor as vscode.TextEditor, ['json'])).to.be.false;
    });
});
