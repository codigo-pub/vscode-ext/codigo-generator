/* eslint-disable @typescript-eslint/naming-convention */
import { expect } from "chai";
import { PostgresqlDatabase } from "../../../../db/sql/PostgresqlDatabase";
import { ConfigGenerator } from "../../../../model/ConfigGenerator";
import { TestingTools } from "../../../testing_tools/TestingTools";
import { suite, test, setup, afterEach } from "mocha";
import * as sinon from "sinon";
import { GenericDbTable } from "../../../../db/model/GenericDbTable";
import { GlobalConfig } from "../../../../model/GlobalConfig";
import { GenericDbFKey } from "../../../../db/model/GenericDbFKey";
import { GenericDbField } from "../../../../db/model/GenericDbField";
import { AbstractDatabase } from "../../../../db/AbstractDatabase";

suite("PostgresqlDatabase Test Suite", () => {
  let postgresqlDatabase: PostgresqlDatabase;
  let sandbox: sinon.SinonSandbox;
  let mockedCon: { query: Function };

  setup(() => {
    const configGenerator: ConfigGenerator = TestingTools.getConfig();
    if (configGenerator?.global_config && configGenerator.processors && configGenerator.processors.length > 0) {
      postgresqlDatabase = new PostgresqlDatabase(configGenerator.global_config);
    }
    // Create mocked _con property
    mockedCon = {
      query: () => {},
    };
    // Create sandbox for test
    sandbox = sinon.createSandbox();

    // Mocking the query method
    sandbox.stub(mockedCon, "query").callsFake(async (query: string): Promise<{ rows: any[] }> => {
      let results: any = {};
      // Depends on the actual query
      if (query.indexOf("information_schema.tables") >= 0) {
        //if showing all tables
        results = getFakePostgresqlShowTables();
      } else if (query.indexOf("table_constraints") >= 0) {
        // table description query is the only one that contains 'table_constraints' string
        results = getFakePostgresqlTableDescription();
      } else if (query.trim().indexOf("WHERE ccu.") >= 0) {
        //if there is 'WHERE ccu.' this is the query that search for referenced fkeys
        results = getFakePostgresqlReferencedForeignKeys();
      } else {
        results = getFakePostgresqlForeignKeys();
      }

      return { rows: results };
    });

    // Set mocked _con property using _con getter name : 'con'
    sandbox.stub(postgresqlDatabase, "con").get(() => mockedCon);
  });

  afterEach(() => {
    sandbox.restore();
  });

  /**
   * Testing getAllTablesDescription (note that we need to set async)
   */
  test("getAllTablesDescription returns correct data", async () => {
    const result = await postgresqlDatabase.getAllTablesDescription(AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultAllTablesDesc());
  });

  /**
   * Testing getTableDescription (note that we need to set async)
   */
  test("getTableDescription returns correct data", async () => {
    const result = await postgresqlDatabase.getTableDescription("commandes", AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultTableDesc());
  });

  /**
   * Testing getTableForeignKeys (note that we need to set async)
   */
  test("getTableForeignKeys returns correct data", async () => {
    const result = await postgresqlDatabase.getTableForeignKeys("commandes", AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultFKeys());
  });

  /**
   * Testing getTableReferencedForeignKeys (note that we need to set async)
   */
  test("getTableReferencedForeignKeys returns correct data", async () => {
    const result = await postgresqlDatabase.getTableReferencedForeignKeys("commandes", AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultReferencedFKeys());
  });

  /**
   * Testing getFieldDescription
   */
  test("getFieldDescription returns correct data", () => {
    const result = postgresqlDatabase.getFieldDescription(getFakePostgresqlTableDescription()[0]);
    expect(result).to.deep.equal(getExpectedFieldDescription());
  });

  /**
   * Testing getSchema
   */
  test("getTableForeignKeys returns correct data", () => {
    // Set config  property using _con getter name : 'con'
    sandbox.stub(postgresqlDatabase, "config").get(() => {
      return {
        database: {
          host: "localhost",
          user: "postgres",
          password: "${POSTGRESQK_DB_ROOT_PASS}",
          database: "postgres",
          port: 5432,
        },
      } as GlobalConfig;
    });
    // Call with no schema
    expect(postgresqlDatabase.getSchema()).to.deep.equal("postgres");
    sandbox.stub(postgresqlDatabase, "config").get(() => {
      return {
        database: {
          host: "localhost",
          user: "postgres",
          password: "${POSTGRESQK_DB_ROOT_PASS}",
          database: "postgres",
          port: 5432,
          schema: "mySchema",
        },
      } as GlobalConfig;
    });
    // Call with schema
    expect(postgresqlDatabase.getSchema()).to.deep.equal("mySchema");
  });

  /**
   * get fake query data from database when asking for tables list
   * @returns fake query data from database
   */
  function getFakePostgresqlShowTables(): {}[] {
    return [
      {
        table_catalog: "postgres",
        table_schema: "clients",
        table_name: "commandes",
        table_type: "BASE TABLE",
        self_referencing_column_name: null,
        reference_generation: null,
        user_defined_type_catalog: null,
        user_defined_type_schema: null,
        user_defined_type_name: null,
        is_insertable_into: "YES",
        is_typed: "NO",
        commit_action: null,
      },
    ];
  }

  /**
   * get fake query data from database when asking for table description
   * @returns fake query data from database
   */
  function getFakePostgresqlTableDescription(): {}[] {
    return [
      {
        column_name: "id",
        data_type: "integer",
        character_maximum_length: null,
        constraint_name: "commandes_pkey",
        ordinal_position: 1,
        position_in_unique_constraint: null,
        referenced_table_name: "commandes",
        referenced_column_name: "id",
        ispkey: "commandes_pkey",
        isautogenerated: "YES",
        update_rule: null,
        delete_rule: null,
        iskey: null,
      },
      {
        column_name: "client_id",
        data_type: "integer",
        character_maximum_length: null,
        constraint_name: "commandes_client_id_fkey",
        ordinal_position: 1,
        position_in_unique_constraint: 1,
        referenced_table_name: "clients",
        referenced_column_name: "id",
        ispkey: null,
        isautogenerated: "NO",
        update_rule: "NO ACTION",
        delete_rule: "NO ACTION",
        iskey: "commandes_client_id_fkey",
      },
      {
        column_name: "date_commande",
        data_type: "date",
        character_maximum_length: null,
        constraint_name: null,
        ordinal_position: null,
        position_in_unique_constraint: null,
        referenced_table_name: null,
        referenced_column_name: null,
        ispkey: null,
        isautogenerated: "NO",
        update_rule: null,
        delete_rule: null,
        iskey: null,
      },
    ];
  }

  /**
   * get fake query data from database when asking for table foreign keys
   * @returns fake query data from database
   */
  function getFakePostgresqlForeignKeys(): {}[] {
    return [
      {
        table_name: "commandes",
        column_name: "client_id",
        constraint_name: "commandes_client_id_fkey",
        ordinal_position: 1,
        position_in_unique_constraint: 1,
        update_rule: "NO ACTION",
        delete_rule: "NO ACTION",
        referenced_table_name: "clients",
        referenced_column_name: "id",
      },
    ];
  }

  /**
   * get fake query data from database when asking for table foreign keys
   * @returns fake query data from database
   */
  function getFakePostgresqlReferencedForeignKeys(): {}[] {
    return [
      {
        table_name: "details_commande",
        column_name: "commande_id",
        constraint_name: "details_commande_commande_id_fkey",
        ordinal_position: 1,
        position_in_unique_constraint: 1,
        update_rule: "NO ACTION",
        delete_rule: "NO ACTION",
        referenced_table_name: "commandes",
        referenced_column_name: "id",
      },
    ];
  }

  /**
   * get expected result when calling getAllTablesDescription
   * @returns expected result
   */
  function getExpectedResultAllTablesDesc(): GenericDbTable[] {
    return [
      {
        tableName: "commandes",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            isAutogenerated: true,
            type: {
              type: "int",
              length: undefined,
            },
          },
          {
            fieldName: "client_id",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            isAutogenerated: false,
            type: {
              type: "int",
              length: undefined,
            },
          },
          {
            fieldName: "date_commande",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            isAutogenerated: false,
            type: {
              type: "date",
              length: undefined,
            },
          },
        ],
        fKeys: [
          {
            tableName: "commandes",
            columnName: "client_id",
            constraintName: "commandes_client_id_fkey",
            referencedTable: "clients",
            referencedColumn: "id",
            constraintPosition: 1,
            updateRule: "no_action",
            deleteRule: "no_action",
          },
        ],
        referencedFKeys: [
          {
            tableName: "details_commande",
            columnName: "commande_id",
            constraintName: "details_commande_commande_id_fkey",
            referencedTable: "commandes",
            referencedColumn: "id",
            constraintPosition: 1,
            updateRule: "no_action",
            deleteRule: "no_action",
          },
        ],
      },
    ] as GenericDbTable[];
  }

  /**
   * get expected result when calling getTableDescription
   * @returns expected result
   */
  function getExpectedResultTableDesc(): GenericDbTable {
    let tableDescription = getExpectedResultAllTablesDesc()[0];
    return tableDescription;
  }

  /**
   * get expected result when calling getTableForeignKeys
   * @returns expected result
   */
  function getExpectedResultFKeys(): GenericDbFKey[] {
    return getExpectedResultAllTablesDesc()[0].fKeys as GenericDbFKey[];
  }

  /**
   * get expected result when calling getTableReferencedForeignKeys
   * @returns expected result
   */
  function getExpectedResultReferencedFKeys(): GenericDbFKey[] {
    return getExpectedResultAllTablesDesc()[0].referencedFKeys as GenericDbFKey[];
  }

  /**
   * get expected result when calling getFieldDescription
   * @returns expected result
   */
  function getExpectedFieldDescription(): GenericDbField {
    return {
      fieldName: "id",
      isNullable: false,
      isPKey: true,
      isFKey: false,
      isAutogenerated: true,
      type: {
        type: "int",
        length: undefined,
      },
    } as GenericDbField;
  }
});
