/* eslint-disable @typescript-eslint/naming-convention */
import { expect } from "chai";
import { MysqlDatabase } from "../../../../db/sql/MysqlDatabase";
import { ConfigGenerator } from "../../../../model/ConfigGenerator";
import { TestingTools } from "../../../testing_tools/TestingTools";
import { suite, test, setup, afterEach } from "mocha";
import * as sinon from "sinon";
import { GenericDbTable } from "../../../../db/model/GenericDbTable";
import mysql from "mysql2/promise";
import { GlobalConfig } from "../../../../model/GlobalConfig";
import { GenericDbFKey } from "../../../../db/model/GenericDbFKey";
import { GenericDbField } from "../../../../db/model/GenericDbField";
import { AbstractDatabase } from "../../../../db/AbstractDatabase";
import { EnumDbRule } from "../../../../db/model/EnumDbRule";

suite("MysqlDatabase Test Suite", () => {
  let mysqlDatabase: MysqlDatabase;
  let sandbox: sinon.SinonSandbox;
  let mockedCon: { query: Function };

  setup(() => {
    const configGenerator: ConfigGenerator = TestingTools.getConfig();
    if (configGenerator?.global_config && configGenerator.processors && configGenerator.processors.length > 0) {
      mysqlDatabase = new MysqlDatabase(configGenerator.global_config);
    }
    // Create mocked _con property
    mockedCon = {
      query: () => {},
    };
    // Create sandbox for test
    sandbox = sinon.createSandbox();

    // Mocking the query method
    sandbox.stub(mockedCon, "query").callsFake(async (query: string): Promise<[mysql.QueryResult, mysql.FieldPacket[]]> => {
      let results: any = {};
      // Depends on the actual query
      if (query === "show tables") {
        results = getFakeMysqlShowTables();
      } else if (query.startsWith("desc")) {
        results = getFakeMysqlTableDescription();
      } else if (query.trim().indexOf("AND kcu.TABLE_NAME") >= 0) {
        //if there is AND TABLE_NAME this is the query that search for referenced fkeys
        results = getFakeMysqlReferencedForeignKeys();
      } else {
        results = getFakeMysqlForeignKeys();
      }
      const fields: mysql.FieldPacket[] = [];
      const resultReturn: [mysql.QueryResult, mysql.FieldPacket[]] = [results, fields];
      return resultReturn;
    });

    // Set mocked _con property using _con getter name : 'con'
    sandbox.stub(mysqlDatabase, "con").get(() => mockedCon);
  });

  afterEach(() => {
    sandbox.restore();
  });

  /**
   * Testing getAllTablesDescription (note that we need to set async)
   */
  test("getAllTablesDescription returns correct data", async () => {
    const result = await mysqlDatabase.getAllTablesDescription(AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultAllTablesDesc());
  });

  /**
   * Testing getTableDescription (note that we need to set async)
   */
  test("getTableDescription returns correct data", async () => {
    const result = await mysqlDatabase.getTableDescription("tests", AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultTableDesc());
  });

  /**
   * Testing getTableForeignKeys (note that we need to set async)
   */
  test("getTableForeignKeys returns correct data", async () => {
    const result = await mysqlDatabase.getTableForeignKeys("tests", AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultFKeys());
  });
  
  /**
   * Testing getTableReferencedForeignKeys (note that we need to set async)
   */
  test("getTableReferencedForeignKeys returns correct data", async () => {
    const result = await mysqlDatabase.getTableReferencedForeignKeys("tests", AbstractDatabase.PUBLIC_SCHEMA);
    expect(result).to.deep.equal(getExpectedResultReferencedFKeys());
  });

  /**
   * Testing getFieldDescription
   */
  test("getFieldDescription returns correct data", () => {
    const result = mysqlDatabase.getFieldDescription(getFakeMysqlTableDescription()[0]);
    expect(result).to.deep.equal(getExpectedFieldDescription());
  });

  /**
   * Testing getSchema
   */
  test("getTableForeignKeys returns correct data", () => {
    // Set config  property using _con getter name : 'con'
    sandbox.stub(mysqlDatabase, "config").get(() => {
      return {
        database: {
          host: "localhost",
          user: "quizz_user",
          password: "${QUIZ_DB_PASS}",
          database: "quizz",
          port: 3306,
        },
      } as GlobalConfig;
    });
    // Call with no schema (with mysql, schema becomes database if no schema specified)
    expect(mysqlDatabase.getSchema()).to.deep.equal("quizz");
    sandbox.stub(mysqlDatabase, "config").get(() => {
      return {
        database: {
          host: "localhost",
          user: "quizz_user",
          password: "${QUIZ_DB_PASS}",
          database: "quizz",
          port: 3306,
          schema: "mySchema",
        },
      } as GlobalConfig;
    });
    // Call with schema
    expect(mysqlDatabase.getSchema()).to.deep.equal("mySchema");
  });

  /**
   * get fake query data from database when asking for tables list
   * @returns fake query data from database
   */
  function getFakeMysqlShowTables(): {}[] {
    return [
      {
        Tables_in_quizz: "tests",
      },
    ];
  }

  /**
   * get fake query data from database when asking for table description
   * @returns fake query data from database
   */
  function getFakeMysqlTableDescription(): {}[] {
    return [
      {
        Field: "id",
        Type: "int(11)",
        Null: "NO",
        Key: "PRI",
        Default: null,
        Extra: "auto_increment",
      },
      {
        Field: "teacher_id",
        Type: "int(11)",
        Null: "NO",
        Key: "MUL",
        Default: null,
        Extra: "",
      },
      {
        Field: "name",
        Type: "varchar(255)",
        Null: "NO",
        Key: "",
        Default: null,
        Extra: "",
      },
      {
        Field: "date",
        Type: "date",
        Null: "NO",
        Key: "",
        Default: null,
        Extra: "",
      },
      {
        Field: "status_id",
        Type: "int(11)",
        Null: "NO",
        Key: "MUL",
        Default: null,
        Extra: "",
      },
      {
        Field: "subject",
        Type: "varchar(255)",
        Null: "NO",
        Key: "",
        Default: null,
        Extra: "",
      },
      {
        Field: "bignumber",
        Type: "bigint(20)",
        Null: "YES",
        Key: "",
        Default: null,
        Extra: "",
      },
    ];
  }


  /**
   * get fake query data from database when asking for table foreign keys
   * @returns fake query data from database
   */
  function getFakeMysqlForeignKeys(): {}[] {
    return [
      {
        TABLE_NAME: "tests",
        COLUMN_NAME: "teacher_id",
        CONSTRAINT_NAME: "tests_fk0",
        REFERENCED_TABLE_NAME: "teachers",
        REFERENCED_COLUMN_NAME: "id",
        UPDATE_RULE: "SET NULL",
        DELETE_RULE: "CASCADE",
      },
      {
        TABLE_NAME: "tests",
        COLUMN_NAME: "status_id",
        CONSTRAINT_NAME: "tests_fk1",
        REFERENCED_TABLE_NAME: "status",
        REFERENCED_COLUMN_NAME: "id",
        UPDATE_RULE: "SET DEFAULT",
        DELETE_RULE: "CASCADE",
      },
    ];
  }

  function getFakeMysqlReferencedForeignKeys(): {}[] {
    return [
      {
        TABLE_NAME: "tests_has_questions",
        COLUMN_NAME: "tests_id",
        CONSTRAINT_NAME: "fk_tests_has_questions_tests1",
        REFERENCED_TABLE_NAME: "tests",
        REFERENCED_COLUMN_NAME: "id",
        UPDATE_RULE: "CASCADE",
        DELETE_RULE: "CASCADE",
      },
      {
        TABLE_NAME: "classrooms_has_tests",
        COLUMN_NAME: "tests_id",
        CONSTRAINT_NAME: "fk_classrooms_has_tests_tests1",
        REFERENCED_TABLE_NAME: "tests",
        REFERENCED_COLUMN_NAME: "id",
        UPDATE_RULE: "CASCADE",
        DELETE_RULE: "CASCADE",
      },
      {
        TABLE_NAME: "students_has_tests",
        COLUMN_NAME: "tests_id",
        CONSTRAINT_NAME: "fk_students_has_tests_tests1",
        REFERENCED_TABLE_NAME: "tests",
        REFERENCED_COLUMN_NAME: "id",
        UPDATE_RULE: "CASCADE",
        DELETE_RULE: "CASCADE",
      },
    ];
  }

  /**
   * get expected result when calling getAllTablesDescription
   * @returns expected result
   */
  function getExpectedResultAllTablesDesc(): GenericDbTable[] {
    return [
      {
        tableName: "tests",
        fields: [
          {
            fieldName: "id",
            isNullable: false,
            isPKey: true,
            isFKey: false,
            isAutogenerated: true,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "teacher_id",
            isNullable: false,
            isPKey: false,
            isFKey: true,
            isAutogenerated: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "name",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            isAutogenerated: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "date",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            isAutogenerated: false,
            type: {
              type: "date",
              length: undefined,
            },
          },
          {
            fieldName: "status_id",
            isNullable: false,
            isPKey: false,
            isFKey: true,
            isAutogenerated: false,
            type: {
              type: "int",
              length: 11,
            },
          },
          {
            fieldName: "subject",
            isNullable: false,
            isPKey: false,
            isFKey: false,
            isAutogenerated: false,
            type: {
              type: "char",
              length: 255,
            },
          },
          {
            fieldName: "bignumber",
            isNullable: true,
            isPKey: false,
            isFKey: false,
            isAutogenerated: false,
            type: {
              type: "long",
              length: 20,
            },
          },
        ],
        fKeys: [
          {
            tableName: "tests",
            columnName: "teacher_id",
            constraintName: "tests_fk0",
            referencedTable: "teachers",
            referencedColumn: "id",
            updateRule: EnumDbRule.SET_NULL,
            deleteRule: EnumDbRule.CASCADE,
          },
          {
            tableName: "tests",
            columnName: "status_id",
            constraintName: "tests_fk1",
            referencedTable: "status",
            referencedColumn: "id",
            updateRule: EnumDbRule.SET_DEFAULT,
            deleteRule: EnumDbRule.CASCADE,
          },
        ],
        referencedFKeys: [
          {
            tableName: "tests_has_questions",
            columnName: "tests_id",
            constraintName: "fk_tests_has_questions_tests1",
            referencedTable: "tests",
            referencedColumn: "id",
            updateRule: EnumDbRule.CASCADE,
            deleteRule: EnumDbRule.CASCADE,
          },
          {
            tableName: "classrooms_has_tests",
            columnName: "tests_id",
            constraintName: "fk_classrooms_has_tests_tests1",
            referencedTable: "tests",
            referencedColumn: "id",
            updateRule: EnumDbRule.CASCADE,
            deleteRule: EnumDbRule.CASCADE,
          },
          {
            tableName: "students_has_tests",
            columnName: "tests_id",
            constraintName: "fk_students_has_tests_tests1",
            referencedTable: "tests",
            referencedColumn: "id",
            updateRule: EnumDbRule.CASCADE,
            deleteRule: EnumDbRule.CASCADE,
          },
        ],
      }
    ] as GenericDbTable[];
  }

  /**
   * get expected result when calling getTableDescription
   * @returns expected result
   */
  function getExpectedResultTableDesc(): GenericDbTable {
    let tableDescription = getExpectedResultAllTablesDesc()[0];
    return tableDescription;
  }

  /**
   * get expected result when calling getTableForeignKeys
   * @returns expected result
   */
  function getExpectedResultFKeys(): GenericDbFKey[] {
    return getExpectedResultAllTablesDesc()[0].fKeys as GenericDbFKey[];
  }

  /**
   * get expected result when calling getTableReferencedForeignKeys
   * @returns expected result
   */
  function getExpectedResultReferencedFKeys(): GenericDbFKey[] {
    return getExpectedResultAllTablesDesc()[0].referencedFKeys as GenericDbFKey[];
  }

  /**
   * get expected result when calling getFieldDescription
   * @returns expected result
   */
  function getExpectedFieldDescription(): GenericDbField | undefined {
    const field = getExpectedResultAllTablesDesc()[0].fields;
    return (field !== undefined) ? field[0] : undefined;
  }
});
