/* eslint-disable @typescript-eslint/naming-convention */
import { expect } from "chai";
import { AbstractProcessor } from "../../../processors/AbstractProcessor";
import { GenericDbTable } from "../../../db/model/GenericDbTable";
import { IConverter } from "../../../processors/IConverter";
import { ConfigGenerator } from "../../../model/ConfigGenerator";
import { GenericDbField } from "../../../db/model/GenericDbField";
import { GenericDbFKey } from "../../../db/model/GenericDbFKey";
import { TestingTools } from "../../testing_tools/TestingTools";
import { PhpConverter } from "../../../processors/php/PhpConverter";
import path from "path";
import { GeneratorTool } from "../../../tools/GeneratorTool";


/**
 * Class used to access protected members...
 */
class TestProcessor extends AbstractProcessor {
  // This processor can handle AI
  protected canUseAI?: boolean | undefined = false;

  getProcessorFolder(): string {
    return "MyName";
  }
  getProcessorName(): string {
    return "MyName";
  }
  process(): Promise<void> {
    throw new Error("Method not implemented.");
  }
  protected processTable(rTable: GenericDbTable): Promise<void> {
    throw new Error("Method not implemented.");
  }
  protected getImportFrom(rImportName: string): string {
    throw new Error("Method not implemented.");
  }
  protected getConverter(): IConverter {
    return new PhpConverter();
  }
  // Méthodes utilisées uniquement dans les tests pour exposer les méthodes protégées
  public exposeGetTemplateBasePath(): string {
    return this.getTemplateBasePath();
  }

  public exposeGetOutBasePath(): string {
    return this.getOutBasePath();
  }

  public exposeGetOutProcessorPath(): string {
    return this.getOutProcessorPath();
  }

  public exposeProcessClassNameFromTable(rTable: string): string {
    return this.processClassNameFromTable(rTable);
  }

  public exposeTxtToSingularize(rTxt?: string): string | undefined {
    return GeneratorTool.txtToSingularize(rTxt);
  }

  public exposeTxtToPluralize(rTxt?: string): string | undefined {
    return GeneratorTool.txtToPluralize(rTxt);
  }

  public exposeGetFKeyFromField(rField: any, rFKeys?: any[]): any {
    return GeneratorTool.getFKeyFromField(rField, rFKeys);
  }
}

suite("AbstractProcessor Test Suite", () => {
  let processor: TestProcessor;

  setup(() => {

    const configGenerator: ConfigGenerator = TestingTools.getConfig();
    if (configGenerator?.global_config && configGenerator.processors && configGenerator.processors.length > 0) {
      const db = TestingTools.getDbMock();
      // vscode fake context
      configGenerator.context = TestingTools.getFakeVsCodeExtensionContext();
      // Initialize TestProcessor
      processor = new TestProcessor(configGenerator.global_config, configGenerator.processors[0], db);
    }
  });

  test("should return correct template base path", () => {
    expect(processor.exposeGetTemplateBasePath()).to.equal(`php${path.sep}`);
  });

  test("should return correct out base path", () => {
    expect(processor.exposeGetOutBasePath()).to.equal(`${path.sep}tmp${path.sep}out${path.sep}`);
  });

  test("should return correct out processor path", () => {
    expect(processor.exposeGetOutProcessorPath()).to.equal(`${path.sep}tmp${path.sep}out${path.sep}MyName${path.sep}`);
  });

  test("should process class name from table correctly", () => {
    expect(processor.exposeProcessClassNameFromTable("classrooms")).to.equal("Classroom");
  });

  test("should singularize text correctly", () => {
    expect(processor.exposeTxtToSingularize("classrooms")).to.equal("classroom");
  });

  test("should pluralize text correctly", () => {
    expect(processor.exposeTxtToPluralize("classroom")).to.equal("classrooms");
  });

  test("should get foreign key from field correctly", () => {
    const myField = new GenericDbField();
    myField.fieldName = "classroom_id";
    const fKeyFields: GenericDbFKey[] = [];
    fKeyFields.push({ columnName: "xxxxx_id" });
    fKeyFields.push({ columnName: "xxxxx_id" });
    fKeyFields.push({ columnName: "classroom_id" });
    fKeyFields.push({ columnName: "xxxxx_id" });
    // Mock rField and rFKeys if needed
    expect(processor.exposeGetFKeyFromField(myField, fKeyFields)).to.deep.equal({ columnName: "classroom_id" } as GenericDbFKey);
  });
});
