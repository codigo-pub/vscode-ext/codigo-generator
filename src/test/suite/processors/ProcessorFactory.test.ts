/* eslint-disable @typescript-eslint/naming-convention */
import { IDatabase } from "../../../db/IDatabase";
import { ConfigGenerator } from "../../../model/ConfigGenerator";
import { ConfigProcessor } from "../../../model/ConfigProcessor";
import { GlobalConfig } from "../../../model/GlobalConfig";
import { ProcessorFactory } from "../../../processors/ProcessorFactory";
import { JavaModelProcessor } from "../../../processors/java/JavaModelProcessor";
import { PhpModelProcessor } from "../../../processors/php/PhpModelProcessor";
import { PythonDjangoModelProcessor } from "../../../processors/python/django/PythonDjangoModelProcessor";
import { TypescriptModelProcessor } from "../../../processors/typescript/TypescriptModelProcessor";
import { TestingTools } from "../../testing_tools/TestingTools";
import { assert } from "chai";

suite("ProcessorFactory Test Suite", () => {
  let mockConfig: GlobalConfig;
  let mockConfigProcessor: ConfigProcessor;
  let mockDb: IDatabase;

  setup(() => {
    // Initialize mock objects
    mockConfig = {} as GlobalConfig;
    mockConfigProcessor = {} as ConfigProcessor;
    mockDb = {} as IDatabase;
  });

  test("should return TypescriptModelProcessor for Typescript model", () => {
    mockConfig.template = "typescript";
    mockConfigProcessor.name = "model";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.instanceOf(processor, TypescriptModelProcessor);
  });

  test("should return PhpModelProcessor for PHP model", () => {
    mockConfig.template = "php";
    mockConfigProcessor.name = "model";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.instanceOf(processor, PhpModelProcessor);
  });

  test("should return JavaModelProcessor for Java model", () => {
    mockConfig.template = "java";
    mockConfigProcessor.name = "model";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.instanceOf(processor, JavaModelProcessor);
  });

  test("should return PythonDjangoModelProcessor for Django model", () => {
    mockConfig.template = "python";
    mockConfigProcessor.name = "django_model";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.instanceOf(processor, PythonDjangoModelProcessor);
  });

  // ==============================================================================================
  // pessimistic tests
  
  test("should return undefined if template is not provided", () => {
    mockConfigProcessor.name = "model";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.isUndefined(processor);
  });

  test("should return undefined if processor name is not provided", () => {
    mockConfig.template = "typescript";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.isUndefined(processor);
  });

  test("should return undefined for unknown template", () => {
    mockConfig.template = "unknown_template";
    mockConfigProcessor.name = "model";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.isUndefined(processor);
  });

  test("should return undefined for unknown processor name", () => {
    mockConfig.template = "typescript";
    mockConfigProcessor.name = "unknown_processor";
    const processor = ProcessorFactory.getProcessor(mockConfig, mockConfigProcessor, mockDb);
    assert.isUndefined(processor);
  });

});