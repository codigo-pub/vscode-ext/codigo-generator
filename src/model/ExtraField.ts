export class ExtraField {
  importTxt?: string[]; //as array to avoid duplicate entries
  fieldsTxt?: string;
  gettersSettersTxt?: string;
  extra?: any; // Extra data to use as your need


  /**
   * Add all imports
   * @param rImports : imports to add
   */
  addImports(rImports?: string[]): void {
    if (rImports && rImports.length > 0) {
      rImports.forEach(f => { this.addImport(f); });
    }
  }

  /**
   * Add an import
   * @param rImport : import to add
   */
  addImport(rImport?: string): void {
    if (rImport) {
      // Initialize if needed
      if (!this.importTxt) {
        this.importTxt = [];
      }
      // Add import if not already in there
      if (this.importTxt.indexOf(rImport) === -1) {
        this.importTxt.push(rImport);
      }  
    }
  }

  /**
   * Default method to get all imports as text
   * Every import will be put in a new line
   * @returns imports text
   */
  getImportsTxt(): string {
    return this.importTxt ? this.importTxt.join("\n") : "";
  }
}
