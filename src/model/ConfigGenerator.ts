/* eslint-disable @typescript-eslint/naming-convention */
import { ConfigProcessor } from "./ConfigProcessor";
import { GlobalConfig } from "./GlobalConfig";
import * as vscode from "vscode";

export class ConfigGenerator {
    global_config?: GlobalConfig;
    processors?: ConfigProcessor[];
    context?: vscode.ExtensionContext; //<= to store extension context
}
