/* eslint-disable @typescript-eslint/naming-convention */
export enum AIEnum {
    OPENAI = "OpenAI", 
    DEEPSEEK = "DeepSeek",
    MISTRAL = "Mistral"
}