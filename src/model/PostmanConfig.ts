/* eslint-disable @typescript-eslint/naming-convention */
export class PostmanConfig {

  // true to add {{base_api}} variable after port (e.g. : '{{base_host}}:{{base_port}}/{{base_api}}/classroom')
  // default to true. If false => '{{base_host}}:{{base_port}}/classroom'
  with_base_api: boolean = true;

  /**
   * paths strings that will be added to base path for crud requests
   * e.g. if you add this value to create: ["create"]
   * request path for model Student, for example, will loke like : 
   *     "path": [
   *         "student",
   *         "create"
   *      ],
   * So, in postmal url something like : localhost:8080/quizz4/student/create
   */
  path_adds?: {
    get_all?: string[];
    create?: string[];
    get_by_id?: string[];
    update_by_id?: string[];
    delete_by_id?: string[];
  };
  
  many_to_many_sep?: string;

  // Set this property to left or right to only keep pivot tables with part related (@see SqlTool.hasTableLeftOrRight method)
  part?: 'left' | 'right';
}
