import { GenericDbField } from "../db/model/GenericDbField";
import { GenericDbTable } from "../db/model/GenericDbTable";

/**
 * Expose convenient data on a ManyToMany relation
 */
export class ManyToManyRelation {
  // local table
  tableName?: string;
  tableNameUp?: string; // table name with first letter upper case
  className?: string;
  // pivot table
  tablePivotName?: string;
  classNamePivot?: string;
  pivotDistantPkeyName?: string; // Store pivot single pkey field name related to distant table (sql type is naturally the same as distantPkeyField below)
  // distant table
  tableDistant?: GenericDbTable;
  classNameDistant?: string;
  distantPkeyField?: GenericDbField; // Store single pkey field here of distant table
}
