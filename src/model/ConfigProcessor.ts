/* eslint-disable @typescript-eslint/naming-convention */
export class ConfigProcessor {
    name?: string;
    generate?: boolean;
    parameter_prefix?: string;
    custom_base_template?: string;
    extra_config?: any;
    version?: string;
    useCamelCaseForClass?: boolean;
    useCamelCaseForField?: boolean;
    useAI?: boolean;
}
