import { AIEnum } from "./AIEnum";

/* eslint-disable @typescript-eslint/naming-convention */
export class AIConfig {
    aiName?: AIEnum;
    enabled?: boolean;
    apiUrl?: string;
    apiKey?: string;
    model?: string; // e.g. "gpt-4"
    prompt?: string; // hidden property use to set main prompt for AI
    temperature?: number; // AI creativity
    max_completion_tokens?: number; // response token limit
    duration_warning?: number; // number of seconds to trigger a warning during every AI process
}
