import { EnumDbType } from "../db/model/EnumDbType";

export class SqlConvertEnumDbType {
  sqlType?: EnumDbType;
  targetType?: string;
  import?: string[]; // if import needed, must be defined here
}
