import { AIConfig } from "./AIConfig";

/* eslint-disable @typescript-eslint/naming-convention */
export class GlobalConfig {
  db_type?: string;
  database?: {
    host?: string;
    user?: string | null;
    password?: string | null;
    database?: string;
    port?: number;
    schema?: string;
    many_to_many_sep?: string;
  };

  useAI?: AIConfig;

  template?: string;
  packageBaseName?: string;
  out_folder?: string;
  skip_these_tables?: string[];
  only_these_tables?: string[];

  baseTemplatesFolder?: string; // Base folder for the extension templates. This property is set by the program
}
