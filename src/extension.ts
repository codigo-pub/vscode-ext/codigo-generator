// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { GeneratorCommand } from './commands/GeneratorCommand';
import { ICommand } from './commands/ICommand';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	// vscode.window.showInformationMessage('Congratulations, your extension "codigo-generator" is now active!');

    // codigo-generator.generate command
    let generatorCommand = vscode.commands.registerCommand('codigo-generator.generate', () => {
        let process: ICommand = new GeneratorCommand();
        process.run(context);
    });

    // Add commands
	context.subscriptions.push(
        generatorCommand
    );
}

// This method is called when your extension is deactivated
export function deactivate() {}
