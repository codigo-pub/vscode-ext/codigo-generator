/* eslint-disable @typescript-eslint/naming-convention */
import { GlobalConfig } from "../model/GlobalConfig";
import { IDatabase } from "./IDatabase";
import { IDbConverter } from "./IDbConverter";
import { GenericDbFKey } from "./model/GenericDbFKey";
import { GenericDbField } from "./model/GenericDbField";
import { GenericDbRow } from "./model/GenericDbRow";
import { GenericDbTable } from "./model/GenericDbTable";
import { GenericDbTypeField } from "./model/GenericDbTypeField";

export abstract class AbstractDatabase<T> implements IDatabase {
  _converter: IDbConverter | undefined;

  private _con?: T; // keep scope private as access must be handle by getter/setter

  protected _config: GlobalConfig;

  static readonly PUBLIC_SCHEMA = "public";

  abstract readonly FIELD_TABLE_NAME: string;
  abstract readonly FIELD_TYPE_NAME: string;

  constructor(rConfig: GlobalConfig) {
    this._config = rConfig;
    this._converter = this.getDbConverter();
  }

  /**
   * Retrieves the database converter.
   * @abstract
   * @returns {IDbConverter} The database converter.
   */
  abstract getDbConverter(): IDbConverter;

  /**
   * Fetches a row from the database.
   * @abstract
   * @param {any} rRow - The raw row data.
   * @returns {GenericDbRow} The fetched row.
   */
  abstract fetchRow(rRow: any): GenericDbRow;

  /**
   * Converts a generic field from the database.
   * @abstract
   * @param {any} field - The raw field data.
   * @returns {GenericDbField} The converted field.
   */
  abstract convertField(field: any): GenericDbField;

  /**
   * Executes a query against the database.
   * @abstract
   * @param {string} rQuery - The query string.
   * @returns {Promise<GenericDbRow[]>} A promise that resolves with an array of rows.
   */
  abstract query(rQuery: string): Promise<GenericDbRow[]>;

  /**
   * Closes the connection to the database.
   * @abstract
   * @returns {Promise<void>} A promise that resolves when the connection is closed.
   */
  abstract close(): Promise<void>;

  /**
   * Retrieves generic descriptions of all tables in the database.
   * @abstract
   * @param {string} rSchema - The schema
   * @returns {Promise<GenericDbTable[]>} A promise that resolves with an array of table descriptions.
   */
  abstract getAllTablesDescription(rSchema: string): Promise<GenericDbTable[]>;

  /**
   * Retrieves the generic description of a specific table in the database.
   * @abstract
   * @param {string} rTable - The name of the table.
   * @param {string} rSchema - The schema of the table.
   * @returns {Promise<GenericDbTable>} A promise that resolves with the table description.
   */
  abstract getTableDescription(rTable: string, rSchema: string): Promise<GenericDbTable>;

  /**
   * Retrieves foreign keys for a specific table.
   * if a table has an id_school that is a foreign key, it will be on that list
   * @protected
   * @abstract
   * @param {string} rTable - The name of the table.
   * @param {string} rSchema - The schema of the table.
   * @returns {Promise<GenericDbFKey[]>} A promise that resolves with an array of foreign keys.
   */
  protected abstract getTableForeignKeys(rTable: string, rSchema: string): Promise<GenericDbFKey[]>;

  /**
   * Retrieves referenced foreign keys for a specific table.
   * if a table has a key references as a foregin key by another table, it will be on that list
   * @protected
   * @abstract
   * @param {string} rTable - The name of the table.
   * @param {string} rSchema - The schema of the table.
   * @returns {Promise<GenericDbFKey[]>} A promise that resolves with an array of referenced foreign keys.
   */
  protected abstract getTableReferencedForeignKeys(rTable: string, rSchema: string): Promise<GenericDbFKey[]>;

  /**
   * Retrieves the description of a specific field.
   * @protected
   * @abstract
   * @param {any} rField - The field data.
   * @returns {GenericDbField | undefined} The field description, or undefined if not found.
   */
  protected abstract getFieldDescription(rField: any): GenericDbField | undefined;

  /**
   * Retrieves the type of a specific field.
   * @protected
   * @abstract
   * @param {any} rField - The field data.
   * @returns {GenericDbTypeField} The type of the field.
   */
  protected abstract getTypeField(rField: any): GenericDbTypeField;

  /**
   * Checks if a field is a primary key.
   * @protected
   * @abstract
   * @param {any} rField - The field data.
   * @returns {boolean} True if the field is a primary key, otherwise false.
   */
  protected abstract isPrimaryKey(rField: any): boolean;

  /**
   * Checks if a field is a primary foreign key.
   * @protected
   * @abstract
   * @param {any} rField - The field data.
   * @returns {boolean} True if the field is a primary foreign key, otherwise false.
   */
  protected abstract isPrimaryForeignKey(rField: any): boolean;

  /**
   * Check if field is auto generated
   * @param rField : Field
   * @returns true if field is auto generated
   */
  protected abstract isAutoGenerated(rField: any): boolean;

  /**
   * Initialize connexion
   */
  abstract initConnexion(): Promise<T>;

  async init() {
    this._con = await this.initConnexion();
  }

  /**
   * Get actual schema or 'public' if no one is specified
   * @returns actual schema
   */
  getSchema(): string {
    return this.config?.database?.schema ?? this.config?.database?.database ?? AbstractDatabase.PUBLIC_SCHEMA;
  }

  get config(): GlobalConfig {
    return this._config;
  }

  /**
   * Getter for _con property
   */
  public get con(): T | undefined {
    return this._con;
  }

  /**
   * Only child classes can set '_con' property so we set a protected scope !
   */
  protected set con(rCon: T | undefined) {
    this._con = rCon;
  }
}
