/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractDatabase } from "../AbstractDatabase";
import { GenericDbField } from "../model/GenericDbField";
import { GenericDbRow } from "../model/GenericDbRow";
import { GenericDbTable } from "../model/GenericDbTable";
import { GenericDbTypeField } from "../model/GenericDbTypeField";
import { IDbConverter } from "../IDbConverter";
import { GenericDbFKey } from "../model/GenericDbFKey";
import * as vscode from "vscode";
import { Client, ConnectionConfig } from "pg";
import { PostgresqlConverter } from "./PostgresqlConverter";
import { EnumDbRule } from "../model/EnumDbRule";

export class PostgresqlDatabase extends AbstractDatabase<Client> {
  readonly FIELD_TABLE_NAME: string = "table_name";
  readonly FIELD_TYPE_NAME: string = "data_type";

  async initConnexion(): Promise<Client> {
    // Create client
    const client = new Client(this.config.database as ConnectionConfig);
    await client.connect();
    return client;
  }

  getDbConverter(): IDbConverter {
    return new PostgresqlConverter();
  }

  async getAllTablesDescription(rSchema: string): Promise<GenericDbTable[]> {
    let tablesDesc: GenericDbTable[] = [];
    if (!this.con) {
      throw new Error("No connexion ! Can't query");
    }
    if (rSchema) {
      // query for tables descriptions
      const results = await this.con.query(`SELECT * FROM information_schema.tables WHERE table_schema = '${rSchema}'`);
      for (const table of results.rows) {
        console.log(table);
      }
      for (const table of results.rows) {
        let tableName = table[this.FIELD_TABLE_NAME];
        if (tableName !== undefined && tableName !== "") {
          // get description from this table
          const currentTableDesc = await this.getTableDescription(tableName, rSchema);
          // add it
          if (currentTableDesc) {
            tablesDesc.push(currentTableDesc);
          }
        }
      }
    } else {
      return Promise.reject("No schema name provided");
    }
    // return array of table description
    return tablesDesc;
  }

  async getTableDescription(rTable: string, rSchema: string): Promise<GenericDbTable> {
    if (!this.con) {
      throw new Error("No connexion ! Can't query");
    }
    if (rTable && rSchema) {
      // Prepare model
      let dbTable: GenericDbTable = new GenericDbTable(rTable);
      dbTable.fields = [];
      // Query data for table
      const results = await this.con.query(this.getSqlTableDescription(rTable, rSchema));
      // Fetch all data for generic
      for (const rowData of results.rows) {
        const fieldDesc = this.getFieldDescription(rowData);
        if (fieldDesc) {
          dbTable.fields.push(fieldDesc);
        }
      }
      // Get fkeys of the current table
      const fKeys = await this.getTableForeignKeys(rTable, rSchema);
      if (fKeys && fKeys.length > 0) {
        dbTable.fKeys = fKeys;
      }
      // Get refrenced fkeys of the current table
      const referencedFKeys = await this.getTableReferencedForeignKeys(rTable, rSchema);
      if (referencedFKeys && referencedFKeys.length > 0) {
        dbTable.referencedFKeys = referencedFKeys;
      }
      return dbTable;
    } else {
      return Promise.reject("No table or schema name provided");
    }
  }

  async getTableForeignKeys(rTable: string, rSchema: string): Promise<GenericDbFKey[]> {
    return this.getTableFKeyOrigin(rTable, rSchema);
  }

  async getTableReferencedForeignKeys(rTable: string, rSchema: string): Promise<GenericDbFKey[]> {
    return this.getTableFKeyOrigin(rTable, rSchema, true);
  }

  /**
   * 
   * @param rTable : table name
   * @param rSchema : schema
   * @param rModeReferenced : true if we want the referenced foreign keys (keys from this table referenced in other tables), 
   * false if we want simply foreign feys of this table. Default to false 
   * @returns 
   */
  private async getTableFKeyOrigin(rTable: string, rSchema: string, rModeReferenced: boolean = false) {
    if (!this.con) {
      throw new Error("No connexion ! Can't query");
    }
    if (rTable) {
      let rowsData: GenericDbFKey[] = [];
      const query = this.getSqlFKeyOrigin(rTable, rSchema, rModeReferenced);
      // Query for foreign keys
      const results = await this.con.query(query);
      // Fetch all data for generic
      for (const rowData of results.rows) {
        const fKey = new GenericDbFKey();
        fKey.tableName = rowData["table_name"];
        fKey.columnName = rowData["column_name"];
        fKey.constraintName = rowData["constraint_name"];
        fKey.referencedTable = rowData["referenced_table_name"];
        fKey.referencedColumn = rowData["referenced_column_name"];
        fKey.constraintPosition = rowData["position_in_unique_constraint"];

        // rules
        fKey.updateRule = this._converter?.fieldConvertToRule(rowData["update_rule"]);
        fKey.deleteRule = this._converter?.fieldConvertToRule(rowData["delete_rule"]);

        rowsData.push(fKey);
      }
      return rowsData;
    } else {
      return Promise.reject("No table name provided");
    }
  }

  getFieldDescription(rField: any): GenericDbField | undefined {
    let genDbField = new GenericDbField();
    // Get name of field
    let fieldName = rField["column_name"];
    if (fieldName !== undefined && fieldName !== "" && this._converter !== undefined) {
      genDbField.fieldName = fieldName;
      genDbField.isNullable = rField["is_nullable"] === "YES" ? true : false;
      genDbField.isPKey = this.isPrimaryKey(rField);
      genDbField.isFKey = this.isPrimaryForeignKey(rField);
      genDbField.type = this.getTypeField(rField);
      genDbField.isAutogenerated = this.isAutoGenerated(rField);
    }
    return genDbField;
  }

  protected getTypeField(rField: any): GenericDbTypeField {
    let field = new GenericDbTypeField();
    field.type = this._converter?.fieldConvertToType(rField[this.FIELD_TYPE_NAME]);
    field.length = rField["character_maximum_length"] ?? undefined; // we don't want null value, force to undefined if needed
    return field;
  }

  async close(): Promise<void> {
    try {
      this.con?.end();
    } catch (error) {
      vscode.window.showErrorMessage(`Process error : ${error}`);
    }
  }

  /**
   * check if field is a primary key
   * @param rField : field
   * @returns true if field is a pkey
   */
  protected isPrimaryKey(rField: any): boolean {
    // note that we don't have choice to check ispkey as a lowercase
    return rField !== undefined && rField["ispkey"] !== undefined && rField["ispkey"] !== null;
  }

  /**
   * check if field is a primary foreign key
   * @param rField : field
   * @returns true if field is a pfkey
   */
  protected isPrimaryForeignKey(rField: any): boolean {
    // note that we don't have choice to check ispkey as a lowercase
    return rField !== undefined && rField["isfkey"] !== undefined && rField["ispkey"] !== null;
  }

  /**
   * Check if field is auto generated
   * @param rField : Field
   * @returns true if field is auto generated
   */
  protected isAutoGenerated(rField: any): boolean {
    return rField && rField["isautogenerated"] === "YES";
  }

  async query(rQuery: string): Promise<GenericDbRow[]> {
    throw new Error("Method not implemented.");
  }

  fetchRow(rRow: any): GenericDbRow {
    throw new Error("Method not implemented.");
  }
  convertField(field: any): GenericDbField {
    throw new Error("Method not implemented.");
  }

  /**
   * Get SQL query to get table description
   * !!! careful !!!, postgres will put every query column name in lowercase
   * @param rTablename : table name
   * @param rSchema : schema
   * @returns query
   */
  private getSqlTableDescription(rTablename: string, rSchema: string) {
    return `
    SELECT 
    c.column_name, c.data_type, c.character_maximum_length, 
    k.constraint_name, k.ordinal_position, k.position_in_unique_constraint,
    ccu.table_name AS referenced_table_name, ccu.column_name AS referenced_column_name, 
    tc.constraint_name as ispkey,
    rc.update_rule, rc.delete_rule, 
    rc.constraint_name as iskey,
    (CASE 
      WHEN a.attidentity != '' THEN 'YES' 
      WHEN pg_get_expr(ad.adbin, ad.adrelid) LIKE 'nextval(%::regclass)' THEN 'YES'
      ELSE 'NO'
    END) AS isautogenerated    
    FROM information_schema.columns c 
    left join information_schema.key_column_usage k on k.table_schema = c.table_schema and k.table_name = c.table_name and k.column_name = c.column_name 
    left join information_schema.constraint_column_usage ccu on ccu.table_schema = k.table_schema and ccu.constraint_name = k.constraint_name 
    left join information_schema.table_constraints tc on tc.constraint_schema = c.table_schema and tc.constraint_name = k.constraint_name and tc.constraint_type = 'PRIMARY KEY'
    left join information_schema.referential_constraints rc on rc.constraint_schema = k.table_schema and rc.constraint_name = k.constraint_name 
    left join pg_catalog.pg_attribute a on a.attname = c.column_name and a.attrelid = (
      SELECT cl.oid 
      FROM pg_catalog.pg_class cl 
      WHERE cl.relname = c.table_name 
      AND cl.relnamespace = (SELECT oid FROM pg_catalog.pg_namespace WHERE nspname = c.table_schema)
    )    
    left join pg_catalog.pg_attrdef ad ON ad.adrelid = a.attrelid AND ad.adnum = a.attnum
    WHERE c.table_name = '${rTablename}'
      and c.table_schema = '${rSchema}'
      order by k.ordinal_position
    `;
  }

  /**
   * Get query to retrieve foregin keys or referenced foreign keys
   * !!! careful !!!, postgres will put every query column name in lowercase
   * @param rTablename : table name
   * @param rSchema : schema
   * @param rModeReferenced : true if we want the referenced foreign keys (keys from this table referenced in other tables), 
   * false if we want simply foreign feys of this table. Default to false 
   * @returns 
   */
  private getSqlFKeyOrigin(rTablename: string, rSchema: string, rModeReferenced: boolean = false) {
    const aliasTable = rModeReferenced ? "ccu" : "c";
    return `
    SELECT 
    c.table_name, c.column_name, 
    k.constraint_name, k.ordinal_position, k.position_in_unique_constraint,
    rc.update_rule, rc.delete_rule, 
    ccu.table_name AS referenced_table_name, ccu.column_name AS referenced_column_name
    FROM information_schema.columns c 
    join information_schema.key_column_usage k on k.table_schema = c.table_schema and k.table_name = c.table_name and k.column_name = c.column_name 
    join information_schema.constraint_column_usage ccu on ccu.table_schema = k.table_schema and ccu.constraint_name = k.constraint_name 
    join information_schema.referential_constraints rc on rc.constraint_schema = k.table_schema and rc.constraint_name = k.constraint_name 
    WHERE ${aliasTable}.table_name = '${rTablename}'
    and c.table_schema = '${rSchema}'
    order by k.ordinal_position
    `;
  }
}
