/* eslint-disable @typescript-eslint/naming-convention */
import { AbstractDatabase } from "../AbstractDatabase";
import mysql, { Connection, ConnectionConfig } from "mysql2/promise";
import { GenericDbField } from "../model/GenericDbField";
import { GenericDbRow } from "../model/GenericDbRow";
import { GenericDbTable } from "../model/GenericDbTable";
import { GenericDbTypeField } from "../model/GenericDbTypeField";
import { IDbConverter } from "../IDbConverter";
import { MysqlConverter } from "./MysqlConverter";
import { SqlTool } from "../../tools/SqlTool";
import { GenericDbFKey } from "../model/GenericDbFKey";
import * as vscode from "vscode";
import { EnumDbType } from "../model/EnumDbType";

export class MysqlDatabase extends AbstractDatabase<Connection> {
  protected readonly PREFIX_KEY_TBL = "Tables_in_";

  readonly FIELD_TABLE_NAME: string = ""; // <= not used here
  readonly FIELD_TYPE_NAME: string = "Type";

  async initConnexion(): Promise<Connection> {
    // Create connection
    return await mysql.createConnection(this.config.database as ConnectionConfig);
  }

  getDbConverter(): IDbConverter {
    return new MysqlConverter();
  }

  async getAllTablesDescription(rSchema: string): Promise<GenericDbTable[]> {
    let tablesDesc: GenericDbTable[] = [];
    if (!this.con) {
      throw new Error("No connexion ! Can't query");
    }
    if (rSchema) {
      // query for tables descriptions
      const [results, fields] = await this.con.query("show tables");
      for (const table of results as any[]) {
        let tableName = table[this.PREFIX_KEY_TBL + this.config?.database?.database];
        if (tableName !== undefined && tableName !== "") {
          // get description from this table
          const currentTableDesc = await this.getTableDescription(tableName, rSchema);
          // add it
          if (currentTableDesc) {
            tablesDesc.push(currentTableDesc);
          }
        }
      }
    } else {
      return Promise.reject("No schema name provided");
    }
    // return array of table description
    return tablesDesc;
  }

  async getTableDescription(rTable: string, rSchema: string): Promise<GenericDbTable> {
    if (!this.con) {
      throw new Error("No connexion ! Can't query");
    }
    if (rTable) {
      // Prepare model
      let dbTable: GenericDbTable = new GenericDbTable(rTable);
      dbTable.fields = [];
      // Query data for table
      const [rows, fields] = await this.con.query(`desc \`${rTable}\``);
      // Fetch all data for generic
      for (const rowData of rows as any[]) {
        const fieldDesc = this.getFieldDescription(rowData);
        if (fieldDesc) {
          dbTable.fields.push(fieldDesc);
        }
      }
      // Get fkeys of the current table
      const fKeys = await this.getTableForeignKeys(rTable, rSchema);
      if (fKeys && fKeys.length > 0) {
        dbTable.fKeys = fKeys;
      }
      // Get refrenced fkeys of the current table
      const referencedFKeys = await this.getTableReferencedForeignKeys(rTable, rSchema);
      if (referencedFKeys && referencedFKeys.length > 0) {
        dbTable.referencedFKeys = referencedFKeys;
      }
      // return back generic table
      return dbTable;
    } else {
      return Promise.reject("No table name provided");
    }
  }

  async getTableForeignKeys(rTable: string, rSchema: string): Promise<GenericDbFKey[]> {
    return this.getTableFKeyOrigin(rTable, rSchema);
  }

  async getTableReferencedForeignKeys(rTable: string, rSchema: string): Promise<GenericDbFKey[]> {
    return this.getTableFKeyOrigin(rTable, rSchema, true);
  }

  /**
   *
   * @param rTable : table name
   * @param rSchema : schema
   * @param rModeReferenced : true if we want the referenced foreign keys (keys from this table referenced in other tables),
   * false if we want simply foreign feys of this table. Default to false
   * @returns
   */
  private async getTableFKeyOrigin(rTable: string, rSchema: string, rModeReferenced: boolean = false) {
    if (!this.con) {
      throw new Error("No connexion ! Can't query");
    }
    if (rTable) {
      // Prepare model
      let rowsData: GenericDbFKey[] = [];
      // Query for foreign keys
      const [rows, fields] = await this.con.query(this.getSqlFKeyOrigin(rTable, rSchema, rModeReferenced));
      // Fetch all data for generic
      for (const rowData of rows as any[]) {
        const fKey = new GenericDbFKey();
        fKey.tableName = rowData["TABLE_NAME"];
        fKey.columnName = rowData["COLUMN_NAME"];
        fKey.constraintName = rowData["CONSTRAINT_NAME"];
        fKey.referencedTable = rowData["REFERENCED_TABLE_NAME"];
        fKey.referencedColumn = rowData["REFERENCED_COLUMN_NAME"];

        // rules
        fKey.updateRule = this._converter?.fieldConvertToRule(rowData["UPDATE_RULE"]);
        fKey.deleteRule = this._converter?.fieldConvertToRule(rowData["DELETE_RULE"]);

        rowsData.push(fKey);
      }
      return rowsData;
    } else {
      return Promise.reject("No table name provided");
    }
  }

  getFieldDescription(rField: any): GenericDbField | undefined {
    let genDbField = new GenericDbField();
    // Get name of field
    let fieldName = rField["Field"];
    if (fieldName !== undefined && fieldName !== "" && this._converter !== undefined) {
      genDbField.fieldName = fieldName;
      genDbField.isNullable = rField["Null"] === "YES" ? true : false;
      genDbField.isPKey = this.isPrimaryKey(rField);
      genDbField.isFKey = this.isPrimaryForeignKey(rField);
      genDbField.type = this.getTypeField(rField);
      genDbField.isAutogenerated = this.isAutoGenerated(rField);
    }
    return genDbField;
  }

  protected getTypeField(rField: any): GenericDbTypeField {
    let field = new GenericDbTypeField();
    const fieldTypeValue = rField[this.FIELD_TYPE_NAME];
    let typeExtracted = SqlTool.extractType(fieldTypeValue);
    field.type = this._converter?.fieldConvertToType(typeExtracted);
    field.length = SqlTool.extractNumber(fieldTypeValue);
    // if field is enum, extract values 
    if (field.type === EnumDbType.enum) {
      field.extra = SqlTool.extractEnumValues(fieldTypeValue);
    }
    return field;
  }

  async close(): Promise<void> {
    try {
      this.con?.end();
    } catch (error) {
      vscode.window.showErrorMessage(`Process error : ${error}`);
    }
  }

  /**
   * check if field is a primary key
   * @param rField : field
   * @returns true if field is a pkey
   */
  protected isPrimaryKey(rField: any): boolean {
    return rField !== undefined && rField["Key"] === "PRI";
  }

  /**
   * check if field is a primary foreign key
   * @param rField : field
   * @returns true if field is a pfkey
   */
  protected isPrimaryForeignKey(rField: any): boolean {
    return rField !== undefined && rField["Key"] === "MUL";
  }

  /**
   * Check if field is auto generated
   * @param rField : Field
   * @returns true if field is auto generated
   */
  protected isAutoGenerated(rField: any): boolean {
    return rField && rField["Extra"] === "auto_increment";
  }

  async query(rQuery: string): Promise<GenericDbRow[]> {
    throw new Error("Method not implemented.");
  }

  fetchRow(rRow: any): GenericDbRow {
    throw new Error("Method not implemented.");
  }
  convertField(field: any): GenericDbField {
    throw new Error("Method not implemented.");
  }

  /**
   * Get query to retrieve foreign key or referenced foreign keys for this table
   * @param rTable : table name
   * @param rSchema : schema
   * @param rModeReferenced : true if we want the referenced foreign keys (keys from this table referenced in other tables),
   * false if we want simply foreign feys of this table. Default to false
   * @returns
   */
  private getSqlFKeyOrigin(rTablename: string, rSchema: string, rModeReferenced: boolean = false) {
    const column1 = rModeReferenced ? "REFERENCED_TABLE_NAME" : "TABLE_NAME";
    const column2 = rModeReferenced ? "TABLE_NAME" : "REFERENCED_TABLE_NAME";
    // !!! CAREFUL !!! : if you change this query you should check 'sandbox.stub(mockedCon, "query").callsFake' on MysqlDatabase.test.ts
    // cause we detect the query with 'AND kcu.TABLE_NAME' so ensure tests are running ok
    return `SELECT
    kcu.TABLE_NAME,
    kcu.COLUMN_NAME,
    kcu.CONSTRAINT_NAME,
    kcu.REFERENCED_TABLE_NAME,
    kcu.REFERENCED_COLUMN_NAME,
    rc.UPDATE_RULE,
    rc.DELETE_RULE
    FROM
    information_schema.KEY_COLUMN_USAGE kcu 
    LEFT JOIN information_schema.REFERENTIAL_CONSTRAINTS rc 
        ON kcu.CONSTRAINT_NAME = rc.CONSTRAINT_NAME 
        AND rc.TABLE_NAME = kcu.TABLE_NAME 
      	AND rc.CONSTRAINT_SCHEMA = kcu.CONSTRAINT_SCHEMA
WHERE
    kcu.${column1}  = '${rTablename}'
    AND kcu.TABLE_SCHEMA = '${rSchema}'
    AND kcu.${column2} IS NOT NULL;
    `;
  }
}
