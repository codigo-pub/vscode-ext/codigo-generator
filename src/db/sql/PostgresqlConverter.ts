import { AbstractDbConverter } from "../AbstractDbConverter";
import { DbConvertMapping } from "../model/DbConvertMapping";
import { DbRuleMapping } from "../model/DbRuleMapping";
import { EnumDbType } from "../model/EnumDbType";
import { EnumDbRule } from "../model/EnumDbRule";

export class PostgresqlConverter extends AbstractDbConverter {
  typeMappings: DbConvertMapping[] = [
    { sqlType: "character varying", enumDbType: EnumDbType.varchar },
    { sqlType: "varchar", enumDbType: EnumDbType.varchar },
    { sqlType: "char", enumDbType: EnumDbType.char },
    { sqlType: "text", enumDbType: EnumDbType.longtext },
    { sqlType: "uuid", enumDbType: EnumDbType.uuid },
    { sqlType: "inet", enumDbType: EnumDbType.varchar },
    { sqlType: "enum", enumDbType: EnumDbType.enum },

    { sqlType: "bigint", enumDbType: EnumDbType.long },
    { sqlType: "integer", enumDbType: EnumDbType.int },
    { sqlType: "smallint", enumDbType: EnumDbType.int },
    { sqlType: "int", enumDbType: EnumDbType.int },
    { sqlType: "float", enumDbType: EnumDbType.float },
    { sqlType: "double precision", enumDbType: EnumDbType.double },
    { sqlType: "double", enumDbType: EnumDbType.double },
    { sqlType: "decimal", enumDbType: EnumDbType.double },
    { sqlType: "numeric", enumDbType: EnumDbType.numeric },

    { sqlType: "datetime", enumDbType: EnumDbType.datetime },
    { sqlType: "date", enumDbType: EnumDbType.date },
    { sqlType: "time", enumDbType: EnumDbType.time },
    { sqlType: "timestamp", enumDbType: EnumDbType.timestamp },

    { sqlType: "interval", enumDbType: EnumDbType.interval },
    { sqlType: "boolean", enumDbType: EnumDbType.boolean },
    { sqlType: "bytea", enumDbType: EnumDbType.blob },
    { sqlType: "json", enumDbType: EnumDbType.json },
    { sqlType: "jsonb", enumDbType: EnumDbType.json },

    { sqlType: "array", enumDbType: EnumDbType.array }
  ];


    ruleMappings: DbRuleMapping[] = [
    { sqlRule: "cascade", enumRule: EnumDbRule.CASCADE },
    { sqlRule: "set null", enumRule: EnumDbRule.SET_NULL },
    { sqlRule: "set default", enumRule: EnumDbRule.SET_DEFAULT },
    { sqlRule: "restrict", enumRule: EnumDbRule.RESTRICT },
    { sqlRule: "no action", enumRule: EnumDbRule.NO_ACTION },
    { sqlRule: "do nothing", enumRule: EnumDbRule.DO_NOTHING },
    { sqlRule: "none ", enumRule: EnumDbRule.DO_NOTHING }, // same as do nothing
  ];

}
