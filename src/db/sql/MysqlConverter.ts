import { AbstractDbConverter } from "../AbstractDbConverter";
import { DbConvertMapping } from "../model/DbConvertMapping";
import { DbRuleMapping } from "../model/DbRuleMapping";
import { EnumDbRule } from "../model/EnumDbRule";
import { EnumDbType } from "../model/EnumDbType";

export class MysqlConverter extends AbstractDbConverter {
    typeMappings: DbConvertMapping[] = [
        { sqlType: "char", enumDbType: EnumDbType.char },
        { sqlType: "varchar", enumDbType: EnumDbType.varchar },
        
        { sqlType: "bigint", enumDbType: EnumDbType.long },
        { sqlType: "int", enumDbType: EnumDbType.int },
        { sqlType: "float", enumDbType: EnumDbType.float },
        { sqlType: "double", enumDbType: EnumDbType.double },
        { sqlType: "decimal", enumDbType: EnumDbType.double },
        { sqlType: "longtext", enumDbType: EnumDbType.longtext },
        { sqlType: "text", enumDbType: EnumDbType.longtext },
        { sqlType: "enum", enumDbType: EnumDbType.enum },

        { sqlType: "datetime", enumDbType: EnumDbType.datetime },
        { sqlType: "date", enumDbType: EnumDbType.date },
        { sqlType: "timestamp", enumDbType: EnumDbType.timestamp },
        { sqlType: "time", enumDbType: EnumDbType.time },

        { sqlType: "numeric", enumDbType: EnumDbType.numeric },
        { sqlType: "year", enumDbType: EnumDbType.int },

        { sqlType: "boolean", enumDbType: EnumDbType.boolean },
        { sqlType: "blob", enumDbType: EnumDbType.blob },
        { sqlType: "json", enumDbType: EnumDbType.json }
      ];
    
      ruleMappings: DbRuleMapping[] = [
        { sqlRule: "cascade", enumRule: EnumDbRule.CASCADE },
        { sqlRule: "set null", enumRule: EnumDbRule.SET_NULL },
        { sqlRule: "set default", enumRule: EnumDbRule.SET_DEFAULT },
        { sqlRule: "restrict", enumRule: EnumDbRule.RESTRICT },
        { sqlRule: "no action", enumRule: EnumDbRule.NO_ACTION }
      ];
}