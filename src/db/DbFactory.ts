import { GlobalConfig } from "../model/GlobalConfig";
import { IDatabase } from "./IDatabase";
import { MysqlDatabase } from "./sql/MysqlDatabase";
import * as vscode from "vscode";
import { PostgresqlDatabase } from "./sql/PostgresqlDatabase";
import { EnumDb } from "./model/EnumDb";

export class DbFactory {
  static getDb(rConfig?: GlobalConfig): IDatabase | undefined {
    let db: IDatabase | undefined;
    // if template was specified
    if (rConfig?.db_type) {
      const dbType: EnumDb = EnumDb[rConfig.db_type.toUpperCase() as keyof typeof EnumDb];
      switch (dbType) {
        case EnumDb.MARIADB:
        case EnumDb.MYSQL:
          db = new MysqlDatabase(rConfig);
          break;
        case EnumDb.POSTGRESQL:
          db = new PostgresqlDatabase(rConfig);
          break;
        default:
          vscode.window.showErrorMessage("db_type unknown ! [" + rConfig.db_type + "]");
          break;
      }
    }
    return db;
  }
}
