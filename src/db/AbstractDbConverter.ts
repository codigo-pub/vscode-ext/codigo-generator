import { IDbConverter } from "./IDbConverter";
import { DbConvertMapping } from "./model/DbConvertMapping";
import { DbRuleMapping } from "./model/DbRuleMapping";
import { EnumDbType } from "./model/EnumDbType";
import * as vscode from 'vscode';
import { EnumDbRule } from "./model/EnumDbRule";

export abstract class AbstractDbConverter implements IDbConverter {
  abstract readonly typeMappings: DbConvertMapping[];
  abstract readonly ruleMappings: DbRuleMapping[];

  fieldConvertToType(rFieldType: any): EnumDbType | undefined {
    return this.fieldConvertToTypeFromMapping(rFieldType, this.typeMappings);
  }

  fieldConvertToTypeFromMapping(rFieldType: any, rTypeMappings: DbConvertMapping[]): EnumDbType | undefined {
    if (rFieldType === undefined) {
      return undefined;
    }

    const typeField = rFieldType.toLowerCase();
    for (const mapping of rTypeMappings) {
      if (typeField.indexOf(mapping.sqlType) >= 0) {
        return mapping.enumDbType;
      }
    }
    vscode.window.showWarningMessage(`unknown type detected : [${rFieldType}]`);
    return undefined;
  }


  fieldConvertToRule(rFieldRule: string): EnumDbRule | undefined {
    return this.fieldConvertToRuleFromMapping(rFieldRule, this.ruleMappings);
  }

  fieldConvertToRuleFromMapping(rFieldRule: any, rRuleMappings: DbRuleMapping[]): EnumDbRule | undefined {
    if (rFieldRule === undefined) {
      return undefined;
    }

    const ruleField = rFieldRule.toLowerCase();
    for (const mapping of rRuleMappings) {
      if (ruleField.indexOf(mapping.sqlRule) >= 0) {
        return mapping.enumRule;
      }
    }
    vscode.window.showWarningMessage(`unknown rule detected : [${rFieldRule}]`);
    return undefined;
  }

}
