import { GenericDbRow } from "./model/GenericDbRow";
import { GenericDbTable } from "./model/GenericDbTable";

export interface IDatabase {
    
    /**
     * Query database
     * @param rQuery : query
     * @return array of objects
     */
    query(rQuery: string): Promise<GenericDbRow[]>;

    /**
     * Close connection
     */
    close(): Promise<void>;

    /**
     * Init component, essentially init connection 
     */
    init(): Promise<void>;

    /**
     * Fetch row
     * @param rRow 
     */
    fetchRow(rRow: any): GenericDbRow;

    /**
     * Get all tables descriptions
     * @param rSchema : schema (not necessary used, depends database type)
     */
    getAllTablesDescription(rSchema: string): Promise<GenericDbTable[]>;

    /**
     * Get table description
     * @param rTable : table name
     * @param rSchema : schema (not necessary used, depends database type)
     */
    getTableDescription(rTable: string, rSchema: string): Promise<GenericDbTable>;

    /**
     * Get schema from config file, default to public
     */
    getSchema(): string;
}