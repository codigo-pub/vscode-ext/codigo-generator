import { DbConvertMapping } from "./model/DbConvertMapping";
import { DbRuleMapping } from "./model/DbRuleMapping";
import { EnumDbRule } from "./model/EnumDbRule";
import { EnumDbType } from "./model/EnumDbType";

export interface IDbConverter {
  
  /**
   * Convert field type to enum type based on default mapping (@ee AbstractDbConverter)
   * @param rFieldType : field type
   * @returns EnumDbType
   */
  fieldConvertToType(rFieldType: any): EnumDbType | undefined;

  /**
   * Convert field to enum type based on mapping 
   * @param rFieldType : field
   * @param rTypeMappings : array of mappings
   * @returns EnumDbType
   */
  fieldConvertToTypeFromMapping(rFieldType: any, rTypeMappings: DbConvertMapping[]): EnumDbType | undefined;

  /**
   * Convert field rule to enum rule based on default mapping (@ee AbstractDbConverter)
   * @param rFieldRule : field rule
   * @returns EnumDbRule
   */
  fieldConvertToRule(rFieldRule: any): EnumDbRule | undefined;

  /**
   * Convert field to enum rule based on mapping 
   * @param rFieldRule : field
   * @param rRuleMappings : array of mappings
   * @returns EnumDbRule
   */
  fieldConvertToRuleFromMapping(rFieldRule: any, rRuleMappings: DbRuleMapping[]): EnumDbRule | undefined;

}
