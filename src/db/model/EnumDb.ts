/* eslint-disable @typescript-eslint/naming-convention */
export enum EnumDb {
    MARIADB = "mariadb",
    MYSQL = "mysql",
    POSTGRESQL = "postgresql"
}