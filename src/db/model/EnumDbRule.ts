/* eslint-disable @typescript-eslint/naming-convention */
export enum EnumDbRule {
    NO_ACTION = "no_action",
    RESTRICT = "restrict",
    CASCADE = "cascade",
    SET_NULL = "set_null",
    SET_DEFAULT = "set_default",
    DO_NOTHING = "do_nothing"
}