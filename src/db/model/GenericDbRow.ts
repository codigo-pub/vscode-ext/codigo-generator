import { GenericDbField } from "./GenericDbField";

export class GenericDbRow {
    fields: GenericDbField[] | undefined;    
}