export enum EnumDbType {
  char = "char",
  varchar = "varchar",
  uuid = "uuid",
  longtext = "longtext",
  enum = "enum",
  
  long = "long",
  int = "int",
  float = "float",
  double = "double",
  numeric = "numeric",

  date = "date",
  datetime = "datetime",
  timestamp = "timestamp",
  time = "time",
  interval = "interval",

  boolean = "boolean",

  blob = "blob",
  json = "json",

  array = "array" // for postgresql mostly
}
