import { GenericDbFKey } from "./GenericDbFKey";
import { GenericDbField } from "./GenericDbField";

export class GenericDbTable {
    tableName: string;
    fields: GenericDbField[] | undefined;
    fKeys: GenericDbFKey[] | undefined;
    referencedFKeys: GenericDbFKey[] | undefined;

    constructor(rTableName: string) {
        this.tableName = rTableName;
    }
}