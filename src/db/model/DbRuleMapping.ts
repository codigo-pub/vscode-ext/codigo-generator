import { EnumDbRule } from "./EnumDbRule";

export class DbRuleMapping {
    sqlRule: string | undefined;
    enumRule: EnumDbRule | undefined;
}