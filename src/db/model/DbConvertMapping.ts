import { EnumDbType } from "./EnumDbType";

export class DbConvertMapping {
    sqlType: string | undefined;
    enumDbType: EnumDbType | undefined;
}