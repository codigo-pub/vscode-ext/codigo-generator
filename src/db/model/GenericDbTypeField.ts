import { EnumDbType } from "./EnumDbType";

export class GenericDbTypeField {
    type: EnumDbType | undefined;
    length: number | undefined;
    extra: string[] | undefined;
}