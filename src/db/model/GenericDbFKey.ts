import { EnumDbRule } from "./EnumDbRule";

export class GenericDbFKey {
    tableName?: string;
    columnName?: string;
    constraintName?: string;
    referencedTable?: string;
    referencedColumn?: string;
    constraintPosition?: number;

    updateRule?: EnumDbRule;
    deleteRule?: EnumDbRule;

}