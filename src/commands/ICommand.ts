import * as vscode from 'vscode';

export interface ICommand {
    run(context: vscode.ExtensionContext): void;
}