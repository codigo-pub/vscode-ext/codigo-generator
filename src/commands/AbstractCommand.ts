import { config  } from 'dotenv'; 
import path = require('path');
import * as vscode from 'vscode';
import { ICommand } from './ICommand';

export abstract class AbstractCommand implements ICommand {

  protected workspaceRootPath?: string;
  
  constructor() {
    this.loadEnv();
  }

  /**
   * Runs process
   */
  abstract run(context: vscode.ExtensionContext): void;

  /**
   * Load .env file with dotenv
   */
  loadEnv() {
    // note that those commands need some environment variables loaded by dotenv
    // here we call config to load .env from workspace folder
    if (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders.length > 0) {
      this.workspaceRootPath = vscode.workspace.workspaceFolders[0].uri.fsPath;
      config({ path: path.join(this.workspaceRootPath, ".env") });
    }
  }
}
