import { ConfigGeneratorService } from "../service/config/ConfigGeneratorService";
import { DocTool } from "../tools/DocTool";
import * as vscode from "vscode";
import { l10n } from "vscode";
import { AbstractCommand } from "./AbstractCommand";
import { ConfigGenerator } from "../model/ConfigGenerator";
import { DbFactory } from "../db/DbFactory";
import { ServiceGeneratorFactory } from "../service/ServiceGeneratorFactory";

export class GeneratorCommand extends AbstractCommand {
  private readonly expectedExtCfgFileId = ["json"];
  private cfgService: ConfigGeneratorService;

  constructor() {
    super();
    this.cfgService = new ConfigGeneratorService();
  }

  run(context: vscode.ExtensionContext): void {
    try {
      // Check actual file
      if (DocTool.checkDoc(vscode.window.activeTextEditor, this.expectedExtCfgFileId)) {
        // Get config file fromm actual file
        const dataCfgFile = vscode.window.activeTextEditor?.document?.getText();
        const config: ConfigGenerator = this.cfgService.loadConfigFile(dataCfgFile);
        if (config) {
          // set context (processors will need it to get template files)
          config.context = context;
          this.runGeneratorSample(config);
        }
      }
    } catch (error) {
      vscode.window.showErrorMessage(`run-${l10n.t("Process error")} : ${error}`);
    }
  }

  /**
   * Get data based on config file
   */
  private async runGeneratorSample(rConfigGenerator: ConfigGenerator) {
    if (rConfigGenerator?.global_config) {
      // Get db from config
      const db = DbFactory.getDb(rConfigGenerator.global_config);
      if (db) {
        // get service
        const serviceGenerator = ServiceGeneratorFactory.getGenerator(rConfigGenerator, db);
        // call process
        if (serviceGenerator) {
          await serviceGenerator.process();
          vscode.window.showInformationMessage(`${l10n.t("Done")} !`);
        } else {
          vscode.window.showErrorMessage(`${l10n.t("Can't load generator service for this config")} !`);
        }
      } else {
        vscode.window.showErrorMessage(`${l10n.t("Can't get db information, check your config file")} !`);
      }
    } else {
      let msg = l10n.t("Config data missing");
      vscode.window.showErrorMessage(`${msg}`);
    }
  }
}
